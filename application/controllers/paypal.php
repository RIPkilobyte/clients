<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	/**
    * Последние сообщения об ошибках
    * @var array
    */
	protected $_errors = array();
   
	/**
    * Данные API
    * Обратите внимание на то, что для песочницы нужно использовать соответствующие данные
    * @var array
    */
	protected $_credentials = array(
		'USER' => 'ripkilobyte1_api1.gmail.com',
		'PWD' => '1393404777',
		'SIGNATURE' => 'AlZezKQDC8UYeahj4ulPv0sLhk6VAMFdjwAMbe2HPLp-qHP0uGxT6aHE',
	);

	/**
    * Указываем, куда будет отправляться запрос
    * Реальные условия - https://api-3t.paypal.com/nvp
    * Песочница - https://api-3t.sandbox.paypal.com/nvp
    * @var string
    */
	//protected $_endPoint = 'https://api-3t.paypal.com/nvp';
	//protected $_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';
	protected $_endPoint = 'http://api-3t.sandbox.paypal.com/nvp';
	
	/**
    * Версия API
    * @var string
    */
	protected $_version = '74.0';	

	/**
    * Сформировываем запрос
    *
    * @param string $method Данные о вызываемом методе перевода
    * @param array $params Дополнительные параметры
    * @return array / boolean Response array / boolean false on failure
    */
	public function request($method,$params = array()) 
	{
		$this->_errors = array();
		if(empty($method)) { // Проверяем, указан ли способ платежа
			$this->_errors = array('Не указан метод перевода средств');
		return false;
		}

		// Параметры нашего запроса
		$requestParams = array(
			'METHOD' => $method,
			'VERSION' => $this->_version
		) + $this->_credentials;

		// Сформировываем данные для NVP
		$request = http_build_query($requestParams + $params);

		// Настраиваем cURL
		$curlOptions = array (
			CURLOPT_URL => $this->_endPoint,
			CURLOPT_VERBOSE => 1,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_CAINFO => 'assets/cacert.pem', // Файл сертификата
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $request
		);

		$ch = curl_init();
		curl_setopt_array($ch,$curlOptions);

		// Отправляем наш запрос, $response будет содержать ответ от API
		$response = curl_exec($ch);
var_dump($response);
		// Проверяем, нету ли ошибок в инициализации cURL
		if (curl_errno($ch)) {
			$this->_errors = curl_error($ch);
			curl_close($ch);
			return false;
		}
		else {
			curl_close($ch);
			$responseArray = array();
			parse_str($response,$responseArray); // Разбиваем данные, полученные от NVP в массив
			return $responseArray;
		}
	}	
	public function index()
	{
		// Параметры нашего запроса
		$requestParams = array(
			'RETURNURL' => $this->config->item('base_url').'paypal/success',
			'CANCELURL' => $this->config->item('base_url').'paypal/cancelled'
		);

		$orderParams = array(
			'PAYMENTREQUEST_0_AMT' => '500',
			'PAYMENTREQUEST_0_SHIPPINGAMT' => '4',
			'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
			'PAYMENTREQUEST_0_ITEMAMT' => '496'
		);

		$item = array(
			'L_PAYMENTREQUEST_0_NAME0' => 'iPhone',
			'L_PAYMENTREQUEST_0_DESC0' => 'White iPhone, 16GB',
			'L_PAYMENTREQUEST_0_AMT0' => '496',
			'L_PAYMENTREQUEST_0_QTY0' => '1'
		);
		$response = $this->request('SetExpressCheckout',$requestParams + $orderParams + $item);
		//var_dump($response);
		if(is_array($response) && $response['ACK'] == 'Success') { // Запрос был успешно принят
			$token = $response['TOKEN'];
			//var_dump($token);
			redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$token);
		}
	}
	public function success()
	{
		if( isset($_GET['token']) && !empty($_GET['token']) ) { // Токен присутствует
		   // Получаем детали оплаты, включая информацию о покупателе.
		   // Эти данные могут пригодиться в будущем для создания, к примеру, базы постоянных покупателей
		   $checkoutDetails = $this->request('GetExpressCheckoutDetails', array('TOKEN' => $_GET['token']));
		var_dump($checkoutDetails);
		echo '<br />';
		   // Завершаем транзакцию
		   $requestParams = array(
		      'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
		      'PAYERID' => $_GET['PayerID']
		   );

		   $response = $this->request('DoExpressCheckoutPayment',$requestParams);
		   if( is_array($response) && $response['ACK'] == 'Success') { // Оплата успешно проведена
		      // Здесь мы сохраняем ID транзакции, может пригодиться во внутреннем учете
		      $transactionId = $response['PAYMENTINFO_0_TRANSACTIONID'];
		   }
		var_dump($response);
		echo '<br />';
		}
	}
	public function cancelled()
	{

	}
}