<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Show_email extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}	
	public function index()
	{
		$hash = $this->uri->segment(2);
		if($hash) {
			$email = $this->user_model->get_by_hash($hash);
			if($email) {
				$this->load->view('user/email.tpl', $email);
			}
			else
				redirect($this->config->item('base_url'));
		}
		else
			redirect($this->config->item('base_url'));
	}
}