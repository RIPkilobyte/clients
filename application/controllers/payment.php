<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {

	private $WebAddress = "http://eagle-essays.com/payment";
	//private $WebAddress = "http://clients/payment";
	private $PreSharedKey = "N/jRZ/ZRdBEBDfE9OuPLyBGTtSkOOXTmWdGbfLWPJFLbjJIuU7YKyPTnZg==";
	private $MerchantID = "UKCity-9365729";
	private $Password = "Papers1234";
	private $CurrencyCode = "840"; // 840-dollar, 978-euro, 826-UK pound
	private $EchoAVSCheckResult = 'true';
	private $EchoCV2CheckResult = 'true';
	private $EchoThreeDSecureAuthenticationCheckResult = 'true';
	private $EchoCardType = 'true';
	private $AVSOverridePolicy = 'true';
	private $CV2OverridePolicy = 'true';
	private $ThreeDSecureOverridePolicy = 'true';
	private $TransactionType = 'SALE';
	private $CallbackURL = 'result';
	private $EmailAddressEditable = 'false';
	private $PhoneNumberEditable = 'false';
	private $CV2Mandatory = 'true';
	private $Address1Mandatory = 'true';
	private $CityMandatory = 'true';
	private $PostCodeMandatory = 'true';
	private $StateMandatory = 'true';
	private $CountryMandatory = 'true';
	private $ResultDeliveryMethod = 'POST';

	private $Address1 = '';
	private $Address2 = '';
	private $Address3 = '';
	private $Address4 = '';
	private $City = '';
	private $State = '';
	private $PostCode = '';
	private $CountryCode = '';
	private $ServerResultURL = '';
	private $PhoneNumber = '';
	private $PaymentFormDisplaysResult = false;
	private $ServerResultURLCookieVariables = '';
	private $ServerResultURLFormVariables = '';
	private $ServerResultURLQueryStringVariables = '';
	private $ThreeDSecureCompatMode = 'false';

	function __construct()
	{
		parent::__construct();
		$this->load->model('assist_model');
		$this->load->model('order_model');
		$this->load->model('payment_model');
	}
	public function index()
	{
		redirect('/user');
	}
	public function pay_order($id = '', $token = '')
	{
		if(!Check::login())
			redirect('/user/login');
		if(!$id || !$token)
			redirect('/user');
		$order = $this->order_model->get($id);
		if(!$order)
			redirect('/user');
		if(md5($order['date_add']) != $token)
			redirect('/user');
		
		$user = $this->my_auth->get_by_id($_SESSION['user_id']);
		$Amount = number_format($order['price'],2,'','');
		$gatewaydatetime = $this->gatewaydatetime();
		$CallbackURL = $this->WebAddress."/".$this->CallbackURL;
		$CustomerName = $user['first_name']. ' '.$user['last_name'];

		$CustomerName = $this->stripGWInvalidChars($CustomerName);
		$Address1 =  $this->stripGWInvalidChars($this->Address1);
		$Address2 =  $this->stripGWInvalidChars($this->Address2);
		$Address3 =  $this->stripGWInvalidChars($this->Address3);
		$Address4 =  $this->stripGWInvalidChars($this->Address4);
		$City =  $this->stripGWInvalidChars($this->City);
		$State =  $this->stripGWInvalidChars($this->State);
		$PostCode =  $this->stripGWInvalidChars($this->PostCode);
		$EmailAddress =  $this->stripGWInvalidChars($user['email']);
		$PhoneNumber =  $this->stripGWInvalidChars($this->PhoneNumber);

		$HashString="PreSharedKey=".$this->PreSharedKey;
		$HashString=$HashString . '&MerchantID='.$this->MerchantID;
		$HashString=$HashString . '&Password=' . $this->Password;
		$HashString=$HashString . '&Amount=' . $Amount;
		$HashString=$HashString . '&CurrencyCode=' . $this->CurrencyCode;
		$HashString=$HashString . '&EchoAVSCheckResult=' . $this->EchoAVSCheckResult;
		$HashString=$HashString . '&EchoCV2CheckResult=' . $this->EchoCV2CheckResult;
		$HashString=$HashString . '&EchoThreeDSecureAuthenticationCheckResult=' . $this->EchoThreeDSecureAuthenticationCheckResult;
		$HashString=$HashString . '&EchoCardType=' . $this->EchoCardType;
		$HashString=$HashString . '&OrderID=' . $order["order_id"];
		$HashString=$HashString . '&TransactionType=' . $this->TransactionType;
		$HashString=$HashString . '&TransactionDateTime=' . $gatewaydatetime;
		$HashString=$HashString . '&CallbackURL=' . $CallbackURL;
		$HashString=$HashString . '&OrderDescription=' . $order["topic"];
		$HashString=$HashString . '&CustomerName=' . $CustomerName;
		$HashString=$HashString . '&Address1=' . $Address1;
		$HashString=$HashString . '&Address2=' . $Address2;
		$HashString=$HashString . '&Address3=' . $Address3;
		$HashString=$HashString . '&Address4=' . $Address4;
		$HashString=$HashString . '&City=' . $City;
		$HashString=$HashString . '&State=' . $State;
		$HashString=$HashString . '&PostCode=' . $PostCode;
		$HashString=$HashString . '&CountryCode=' . $this->CountryCode;
		$HashString=$HashString . '&EmailAddress=' . $EmailAddress;
		$HashString=$HashString . '&PhoneNumber=' . $PhoneNumber;
		$HashString=$HashString . '&EmailAddressEditable=' . $this->EmailAddressEditable;
		$HashString=$HashString . '&PhoneNumberEditable=' . $this->PhoneNumberEditable;
		$HashString=$HashString . "&CV2Mandatory=" . $this->CV2Mandatory;
		$HashString=$HashString . "&Address1Mandatory=" . $this->Address1Mandatory;
		$HashString=$HashString . "&CityMandatory=" . $this->CityMandatory;
		$HashString=$HashString . "&PostCodeMandatory=" . $this->PostCodeMandatory;
		$HashString=$HashString . "&StateMandatory=" . $this->StateMandatory;
		$HashString=$HashString . "&CountryMandatory=" . $this->CountryMandatory;
		$HashString=$HashString . "&ResultDeliveryMethod=" . $this->ResultDeliveryMethod;
		$HashString=$HashString . "&ServerResultURL=" . $this->ServerResultURL;
		$HashString=$HashString . "&PaymentFormDisplaysResult=" . $this->PaymentFormDisplaysResult;

		$HashDigest = sha1($HashString);

		$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
		$html .= '<head>';
		$html .= '<script type="text/javascript"> function OnLoadEvent() { document.forms["contactForm"].submit(); }</script>';
		$html .= '<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />';
		$html .= '</head>';
		$html .= '<body OnLoad="OnLoadEvent();">';
		$html .= '<form name="contactForm" id="contactForm" method="post" action="https://mms.cardsaveonlinepayments.com/Pages/PublicPages/PaymentForm.aspx" target="_self">';
		$html .= '<input type="hidden" name="HashDigest" value="'.$HashDigest.'" />';
		$html .= '<input type="hidden" name="MerchantID" value="'.$this->MerchantID.'" />';
		$html .= '<input type="hidden" name="Amount" value="'.$Amount.'" />';
		$html .= '<input type="hidden" name="CurrencyCode" value="'.$this->CurrencyCode.'" />';
		$html .= '<input type="hidden" name="EchoAVSCheckResult" value="'.$this->EchoAVSCheckResult.'" />';
		$html .= '<input type="hidden" name="EchoCV2CheckResult" value="'.$this->EchoCV2CheckResult.'" />';
		$html .= '<input type="hidden" name="EchoThreeDSecureAuthenticationCheckResult" value="'.$this->EchoThreeDSecureAuthenticationCheckResult.'" />';
		$html .= '<input type="hidden" name="EchoCardType" value="'.$this->EchoCardType.'" />';
		$html .= '<input type="hidden" name="OrderID" value="'.$order['order_id'].'" />';
		$html .= '<input type="hidden" name="TransactionType" value="'.$this->TransactionType.'" />';
		$html .= '<input type="hidden" name="TransactionDateTime" value="'.$gatewaydatetime.'" />';
		$html .= '<input type="hidden" name="CallbackURL" value="'.$CallbackURL.'" />';
		$html .= '<input type="hidden" name="OrderDescription" value="'.$order['topic'].'" />';
		$html .= '<input type="hidden" name="CustomerName" value="'.$CustomerName.'" />';
		$html .= '<input type="hidden" name="Address1" value="'.$Address1.'" />';
		$html .= '<input type="hidden" name="Address2" value="'.$Address2.'" />';
		$html .= '<input type="hidden" name="Address3" value="'.$Address3.'" />';
		$html .= '<input type="hidden" name="Address4" value="'.$Address4.'" />';
		$html .= '<input type="hidden" name="City" value="'.$City.'" />';
		$html .= '<input type="hidden" name="State" value="'.$State.'" />';
		$html .= '<input type="hidden" name="PostCode" value="'.$PostCode.'" />';
		$html .= '<input type="hidden" name="CountryCode" value="'.$this->CountryCode.'" />';
		$html .= '<input type="hidden" name="EmailAddress" value="'.$EmailAddress.'" />';
		$html .= '<input type="hidden" name="PhoneNumber" value="'.$PhoneNumber.'" />';
		$html .= '<input type="hidden" name="EmailAddressEditable" value="'.$this->EmailAddressEditable.'" />';
		$html .= '<input type="hidden" name="PhoneNumberEditable" value="'.$this->PhoneNumberEditable.'" />';
		$html .= '<input type="hidden" name="CV2Mandatory" value="'.$this->CV2Mandatory.'" />';
		$html .= '<input type="hidden" name="Address1Mandatory" value="'.$this->Address1Mandatory.'" />';
		$html .= '<input type="hidden" name="CityMandatory" value="'.$this->CityMandatory.'" />';
		$html .= '<input type="hidden" name="PostCodeMandatory" value="'.$this->PostCodeMandatory.'" />';
		$html .= '<input type="hidden" name="StateMandatory" value="'.$this->StateMandatory.'" />';
		$html .= '<input type="hidden" name="CountryMandatory" value="'.$this->CountryMandatory.'" />';
		$html .= '<input type="hidden" name="ResultDeliveryMethod" value="'.$this->ResultDeliveryMethod.'" />';
		$html .= '<input type="hidden" name="ServerResultURL" value="'.$this->ServerResultURL.'" />';
		$html .= '<input type="hidden" name="PaymentFormDisplaysResult" value="'.$this->PaymentFormDisplaysResult.'" />';
		$html .= '<input type="hidden" name="ThreeDSecureCompatMode" value="'.$this->ThreeDSecureCompatMode.'" />';
		$html .= '</form>';
		$html .= '</body>';
		$html .= '</html>';
		echo $html;
	}
	public function test()
	{
		$payment = $this->payment_model->get(6);
		$result = unserialize($payment['details']);
		foreach ($result as $key => $value) {
			echo $key.' - '.$value;
		}
	}
	public function result()
	{
		//var_dump($_POST);
		$StatusCode = $this->input->post('StatusCode', true);
		$OrderID = $this->input->post('OrderID', true);
		$Message = $this->input->post('Message', true);
		$MessageDetails = explode(':', $Message);

		$AuthCode = $this->input->post('AuthCode', true);
		$AddressNumericCheckResult = $this->input->post('AddressNumericCheckResult', true);
		$PostCodeCheckResult = $this->input->post('PostCodeCheckResult', true);
		$CV2CheckResult = $this->input->post('CV2CheckResult', true);
		$ThreeDSecureAuthenticationCheckResult = $this->input->post('ThreeDSecureAuthenticationCheckResult', true);
		$TransactionType = $this->input->post('TransactionType', true);
		$Amount = $this->input->post('Amount', true);

		if(is_numeric($StatusCode)) {
			if($StatusCode != '30') {
				switch ($StatusCode) {
					case '0':

						$Response = 'Transaction Sucessful!';
						
						$cent = substr($Amount, -2);
						$summ = substr($Amount,0,strlen($Amount)-2);
						$all = $summ.'.'.$cent;
						$request = '';
						foreach ($_POST as $key => $value) {
							//if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1)
								//$value = urlencode(stripslashes($value)); 
							//else
								$value = urlencode($value);
							$request .= "&$key=$value";
						}
						$data = array(
							'system' => 'Debit/Credit cards',
							'type' => $TransactionType,
							'transaction_id' => '',
							'status' => 1,
							'user_id' => $_SESSION['user_id'],
							'order_id' => $OrderID,
							'amount' => $all,
							'date' => date('Y-m-d H:i:s'),
							'details' => $request
						);
						$payment_id = $this->payment_model->save($data);
						$this->payment_model->pay_order($OrderID, $payment_id);
						$user = $this->my_auth->get_by_id($_SESSION['user_id']);
						$user['order_id'] = $OrderID;
						$this->order_model->update($OrderID, array('type' => 'order'));
						
						$text = Mail::text_body('client_receive_payment', $user);
						//$this->order_model->add_message(1, $user['user_id'], $OrderID, $text, 1, 1);

						Mail::prepare('client_receive_payment', $user);
						$this->assist_model->save_log(array('text' => serialize($_POST)));
						break;
					case '4':
						$Response = "Card Referred";
						$this->assist_model->save_log(array('text' => serialize($_POST)));
						break;
					case '5':
						$Response = "Your payment was not successful.";
						if($AddressNumericCheckResult == 'FAILED')
							$Response .= "<br />Billing address check failed - Please check your billing address.";
						if($PostCodeCheckResult == 'FAILED')
							$Response .= "<br />Billing postcode check failed - Please check your billing postcode.";
						if($CV2CheckResult == 'FAILED')
							$Response .= "<br />The CV2 number you entered is incorrect.";
						if($ThreeDSecureAuthenticationCheckResult == 'FAILED')
							$Response .= "<br />Your bank declined the transaction due to Verified by Visa / MasterCard SecureCode.";
						if($MessageDetails[1] == 'Card declined' || $MessageDetails[1] == 'Card referred')
							$Response .= "<br />Your bank declined the payment.";
						$this->assist_model->save_log(array('text' => serialize($_POST)));
						break;
					case '20':
						$Response = 'Duplicate transaction - check PreviousTransactionResult for PreviousStatusCode & PreviousMessage';
						$this->assist_model->save_log(array('text' => serialize($_POST)));
						break;						
					default:
						$Response = "An error has occurred: " . $Message;
						$this->assist_model->save_log(array('text' => serialize($_POST)));
						break;
				}
			}
			else
				$Response = "An error has occurred: " . $Message;
		}
		$data['response'] = $Response;
		$data['order_id'] = $OrderID;
		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value) {
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		}
		$tpl['title'] = $this->config->config['tp_title']."Payment result";
		$tpl['content'] = $this->load->view('user/payment.tpl', $data, TRUE);
		$this->load->view('user_template.tpl', $tpl);
	}


	private function gatewaydatetime()
	{
		$str=date('Y-m-d H:i:s P');
		return $str;
	}
	private function guid()
	{
		if (function_exists('com_create_guid'))
			return com_create_guid();
		else {
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
					.substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12)
					.chr(125);// "}"
			return $uuid;
		}
	}
	private function stripGWInvalidChars($strToCheck) {
		$toReplace = array("#","\\",">","<", "\"", "[", "]");
		$cleanString = str_replace($toReplace, "", $strToCheck);
		return $cleanString;
	}	
}