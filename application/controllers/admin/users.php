<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		$this->load->model('user_model');
		Check::adminlogin();
	}
	public function index()
	{
		$data = array();
		$tpl['content'] = $this->load->view('admin/users/list.tpl', $data, TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function ajax_get_users()
	{
		$data = array();
		$page = $this->input->post('page', true);
		$limit = $this->input->post('limit', true);
		$email = $this->input->post('email', true);
		$user_id = $this->input->post('user_id', true);

        $count = $this->user_model->count_users($email, $user_id);
        //echo $this->db->last_query();
        if($count)
            $total_pages = ceil($count/$limit);
        else
            $total_pages = 0;
        if($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if($start < 0)
            $start = 0;
        $data['page'] = $page;
        $data['total'] = $total_pages;
        $data['records'] = $count;
        $data['rows'] = $this->user_model->get_users($limit, $start, $email, $user_id);
        $data['count'] = count($data['rows']);
        foreach ($data['rows'] as $key => $value) {
			switch ($value['role']) {
				case 'client':
					$data['rows'][$key]['role'] = 'Eagle-Essays';
				break;
				case 'professor':
					$data['rows'][$key]['role'] = 'Professor-Essays';
				break;
				case 'writer':
					$data['rows'][$key]['role'] = 'Writer-Center';
				break;
				default:
					$data['rows'][$key]['role'] = '';
				break;
			}        	
        }
        echo json_encode($data);
	}
	public function ajax_delete_user()
	{
		$data = array('status' => 'ok');
		$user_id = $this->input->post('user_id', true);
		$this->user_model->delete_user($user_id);
		echo json_encode($data);
	}
	public function ajax_update_user()
	{
		$data = array('status' => 'ok');
		$user_id = $this->input->post('user_id', true);
		$password = $this->input->post('password', true);
		$update = array(
			'password' => md5($password)
		);
		$this->user_model->update_user($user_id, $update);
		echo json_encode($data);
	}
}