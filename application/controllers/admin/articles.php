<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('Articles_model');
		Check::adminlogin();
	}
	
	public function index() {
		
		// Фильтр категорий материалов
		if (@$this->input->post('filter_artcat', TRUE)) {
			$this->session->set_userdata('filterartcat', $this->input->post('filter_artcat', TRUE));
			
			if ($this->input->post('filter_artcat', TRUE) == 'no_filter') {
				$this->session->set_userdata('filterartcat', '');
			}
		}
		
		$filter_artcat = @$this->session->userdata(filterartcat);
		
		$list['list'] = $this->Articles_model->get_articles_filter($filter_artcat);
		$list['categories'] = $this->Articles_model->get_artcats();
		$list['filter_artcat'] = @$filter_artcat;
		foreach($list['list'] as $key => $value) {
			if (@$value['artcat_id']) {
				$list['list'][$key]['category'] = $data = $this->Articles_model->get_artcat($value['artcat_id']);
			}else{
				$list['list'][$key]['category']['title'] = '<b style="color:red;">'.$this->lang->line('no_category').'</b>';
			}
		}
		
        $tpl['content'] = $this->load->view('admin/articles/list.tpl', $list, TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);
		
	}
	
	public function edit($arg = NULL) {
		
		if (@$arg) {
			$data = $this->Articles_model->get_article(mysql_real_escape_string($arg));
		}else{
			$data = array();
		}
		
		$data['categories'] = $this->Articles_model->get_artcats();
		
		$tpl['content'] = $this->load->view('admin/articles/edit.tpl', $data, TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);
		
	}
	
	public function ajax_deleteImage($arg) {
		$data = $this->Articles_model->get_article(mysql_real_escape_string($arg));
		if ($data['image'] != "")
		{
			if (Image::deleteImage($data['image']))
			{
				$this -> Articles_model -> update_article($arg, "image", "");
				echo "success";
				return 1;
			}
		}
		exit();
	}
	
	public function ajax_article_save($article_id = NULL) {
        $this -> load -> model('Articles_model');
        $aid = $this -> Articles_model -> save_article($article_id);
		
		$ret = array("qstatus" => 1);


		//фото
		if ((count($_FILES) > 0) && ($_FILES['image']['error'] == 0))
		{
			$upl = Image::upload($_FILES['image'], '/upload/images/', array('width' => 1024, 'height' => 768));
			if ($upl !=  FALSE)
			{
				$data = $this->Articles_model->get_article(mysql_real_escape_string($aid));
				if ($data['image'] != "")
				{
					Image::deleteImage($data['image']);
				}
				
				$this -> Articles_model -> update_article($aid, "image", $upl);
				$ret['image'] = $upl;
			}
			else
			{
				$ret['qstatus'] = 0;
				$ret['error'] = "image fail";
			}
		}
		$ret["aid"] = $aid;
        $json = json_encode($ret);
		echo $json;
		exit;
    }
	
	public function delete($arg) {
		$data = $this->Articles_model->get_article(mysql_real_escape_string($arg));
		if ($data['image'] != "")
		{
			Image::deleteImage($data['image']);
		}
		$this->Articles_model->delete_article(mysql_real_escape_string($arg));
		redirect($this->config->base_url() . 'admin/articles', 'refresh');
	}
	
	public function changestatus($arg) {
		$article_id = mysql_real_escape_string($arg);
		$article = $this->Articles_model->get_article($article_id);
		
		if ($article['status']!=1) {
			$value = 1;
		}else{
			$value = 0;
		}
		
		$this->Articles_model->update_article($article_id, 'status', $value);
		redirect($this->config->base_url() . 'admin/articles', 'refresh');
	}
	
	public function changesolo($arg) {
		$article_id = mysql_real_escape_string($arg);
		$article = $this->Articles_model->get_article($article_id);
		
		if ($article['solo']!=1) {
			$value = 1;
		}else{
			$value = 0;
		}
		
		$this->Articles_model->update_article($article_id, 'solo', $value);
		redirect($this->config->base_url() . 'admin/articles', 'refresh');
	}
	
}