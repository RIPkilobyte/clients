<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		Check::adminlogin();
		$this->load->model('gallery_model');
	}

	public function index()
	{
		$data = array();
		$data['images'] = $this->gallery_model->get_gallery_by_limit();

        $tpl['content'] = $this->load->view('admin/gallery/list.tpl', $data, TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);		
	}
	public function edit($id = 0)
	{
		$this->load->helper('url');
		$config['upload_path'] = './gallery/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size']	= '2048';
		//$config['max_width']  = '2048';
		//$config['max_height']  = '1536';
		$this->load->library('upload', $config);

		if($id) {
			$image = $this->gallery_model->get_by_id($id);
			$data['image'] = $image[0];			
			if(isset($_POST['gallery_id'])) {
				$update['gallery_id'] = $_POST['gallery_id'];
				$update['name'] = $this->input->post('name', true);
				$update['description'] = $this->input->post('description', true);

				if(isset($_FILES['file'])) { //удаление старого файла и загрузка нового
					if (!$this->upload->do_upload('file')) {
						$data['errors'] = $this->upload->display_errors();
					}
					else {
						unlink('./gallery/'.$data['image']['file']);
						unlink('./gallery/thumb_'.$data['image']['file']);
						$file = $this->upload->data();
						$this->thumbnail($file['full_path'], $file['file_path'].'thumb_'.$file['raw_name'].$file['file_ext'], 150, 150);
						$update['file'] = $file['file_name'];
						$update['full_path'] = $file['full_path'];
						$update['date_add'] = date("Y-m-d H:i:s");
						
					}
				}
				$this->gallery_model->save_image($update);
				redirect('/admin/gallery/');
			}
		}
		else {
			$data = array();
			if(isset($_POST['gallery_id'])) {

				if (!$this->upload->do_upload('file')) {
					$data['errors'] = $this->upload->display_errors();
				}
				else {
					$file = $this->upload->data();
					$this->thumbnail($file['full_path'], $file['file_path'].'thumb_'.$file['raw_name'].$file['file_ext'], 150, 150);
					$data['name'] = $this->input->post('name', true);
					$data['description'] = $this->input->post('description', true);
					$data['file'] = $file['file_name'];
					$data['full_path'] = $file['full_path'];
					$data['date_add'] = date("Y-m-d H:i:s");
					$this->gallery_model->save_image($data);
					redirect('/admin/gallery/');
				}
			}
		}
		$tpl['content'] = $this->load->view('admin/gallery/edit.tpl', $data, TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);		
	}
	public function delete($id)
	{
		if($id) {
			$image = $this->gallery_model->get_by_id($id);
			if($image) {
				$this->gallery_model->delete($id);
				@unlink('./gallery/'.$image[0]['file']);
				@unlink('./gallery/thumb_'.$image[0]['file']);				
				echo json_encode(array('status' => 'ok'));
			}
		}
		else {
			echo json_encode(array('status' => 'fail'));
		}
	}




	private function thumbnail($src, $dest, $width, $height, $rgb = 0xFFFFFF, $quality = 100)
	{
	    if (!file_exists($src)) {  
	        return false;  
	    }  
	  
	    $size = getimagesize($src);  
	  
	    if ($size === false) {  
	        return false;  
	    }  

	    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));  
	    $icfunc = 'imagecreatefrom'.$format;  

	    if (!function_exists($icfunc)) {  
	        return false;  
	    }  
	  
	    $x_ratio = $width  / $size[0];  
	    $y_ratio = $height / $size[1];  
	  
	    if ($height == 0) {  
	  
	        $y_ratio = $x_ratio;  
	        $height  = $y_ratio * $size[1];  
	  
	    } elseif ($width == 0) {  
	  
	        $x_ratio = $y_ratio;  
	        $width   = $x_ratio * $size[0];  
	  
	    }  
	  
	    $ratio       = min($x_ratio, $y_ratio);  
	    $use_x_ratio = ($x_ratio == $ratio);  
	  
	    $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);  
	    $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);  
	    $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width)   / 2);  
	    $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);  
	  
	    $isrc  = $icfunc($src);  
	    $idest = imagecreatetruecolor($width, $height);  
	  
	    //imagefill($idest, 0, 0, $rgb);  
	    imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);  
	  
	    imagejpeg($idest, $dest, $quality);  
	  
	    imagedestroy($isrc);  
	    imagedestroy($idest);  
	  
	    return true;  

	}
}
