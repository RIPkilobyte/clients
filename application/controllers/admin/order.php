<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		Check::adminlogin();
		$this->load->model('order_model');
	}
	public function prices()
	{
		$data = array();
		$data['academic_levels'] = $this->order_model->get_academic_levels();
		$data['deadlines'] = $this->order_model->get_deadlines();
		$data['prices'] = $this->order_model->get_prices();
		$tpl['content'] = $this->load->view('admin/order/prices.tpl', $data, TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function ajax_save_price()
	{
		$data = array('status' => 'fail');
		$price = (float)$this->input->post('price', true);
		$id = $this->input->post('id', true);
		$filter = explode('_', $id);
		if(isset($filter[0]) && isset($filter[1]) && isset($filter[2]) && $filter[0] && $filter[1] && $filter[2]) {

			$old_price = $this->order_model->get_price_by_filter($filter[0], $filter[1], $filter[2]);
			if($old_price) {
				$this->order_model->update_price($old_price['price_id'], array('value'=>$price));
			}
			else {
				$new_price = array(
					'type' => $filter[0],
					'academic_level_id' => $filter[1],
					'deadline_id' => $filter[2],
					'value' => $price,
				);
				$this->order_model->add_price($new_price);
			}
			$data['status'] = 'ok';
		}
		echo json_encode($data);
	}
	public function index()
	{
		$tpl['content'] = $this->load->view('admin/order/main.tpl', array(), TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function al($type = '')
	{
		$data['records'] = array();
		switch ($type) {
			case 'list':
				$data['records'] = $this->order_model->get_academic_levels();
				$tpl['content'] = $this->load->view('admin/order/al_list.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
				return NULL;
			break;
			case 'edit':
				$data['record_id'] = $this->uri->segment(5);

				if($data['record_id']) {
					$data['record'] = $this->order_model->get_academic_level_by_id($data['record_id']);
				}
				else {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				if(isset($_POST['submit']) && $_POST['submit']) {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				if($this->form_validation->run()) {
					if(!$data['record']['position'])
						$data['record']['position'] = 99;
					if($data['record_id'])
						$this->order_model->update_academic_level($data['record_id'], $data['record']);
					else
						$this->order_model->add_academic_level($data['record']);
					redirect('/admin/order/al/list');
				}
				else {
					$data['errors'] = validation_errors();
				}

				$tpl['content'] = $this->load->view('admin/order/al_edit.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
			break;
			case 'delete':
				$data['record_id'] = $this->uri->segment(5);
				if($data['record_id']) {
					$this->order_model->delete_academic_level($data['record_id']);
				}
				redirect('/admin/order/al/list');
			break;
			default:
				redirect('/admin/order');
			break;
		}
	}
	public function pt($type = '')
	{
		switch ($type) {
			case 'list':
				$data['records'] = $this->order_model->get_paper_types();
				$tpl['content'] = $this->load->view('admin/order/pt_list.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
				return NULL;
			break;
			case 'edit':
				$data['record_id'] = $this->uri->segment(5);

				if($data['record_id']) {
					$data['record'] = $this->order_model->get_paper_type_by_id($data['record_id']);
				}
				else {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				if(isset($_POST['submit']) && $_POST['submit']) {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				if($this->form_validation->run()) {
					if(!$data['record']['position'])
						$data['record']['position'] = 99;
					if($data['record_id'])
						$this->order_model->update_paper_type($data['record_id'], $data['record']);
					else
						$this->order_model->add_paper_type($data['record']);
					redirect('/admin/order/pt/list');
				}
				else {
					$data['errors'] = validation_errors();
				}

				$tpl['content'] = $this->load->view('admin/order/pt_edit.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
			break;
			case 'delete':
				$data['record_id'] = $this->uri->segment(5);
				if($data['record_id']) {
					$this->order_model->delete_paper_type($data['record_id']);
				}
				redirect('/admin/order/pt/list');
			break;
			default:
				redirect('/admin/order');
			break;
		}
	}
	public function pf($type = '')
	{
		switch ($type) {
			case 'list':
				$data['records'] = $this->order_model->get_paper_formats();
				$tpl['content'] = $this->load->view('admin/order/pf_list.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
				return NULL;
			break;
			case 'edit':
				$data['record_id'] = $this->uri->segment(5);

				if($data['record_id']) {
					$data['record'] = $this->order_model->get_paper_format_by_id($data['record_id']);
				}
				else {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				if(isset($_POST['submit']) && $_POST['submit']) {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				if($this->form_validation->run()) {
					if(!$data['record']['position'])
						$data['record']['position'] = 99;
					if($data['record_id'])
						$this->order_model->update_paper_format($data['record_id'], $data['record']);
					else
						$this->order_model->add_paper_format($data['record']);
					redirect('/admin/order/pf/list');
				}
				else {
					$data['errors'] = validation_errors();
				}

				$tpl['content'] = $this->load->view('admin/order/pf_edit.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
			break;
			case 'delete':
				$data['record_id'] = $this->uri->segment(5);
				if($data['record_id']) {
					$this->order_model->delete_paper_format($data['record_id']);
				}
				redirect('/admin/order/pf/list');
			break;
			default:
				redirect('/admin/order');
			break;
		}
	}
	public function subject($type = '')
	{
		switch ($type) {
			case 'list':
				$data['records'] = $this->order_model->get_subjects();
				$tpl['content'] = $this->load->view('admin/order/subject_list.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
				return NULL;
			break;
			case 'edit':
				$data['record_id'] = $this->uri->segment(5);

				if($data['record_id']) {
					$data['record'] = $this->order_model->get_subject_by_id($data['record_id']);
				}
				else {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				if(isset($_POST['submit']) && $_POST['submit']) {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				if($this->form_validation->run()) {
					if(!$data['record']['position'])
						$data['record']['position'] = 99;
					if($data['record_id'])
						$this->order_model->update_subject($data['record_id'], $data['record']);
					else
						$this->order_model->add_subject($data['record']);
					redirect('/admin/order/subject/list');
				}
				else {
					$data['errors'] = validation_errors();
				}

				$tpl['content'] = $this->load->view('admin/order/subject_edit.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
			break;
			case 'delete':
				$data['record_id'] = $this->uri->segment(5);
				if($data['record_id']) {
					$this->order_model->delete_subject($data['record_id']);
				}
				redirect('/admin/order/subject/list');
			break;
			default:
				redirect('/admin/order');
			break;
		}
	}
	public function deadline($type = '')
	{
		switch ($type) {
			case 'list':
				$data['records'] = $this->order_model->get_deadlines();
				$tpl['content'] = $this->load->view('admin/order/deadline_list.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
				return NULL;
			break;
			case 'edit':
				$data['record_id'] = $this->uri->segment(5);

				if($data['record_id']) {
					$data['record'] = $this->order_model->get_deadline_by_id($data['record_id']);
				}
				else {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				if(isset($_POST['submit']) && $_POST['submit']) {
					$data['record'] = array(
						'title' => $this->input->post('title', true),
						'position' => $this->input->post('position', true)
					);
				}
				$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				if($this->form_validation->run()) {
					if(!$data['record']['position'])
						$data['record']['position'] = 99;
					if($data['record_id'])
						$this->order_model->update_deadline($data['record_id'], $data['record']);
					else
						$this->order_model->add_deadline($data['record']);
					redirect('/admin/order/deadline/list');
				}
				else {
					$data['errors'] = validation_errors();
				}

				$tpl['content'] = $this->load->view('admin/order/deadline_edit.tpl', $data, TRUE);
				$this->load->view('admin/admin.tpl', $tpl);
			break;
			case 'delete':
				$data['record_id'] = $this->uri->segment(5);
				if($data['record_id']) {
					$this->order_model->delete_deadline($data['record_id']);
				}
				redirect('/admin/order/deadline/list');
			break;
			default:
				redirect('/admin/order');
			break;
		}
	}
}