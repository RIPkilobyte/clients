<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajaxadmin extends CI_Controller {

    public function Ajaxadmin() {
        parent::__construct();
        Check::adminlogin();
    }

    public function index() {
        echo "Пустой аякс";
        exit();
    }
    
    public function article_save($article_id = NULL) {
        $this -> load -> model('Articles_model');
        $this -> Articles_model -> save_article($article_id);
        echo "success";
    }
    
    public function artcat_save($artcat = NULL) {
        $this -> load -> model('Articles_model');
        $this -> Articles_model -> save_artcat($artcat);
        echo "success";
    }
    
}