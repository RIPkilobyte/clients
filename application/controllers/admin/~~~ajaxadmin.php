<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajaxadmin extends CI_Controller {

    public function Ajaxadmin() {
        parent::__construct();
        Check::adminlogin();
    }

    public function index() {
        echo "Пустой аякс";
        exit();
    }
    
    public function article_save($article_id = NULL) {
        $this -> load -> model('Articles_model');
        $this -> Articles_model -> save_article($article_id);
		
		$ret = array("status" => 1);


		//фото
		if ((count($_FILES) > 0) && ($_FILES['image']['error'] == 0))
		{
			$upl = Image::upload($_FILES['image'], '/upload/images/', array('width' => 1024, 'height' => 768));
			if ($upl !=  FALSE)
			{
				$data = $this->Articles_model->get_article(mysql_real_escape_string($article_id));
				if ($data['image'] != "")
				{
					Image::deleteImage($data['image']);
				}
				
				$this -> Articles_model -> update_article($article_id, "image", $upl);
				$ret['image'] = $upl;
			}
			else
			{
				$ret['status'] = 0;
				$ret['error'] = "image fail";
			}
		}
        $json = json_encode($ret);
		echo $json;
		exit;
    }
    
    public function artcat_save($artcat = NULL) {
        $this -> load -> model('Articles_model');
        $this -> Articles_model -> save_artcat($artcat);
        echo "success";
    }
    
}