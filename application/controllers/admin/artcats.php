<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artcats extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('Articles_model');
		Check::adminlogin();
	}
	
	public function index() {
		
		$list['list'] = $this->Articles_model->get_artcats();
        $tpl['content'] = $this->load->view('admin/artcats/list.tpl', $list, TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);
		
	}
	
	public function edit($arg = NULL) {
		
		if (@$arg) {
			$data = $this->Articles_model->get_artcat(mysql_real_escape_string($arg));
		}else{
			$data = array();
		}
		
		$tpl['content'] = $this->load->view('admin/artcats/edit.tpl', $data, TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);
		
	}
	
	public function ajax_deleteImage($arg) {
		$data = $this->Articles_model->get_artcat(mysql_real_escape_string($arg));
		if ($data['image'] != "")
		{
			if (Image::deleteImage($data['image']))
			{
				$this -> Articles_model -> update_artcat($arg, "image", "");
				echo "success";
				return 1;
			}
		}
		exit();
	}
	
	public function ajax_artcat_save($artcat = NULL) {
        $this -> load -> model('Articles_model');
        $aid = $this -> Articles_model -> save_artcat($artcat);
		
        $ret = array("qstatus" => 1);

		//фото
		if ((count($_FILES) > 0) && ($_FILES['image']['error'] == 0))
		{
			$upl = Image::upload($_FILES['image'], '/upload/images/', array('width' => 1024, 'height' => 768));
			if ($upl !=  FALSE)
			{
				$data = $this->Articles_model->get_artcat(mysql_real_escape_string($aid));
				if ($data['image'] != "")
				{
					Image::deleteImage($data['image']);
				}
				
				$this -> Articles_model -> update_artcat($aid, "image", $upl);
				$ret['image'] = $upl;
			}
			else
			{
				$ret['qstatus'] = 0;
				$ret['error'] = "image fail";
			}
		}
		$ret["aid"] = $aid;
        $json = json_encode($ret);
		echo $json;
		exit;
    }
	
	public function delete($arg) {
		$data = $this->Articles_model->get_artcat(mysql_real_escape_string($arg));
		if ($data['image'] != "")
		{
			Image::deleteImage($data['image']);
		}
		$this->Articles_model->delete_artcat(mysql_real_escape_string($arg));
		redirect($this->config->base_url() . 'admin/artcats', 'refresh');
	}
	
	public function changestatus($arg) {
		$artcat_id = mysql_real_escape_string($arg);
		$artcat = $this->Articles_model->get_artcat($artcat_id);
		
		if ($artcat['status']!=1) {
			$value = 1;
		}else{
			$value = 0;
		}
		
		$this->Articles_model->update_artcat($artcat_id, 'status', $value);
		redirect($this->config->base_url() . 'admin/artcats', 'refresh');
	}
	
}