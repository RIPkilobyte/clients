<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		$this->load->model('blog_model');
		Check::adminlogin();
	}
	public function index()
	{
		$data['records'] = $this->blog_model->get_list();
		foreach ($data['records'] as $k => $v) {
			$data['records'][$k]['date'] = date("M j h:i A", strtotime($v['date']));
			$data['records'][$k]['comments'] = $this->blog_model->count_blog_comments($v['blog_id']);
			$data['records'][$k]['unmoderate'] = $this->blog_model->count_unmoderated_blog_comments($v['blog_id']);
		}
		$tpl['content'] = $this->load->view('admin/blog/list.tpl', $data, TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function edit($id = 0)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('href', 'Link to record', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		if(!isset($_POST['submit']) && $id) {
			$data = $this->blog_model->get($id);
		}
		else {
			$data = array(
				'title' => $this->input->post('title', true),
				'href' => $this->input->post('href', true),
				'pre' => $this->input->post('pre'),
				'text' => $this->input->post('text'),
				'date' => $this->input->post('date', true),
				'comments_on' => $this->input->post('comments_on', true),
				'moderate' => $this->input->post('moderate', true),
				'date' => $this->input->post('date', true),
			);
		}
		if(!$data['date'])
			$data['date'] = date("Y-m-d H:i:s");
		if($this->form_validation->run()) {
			if($id)
				$this->blog_model->update($id, $data);
			else
				$this->blog_model->add($data);
			redirect('/admin/blog');
		}
		else {
			$data['comments_on'] = 1;
			$data['moderate'] = 1;
			//$data['date'] = date("M j h:i A", strtotime($data['date']));
			$data['blog_id'] = $id;
			$data['errors'] = validation_errors();
		}
		$tpl['content'] = $this->load->view('admin/blog/edit.tpl', $data, TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function delete($id)
	{
		if($id) {
			$this->blog_model->delete($id);
		}
		redirect('/admin/blog');
	}
	public function json_delete($id)
	{
		$json['status'] = 'fail';
		if($id) {
			$id = $this->input->post('id', true);
			$this->blog_model->delete($id);
			$json['status'] = 'ok';
		}
		echo json_encode($json);
	}
	public function delete_comment($id)
	{
		if($id) {
			$this->blog_model->delete_comment($id);
		}
		redirect('/admin/blog');
	}
	public function ajax_delete_comment($id)
	{
		$json['status'] = 'fail';
		if($id) {
			$id = $this->input->post('id', true);
			$this->blog_model->delete_comment($id);
			$json['status'] = 'ok';
		}
		echo json_encode($json);
	}
	public function ajax_moderate_comment()
	{
		$json['status'] = 'fail';
		$id = $this->input->post('id', true);
		if($id) {
			$comment = $this->blog_model->get_comment($id);
			if($comment) {
				if($comment['moderated'] == '1')
					$this->blog_model->update_comment($id, array('moderated' => 0));
				else
					$this->blog_model->update_comment($id, array('moderated' => 1));
				$json['status'] = 'ok';
			}
		}
		echo json_encode($json);
	}
	public function comments($id = 0)
	{
		if(!$id)
			redirect('/admin/blog');
		$data['blog_id'] = $id;
		$data['records'] = $this->blog_model->get_blog_comments($id);
		foreach ($data['records'] as $k => $v) {
		}
		$tpl['content'] = $this->load->view('admin/blog/comments.tpl', $data, TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function unmoderated($id)
	{
		if(!$id)
			redirect('/admin/blog');
		$data['blog_id'] = $id;
		$data['records'] = $this->blog_model->get_unmoderated_blog_comments($id);
		foreach ($data['records'] as $k => $v) {
		}
		$tpl['content'] = $this->load->view('admin/blog/comments.tpl', $data, TRUE);
		$this->load->view('admin/admin.tpl', $tpl);
	}
}