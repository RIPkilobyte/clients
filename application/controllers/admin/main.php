<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		Check::adminlogin();
		$this->load->model('Other_model');
	}
	public function index() {
		
		$msg = "";
		// Если сохраняем настройки
		if ($this->input->post('save_settings', TRUE)) {
			$this->Other_model->save_settings();
			redirect($this->config->base_url() . 'admin', 'refresh');
		}
		
		if ($this->input->post('save_pwd', TRUE)) 
		{
			$this->load->model("adminusers_model");
       		$user = $this->adminusers_model->get($this->session->userdata['login']);	
        	if ($user != FALSE && md5($this->input->post('pwd_old', TRUE)) == $user['password']) 
			{
				if (($this->input->post('pwd_new', TRUE) == $this->input->post('pwd_conf', TRUE)) && $this->input->post('pwd_new', TRUE) != "")
				{
						$this->adminusers_model->update($this->session->userdata['login'], array('password' => md5($this->input->post('pwd_new', TRUE))));
						$this->session->userdata['password'] = md5($this->input->post('pwd_new', TRUE));
						$msg = $this->lang->line('Password_has_changed');
				}
				else
				{
					$msg = $this->lang->line('Passwords_do_not_match');
				}
			}
			else
			{
				$msg = $this->lang->line('Password_is_not_correct');
			}
		}
		$main_page_text = $this->other_model->get_main_page_text();
		$video = $this->other_model->get_video();
        $tpl['content'] = $this->load->view('admin/main.tpl', array("msg"=>$msg, 'main_page_text'=>$main_page_text, 'video'=>$video), TRUE);
		
		$this->load->view('admin/admin.tpl', $tpl);
	}
	public function logoutsupport()
	{
		$this->load->model('my_auth_model');
		$data = array(
			'salt' => md5('1'.time())
			);
		$this->my_auth_model->update_user(1, $data);

		redirect($_SERVER['HTTP_REFERER']);
	}
}