<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index() {
        		
		if ($this->input->post('login', TRUE) || $this->input->post('password', TRUE)) {
            $this->_processing();
            return NULL;
        }
		
        $this->load->view('admin_login.tpl', FALSE);
        
	}
    
    function _processing() {
        
        if (@!$this->input->post('login', TRUE) || @!$this->input->post('password', TRUE)) {
            $this->load->view('admin_login.tpl', array('msg'=>$this->lang->line('You_have_not_entered_the_username_and_password')));
            return NULL;
        }
		
		$this->load->model("adminusers_model");
        $user = $this->adminusers_model->get($this->input->post('login', TRUE));

        if ($user != FALSE && md5($this->input->post('password', TRUE)) == $user['password']) {
			$this->session->set_userdata(array("role" => 'superuser', "login" => $user['login'], 'password' => $user['password']));
			redirect($this->config->base_url() . 'admin', 'refresh');
			echo 'Error redirect';
			exit();
		}
		
		$this->load->view('admin_login.tpl', array('msg'=>$this->lang->line('You_have_not_entered_the_username_and_password')));
        
    }
	
	public function logout() {
		$this->session->sess_destroy();
		redirect($this->config->base_url(), 'refresh');
	}
	
}