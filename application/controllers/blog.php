<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {
	private $per_page = 10;
	function __construct() 
	{
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('order_model');
	}
	public function index($offset = 0)
	{
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$config['total_rows'] = $this->blog_model->count_records();
		$config['full_tag_open'] = '<div class="paginate">';
		$config['full_tag_close'] = '</div>';
		$config['num_links'] = 5;
		$config['base_url'] = '/blog/page/';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config); 
        $data = array(
            'academic_levels' => $this->order_model->get_academic_levels(),
            'paper_types' => $this->order_model->get_paper_types(),
            'deadlines' => $this->order_model->get_deadlines()
        );		
		$data['pagination'] = $this->pagination->create_links();
		
		$tpl['title'] = $this->config->config['tp_title']."Blog";
		$tpl['keywords'] = '';
		$tpl['description'] = '';
		$data['title'] = 'Blog';
        $tpl['breadcrumbs'][] = array(
            'title' => 'Main page',
            'href' => '/'
        );
        $tpl['breadcrumbs'][] = array(
            'title' => 'Blog',
            'href' => '/blog'
        );

        $tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
		$data['records'] = $this->blog_model->get_last_records($this->per_page, $offset);
		foreach ($data['records'] as $k => $v) {
			$data['records'][$k]['date'] = date("M j h:i A", strtotime($v['date']));
			$data['records'][$k]['comments'] = $this->blog_model->count_moderated_blog_comments($v['blog_id']);
		}
        $tpl['content'] = $this->load->view('common/blog.tpl', $data, TRUE);
		$this->load->view('template.tpl', $tpl);
	}
	public function page($offset = 0)
	{
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$config['total_rows'] = $this->blog_model->count_records();
		$config['full_tag_open'] = '<div class="paginate">';
		$config['full_tag_close'] = '</div>';
		$config['num_links'] = 5;
		$config['base_url'] = '/blog/page/';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config); 
        $data = array(
            'academic_levels' => $this->order_model->get_academic_levels(),
            'paper_types' => $this->order_model->get_paper_types(),
            'deadlines' => $this->order_model->get_deadlines()
        );		
		$data['pagination'] = $this->pagination->create_links();
		
		$tpl['title'] = $this->config->config['tp_title']."Blog";
		$tpl['keywords'] = '';
		$tpl['description'] = '';
		$data['title'] = 'Blog';
        $tpl['breadcrumbs'][] = array(
            'title' => 'Main page',
            'href' => '/'
        );
        $tpl['breadcrumbs'][] = array(
            'title' => 'Blog',
            'href' => '/blog'
        );
        $tpl['breadcrumbs'][] = array(
            'title' => 'Blog',
            'href' => '/blog'
        );
        $tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
		$data['records'] = $this->blog_model->get_last_records($this->per_page, $offset);
		foreach ($data['records'] as $k => $v) {
			$data['records'][$k]['date'] = date("M j h:i A", strtotime($v['date']));
			$data['records'][$k]['comments'] = $this->blog_model->count_moderated_blog_comments($v['blog_id']);
		}
        $tpl['content'] = $this->load->view('common/blog.tpl', $data, TRUE);
		$this->load->view('template.tpl', $tpl);
	}
	public function view($href = 0)
	{
		if(!$href)
			redirect('/blog');
        $data = array(
            'academic_levels' => $this->order_model->get_academic_levels(),
            'paper_types' => $this->order_model->get_paper_types(),
            'deadlines' => $this->order_model->get_deadlines()
        );
		$data['record'] = $this->blog_model->get_by_href($href);
		if(!$data['record'])
			redirect('/blog');
		$data['record']['date'] = date("M j h:i A", strtotime($data['record']['date']));
		$data['comments'] = $this->blog_model->get_moderated_blog_comments($data['record']['blog_id']);
		$tpl['title'] = $this->config->config['tp_title'].$data['record']['title'];
		$tpl['keywords'] = '';
		$tpl['description'] = '';
		$data['title'] = 'Blog';
        $tpl['breadcrumbs'][] = array(
            'title' => 'Main page',
            'href' => '/'
        );
        $tpl['breadcrumbs'][] = array(
            'title' => 'Blog',
            'href' => '/blog'
        );
        $tpl['breadcrumbs'][] = array(
            'title' => $data['record']['title'],
            'href' => '/blog/'.$href
        );
        $tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
        $tpl['content'] = $this->load->view('common/blog_details.tpl', $data, TRUE);
		$this->load->view('template.tpl', $tpl);
	}
	public function add_comment()
	{
		$json['status'] = 'fail';
		$id = $this->input->post('id', true);
		$name = $this->input->post('name', true);
		$email = $this->input->post('email', true);
		$text = $this->input->post('text', true);
		if($id && $name && $email && $text) {
			$this->blog_model->add_comment(array(
				'blog_id' => $id,
				'name' => $name,
				'email' => $email,
				'text' => $text,
				'date' => date("Y-m-d H:i:s")
			));
			$json['status'] = 'ok';
		}
		echo json_encode($json);
	}
}