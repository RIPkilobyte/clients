<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Discounts extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
	}
	public function index() 
	{
		if(!Check::login())
			redirect('/user/login');
		$data = array();
		
		$data['user_orders'] = $this->order_model->get_my_orders($_SESSION['user_id']);
		$data['discount_orders'] = $this->order_model->get_discount_orders($_SESSION['user_id'].'02');
		$amount = $_SESSION['userdata']['amount'];
		foreach ($data['discount_orders'] as $key => $value) {
			$data['discount_orders'][$key]['discount_save'] = '0.00';
			if($value['payment_id'] && !$value['taken']) {
				$this->order_model->update($value['order_id'], array('taken'=>1));
				$data['discount_orders'][$key]['discount_save'] = $value['discount_save'];
				$amount += $value['discount_save'];
			}
			if($value['payment_id'] && $value['taken']) {
				$data['discount_orders'][$key]['discount_save'] = $value['discount_save'];
			}

		}
		$discount = array(
			'amount' => $amount
			);
		$_SESSION['userdata']['amount'] = $amount;
		$this->my_auth->update_user($discount, $_SESSION['user_id']);

		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value)
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		$tpl['title'] = $this->config->config['tp_title']."Your discounts";
		$tpl['content'] = $this->load->view('user/discounts.tpl', $data, true);
		$this->load->view('user_template.tpl', $tpl);

	}
}