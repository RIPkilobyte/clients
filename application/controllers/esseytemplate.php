<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('assist_model');
		$this->load->model('order_model');
	}
	
	public function index() 
	{
		$block_array = array();
		$this->load->helper('captcha');
		$string = random_string('alnum', 7);
		$vals = array(
			'word' => $string, // фраза, которая будет показана на капче
			'img_path' => './captcha/', // папка, куда будет сохраняться капча
			'img_url' => base_url().'captcha/', // ссылка к картинке с капчей
			'font_path' => './system/fonts/texb.ttf', // шрифт, которым будет написана капча
			'img_width' => 215, // ширина капчи
			'img_height' => 80, // высота капчи
			'expiration' => 600 // время хранения картинки капчи в папке
		);
        $data = array(
            'academic_levels' => $this->order_model->get_academic_levels(),
            'paper_types' => $this->order_model->get_paper_types(),
            'deadlines' => $this->order_model->get_deadlines()
            ); 		
		$data['captcha'] = create_captcha($vals);
		$this->assist_model->add_captcha($data['captcha']);		

		$tpl['title'] = $this->config->config['tp_title']."Contact Us";
		//$data['right_block'] = $this->load->view('common/right_block.tpl', false, TRUE);
		$data['title'] = 'Contact Form';
		$tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
		$tpl['description'] = 'Looking for writing help? Our professionals can write academic papers on any topic and of any level. Just place your order with all paper details and receive perfect result up to the 8 hours!';
		$tpl['keywords'] = 'high school papers, custom essay writing help';

        $tpl['content'] = $this->load->view('information/contact.tpl', $data, TRUE);
		
		$this->load->view('template.tpl', $tpl);
	}
	
}