<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Router extends CI_Controller {
    
    function __construct() {
		parent::__construct();
		$this->load->model('Articles_model');
        $this->load->model('order_model');
        $this->load->model('other_model');
	}
	
	public function index() 
    {
        $block_array = array('main');
        // Достаем сегменты
        if ($this -> uri -> segment(1)) {
            $segment1 = mysql_real_escape_string($this -> uri -> segment(1));
        }
        else{
            show_404();
        }
        if ($this -> uri -> segment(2)) {
            $segment2 = mysql_real_escape_string($this -> uri -> segment(2));
        }
        if ($this -> uri -> segment(3)) {
            $segment2 = mysql_real_escape_string($this -> uri -> segment(3));
        }
        $data = array(
            'academic_levels' => $this->order_model->get_academic_levels(),
            'paper_types' => $this->order_model->get_paper_types(),
            'deadlines' => $this->order_model->get_deadlines()
            );
        $prices = $this->order_model->get_prices();
        $data['prices'] = array();
        foreach ($data['academic_levels'] as $key => $academic_level) {
            foreach ($data['deadlines'] as $key => $deadline) {
                foreach ($prices as $key => $price) {
                    if($price['type'] == 'new' && $price['academic_level_id'] == $academic_level['academic_level_id'] && $price['deadline_id'] == $deadline['deadline_id'])
                        $data['prices']['new'][$academic_level['academic_level_id']][$deadline['deadline_id']] = $price['value'];
                    if($price['type'] == 'edit' && $price['academic_level_id'] == $academic_level['academic_level_id'] && $price['deadline_id'] == $deadline['deadline_id'])
                        $data['prices']['edit'][$academic_level['academic_level_id']][$deadline['deadline_id']] = $price['value'];
                }
            }
        }
        $tpl['main_page_text'] = $this->other_model->get_main_page_text();        
        // ищем избранные статьи (которые выводятся напрямую)
        if(isset($segment3)) {

            $solo3 = $this -> Articles_model -> get_article_solo($segment3);
            if (!empty($solo3)) {
                $solo3['breadcrumbs'][] = array(
                    'title' => 'Main page',
                    'href' => '/'
                );
                if($solo3['artcat_id']) {
                    $category = $this -> Articles_model -> get_artcat($solo3['artcat_id']);
                    if($category) {
                        $solo3['breadcrumbs'][] = array(
                            'title' => $category['title'],
                            'href' => $category['alias']
                        );
                    }
                }
                $solo3['breadcrumbs'][] = array(
                    'title' => $solo3['title'],
                    'href' => $solo3['alias']
                );                
                if(isset($solo3['meta_title']) && $solo3['meta_title'])
                    $tpl['title'] = $solo3['meta_title'].$this->config->config['tp_article_title'];
                else
                    $tpl['title'] = $this->config->config['tp_title'].$solo3['title'];
                if(!in_array($segment3, $block_array))
                    $tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
                $tpl['content'] = $this->load->view('common/article.tpl', $solo3, TRUE);
                $tpl['description'] = $solo3['meta_description'];
                $tpl['keywords'] = $solo3['meta_keyword'];
                $tpl['video'] = $solo3['video'];
                $tpl['video_class'] = 'slave_video';
                $this->load->view('template.tpl', $tpl);
                return NULL;
            }
        }        
        elseif(isset($segment2)) {
            $solo2 = $this -> Articles_model -> get_article_solo($segment2);
            if (!empty($solo2)) {
                $solo2['breadcrumbs'][] = array(
                    'title' => 'Main page',
                    'href' => '/'
                );
                if($solo2['artcat_id']) {
                    $category = $this -> Articles_model -> get_artcat($solo2['artcat_id']);
                    if($category) {
                        $solo2['breadcrumbs'][] = array(
                            'title' => $category['title'],
                            'href' => $category['alias']
                        );
                    }
                }
                $solo2['breadcrumbs'][] = array(
                    'title' => $solo2['title'],
                    'href' => $solo2['alias']
                );                
                if(isset($solo2['meta_title']) && $solo2['meta_title'])
                    $tpl['title'] = $solo2['meta_title'].$this->config->config['tp_article_title'];
                else
                    $tpl['title'] = $this->config->config['tp_title'].$solo2['title'];

                if(!in_array($segment2, $block_array))
                    $tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
                $tpl['content'] = $this->load->view('common/article.tpl', $solo2, TRUE);
                $tpl['description'] = $solo2['meta_description'];
                $tpl['keywords'] = $solo2['meta_keyword'];
                $tpl['video'] = $solo2['video'];
                $tpl['video_class'] = 'slave_video';
                $this->load->view('template.tpl', $tpl);
                return NULL;
            }
        }
        else {

            $solo = $this -> Articles_model -> get_article_solo($segment1);

            if(!empty($solo)) {
                $solo['breadcrumbs'][] = array(
                    'title' => 'Main page',
                    'href' => '/'
                );
                if($solo['artcat_id']) {
                    $category = $this -> Articles_model -> get_artcat($solo['artcat_id']);
                    if($category) {
                        $solo['breadcrumbs'][] = array(
                            'title' => $category['title'],
                            'href' => $category['alias']
                        );
                    }
                }
                $solo['breadcrumbs'][] = array(
                    'title' => $solo['title'],
                    'href' => $solo['alias']
                );
                if(isset($solo['meta_title']) && $solo['meta_title'])
                    $tpl['title'] = $solo['meta_title'].$this->config->config['tp_article_title'];
                else
                    $tpl['title'] = $this->config->config['tp_title'].$solo['title'];

                if(!in_array($segment1, $block_array))
                    $tpl['left_block'] = $this->load->view('common/left_block.tpl', $data, TRUE);
                if($segment1 == 'main') {
                    $data = array(
                        'academic_levels' => $this->order_model->get_academic_levels(),
                        'paper_types' => $this->order_model->get_paper_types(),
                        'deadlines' => $this->order_model->get_deadlines()
                    );
                    $tpl['calc'] = $this->load->view('home/calc.tpl', $data, TRUE);
                }                
                $tpl['content'] = $this->load->view('common/article.tpl', $solo, TRUE);
                $tpl['description'] = $solo['meta_description'];
                $tpl['keywords'] = $solo['meta_keyword'];
                $tpl['video'] = $solo['video'];
                $tpl['video_class'] = 'slave_video';
                $this->load->view('template.tpl', $tpl);
                return NULL;
            }
        }
        // ищем категории
        $category = $this -> Articles_model -> get_artcat_fralias($segment1);

        if (!empty($category)) {
            
            // если 2-го сегмента нет, то выводим список статей категории
            if (@!$segment2) {
                
                // выбираем все статьи в этой категории
                $articles['articles'] = $this -> Articles_model -> get_artcles_frartcat($category['artcat_id']);
                $articles['category'] = $category;
                
                if (empty($articles['articles'])) {
                    show_404();
                }
                $tpl['description'] = $this->other_model->get_description();
                $tpl['description'] = $tpl['description']['value'];
                $tpl['keywords'] = $this->other_model->get_keywords();
                $tpl['keywords'] = $tpl['keywords']['value'];                
                $tpl['title'] = $this->config->config['tp_title'].$category['title'];
                $tpl['content'] = $this->load->view('common/artcat.tpl', $articles, TRUE);
                $this->load->view('template.tpl', $tpl);
                return NULL;
            
            }else{ // если имеется второй сегмент - то ищем матирал
                
                $article = $this -> Articles_model -> get_article_fralias($segment2);
                if (@$article) {
                    $tpl['title'] = $this->config->config['tp_title'].$article['title'];
                    $tpl['content'] = $this->load->view('common/article.tpl', $article, TRUE);
                    $tpl['description'] = $article['meta_description'];
                    $tpl['keywords'] = $article['meta_keyword'];     
                    $this->load->view('template.tpl', $tpl);
                    return NULL;
                }
                
            }
            
        }
        
        // Если все попытки найти материал впустую
        // show_404(); ниже 404 кастомизированная
 				$tpl['description'] = $this->other_model->get_description();
                $tpl['description'] = "404 error";
                $tpl['keywords'] = $this->other_model->get_keywords();
                $tpl['keywords'] = "Error"; 
				
				$tpl['title'] = $this->other_model->get_keywords();
                $tpl['title'] = "404 error"; 
				$tpl['calc'] = $this->load->view('home/calc.tpl', $data, TRUE);
				$tpl['content'] = $this->load->view('404error.tpl', $data, TRUE);
				$this->load->view('template.tpl', $tpl);
				
    }
    
}