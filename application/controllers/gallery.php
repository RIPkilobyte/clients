<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('gallery_model');
	}
	
	public function index() 
	{
		$data = array();
		$tpl['title'] = 'Галерея';
		$data['images'] = $this->gallery_model->get_gallery_by_limit();
		$tpl['content'] = $this->load->view('common/gallery.tpl', $data, TRUE); 
		$tpl['description'] = $this->other_model->get_description();
		$tpl['description'] = $tpl['description']['value'];
		$tpl['keywords'] = $this->other_model->get_keywords();
		$tpl['keywords'] = $tpl['keywords']['value'];
		$this->load->view('template.tpl', $tpl);
    }
    
}