<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
		$this->load->model('assist_model');
		$this->load->model('user_model');
	}
	public function test()
	{
		$data = array(
			'user_id' => 3,
			'email' => 'ripkilobyte1@gmail.com',
			'first_name' => 'frst',
			'last_name' => 'lst',
			'order_id' => 1,
			'subject_title' => 'subject',
			'topic' => 'topic',
			'paper_type_title' => 'pttitle',
			'academic_level_title' => 'altitle',
			'pages' => 0,
			'deadline_title' => 'dtest',
			'reason' => 'reason'
		);
		Mail::prepare('client_place_order', $data);
	}
	public function index() 
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$data = $_POST;
		$tpl['breadcrumbs'][] = array(
			'title' => 'Main page',
			'href' => '/'
		);
		$tpl['breadcrumbs'][] = array(
			'title' => 'Order now',
			'href' => '/order'
		);
		$this->load->helper('captcha');
		/*
		$possible = 'ABCDEFGHIJKLMNOPQRSTVWXYZabcdefghijklmnopqrstvwxyz0123456789';
		$i = 0;
		$string = '';
		while($i < 7) {
			$string .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		//*/
		//$string = random_string('alnum', 7);
		$vals = array(
			//'word' => $string, // фраза, которая будет показана на капче
			'img_path' => './captcha/', // папка, куда будет сохраняться капча
			'img_url' => base_url().'captcha/', // ссылка к картинке с капчей
			'font_path' => './system/fonts/texb.ttf', // шрифт, которым будет написана капча
			'img_width' => 215, // ширина капчи
			'img_height' => 80, // высота капчи
			'expiration' => 600 // время хранения картинки капчи в папке
		);

		$data['captcha'] = create_captcha($vals);

		$this->assist_model->add_captcha($data['captcha']);

		if(isset($_POST['submit'])) {
			$data['fields'] = $_POST;
			if(!$this->assist_model->check_captcha($this->input->post('captcha', true)))
				unset($_POST['captcha']);
			$this->form_validation->set_rules('work_type', 'Type of service', 'required');
			$this->form_validation->set_rules('paper_type', 'Type of paper', 'required');
			$this->form_validation->set_rules('deadline', 'Deadline', 'required');
			
			$this->form_validation->set_rules('topic', 'Topic', 'required');
			$this->form_validation->set_rules('paper_details', 'Paper Details', 'required');
			$this->form_validation->set_rules('captcha', 'Captcha', 'required');
			$this->form_validation->set_rules('agree', 'Agree with Money Back Guarantee, Privacy Policy, Terms of Use', 'required');
			$this->form_validation->set_rules('paper_format', 'Paper Format', 'required');
			$this->form_validation->set_rules('academic_level', 'Academic Level', 'required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');

			
			if($data['fields']['user'] == '2') {
				$this->form_validation->set_rules('email_login', 'Email', 'required|valid_email');
				$this->form_validation->set_rules('password_login', 'Password', 'required');
			}
			else {
				$this->form_validation->set_rules('email_reg', 'Email', 'required|matches[confirm_email]|valid_email');
				$this->form_validation->set_rules('confirm_email', 'Confirm Email', 'required|valid_email');
				$this->form_validation->set_rules('first_name', 'First Name', 'required');
				$this->form_validation->set_rules('last_name', 'Last name', 'required');
				$this->form_validation->set_rules('password_reg', 'Password', 'required|matches[confirm_password]');
				$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');
				$this->form_validation->set_rules('country', 'Country', 'required');
				$this->form_validation->set_rules('phone_number', 'Phone', 'required');
			}
		}
		if($this->form_validation->run()) {
			if($data['fields']['user'] == '2') {
				$email = $this->input->post('email_login', true);
				$password = $this->input->post('password_login', true);
				if(!$this->my_auth->login($email, $password))
					$data['errors']['login_error'] = '<div class="error">Wrong email or password</div>';
			}
			else {
				if($this->my_auth->email_check($this->input->post('email_reg', true))) {
					$data['errors']['login_error'] = '<div class="error">There is an account registered with this email. Please use restore password option to access it or use another email for registration.</div>';
					$data['errors']['email_check_fail'] = '<div class="error">There is an account registered with this email.</div>';
				}
				else {
					$data['email'] = $this->input->post('email_reg', true);
					$data['password'] = $this->input->post('password_reg', true);
					$data['first_name'] = $this->input->post('first_name', true);
					$data['last_name'] = $this->input->post('last_name', true);
					$data['country'] = $this->input->post('country', true);
					$data['phone'] = $this->input->post('phone_number', true);
					$data['verification'] = md5($data['email'].time());
					if($this->my_auth->register($data)) {
						//PrepareMail::send('client_after_register', $data['email'], $data['first_name'], '', '', $data['email'], $data['password'], $data['verification']);
						Mail::prepare('client_register', $data);
						$this->my_auth->login($data['email'], $data['password']);
					}
					else
						$data['errors']['login_error'] = '<div class="error">Wrong email or password</div>';
				}
			}
			//var_dump($_POST);
			//echo '<hr />';
			if(!isset($data['errors'])) {
				$data['paper_type'] = (int)$this->input->post('paper_type', true);
				$data['academic_level'] = (int)$this->input->post('academic_level', true);
				$data['deadline'] = $this->input->post('deadline', true);
				$data['pages'] = (int)$this->input->post('pages', true);
				if($data['pages'] == 0)
					$data['pages'] = 1;

				$data['paper_format'] = (int)$this->input->post('paper_format', true);
				$data['subject'] = $this->input->post('subject', true);
				$data['institution'] = $this->input->post('institution', true);
				$data['institution_type'] = (int)$this->input->post('institution_type', true);
				$data['job_title_or_industry'] = $this->input->post('job_title_or_industry', true);
				$data['reasons_for_applying'] = $this->input->post('reasons_for_applying', true);

				$data['sources'] = (int)$this->input->post('sources_needed', true);
				$data['work_type'] = $this->input->post('work_type', true);
				$data['additional_materials'] = $this->input->post('additional_materials', true);
				$data['spacing'] = $this->input->post('spacing', true);
				$data['topic'] = $this->input->post('topic', true);
				$data['slides'] = $this->input->post('slides', true);
				$data['paper_details'] = $this->input->post('paper_details', true);
				$data['plagiarism_report'] = (int)$this->input->post('plagiarism_report', true);
				$data['abstract_page'] = (int)$this->input->post('abstract_page', true);
				$data['priority_customer'] = (int)$this->input->post('top_priority', true);
				$data['preferred_writer'] = (int)$this->input->post('preferred_writer', true);
				if($data['preferred_writer'] == '3')
					$data['preferred_writer_id'] = (int)$this->input->post('preferred_writer_id', true);
				else
					$data['preferred_writer_id'] = 0;

				$tmp['prices'] = Assist::get_prices();
				$tmp['paper_types_comparison'] = Assist::get_paper_types_comparison();
				$tmp['deadlines_comparison'] = Assist::get_deadlines_comparison();
				$tmp['academic_levels_comparison'] = Assist::get_academic_levels_comparison();
				$tmp['paper_formats_comparison'] = Assist::get_paper_formats_comparison();
				$data['price'] = $tmp['prices'][$tmp['paper_types_comparison'][$data['paper_type']]][$tmp['deadlines_comparison'][$data['deadline']]][$tmp['academic_levels_comparison'][$data['academic_level']]];
				if($data['pages'] > 1)
					$data['price'] = ceil_float($data['price'] * $data['pages']);
				if($data['work_type'])
					$data['price'] = ceil_float($data['price']*0.6);
				if($data['abstract_page'])
					$data['price'] = ceil_float($data['price'] + 14.99);

				$data['date_add'] = date("Y-m-d H:i:s");
				$data['last_action'] = date("Y-m-d H:i:s");
				switch ($data['deadline']) {
					case '7':
						$shift = 14*24*60*60;
						break;
					case '6':
						$shift = 10*24*60*60;
						break;
					case '5':
						$shift = 6*24*60*60;
						break;
					case '4':
						$shift = 3*24*60*60;
						break;
					case '3':
						$shift = 2*24*60*60;
						break;
					case '2':
						$shift = 24*60*60;
						break;
					case '1':
						$shift = 8*60*60;
						break;
					default:
						break;
				}
				$data['date_end'] = date("Y-m-d H:i:s", time()+$shift);
				
				$order_details = array(
					'type' => 'order',
					'status' => 'Needs confirmation',
					'status_client' => 'Writer’s Review',
					'status_writer' => '',
					'customer_id' => $_SESSION['user_id'],
					'writer_id' => 0,
					'last_action' => $data['last_action'],
					'date_add' => $data['date_add'],
					'date_end' => $data['date_end'],
					'date_end_writer' => $data['date_end'],
					'deadline' => $data['deadline'],
					'work_type' => $data['work_type'],
					'paper_type' => $data['paper_type'],
					'subject' => $data['subject'],
					'topic' => $data['topic'],
					'paper_details' => $data['paper_details'],
					'paper_format' => $data['paper_format'],
					'sources' => $data['sources'],
					'academic_level' => $data['academic_level'],
					'spacing' => $data['spacing'],
					'pages' => $data['pages'],
					'slides' => $data['slides'],
					'institution_type' => $data['institution_type'],
					'institution' => $data['institution'],
					'job_title_or_industry' => $data['job_title_or_industry'],
					'reasons_for_applying' => $data['reasons_for_applying'],
					'additional_materials' => $data['additional_materials'],
					'plagiarism_report' => $data['plagiarism_report'],
					'abstract_page' => $data['abstract_page'],
					'priority_customer' => $data['priority_customer'],
					'preferred_writer' => $data['preferred_writer'],
					'preferred_writer_id' => $data['preferred_writer_id'],
					'price' => $data['price'],
					'writer_price' => round($data['price']/3,2)
				);
				$order_id = $this->order_model->add($order_details);

				//redirect('/user');
				$data['deadline_title'] = date("M j h:i A", strtotime($data['date_end']));
				$subject = $this->order_model->get_subject_by_id($data['subject']);
				$data['subject_title'] = $subject['title'];
				$academic_level = $this->order_model->get_academic_level_by_id($data['academic_level']);
				$data['academic_level_title'] = $academic_level['title'];
				$paper_type = $this->order_model->get_paper_type_by_id($data['paper_type']);
				$data['paper_type_title'] = $paper_type['title'];
				$user = $this->my_auth->get_by_id($_SESSION['user_id']);
				$data['user_id'] = $user['user_id'];
				$data['first_name'] = $user['first_name'];
				$data['last_name'] = $user['last_name'];
				$data['email'] = $user['email'];
				$data['order_id'] = $order_id;

				$text = Mail::text_body('client_place_order', $data);
				//$this->order_model->add_message(1, $data['user_id'], $data['order_id'], $text, 1);

				Mail::prepare('client_place_order', $data);
				$data['email'] = 'manager@eagle-essays.com';
				Mail::prepare('support_client_place_order', $data);

        if(isset($_FILES['file'])) {
            $path = $this->config->item('order_upload_path').'/'.$order_id;

            if(!is_dir($path)) {
                @mkdir($path);
                @chmod($path, 0755);
            }
            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = md5($_FILES['file']['name'].time());
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file'))
                $data['status'] = 'fail';
            else {
                $order = $this->order_model->get($order_id);
                $receiver_id = 0;
                $file = $this->upload->data();

                $data = array(
                    'order_id' => $order_id,
                    'sender_id' => $_SESSION['user_id'],
                    'receiver_id' => $receiver_id,
                    'date' => date("Y-m-d H:i:s"),
                    'client_name' => $file['client_name'],
                    'file_name' => $file['file_name'],
                    'file_path' => $file['file_path'],
                    'raw_name' => $file['raw_name'],
                    'file_ext' => $file['file_ext'],
                    'moderated' => 1
                );
                $file_id = $this->order_model->save_file($data);
            }
        }
				redirect('/cart/checkout/'.$order_id);
			}
			else {
			if(isset($_FILES['file']) && $_FILES['file']['size']) {
				$data['file_error'] = 1;
			}
				$data['academic_levels'] = $this->order_model->get_academic_levels();
				$data['paper_types'] = $this->order_model->get_paper_types();
				$data['deadlines'] = $this->order_model->get_deadlines();
				$data['subjects'] = $this->order_model->get_subjects();
				$data['paper_formats'] = $this->order_model->get_paper_formats();
				$data['institution_types'] = $this->order_model->get_institution_types();
				$data['countries'] = $this->assist_model->get_countries();
	        	foreach ($data['paper_types'] as $key => $value) {
	        		if($value['position'] == '3')
	        			$data['paper_types'][$key]['class'] = 'indent';
	        	}
	        	foreach ($data['countries'] as $key => $value) {
	        		if($value['position'] == '6')
	        			$data['countries'][$key]['class'] = 'indent';
	        	}
				$tpl['title'] = $this->config->config['tp_title']."Place your order";
				$tpl['description'] = $this->other_model->get_description();
				$tpl['description'] = $tpl['description']['value'];
				$tpl['keywords'] = $this->other_model->get_keywords();
				$tpl['keywords'] = $tpl['keywords']['value'];
				
				$tpl['content'] = $this->load->view('common/order.tpl', $data, TRUE);
				$this->load->view('template.tpl', $tpl);
			}
		}
		else {

			$data['errors'] = validation_errors();
			if(isset($_FILES['file']) && $_FILES['file']['size']) {
				$data['file_error'] = 1;
			}
			$data['academic_levels'] = $this->order_model->get_academic_levels();
			$data['paper_types'] = $this->order_model->get_paper_types();
			$data['deadlines'] = $this->order_model->get_deadlines();
			$data['subjects'] = $this->order_model->get_subjects();
			$data['paper_formats'] = $this->order_model->get_paper_formats();
			$data['institution_types'] = $this->order_model->get_institution_types();
			$data['countries'] = $this->assist_model->get_countries();
        	foreach ($data['paper_types'] as $key => $value) {
        		if($value['position'] == '3')
        			$data['paper_types'][$key]['class'] = 'indent';
        	}
        	foreach ($data['countries'] as $key => $value) {
        		if($value['position'] == '6')
        			$data['countries'][$key]['class'] = 'indent';
        	}
			$tpl['description'] = $this->other_model->get_description();
			$tpl['description'] = $tpl['description']['value'];
			$tpl['keywords'] = $this->other_model->get_keywords();
			$tpl['keywords'] = $tpl['keywords']['value'];
		
			$tpl['title'] = $this->config->config['tp_title']."Place your order";
			$tpl['content'] = $this->load->view('common/order.tpl', $data, TRUE);
			$this->load->view('template.tpl', $tpl);
		}
	}
}