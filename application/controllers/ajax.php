<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct() 
    {
        parent::__construct();
        $this->load->model('assist_model');
        $this->load->model('user_model');
        $this->load->model('order_model');
        $this->load->model('payment_model');
    }
    public function index() 
    {
        echo json_encode(array('status' => 'fail'));
    }
    public function get_orders($status)
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        $field_sort = $this->input->post('sidx', true);
        $type_sort = $this->input->post('sord', true);
        $page = $this->input->post('page', true);
        $limit = $this->input->post('rows', true);

        switch ($status) {
            case 'pending':
                $status = "'Needs confirmation', 'Confirmed'";
            break;
            case 'current':
                $status = "'In progress','File submitted','File delivered','Revision in progress'";
            break;
            case 'finished':
                $status = "'Completed'";
            break;
            case 'canceled':
                $status = "'Canceled'";
            break;
            case 'active':
                $status = "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'";
            break;
            case 'previous':
                $status = "'Completed', 'Canceled'";
            break;
            default:
            break;
        }
        $count = $this->order_model->count_c_user_orders($_SESSION['user_id'], $status);
        if($count)
            $total_pages = ceil($count/$limit);
        else
            $total_pages = 0;
        if($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit;
        if($start < 0)
            $start = 0;
        $data['page'] = $page;
        $data['total'] = $total_pages;
        $data['records'] = $count;

        $rows = $this->order_model->get_c_user_orders($_SESSION['user_id'], $status, $field_sort, $type_sort, $start, $limit);
        $data['count'] = count($rows);
        $data['rows'] = $rows;

        if($data['rows']) {
            $subjects = $this->order_model->get_subjects();
            foreach ($data['rows'] as $key => $value) {
                if(isset($subjects[$data['rows'][$key]['subject']])) {
                    foreach ($subjects as $subject) {
                        if($subject['subject_id'] == $data['rows'][$key]['subject']) {
                            $data['rows'][$key]['subject_title'] = $subject['title'];
                            continue;
                        }
                    }
                }
                else
                    $data['rows'][$key]['subject_title'] = 'No subject';
                $data['rows'][$key]['deadline_title'] = date("M j h:i A", strtotime($data['rows'][$key]['date_end']));
                $data['rows'][$key]['price_title'] = '$'.$data['rows'][$key]['price'];
                if($data['rows'][$key]['payment_id']) {
                    switch ($data['rows'][$key]['status_client']) {
                        case 'Writer’s Review':
                            $data['rows'][$key]['status_client'] = 'Work in progress';
                            break;
                        case 'In progress':
                            $data['rows'][$key]['status_client'] = 'Work in progress';
                            break;
                        case 'Delivered':
                            $data['rows'][$key]['status_client'] = 'Draft submitted';
                            break;
                        case 'Revision in progress':
                            $data['rows'][$key]['status_client'] = 'Revision';
                            break;
                        case 'Canceled':
                            $data['rows'][$key]['status_client'] = 'Canceled';
                            break;
                        case 'Approved':
                            $data['rows'][$key]['status_client'] = 'Completed';
                            break;
                        default:
                            $data['rows'][$key]['status_client'] = $data['rows'][$key]['status_client'];
                            break;
                    }
                }
                else {
                    if($data['rows'][$key]['status_client'] == 'Canceled')
                        $data['rows'][$key]['status_client'] = 'Canceled';
                    else
                        $data['rows'][$key]['status_client'] = 'Waiting for payment';
                }
            }
        }
        echo json_encode($data);
    }
    public function order_action()
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        $json = array('status' => 'fail');
        $order_id = $this->input->post('id', true);
        $type = $this->input->post('type', true);
        $order = $this->order_model->get($order_id);
        if($order && $order['customer_id'] == $_SESSION['user_id']) {
            switch ($type) {
                case 'revision':
                    $reason = $this->input->post('text', true);
                    $hours = $this->input->post('hours', true);
                    $json = array('status' => 'ok');
                    //$this->order_model->add_message($_SESSION['user_id'], $order['writer_id'], $order['order_id'], $reason);
                    $data = array(
                        'status' => 'Revision in progress',
                        'status_client' => 'Revision in progress',
                        'status_writer' => 'Revision in progress',
                        'revisions' => $order['revisions']+1,
                        'revision_details' => $reason,
                        'attention' => 1
                    );
                    if($hours) {
                        $currentDate = strtotime($order['date_end']);
                        $futureDate = $currentDate+(60*60*$hours);                      
                        $data['date_end'] = date("Y-m-d H:i:s", $futureDate);
                    }
                    //var_dump($_POST);
                    $this->order_model->update($order_id, $data);
                    $user = $this->my_auth->get_by_id($order['customer_id']);
                    $user['reason'] = $reason;
                    $user['hours'] = $hours;
                    $user['order_id'] = $order['order_id'];
        
                    $text = Mail::text_body('client_request_revision', $user);
                    $this->order_model->add_message(1, $order['customer_id'], $user['order_id'], $text, 1, 1);

                    Mail::prepare('client_request_revision', $user);

                    $user = $this->my_auth->get_by_id($order['writer_id']);
                    $user['reason'] = $reason;
                    $user['hours'] = $hours;
                    $user['order_id'] = $order['order_id'];

                    $text = Mail::text_body('writer_customer_request_revision', $user);
                    $this->order_model->add_message(1, $order['writer_id'], $user['order_id'], $text, 1, 1);
                    //$this->order_model->add_message($_SESSION['user_id'], 1, $user['order_id'], $text, 1, 1);

                    Mail::prepare('writer_customer_request_revision', $user);
                break;
                case 'approve':
                    $json = array('status' => 'ok');
                    $data = array(
                        'status' => 'Completed',
                        'status_client' => 'Approved',
                        'status_writer' => 'Approved',
                        'attention' => 1,
                        'date_approve' => date("Y-m-d H:i:s")
                    );
                    $this->order_model->update($order_id, $data);
                    $user = $this->my_auth->get_by_id($order['customer_id']);
                    $user['order_id'] = $order_id;
                    $text = Mail::text_body('client_approve_order', $user);
                    //$this->order_model->add_message(1, $order['customer_id'], $user['order_id'], $text, 1);                 
                    Mail::prepare('client_approve_order', $user);

                    $user = $this->my_auth->get_by_id($order['writer_id']);
                    $user['order_id'] = $order_id;
                    $user['amount'] = $order['writer_price'];
                    $text = Mail::text_body('writer_customer_approved', $user);
                    //$this->order_model->add_message(1, $order['writer_id'], $user['order_id'], $text, 1);
                    $this->order_model->add_message(1, 1, $user['order_id'], $text, 1);

                    Mail::prepare('writer_customer_approved', $user);                   
                break;
                case 'preview':
                break;
                case 'download':
                break;
                default:
                break;
            }
        }
        echo json_encode($json);
    }
    public function submit_inquiry()
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        $json = array('status' => 'fail');
        $order_id = $this->input->post('order_id', true);
        $user_id = $_SESSION['user_id'];
        $order = $this->order_model->get($order_id);
        if($order && $user_id) {
            if($order['moderated'] == '1' && $order['type'] == 'inquiry') {
                $this->order_model->publish_inquiry($order_id);
                $json = array('status' => 'ok');
            }
        }
        echo json_encode($json);
    }
/*
        $sender_id = $this->input->post('sender', true);
        $text = $this->input->post('text', true);
        if($sender_id == $_SESSION['user_id']) {
            $order_id = $this->input->post('order', true);
            $recipient = $this->input->post('recipient', true);
            if($recipient == 1) {
                $json['status'] = 'ok';
                $this->user_model->update_last_action($order_id);
                $this->order_model->add_message($_SESSION['user_id'], 1, $order_id, $text, 1, 1);
            }
            else {
                $order = $this->order_model->get($order_id);
                if($order && $order['writer_id'] > '0') {
                    $writer = $this->my_auth->get_by_id($order['writer_id']);
                    $user = $this->my_auth->get_by_id($recipient);
                    if($writer) {
                        $this->user_model->update_last_action($order_id);
                        $json['status'] = 'ok';
                        $user['order_id'] = $order_id;
                        $this->order_model->add_message($_SESSION['user_id'], $writer['user_id'], $order['order_id'], $text, 1, 1);
                        Mail::prepare('writer_client_commented', $user);
                    }
                }
                    $this->order_model->add_message($_SESSION['user_id'], 0, $order['order_id'], $text, 1, 1);
    //*/    
    public function add_file()
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        $data = array();
        if(isset($_FILES['file'])) {
            $order_id = $this->input->post('order_id', true);
            $sender = $this->input->post('sender', true);
            $path = $this->config->item('order_upload_path').'/'.$order_id;

            if(!is_dir($path)) {
                @mkdir($path);
                @chmod($path, 0755);
            }

            $config['upload_path'] = $path;
            $config['allowed_types'] = $this->config->item('allowed_types');
            //$config['allowed_types'] = '*';
            $config['file_name'] = md5($_FILES['file']['name'].time());
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file'))
                $data['status'] = 'fail';
            else {
                $order = $this->order_model->get($order_id);
                if($_SESSION['user_id'] == $order['customer_id'])
                    $receiver_id = $order['writer_id'];
                else
                    $receiver_id = $order['customer_id'];
                $file = $this->upload->data();

                $text = $this->input->post('text', true);
                $data = array(
                    'order_id' => $order_id,
                    'sender_id' => $_SESSION['user_id'],
                    'receiver_id' => $receiver_id,
                    'date' => date("Y-m-d H:i:s"),
                    'client_name' => $file['client_name'],
                    'file_name' => $file['file_name'],
                    'file_path' => $file['file_path'],
                    'raw_name' => $file['raw_name'],
                    'file_ext' => $file['file_ext'],
                    'moderated' => 1
                );
                if($text) {
                    $data['order_id'] = 0;
                }
                $this->user_model->update_last_action($order_id);
                $file_id = $this->order_model->save_file($data);

                //add message to file
                if($text) {
                    $this->order_model->add_message($_SESSION['user_id'], $receiver_id, $order_id, $text, 1, 1, $file_id);
                }

                $data = array('status' => 'ok');
            }
            echo json_encode($data);
        }
    }
    public function get_file($file_id)
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        if($file_id) {
            $file = $this->order_model->get_file($file_id);
            if($file && $file['second']) {
                $files[] = $file;
                $files[] = $this->order_model->get_file($file['second']);
                $zipname = $file['file_path'].'files.zip';
                @unlink($zipname);
                $zip = new ZipArchive;
                $zip->open($zipname, ZipArchive::CREATE);
                for($i = 0; $i < 2; $i++) {
                    $zip->addFile($files[$i]['file_path'].$files[$i]['file_name'], $files[$i]['client_name']);
                }
                $zip->close();
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename=files.zip');
                header('Content-Length: ' . filesize($zipname));
                readfile($zipname);
            }
            elseif($file) {
                switch ($file['file_ext']) {
                    case '.jpg':
                        header('Content-Type: image/jpeg');
                        break;
                    case '.pdf':
                        header('Content-Type: application/pdf');
                        break;
                    case '.xml':
                        header('Content-Type: text/xml');
                        break;
                    case '.doc':
                        header('Content-Type: text/msword');
                        break;
                    case '.docx':
                        header('Content-Type: text/msword');
                        break;
                    default:
                        header('Content-Type: text/plain');
                        break;
                }
                header("Content-Transfer-Encoding: binary");
                header("Content-type: application/force-download"); 
                header('Content-Disposition: attachment; filename="'.$file['client_name'].'"');
                echo file_get_contents($file['file_path'].$file['file_name']); 
            }
        }
    }
    public function get_order_file($order_id)
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        if($order_id) {
            $order = $this->order_model->get($order_id);
            if(!$order) {
                echo json_encode('');
                return;
            }
            $file = $this->order_model->get_file($order['file_id']);
            if($file && $file['second']) {
                $files[] = $file;
                $files[] = $this->order_model->get_file($file['second']);
                $zipname = $file['file_path'].'files.zip';
                @unlink($zipname);
                $zip = new ZipArchive;
                $zip->open($zipname, ZipArchive::CREATE);
                for($i = 0; $i < 2; $i++) {
                    $zip->addFile($files[$i]['file_path'].$files[$i]['file_name'], $files[$i]['client_name']);
                }
                $zip->close();
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename=files.zip');
                header('Content-Length: ' . filesize($zipname));
                readfile($zipname);
            }
            elseif($file) {
                switch ($file['file_ext']) {
                    case '.jpg':
                        header('Content-Type: image/jpeg');
                        break;
                    case '.pdf':
                        header('Content-Type: application/pdf');
                        break;
                    case '.xml':
                        header('Content-Type: text/xml');
                        break;
                    case '.doc':
                        header('Content-Type: text/msword');
                        break;
                    case '.docx':
                        header('Content-Type: text/msword');
                        break;
                    default:
                        header('Content-Type: text/plain');
                        break;
                }
                header("Content-Transfer-Encoding: binary");
                header("Content-type: application/force-download"); 
                header('Content-Disposition: attachment; filename="'.$file['client_name'].'"');
                echo file_get_contents($file['file_path'].$file['file_name']); 

            }
        }
    }
    public function get_preview2($file_id)
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        if($file_id) {
            $file = $this->order_model->get_file($file_id, 0);
            if($file) {
                switch ($file['file_ext']) {
                    case '.jpg':
                        header('Content-Type: image/jpeg');
                        break;
                    case '.pdf':
                        header('Content-Type: application/pdf');
                        break;
                    case '.xml':
                        header('Content-Type: text/xml');
                        break;
                    case '.doc':
                        header('Content-Type: text/msword');
                        break;
                    case '.docx':
                        header('Content-Type: text/msword');
                        break;
                    default:
                        header('Content-Type: text/plain');
                        break;
                }
                header("Content-Transfer-Encoding: binary");
                header("Content-type: application/force-download"); 
                header('Content-Disposition: attachment; filename="'.$file['client_name'].'"');
                echo file_get_contents($file['file_path'].$file['file_name']); 
            }           
        }
    }
    public function get_preview($file_id)
    {
        if($file_id) {
            $file = $this->order_model->get_file($file_id, 0);
            if($file) {
                if(file_exists($file['file_path'].$file['raw_name'].'_preview.png')) {
                    header('Content-Type: image/jpeg');
                    header("Content-Transfer-Encoding: binary");
                    header("Content-type: application/force-download"); 
                    header('Content-Disposition: attachment; filename="'.$file['client_name'].'_preview.png"');
                    echo file_get_contents($file['file_path'].$file['raw_name'].'_preview.png'); 
                }
                else {
                    $this->create_preview($file_id);
                    header('Content-Type: image/jpeg');
                    header("Content-Transfer-Encoding: binary");
                    header("Content-type: application/force-download"); 
                    header('Content-Disposition: attachment; filename="'.$file['client_name'].'_preview.png"');
                    echo file_get_contents($file['file_path'].$file['raw_name'].'_preview.png'); 
                }
            }           
        }
    }
    public function send_message()
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }           
        $json = array();
        $sender_id = $this->input->post('sender', true);
        $text = $this->input->post('text', true);
        if($sender_id == $_SESSION['user_id']) {

            $order_id = $this->input->post('order', true);
            $recipient = 1;//$this->input->post('recipient', true);
            if($recipient == 1) {
                $json['status'] = 'ok';
                $this->user_model->update_last_action($order_id);
                $this->user_model->update_order_message($order_id);
                $this->order_model->add_message($_SESSION['user_id'], 1, $order_id, $text, 1, 1);

            }
            else {
                $order = $this->order_model->get($order_id);
                if($order && $order['writer_id'] > '0') {
                    $writer = $this->my_auth->get_by_id($order['writer_id']);
                    $user = $this->my_auth->get_by_id($recipient);
                    if($writer) {
                        $this->user_model->update_last_action($order_id);
                        $this->user_model->update_order_message($order_id);
                        $json['status'] = 'ok';
                        $user['order_id'] = $order_id;
                        $this->order_model->add_message($_SESSION['user_id'], $writer['user_id'], $order['order_id'], $text, 1, 1);
                        Mail::prepare('writer_client_commented', $user);
                    }
                    else
                        $json['status'] = 'writer_fail';
                }
                elseif($order['status'] == 'Needs confirmation' || $order['status'] == 'Confirmed') {
                    $json['status'] = 'ok';
                    $this->order_model->add_message($_SESSION['user_id'], 0, $order['order_id'], $text, 1, 1);
                }
                else
                    $json['status'] = 'writer_fail';
            }
        }
        else
            $json['status'] = 'fail';
        echo json_encode($json);
    }
    public function set_read()
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        $json['status'] = 'fail';
        $message_id = $this->input->post('id', true);
        $message = $this->order_model->get_message($message_id);
        if($message) {
            $order = $this->order_model->get($message['order_id']);
            if($order) {
                $this->order_model->update_message($message_id, array('viewed'=>1));
                $all = $this->order_model->get_all_order_messages($order['order_id']);
                $read = 0;
                foreach($all as $val) {
                    if($val['receiver_id'] == $_SESSION['user_id'] && $val['viewed'] == '0')
                        $read++;
                }
                if($read == '0') {
                    $this->order_model->update($message['order_id'], array('attention' => 0, 'message' => 0));
                }
                //if($read) 
                    //$this->order_model->set_read_order_messages($order['order_id']);
                $json['status'] = 'ok';
                $json['result'] = $read;
            }
        }
        echo json_encode($json);
    }
    public function check_discount()
    {
        $json['status'] = 'fail';
        $code = $this->input->post('code', true);
        if(isset($_SESSION['user_id'])) {
            $user = $this->my_auth->get_by_id($_SESSION['user_id']);
        }
        else {
            $email = $this->input->post('email', true);
            $user = $this->my_auth->get_by_email($email);
        }
        if($user) {
            if($code == $user['user_id'].'01') {
                $json['status'] = 'ok';
                $json['percent'] = '20';
            }
            else {
                $dcode = substr($code, -2);
                $user_id = substr($code, 0, -2);
                $user = $this->my_auth->get_by_id($user_id);
                if($user && $dcode == '02') {
                    $json['status'] = 'ok';
                    $json['percent'] = '10';
                }
            }
        }
        else {
            $user_id = substr($code, 0, -2);
            $user = $this->my_auth->get_by_id($user_id);
            if($user && $code == $user['user_id'].'02') {
                $json['status'] = 'ok';
                $json['percent'] = '10';
            }
        }
        echo json_encode($json);
    }    
    public function send_rating()
    {
        $json['status'] = 'fail';
        $order_id = $this->input->post('order_id', true);
        $rating_writer = $this->input->post('rating_writer', true);
        $rating_support = $this->input->post('rating_support', true);
        $rating_text = $this->input->post('rating_text', true);
        $order = $this->order_model->get($order_id);
        if($order) {
            $json['status'] = 'ok';
            $update = array(
                'rating' => 1,
                'rating_writer' => $rating_writer,
                'rating_support' => $rating_support,
                'rating_text' => $rating_text
            );
            $this->order_model->update($order_id, $update);
        }
        echo json_encode($json);
    }    
    public function get_exchanges()
    {
        $currencies = $this->payment_model->get_all_currencies();
        foreach ($currencies as $key => $value) {
            $rate = $this->get_currency($value['code']);
            $this->payment_model->save_currency($value['code'],$rate);
        }
    }
    public function get_currency($to_Currency = 'EUR', $from_Currency = 'USD', $amount = 1) 
    {
        $amount = urlencode($amount);
        $from_Currency = urlencode($from_Currency);
        $to_Currency = urlencode($to_Currency);
        $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";
        $ch = curl_init();
        $timeout = 0;
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $rawdata = curl_exec($ch);
        curl_close($ch);
        $data = explode('bld>', $rawdata);
        $data = explode($to_Currency, $data[1]);
        $tax = round($data[0], 4);
        return $tax;
    }    
    public function mycontactsend() 
    {
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $user_id = $this->input->post('user_id', true);
        $subject = $this->input->post('subject', true);
        $case_number = $this->input->post('case_number', true);
        $message = $this->input->post('message', true);
        $captcha = $this->input->post('captcha', true);
        
        $json = array('status' => 'error');

        if($this->assist_model->check_captcha($captcha)) {
            $from = $email;
            $to = $this->config->item('client_manager_email');
            if($user_id)
                $body = 'Message from user# '.$user_id."\r\n";
            else
                $body = '';
            switch ($subject) {
                case '1':
                    $theme = 'Eagle-Essays.com - Application/Registration Procedure';
                    $body .= $message;
                break;
                case '2':
                    $theme = 'Eagle-Essays.com - Suggestion/Complaint';
                    $body .= $message;
                break;
                case '3':
                    $theme = 'Eagle-Essays.com - Technical problem';
                    $body .= $message;
                break;
                case '4':
                    $theme = 'Eagle-Essays.com - Question regarding payment';
                    $body .= $message;
                break;
                case '5':
                    $theme = 'Eagle-Essays.com - Issues with a case';
                    $body .= 'Case number# '.$case_number."\r\n";
                    $body .= $message;
                break;
                default:
                    $theme = 'Eagle-Essays.com';
                break;
            }
            Mail::Send($from, $to, $theme, $body);
            $json = array('status' => 'ok');
        }
        else
            $json = array('status' => 'captcha_error');
        echo json_encode($json);
    }
    public function create_preview($file_id)
    {
        $file = $this->order_model->get_file($file_id, 0);
        if($file){
            //$order = $this->order_model->get($file['order_id']);
            @unlink($file['file_path'].$file['file_name']."_preview.png");
            if($file['file_ext'] != '.pdf')
                @unlink($file['file_path'].$file['raw_name'].".pdf");
            exec("sudo /usr/local/bin/unoconv -f pdf ".$file['file_path'].$file['file_name']);
            sleep(4);
            if(!file_exists($file['file_path'].$file['raw_name'].".pdf"))
                sleep(4);
            $image = new imagick($file['file_path'].$file['raw_name'].".pdf");
            $pages = $image->getNumberImages();
            //if($pages > 10)
                //$pages = 10;
            for ($i=0; $i < $pages; $i++) { 
                $new_file = $file['file_path'].$file['raw_name'].'_preview'.$i.'.png';
                @unlink($new_file);
                $image = new imagick($file['file_path'].$file['raw_name'].".pdf[".$i."]");
                $image->writeImage($new_file);
            }
            unset($image);
            $this->split_images($file['client_name'], $file['file_path'], $file['raw_name'], $pages);
            $this->create_watermark($file['file_path'].$file['raw_name'].'_preview.png');
            @unlink($file['file_path'].$file['raw_name'].".pdf");
        }
    }
    public function split_images($name, $path, $raw_name, $pages)
    {
        $tmp_img = @imagecreatefrompng($path.$raw_name.'_preview0.png');
        $outfile_width = @imagesx($tmp_img);
        $outfile_height = 0;
        $outfile = $path.$raw_name.'_preview.png';
        @unlink($outfile);
        $sources = array();
        for ($i=0; $i < $pages ; $i++) { 
            $tmp_img = @imagecreatefrompng($path.$raw_name.'_preview'.$i.'.png');
            $outfile_height += @imagesy($tmp_img);
            $sources[] = $tmp_img;
            unset($tmp_img);
        }
        $image = @imagecreatetruecolor($outfile_width, $outfile_height);
        $x = 0;
        $y = 0;
        $index = 0;     
        for ($i=0; $i < $pages ; $i++) { 
            $width = @imagesx($sources[$index]);
            $height = @imagesy( $sources[$index]);
            @imagecopymerge($image, $sources[$index],$x, $y, 0, 0, $width, $height, 100);
            $y += $height;
            $index++;
        }
        @imagesavealpha($image,TRUE);
        @imagepng($image, $outfile);
        @imagedestroy($image);
        for ($i=0; $i < $pages ; $i++) { 
            @unlink($path.$raw_name.'_preview'.$i.'.png');
        }
        $image = null;
        $sources = null;
    }
    public function create_watermark($path)
    {
        $file = @file_get_contents($path);
        if(!$file)
            return;
        $image = @imagecreatefromstring($file);
        $w = @imagesx($image);
        $h = @imagesy($image);
        $watermark = @imagecreatefrompng('img/watermark.png');
        $ww = @imagesx($watermark);
        $wh = @imagesy($watermark);
        $img_paste_x = 0;
        while($img_paste_x < $w){
            $img_paste_y = 0;
            while($img_paste_y < $h){
                @imagecopy($image, $watermark, $img_paste_x, $img_paste_y, 0, 0, $ww, $wh);
                $img_paste_y += $wh;
            }
            $img_paste_x += $ww;
        }
        @imagepng($image, $path);
        @imagedestroy($image);
        @imagedestroy($watermark);      
    }
}