<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reg_statistics extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('support_model');
		$this->load->model('order_model');
		$this->load->model('assist_model');
	}
	public function index()
	{
		$data = array();
		$tpl['active_item'] = 'reg_stats';
		$tpl['title'] = $this->config->config['tp_title']."Registration statistics";
		$data['learns'] = array(
			'Google' => 0,
			'Facebook' => 0,
			'LinkedIn' => 0,
			'Job-search' => 0,
			'Recommendation' => 0,
			'Other' => 0
		);
		$countries = $this->assist_model->get_countries();
		$disciplines = $this->order_model->get_subjects();
		
		$users_learn = $this->support_model->get_users_learn();
		$users_countries = $this->support_model->get_users_countries();
		$users_disciplines = $this->support_model->get_users_disciplines();
		foreach ($users_learn as $user) {
			if($user['learned_from'] && isset($data['learns'][$user['learned_from']]))
				$data['learns'][$user['learned_from']]++;
		}
		foreach ($users_countries as $user) {
			foreach ($countries as $key => $country) {
				if(!isset($countries[$key]['count']))
					$countries[$key]['count'] = 0;
				if($country['country_id'] == $user['country'])
					$countries[$key]['count']++;
			}
		}
		foreach ($users_disciplines as $user) {
			$user_array = explode(",", $user['disciplines']);
			foreach ($disciplines as $key => $discipline) {
				if(!isset($disciplines[$key]['count']))
					$disciplines[$key]['count'] = 0;
				if(in_array($discipline['subject_id'], $user_array))
					$disciplines[$key]['count']++;
			}
		}		
		$data['countries'] = $countries;
		$data['disciplines'] = $disciplines;
		$tpl['content'] = $this->load->view('support/reg_stats.tpl', $data, TRUE);
		$this->load->view('support/template.tpl', $tpl);
	}
}