<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support()) {
			echo json_encode(array('status' => 'fail'));
			exit;
		}
		$this->load->model('support_model');
		$this->load->model('order_model');
		$this->load->model('assist_model');
		$this->load->model('user_model');
	}
	public function index()
	{
		/*
		$records = $this->db->get('orders')->result_array();
		foreach ($records as $record) {
			$this->db->where('order_id', $record['order_id']);
			$this->db->update('orders', array('date_end_writer' => $record['date_end']));
		}
		//*/
	}
	function ago( $datetime )
	{
	    $interval = date_create('now')->diff( $datetime );
	    $return = '';
	    if ( $v = $interval->y >= 1 ) 
	    	$return = $interval->y.'Y ';
	    if ( $v = $interval->m >= 1 ) 
	    	$return .= $interval->m.'M ';
	    if ( $v = $interval->d >= 1 ) 
	    	$return .= $interval->d.'D ';
    	$return .= $interval->h.':';
    	$return .= $interval->i;
	    return $return;
	}	
	public function get_mails()
	{
		$data = array();
		$filter = array();
		$filters = $this->input->post('filter', true);
		if($filters) {
			foreach ($filters as $key => $value) {
				if($value) {
					$filter[] = array(
						$key => $value
					);
				}
			}
		}
		$field_sort = $this->input->post('sidx', true);
		$type_sort = $this->input->post('sord', true);
		$page = $this->input->post('page', true);
		$limit = $this->input->post('rows', true);

		$count = $this->support_model->count_ajax_mails($filter);
		if($count)
			$total_pages = ceil($count/$limit);
		else
			$total_pages = 0;
		if($page > $total_pages)
			$page = $total_pages;
		$start = $limit * $page - $limit;
		if($start < 0)
			$start = 0;
		$data['page'] = $page;
		$data['total'] = $total_pages;
		$data['records'] = $count;
		$rows = $this->support_model->get_ajax_mails($field_sort, $type_sort, $start, $limit, $filter);
		//echo $this->db->last_query();
		$data['count'] = count($rows);
		$data['rows'] = $rows;
		
		foreach ($data['rows'] as $key => $value) {
			$site_title = '';
			$siteurl = 'eagle-essays.com';
			if($value['role'] == 'client') {
				$site_title = 'Eagle-essays';
				$siteurl = 'eagle-essays.com';
			}
			elseif($value['role'] == 'professor') {
				$site_title = 'Professor-essays';
				$siteurl = 'professor-essays.com';
			}
			elseif($value['role'] == 'essayhub') {
				$siteurl = 'buyessayhub.com';
				$site_title = 'BuyEssayHub';
			}
			elseif($value['role'] == 'writer') {
				$siteurl = 'writer-center.com';
				$site_title = 'Writer-center';
			}
			$data['rows'][$key]['site_title'] = $site_title;
			$data['rows'][$key]['user_title'] = $value['first_name'].' '.$value['last_name'];
			$data['rows'][$key]['email_title'] = $value['email'];
			$data['rows'][$key]['date_title'] = date("H:i:s d.m.Y", strtotime($value['date']));
			$data['rows'][$key]['title'] = '<a href="http://'.$siteurl.'/show_email/'.$value['hash'].'" target="_blank">'.$value['title'].'</a>';
		}
		echo json_encode($data);
	}
	//----------ORDERS----------------------
	public function get_orders($status)
	{
		$data = array();
		$filter = array();
		$filters = $this->input->post('filter', true);
		if($filters)
			foreach ($filters as $key => $value) {
				if($value) {
					$filter[] = array(
						$key => $value
					);
				}
			}
		$field_sort = $this->input->post('sidx', true);
		$type_sort = $this->input->post('sord', true);
		$page = $this->input->post('page', true);
		$limit = $this->input->post('rows', true);

		$count = $this->support_model->count_ajax_orders($status, $filter);
		if($count)
			$total_pages = ceil($count/$limit);
		else
			$total_pages = 0;
		if($page > $total_pages)
			$page = $total_pages;

		$start = $limit * $page - $limit;
		if($start < 0)
			$start = 0;
		$data['page'] = $page;
		$data['total'] = $total_pages;
		$data['records'] = $count;
		$rows = $this->support_model->get_ajax_orders($status, $field_sort, $type_sort, $start, $limit, $filter);
		//echo $this->db->last_query();
		$data['count'] = count($rows);
		$data['rows'] = $rows;

		$subjects = $this->order_model->get_subjects();
		foreach ($data['rows'] as $key => $value) {
			
			/*
			$message_img = 'message_no.png';
			$file_img = 'file_no.png';
			$user_img = 'user_no.png';
			if($data['rows'][$key]['last_action'] != '0000-00-00 00:00:00') {
				$messages = $this->support_model->check_unmoderate_messages($value['order_id']);
				if($messages)
					$message_img = 'message_yes.png';
				$files = $this->support_model->check_unmoderate_files($value['order_id']);
				if($files)
					$file_img = 'file_yes.png';

				$requests = $this->support_model->count_order_requests($value['order_id']);
				if($requests)
					$user_img = 'user_yes.png';

				$data['rows'][$key]['last_action_value'] = date("M j H:i", strtotime($data['rows'][$key]['last_action']));
				$data['rows'][$key]['last_action_value'] = '<span class="order_img_actions"><span class="message_img_action"><img height="16px" src="/img/icon/'.$message_img.'" /></span> <span class="file_img_action"><img height="16px" src="/img/icon/'.$file_img.'" /></span> <span class="user_img_action"><img height="16px" src="/img/icon/'.$user_img.'" /></span> </span>'.$data['rows'][$key]['last_action_value'];
			}
			else {
				$data['rows'][$key]['last_action_value'] = 'No actions';
			}
			if($data['rows'][$key]['moderated'] == '0') {
				$data['rows'][$key]['last_action_value'] = 'Awaiting moderation';
			}
			//*/
			
			$alert_img = 'alert_gray.png';
			if($value['attention'])
				$alert_img = 'alert_red.png';
			$message_img = 'message_gray.png';
			$messages = $this->support_model->check_unmoderate_messages($value['order_id']);
			if($messages)
				$message_img = 'message_red.png';

			$file_img = 'files_gray.png';
			//$files = $this->support_model->check_unmoderate_files($value['order_id']);
			if($value['file'])
				$file_img = 'files_red.png';

			$report_img = 'report_gray.png';
			if($value['status'] == 'Completed' && !$value['report_sent'])
				$report_img = 'report_red.png';




			$data['rows'][$key]['last_action_value'] = $value['status'];
			$data['rows'][$key]['last_action_value'] = '<span class="order_img_actions"><span class="file_img_action"><img height="16px" src="/img/icon/'.$alert_img.'" /></span> <span class="message_img_action"><img height="16px" src="/img/icon/'.$message_img.'" /></span> <span class="file_img_action"><img height="16px" src="/img/icon/'.$file_img.'" /></span> <span class="user_img_action"><img height="16px" src="/img/icon/'.$report_img.'" /></span>';
			
			$pay_img = 'pay_white.png';
			if($value['payment'])
				$pay_img = 'pay_blue.png';
			$data['rows'][$key]['last_action_value'] .= ' <span><img height="20px" src="/img/icon/'.$pay_img.'" /></span>';

			$user_img = 'user_gray.png';
			if($value['writer_id'])
				$user_img = 'user_blue.png';
			else {
				$requests = $this->support_model->get_requests_by_order($value['order_id']);
				$flag = 1;
				foreach ($requests as $request) {
					if($request['approved'] == '1') {
						$value['writer_id'] = $request['user_id'];
						$user_img = 'user_red.png';
						$flag = 0;
					}
				}
				if($flag) {
					$user_img = 'user_orange.png';
				}
			}
			$data['rows'][$key]['last_action_value'] .= ' <span class="user_img_action"><img height="16px" src="/img/icon/'.$user_img.'" />'.$value['writer_id'].'</span>';

			$data['rows'][$key]['last_action_value'] .= '</span>';
			//*/

			$class = '';
			$d1 = new DateTime("now");
			$d2 = new DateTime($data['rows'][$key]['date_end']);
			$prefix = '';
			if($d1 < $d2)
				$class = 'success';
			elseif($d1 > $d2) {
				$class = 'error';
				$prefix = '-';
			}
			$data['rows'][$key]['deadline_title'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'"><span class="'.$class.'">'.date("M j H:i", strtotime($data['rows'][$key]['date_end'])).'</span></a>';
			$interval = $this->ago($d2);
			$data['rows'][$key]['deadline_timeleft'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'"><span class="'.$class.'">'.$prefix.''.$interval.'</span></a>';

			$class = '';
			$d1 = new DateTime("now");
			$d2 = new DateTime($data['rows'][$key]['date_end_writer']);
			$prefix = '';
			if($d1 < $d2)
				$class = 'success';
			elseif($d1 > $d2) {
				$class = 'error';
				$prefix = '-';
			}
			$data['rows'][$key]['writer_deadline_title'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'"><span class="'.$class.'">'.date("M j H:i", strtotime($data['rows'][$key]['date_end_writer'])).'</span></a>';
			$interval = $this->ago($d2);
			$data['rows'][$key]['writer_deadline_timeleft'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'"><span class="'.$class.'">'.$prefix.''.$interval.'</span></a>';

			$data['rows'][$key]['price_title'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'"><span class="error">$'.$data['rows'][$key]['price'].'</span></a>';
			if($data['rows'][$key]['payment_id'])
				$data['rows'][$key]['price_title'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'"><span class="success">$'.$data['rows'][$key]['price'].'</span></a>';

			$data['rows'][$key]['delete_order'] = '<a onclick="delete_order('.$value['order_id'].')"><img src="/img/delete.png" height="16px" /></a>';
			
			if($data['rows'][$key]['note'])
				$data['rows'][$key]['note'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'">'.$data['rows'][$key]['note'].'</a>';
			$data['rows'][$key]['order_id'] = '<a href="/support/orders/order_details/'.$data['rows'][$key]['order_id'].'">'.$data['rows'][$key]['order_id'].'</a>';

			$data['rows'][$key]['topic'] = '<a href="/support/orders/order_details/'.$value['order_id'].'">'.$value['topic'].'</a>';
			/*
			if(isset($subjects[$data['rows'][$key]['subject']])) {
				foreach ($subjects as $subject) {
					if($subject['subject_id'] == $data['rows'][$key]['subject']) {
						$data['rows'][$key]['subject_title'] = $subject['title'];
						continue;
					}
				}
			}
			else
				$data['rows'][$key]['subject_title'] = 'No subject';
			//*/
		}
		echo json_encode($data);
	}
	public function get_inquiries($status)
	{
		$data = array();
		$filter = array();
		$filters = $this->input->post('filter', true);
		if($filters)
			foreach ($filters as $key => $value) {
				if($value) {
					$filter[] = array(
						$key => $value
					);
				}
			}
		$field_sort = $this->input->post('sidx', true);
		$type_sort = $this->input->post('sord', true);
		$page = $this->input->post('page', true);
		$limit = $this->input->post('rows', true);

		$count = $this->support_model->count_ajax_inquiries($filter);
		//echo $this->db->last_query();
		if($count)
			$total_pages = ceil($count/$limit);
		else
			$total_pages = 0;
		if($page > $total_pages)
			$page = $total_pages;

		$start = $limit * $page - $limit;
		if($start < 0)
			$start = 0;
		$data['page'] = $page;
		$data['total'] = $total_pages;
		$data['records'] = $count;
		$rows = $this->support_model->get_ajax_inquiries($field_sort, $type_sort, $start, $limit, $filter);
		$data['count'] = count($rows);
		$data['rows'] = $rows;

		$subjects = $this->order_model->get_subjects();
		foreach ($data['rows'] as $key => $value) {
			$data['rows'][$key]['deadline_title'] = date("M j H:i", strtotime($data['rows'][$key]['date_end']));
			$data['rows'][$key]['price_title'] = '$'.$data['rows'][$key]['price'];
			$data['rows'][$key]['delete_order'] = '<a onclick="delete_order('.$value['order_id'].')"><img src="/img/delete.png" /></a>';
			$data['rows'][$key]['subject_title'] = 'No subject';
			foreach ($subjects as $subject) {
				if($subject['subject_id'] == $data['rows'][$key]['subject']) {
					$data['rows'][$key]['subject_title'] = $subject['title'];
					continue;
				}
			}
		}
		echo json_encode($data);
	}
	public function assign_writer()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order_id', true);
		$user_id = $this->input->post('user_id', true);
		$order = $this->order_model->get($order_id);
		$user = $this->my_auth->get_by_id($user_id);
		$request = $this->support_model->get_request_order_and_user($order_id, $user_id);
		if($order && $user && $request) {
			$this->support_model->remove_assigned_writers($order_id);
			$this->support_model->assign_user_request($request['order_request_id']);
			$user = $this->my_auth->get_by_id($user_id);
			$user['order_id'] = $order_id;
			$user['timeleft'] = '';
			$user['amount'] = $request['price'];
			$date_end = strtotime($order['date_end_writer']);
			$now = time();
			$user['timeleft'] = round(($date_end - $now)/(60*60), 2);
			$text = Mail::text_body('writer_assigned_to_case', $user);
			$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
			Mail::prepare('writer_assigned_to_case', $user);

			$client = $this->my_auth->get_by_id($order['customer_id']);
			$client['order_id'] = $order_id;
			/*
			if($client['role'] == 'professor')
				Mail::prepare('professor_writer_assign', $client);
			elseif($client['role'] == 'client')
				Mail::prepare('client_writer_assign', $client);
			//*/
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function moderate_order()
	{
		$json['status'] = 'fail';
		$order_id = $this->input->post('order_id', true);
		$action = $this->input->post('action', true);
		$order = $this->order_model->get($order_id);
		if($order && $action) {
			if($action == 'approve') {
				$json['status'] = 'ok';
				$data = array(
					'status' => 'Confirmed',
					'status_client' => 'Writer’s Review',
					'status_writer' => 'Case published',
					'support_id' => $_SESSION['user_id'],
					'moderated'=> '1'
				);
				$this->order_model->update($order_id, $data);
			}
			elseif($action == 'reject') {
				$json['status'] = 'ok';
				$data = array(
					'status' => 'Canceled',
					'status_client' => 'Canceled',
					'status_writer' => 'Canceled',
					'support_id' => $_SESSION['user_id'],
					'moderated'=> '1'
				);				
				$this->order_model->update($order_id, $data);
			}
		}
		echo json_encode($json);
	}
	public function send_inquiry_email()
	{
		$json['status'] = 'fail';
		$order_id = $this->input->post('order_id', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$json['status'] = 'ok';
			$user = $this->my_auth->get_by_id($order['customer_id']);
			$user['order_id'] = $order_id;

			if($user['role'] == 'client')
				$text = Mail::text_body('client_found_writer', $user);
			elseif($user['role'] == 'professor')
				$text = Mail::text_body('professor_found_writer', $user);
			//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);

			if($user['role'] == 'client')
				Mail::prepare('client_found_writer', $user);
			elseif($user['role'] == 'professor')
				Mail::prepare('professor_found_writer', $user);
		}
		echo json_encode($json);
	}
	public function delete_order()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order_id', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$data = array(
				'status' => 'Canceled',
				'status_client' => 'Canceled',
				'status_writer' => 'Canceled'
				);			
			$this->order_model->update($order_id, $data);
			$user = $this->my_auth->get_by_id($order['customer_id']);
			$user['order_id'] = $order['order_id'];
			
			/*
			if($user['role'] == 'client')
				$text = Mail::text_body('client_cancel_order', $user);
			elseif($user['role'] == 'professor')
				$text = Mail::text_body('professor_cancel_order', $user);
			//*/
			
			//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
			
			if($user['role'] == 'client')
				Mail::prepare('client_cancel_order', $user);
			elseif($user['role'] == 'professor')
				Mail::prepare('professor_cancel_order', $user);
			if($order['writer_id']) {
				$user = $this->my_auth->get_by_id($order['writer_id']);
				$user['order_id'] = $order['order_id'];
				$text = Mail::text_body('writer_order_cancelled', $user);
				//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
				Mail::prepare('writer_order_cancelled', $user);
			}
			$json = array('status' => 'ok');
		}
		echo json_encode($json);		
		/*
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order_id', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$this->order_model->delete($order_id);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
		//*/
	}
	public function dismiss_writer()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			if($order['writer_id'] > 0) {
				$requests = $this->support_model->get_requests_by_order($order_id);
				foreach ($requests as $request) {
					if($request['user_id'] == $order['writer_id'])
						$this->support_model->update_user_request($request['order_request_id'], array('approved' => 0));
				}
				$data = array(
					'status' => 'Needs confirmation',
					'status_client' => 'Writer’s Review',
					'status_writer' => '',
					'writer_id' => 0,
					'support_id' => $_SESSION['user_id'],
				);
				$this->order_model->update($order_id, $data);

				$user = $this->my_auth->get_by_id($order['writer_id']);
				$user['order_id'] = $order['order_id'];
				$text = Mail::text_body('writer_dismiss_order', $user);
				$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
				
				Mail::prepare('writer_dismiss_order', $user);
			}
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function cancel_order()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$data = array(
				'status' => 'Canceled',
				'status_client' => 'Canceled',
				'status_writer' => 'Canceled'
				);			
			$this->order_model->update($order_id, $data);
			$user = $this->my_auth->get_by_id($order['customer_id']);
			$user['order_id'] = $order['order_id'];
			/*
			if($user['role'] == 'client')
				$text = Mail::text_body('client_cancel_order', $user);
			elseif($user['role'] == 'professor')
				$text = Mail::text_body('professor_cancel_order', $user);
			//*/
			//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
			/*
			if($user['role'] == 'client')
				Mail::prepare('client_cancel_order', $user);
			elseif($user['role'] == 'professor')
				Mail::prepare('professor_cancel_order', $user);
			//*/
			if($order['writer_id']) {
				$user = $this->my_auth->get_by_id($order['writer_id']);
				$user['order_id'] = $order['order_id'];
				$text = Mail::text_body('writer_order_cancelled', $user);
				//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
				Mail::prepare('writer_order_cancelled', $user);
			}
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function mark_finished()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$user = $this->my_auth->get_by_id($order['customer_id']);
			$data = array(
				'status' => 'Completed',
				'status_client' => 'Approved',
				'status_writer' => 'Approved'
				);
			$this->order_model->update($order_id, $data);
			$user = $this->my_auth->get_by_id($order['customer_id']);
			$user['order_id'] = $order['order_id'];
			//Mail::prepare('client_cancel_order', $user);			
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function save_order_changes()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order', true);
		$revisions = $this->input->post('revisions', true);
		$rating_points = $this->input->post('rating_points', true);
		$plagiarism_checks = $this->input->post('plagiarism_checks', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$data = array(
				'revisions' => $revisions,
				//'rating_points' => $rating_points,
				//'plagiarism_checks' => $plagiarism_checks
				);
			//if($order['revisions'] != $revisions || $order['rating_points'] != $rating_points || $order['plagiarism_checks'] != $plagiarism_checks)
				$this->order_model->update($order_id, $data);
			$json = array('status' => 'ok');	
		}
		echo json_encode($json);
	}
	public function save_order_details()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order', true);

		$pages = $this->input->post('pages', true);
		$slides = $this->input->post('slides', true);
		$questions = $this->input->post('questions', true);
		$problems = $this->input->post('problems', true);
		$academic_level = $this->input->post('academic_level', true);
		$deadline = $this->input->post('deadline', true);
		$work_type = $this->input->post('work_type', true);
		$paper_type = $this->input->post('paper_type', true);
		$subject = $this->input->post('subject', true);
		$paper_format = $this->input->post('paper_format', true);
		$sources = $this->input->post('sources', true);
		$topic = $this->input->post('topic', true);
		$paper_details = $this->input->post('paper_details', true);
		$revisions = $this->input->post('revisions', true);

		$order = $this->order_model->get($order_id);
		if($order) {
			$data = array(
				'pages' => $pages,
				'slides' => $slides,
				'questions' => $questions,
				'problems' => $problems,
				'academic_level' => $academic_level,
				'deadline' => $deadline,
				'work_type' => $work_type,
				'paper_type' => $paper_type,
				'subject' => $subject,
				'paper_format' => $paper_format,
				'sources' => $sources,
				'topic' => $topic,
				'paper_details' => $paper_details,
				'revisions' => $revisions
				);
			$this->order_model->update($order_id, $data);
			$json = array('status' => 'ok');	
		}
		echo json_encode($json);
	}
	public function change_order_status()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order', true);
		$order_status = $this->input->post('order_status', true);
		$order = $this->order_model->get($order_id);
		$status = $this->support_model->get_order_status_by_id($order_status);
		if($order && $status) {
			$data = array(
				'status' => $status['status'],
				'status_client' => $status['status_client'],
				'status_writer' => $status['status_writer']
				);
			if($status['order_status_id'] == '1') {
				$data['moderated'] = 0;
				$data['writer_id'] = 0;
			}
			elseif($status['order_status_id'] == '2') {
				$data['moderated'] = 1;
				$data['writer_id'] = 0;
			}
			elseif($status['order_status_id'] == '8') {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				$user['order_id'] = $order['order_id'];
				/*
				if($user['role'] == 'client')
					$text = Mail::text_body('client_cancel_order', $user);
				elseif($user['role'] == 'professor')
					$text = Mail::text_body('professor_cancel_order', $user);
				//*/
				//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
				/*
				if($user['role'] == 'client')
					Mail::prepare('client_cancel_order', $user);	
				elseif($user['role'] == 'professor')
					Mail::prepare('professor_cancel_order', $user);	
				//*/
				if($order['writer_id']) {
					$user = $this->my_auth->get_by_id($order['writer_id']);
					$user['order_id'] = $order['order_id'];
					$text = Mail::text_body('writer_order_cancelled', $user);
					//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
					Mail::prepare('writer_order_cancelled', $user);
				}
			}
			elseif($status['order_status_id'] == '10') {
				$data['writer_id'] = 0;
			}
			$this->order_model->update($order_id, $data);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function save_order_price()
	{
		$order_id = $this->input->post('order_id', true);
		$value = (float)$this->input->post('value', true);
		$order = $this->order_model->get($order_id);
		$json = array('status' => 'fail');
		if($order) {
			if($value && $order['price'] != $value) {
				/*
				if($value < $order['price'])
					$ratio = round(($order['price'] / $value), 2);
				else
					$ratio = round(($value / $order['price']), 2);
				//*/
				$update = array(
					'price' => $value
				);
				$this->order_model->update($order_id, $update);
				$json = array('status' => 'ok');
			}
		}
		echo json_encode($json);
	}
	public function save_order_writer_price()
	{
		$order_id = $this->input->post('order_id', true);
		$value = (float)$this->input->post('value', true);
		$order = $this->order_model->get($order_id);
		$json = array('status' => 'fail');
		if($order) {
			if($value && $order['writer_price'] != $value) {
				/*
				if($value < $order['price'])
					$ratio = round(($order['price'] / $value), 2);
				else
					$ratio = round(($value / $order['price']), 2);
				//*/
				$update = array(
					'writer_price' => $value
				);
				$this->order_model->update($order_id, $update);
				$json = array('status' => 'ok');
			}
		}
		echo json_encode($json);
	}
	public function change_order_deadline()
	{
		$order_id = $this->input->post('order_id', true);
		$value = $this->input->post('value', true);
		$type = $this->input->post('type', true);
		$order = $this->order_model->get($order_id);
		$json = array('status' => 'fail');
		if($order) {
			if($type == 'client' || $type == 'writer') {
				if($type == 'client')
					$update = array('date_end' => $value);
				elseif($type == 'writer')
					$update = array('date_end_writer' => $value);
				$this->order_model->update($order_id, $update);
				$json = array('status' => 'ok');
			}
		}
		echo json_encode($json);
	}
	public function reset_attention()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order_id', true);
		$value = $this->input->post('value', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$update = array('attention' => $value);
			$this->order_model->update($order_id, $update);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function reset_notify()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order_id', true);
		$value = $this->input->post('value', true);
		$type = $this->input->post('type', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$update = array($type => $value);
			$this->order_model->update($order_id, $update);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function sent_report()
	{
		$json = array('status' => 'fail');
		$order_id = $this->input->post('order_id', true);
		$value = $this->input->post('value', true);
		$order = $this->order_model->get($order_id);
		if($order) {
			$update = array('report_sent' => 1);
			$this->order_model->update($order_id, $update);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function save_note()
	{
		$order_id = $this->input->post('order_id', true);
		$note = $this->input->post('note', true);
		$order = $this->order_model->get($order_id);
		$json = array('status' => 'fail');
		if($order) {
			$update = array('note' => $note);
			$this->order_model->update($order_id, $update);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	//----------WRITERS---------------------
	public function get_writers($type='')
	{
		$data = array();
		$filter = array();
		$filters = $this->input->post('filter', true);
		if($filters)
			foreach ($filters as $key => $value) {
				if($value) {
					$filter[] = array(
						$key => $value
					);
				}
			}
		$field_sort = $this->input->post('sidx', true);
		$type_sort = $this->input->post('sord', true);
		$page = $this->input->post('page', true);
		$limit = $this->input->post('rows', true);

		$count = $this->support_model->count_ajax_writers($type, $filter);
		if($count)
			$total_pages = ceil($count/$limit);
		else
			$total_pages = 0;
		if($page > $total_pages)
			$page = $total_pages;

		$start = $limit * $page - $limit;
		if($start < 0)
			$start = 0;
		$data['page'] = $page;
		$data['total'] = $total_pages;
		$data['records'] = $count;
		$rows = $this->support_model->get_ajax_writers($type, $field_sort, $type_sort, $start, $limit, $filter);
		$data['count'] = count($rows);
		$data['rows'] = $rows;
		
		$countries = $this->assist_model->get_countries();
		$disciplines = $this->order_model->get_subjects();
		if($data['rows']) {
			foreach ($data['rows'] as $key => $value) {
				if(!isset($data['rows'][$key]['user_id'])) {
					$user = $this->my_auth->get_by_email($data['rows'][$key]['email']);
					$data['rows'][$key]['user_id'] = $user['user_id'];
					
					$count_essays = $this->user_model->count_essays();
					$essay = rand(1, $count_essays);
					$this->user_model->set_user_essay($user['user_id'], $essay);
				}
				$user_disciplines = explode(",", $value['disciplines']);
				$data['rows'][$key]['disciplines'] = '';
				foreach ($user_disciplines as $k2 => $v2) {
					foreach ($disciplines as $k1 => $v1) {
						if($v2 == $v1['subject_id']) {
							if($data['rows'][$key]['disciplines'])
								$data['rows'][$key]['disciplines'] .= ', '.$v1['title'];
							else
								$data['rows'][$key]['disciplines'] .= $v1['title'];
						}
					}
				}
				foreach ($countries as $country) {
					if($data['rows'][$key]['country'] == $country['country_id']) {
						$data['rows'][$key]['country_title'] = $country['title'];
					}
				}
				$data['rows'][$key]['delete_user'] = '<a onclick="delete_user('.$data['rows'][$key]['user_id'].')"><img src="/img/delete.png" /></a>';
				$data['rows'][$key]['date_add'] = date("M j h:i A", strtotime($data['rows'][$key]['date_add']));
				if($data['rows'][$key]['comment'])
					$data['rows'][$key]['comment'] = '<img src="/img/chbx_check.png" />';
				else
					$data['rows'][$key]['comment'] = '<img src="/img/chbx_nocheck.png" />';
				switch ($value['step']) {
					case 'one':
						if(isset($data['rows'][$key]['diplom']) && $data['rows'][$key]['diplom'] && $data['rows'][$key]['passport'] && $data['rows'][$key]['essay'])
							$data['rows'][$key]['step'] = '<span style="color:red">Step 1 Files Uploaded</span>';
						else
							$data['rows'][$key]['step'] = '<span style="color:gray">Waiting for Step 1 Files</span>';
					break;
					case 'two':
						$data['rows'][$key]['step'] = '<span style="color:gray">Testing </span>';
					break;
					case 'three':
						$data['rows'][$key]['step'] = '<span style="color:red">Test completed</span>';
					break;
					case 'finished':
						if($data['rows'][$key]['status'])
							$data['rows'][$key]['step'] = '<span style="color:green">Approved</span>';
						else
							$data['rows'][$key]['step'] = '<span style="color:red">Rejected</span>';
					break;
					case 'reject_files':
						$data['rows'][$key]['step'] = '<span style="color:red">Rejected</span>';
					break;
					case 'reject_test':
						$data['rows'][$key]['step'] = '<span style="color:red">Rejected</span>';
					break;
					default:
						$data['rows'][$key]['step'] = '<span style="color:gray">Default</span>';
					break;
				}
			}
		}
		echo json_encode($data);
	}
	public function delete_user()
	{
		$json = array('status' => 'fail');
		$user_id = $this->input->post('user_id', true);
		$user = $this->my_auth->get_by_id($user_id);
		if($user) {
			$this->support_model->go_user_to_step($user_id, 'finished', 0);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function send_diplom($user_id ='')
	{
		$json = array('status' => 'fail');
		if($user_id && isset($_FILES) && count($_FILES)) {
			$path = $this->config->item('user_files_upload_path').'/'.$user_id;
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['file_name'] = md5($_FILES['support_diplom']['name'].time());
			$this->load->library('upload', $config);

			if(!is_dir($path)) {
				@mkdir($path);
				@chmod($path, 0755);
			}
			if($this->upload->do_upload('support_diplom')) {
				$file = $this->upload->data();
				$insert = array(
					'user_id' => $user_id,
					'client_name' => $file['client_name'],
					'file_name' => $file['file_name'],
					'file_path' => $file['file_path'],
					'raw_name' => $file['raw_name'],
					'file_ext' => $file['file_ext'],
					'date' => date("Y-m-d H:i:s")
				);	
				$file_id = $this->user_model->add_user_file($insert);
				$this->user_model->update_user_diplom_file($user_id, $file_id);	
				$json = array('status' => 'ok');	
			}
		}
		echo json_encode($json);
	}
	public function send_passport($user_id ='')
	{
		$json = array('status' => 'fail');
		if($user_id && isset($_FILES) && count($_FILES)) {
			$path = $this->config->item('user_files_upload_path').'/'.$user_id;
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['file_name'] = md5($_FILES['support_passport']['name'].time());
			$this->load->library('upload', $config);

			if(!is_dir($path)) {
				@mkdir($path);
				@chmod($path, 0755);
			}
			if($this->upload->do_upload('support_passport')) {
				$file = $this->upload->data();
				$insert = array(
					'user_id' => $user_id,
					'client_name' => $file['client_name'],
					'file_name' => $file['file_name'],
					'file_path' => $file['file_path'],
					'raw_name' => $file['raw_name'],
					'file_ext' => $file['file_ext'],
					'date' => date("Y-m-d H:i:s")
				);	
				$file_id = $this->user_model->add_user_file($insert);
				$this->user_model->update_user_passport_file($user_id, $file_id);	
				$json = array('status' => 'ok');	
			}
		}
		echo json_encode($json);
	}
	public function send_essay($user_id ='')
	{
		$json = array('status' => 'fail');
		if($user_id && isset($_FILES) && count($_FILES)) {
			$path = $this->config->item('user_files_upload_path').'/'.$user_id;
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['file_name'] = md5($_FILES['support_essay']['name'].time());
			$this->load->library('upload', $config);

			if(!is_dir($path)) {
				mkdir($path);
				@chmod($path, 0755);
			}
			if($this->upload->do_upload('support_essay')) {
				$file = $this->upload->data();
				$insert = array(
					'user_id' => $user_id,
					'client_name' => $file['client_name'],
					'file_name' => $file['file_name'],
					'file_path' => $file['file_path'],
					'raw_name' => $file['raw_name'],
					'file_ext' => $file['file_ext'],
					'date' => date("Y-m-d H:i:s")
				);	
				$file_id = $this->user_model->add_user_file($insert);
				$this->user_model->update_user_essay_file($user_id, $file_id);	
				$json = array('status' => 'ok');	
			}
		}
		echo json_encode($json);
	}
	public function export_xls($type = '')
	{
		if(!$type)
			redirect('/support');
		$xls = '';
		$rows =  $this->support_model->export_writers($type);
		if(count($rows)) {
			$xls .= "Number\t";
			$xls .= "Date\t";
			$xls .= "Writer id\t";
			$xls .= "Status\t";
			$xls .= "First name\t";
			$xls .= "Last name\t";
			$xls .= "Email\t";
			$xls .= "Country\t";
			$xls .= "Disciplines\r\n";

			$countries = $this->assist_model->get_countries();
			$disciplines = $this->order_model->get_subjects();		
			$i = 1;
			foreach ($rows as $row) {
				$date_add = date("M j h:i A", strtotime($row['date_add']));
				$user_disciplines = explode(",", $row['disciplines']);
				$row['disciplines'] = '';
				foreach ($user_disciplines as $k2 => $v2) {
					foreach ($disciplines as $k1 => $v1) {
						if($v2 == $v1['subject_id']) {
							if($row['disciplines'])
								$row['disciplines'] .= ', '.$v1['title'];
							else
								$row['disciplines'] .= $v1['title'];
						}
					}
				}
				foreach ($countries as $country) {
					if($row['country'] == $country['country_id']) {
						$country_title = $country['title'];
					}
				}
				switch ($row['step']) {
					case 'one':
						if(isset($row['diplom']) && $row['diplom'] && $row['passport'] && $row['essay'])
							$row['step'] = 'Step 1 Files Uploaded';
						else
							$row['step'] = 'Waiting for Step 1 Files';
					break;
					case 'two':
						$row['step'] = 'Testing';
					break;
					case 'three':
						$row['step'] = 'Test completed';
					break;
					case 'finished':
						if($row['status'])
							$row['step'] = 'Approved';
						else
							$row['step'] = 'Rejected';
					break;
					default:
						$row['step'] = 'Default';
					break;
				}
				$xls .= $i."\t";
				$xls .= $date_add."\t";
				$xls .= $row['user_id']."\t";
				$xls .= $row['step']."\t";
				$xls .= $row['first_name']."\t";
				$xls .= $row['last_name']."\t";
				$xls .= $row['email']."\t";
				$xls .= $country_title."\t";
				$xls .= $row['disciplines']."\r\n";
				$i++;
			}
		}
		switch ($type) {
			case 'all':
				$name = 'All_';
			break;
			case 'new':
				$name = 'New_';
			break;
			case 'files':
				$name = 'Waiting_for_files_';
			break;
			case 'files_complete':
				$name = 'Files_Uploaded_';
			break;
			case 'test':
				$name = 'Testing_';
			break;
			case 'test_complete':
				$name = 'Test_completed_';
			break;
			case 'approved':
				$name = 'Approved_';
			break;
			case 'reject_files':
				$name = 'Rejected_';
			break;
			case 'reject_test':
				$name = 'Rejected_';
			break;			
			default:
				$name = '';
			break;
		}
		$name .= 'Writers';
		$filename = $name.'_'.date("Y-m-d H:i:s");
		header('Content-Disposition: attachment; filename="'.$filename.'.xls"');
		header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
		echo iconv("utf-8", "utf-8//TRANSLIT", $xls);		
	}
	public function reset_writer()
	{
		$user_id = $this->input->post('user', true);
		$type = $this->input->post('type', true);
		$json = array('status' => 'fail');
		$user = $this->my_auth->get_by_id($user_id);
		if($user && $type) {
			switch ($type) {
				case 'files':
					$this->support_model->go_user_to_step($user_id, 'one');
					$this->support_model->delete_user_files($user_id);
					$this->support_model->delete_user_test($user_id);
					$this->my_auth->update_user(array('status' => '0'), $user_id);
					$json = array('status' => 'ok');
				break;
				case 'test':
					$this->support_model->go_user_to_step($user_id, 'two');
					$this->support_model->delete_user_test($user_id);
					$this->my_auth->update_user(array('status' => '0'), $user_id);
					$json = array('status' => 'ok');
				break;
				case 'verification':
					$data = array(
						'status' => '1',
						'verify_code' => '1'
					);
					$this->my_auth->update_user($data, $user_id);
					$json = array('status' => 'ok');
				break;
				default:
				break;
			}
		}
		echo json_encode($json);
	}
	public function save_user_info()
	{
		$json = array('status' => 'fail');
		$user_id = $this->input->post('user_id', true);
		$field = $this->input->post('field', true);
		$value = $this->input->post('new_value', true);
		if($user_id && $field && $value != '') {
			switch ($field) {
				case 'first_name':
					$data = array('first_name' => $value);
					$this->my_auth->update_user($data, $user_id);
					$json = array('status' => 'ok');
				break;
				case 'last_name':
					$data = array('last_name' => $value);
					$this->my_auth->update_user($data, $user_id);
					$json = array('status' => 'ok');
				break;	
				case 'email':
					if(!$this->my_auth->email_check($value)) {
						$data = array();

						$user = $this->my_auth->get_by_id($user_id);
						if($user['verify_code'] == '')
							$data['verify_code'] = 1;
						
						$data['email'] = $value;
						$this->my_auth->update_user($data, $user_id);
						$json = array('status' => 'ok');
					}
					else
						$json = array('status' => 'exist');
				break;
				case 'city':
					$data = array('city' => $value);
					$this->my_auth->update_usermeta($data, $user_id);
					$json = array('status' => 'ok');
				break;
				case 'phone':
					$data = array('phone' => $value);
					$this->my_auth->update_user($data, $user_id);
					$json = array('status' => 'ok');
				break;
				case 'language':
					$data = array('language' => $value);
					$this->my_auth->update_usermeta($data, $user_id);
					$json = array('status' => 'ok');
				break;
				case 'about':
					$data = array('about' => $value);
					$this->my_auth->update_usermeta($data, $user_id);
					$json = array('status' => 'ok');
				break;
				case 'comment':
					$data = array('comment' => $value);
					$this->my_auth->update_usermeta($data, $user_id);
					$json = array('status' => 'ok');
				break;
				case 'status':
					$data = array('status' => $value);
					$this->my_auth->update_user($data, $user_id);
					$json = array('status' => 'ok');
				break;
				default:
				break;
			}
		}
		echo json_encode($json);
	}
	public function save_writer_comment()
	{
		$json = array('status' => 'fail');
		$user_id = $this->input->post('user', true);
		$comment = $this->input->post('comment', true);
		$user = $this->my_auth->get_by_id($user_id);
		if($user) {
			$update = array(
				'comment' => $comment
				);
			$this->my_auth->update_usermeta($update, $user_id);
			$json = array('status' => 'ok');
		}
		echo json_encode($json);
	}
	public function step1()
	{
		$json = array('status' => 'fail');
		$user_id = $this->input->post('user', true);
		$type = $this->input->post('type', true);
		$action = $this->input->post('action', true);
		$user = $this->my_auth->get_by_id($user_id);
		if($user && $action && $type) {
			$json = array('status' => 'reload');
			switch ($type) {
				case 'diplom':
					if($action == 'accept') {
						$this->support_model->update_user_step1($user_id, array('diplom_confirm' => 1));
					}
					else
						$this->support_model->update_user_step1($user_id, array('diplom_confirm' => 2));
				break;
				case 'passport':
					if($action == 'accept') {
						$this->support_model->update_user_step1($user_id, array('passport_confirm' => 1));
					}
					else
						$this->support_model->update_user_step1($user_id, array('passport_confirm' => 2));
				break;
				case 'essay':
					if($action == 'accept') {
						$this->support_model->update_user_step1($user_id, array('essay_confirm' => 1));
					}
					else
						$this->support_model->update_user_step1($user_id, array('essay_confirm' => 2));
				break;
				case 'all':
					if($action == 'accept') {
						$this->support_model->update_user_step1($user_id, array('diplom_confirm' => 1, 'passport_confirm' => 1, 'essay_confirm' => 1));
						$this->support_model->go_user_to_step($user_id, 'two');
						Mail::prepare('writer_files_confirm_ok', $user);
						$json = array('status' => 'reload');
					}
					else {
						$this->support_model->update_user_step1($user_id, array('diplom_confirm' => 2, 'passport_confirm' => 2, 'essay_confirm' => 2));
						$this->support_model->go_user_to_step($user_id, 'reject_files');
						Mail::prepare('writer_files_confirm_fail', $user);
						$json = array('status' => 'reload');
					}
				break;
				default:
				break;
			}
		}
		echo json_encode($json);
	}
	public function step2()
	{
		$json = array('status' => 'fail');
		$user_id = $this->input->post('user', true);
		$action = $this->input->post('action', true);
		$user = $this->my_auth->get_by_id($user_id);
		if($user && $action) {		
			$this->support_model->go_user_to_step($user_id, 'three');
			$json = array('status' => 'reload');
		}
		echo json_encode($json);
	}
	public function step3()
	{
		$json = array('status' => 'fail');
		$action = $this->input->post('action', true);
		$testing_id = $this->input->post('id', true);
		$user_testing = $this->user_model->get_users_testing_by_id($testing_id);
		if($user_testing) {
			$user = $this->my_auth->get_by_id($user_testing['user_id']);
			if($action == 'accept') {
				$this->support_model->moderate_testing($testing_id, 1);
				$this->support_model->go_user_to_step($user_testing['user_id'], 'finished', 1);
				Mail::prepare('writer_test_complete_ok', $user);
			}
			else {
				$this->support_model->moderate_testing($testing_id, 2);
				$this->support_model->go_user_to_step($user_testing['user_id'], 'reject_test');
				Mail::prepare('writer_test_complete_fail', $user);
			}

			$json = array('status' => 'reload');	
		}
		echo json_encode($json);
	}
	public function writer_actions()
	{
		$json = array('status' => 'fail');
		$action = $this->input->post('action', true);
		$user_id = $this->input->post('id', true);
		$user = $this->my_auth->get_by_id($user_id);
		if($user) {
			if($action == 'accept') {
				$this->support_model->go_user_to_step($user_id, 'finished', 1);
				Mail::prepare('writer_test_complete_ok', $user);
			}
			else {
				$this->support_model->go_user_to_step($user_id, 'finished', 0);
				Mail::prepare('writer_test_complete_fail', $user);
			}
			$json = array('status' => 'reload');	
		}
		echo json_encode($json);
	}
	//----------FILES-----------------------
	public function get_file($file_id)
	{
		if($file_id) {
			$file = $this->support_model->get_file_by_id($file_id);
			if($file && $file['second']) {
				$files[] = $file;
				$files[] = $this->order_model->get_file($file['second']);
				$zipname = $file['file_path'].'files.zip';
				@unlink($zipname);
				$zip = new ZipArchive;
				$zip->open($zipname, ZipArchive::CREATE);
				for($i = 0; $i < 2; $i++) {
					$zip->addFile($files[$i]['file_path'].$files[$i]['file_name'], $files[$i]['client_name']);
				}
				$zip->close();
				header('Content-Type: application/zip');
				header('Content-disposition: attachment; filename=files.zip');
				header('Content-Length: ' . filesize($zipname));
				readfile($zipname);
			}
			elseif($file) {
				switch ($file['file_ext']) {
					case '.jpg':
						header('Content-Type: image/jpeg');
						break;
					case '.pdf':
						header('Content-Type: application/pdf');
						break;
					case '.xml':
						header('Content-Type: text/xml');
						break;
					case '.doc':
						header('Content-Type: text/msword');
						break;
					case '.docx':
						header('Content-Type: text/msword');
						break;
					default:
						header('Content-Type: text/plain');
						break;
				}
				header("Content-Transfer-Encoding: binary");
				header("Content-type: application/force-download"); 
				header('Content-Disposition: attachment; filename="'.$file['client_name'].'"');
				echo file_get_contents($file['file_path'].$file['file_name']); 
			}
		}
	}
	
	public function get_user_file($id = '')
	{
		if($id) {
			$file = $this->user_model->get_user_file($id);
			if($file) {
				switch ($file['file_ext']) {
					case '.jpg':
						header('Content-Type: image/jpeg');
						break;
					case '.pdf':
						header('Content-Type: application/pdf');
						break;
					case '.xml':
						header('Content-Type: text/xml');
						break;
					case '.doc':
						header('Content-Type: text/msword');
						break;
					case '.docx':
						header('Content-Type: text/msword');
						break;
					default:
						header('Content-Type: text/plain');
						break;
				}
				header("Content-Transfer-Encoding: binary");
				header("Content-type: application/force-download"); 
				header('Content-Disposition: attachment; filename="'.$file['client_name'].'"');
				echo file_get_contents($file['file_path'].$file['file_name']); 
			}
		}
	}
    public function submit_file()
    {
        if(!Check::login()) {
            echo json_encode(array('status' => 'fail'));
            exit;
        }
        $data = array();
        if(isset($_FILES['file'])) {
            $order_id = $this->input->post('order', true);
            $sender = $this->input->post('sender', true);
            $receiver_id = $this->input->post('recipient', true);
            $path = $this->config->item('order_upload_path').'/'.$order_id;

            if(!is_dir($path)) {
                @mkdir($path);
                @chmod($path, 0755);
            }

            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = md5($_FILES['file']['name'].time());
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file'))
                $data['status'] = 'fail';
            else {
                $order = $this->order_model->get($order_id);

                $file = $this->upload->data();

                $text = $this->input->post('text', true);
                $data = array(
                    'order_id' => $order_id,
                    'sender_id' => $_SESSION['user_id'],
                    'receiver_id' => $receiver_id,
                    'date' => date("Y-m-d H:i:s"),
                    'client_name' => $file['client_name'],
                    'file_name' => $file['file_name'],
                    'file_path' => $file['file_path'],
                    'raw_name' => $file['raw_name'],
                    'file_ext' => $file['file_ext'],
                    'moderated' => 1
                );
                if($text != '<br>') {
                    $data['order_id'] = 0;
                }
                $this->user_model->update_last_action($order_id);
                $file_id = $this->order_model->save_file($data);

                //add message to file
                if($text != '<br>') {
                    $this->order_model->add_message($_SESSION['user_id'], $receiver_id, $order_id, $text, 1, 1, $file_id);
                }
                //echo $this->db->last_query();
            	//var_dump($data);
            	//var_dump($text);

                $data = array('status' => 'ok');
            }
            echo json_encode($data);
        }
    }	
	public function add_file()
	{
		$json = array('status' => 'fail');
		if(isset($_FILES['file'])) {
			$order_id = $this->input->post('id', true);
			$sender = $this->input->post('sender', true);
			$path = $this->config->item('order_upload_path').'/'.$order_id;

			@mkdir($path);
			@chmod($path, 0755);

			$config['upload_path'] = $path;
			//$config['allowed_types'] = 'gif|jpg|png|jpeg|xml|xls|xlsx|doc|docx|csv|odt|ods';
			$config['allowed_types'] = '*';
			$config['file_name'] = md5($_FILES['file']['name'].time());
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('file'))
				$json['status'] = 'fail';
			else {
				$order = $this->order_model->get($order_id);
				$file = $this->upload->data();

				$data = array(
					'order_id' => $order_id,
					'sender_id' => 0,
					'receiver_id' => $order['customer_id'],
					'date' => date("Y-m-d H:i:s"),
					'client_name' => $file['client_name'],
					'file_name' => $file['file_name'],
					'file_path' => $file['file_path'],
					'raw_name' => $file['raw_name'],
					'file_ext' => $file['file_ext'],
					'moderated' => '1',
					'support_id' => $_SESSION['user_id']
				);
				$this->order_model->save_file($data);
				$json = array('status' => 'ok');
			}
			
		}
		echo json_encode($json);
	}
	public function add_second_file()
	{
		$json = array('status' => 'fail');
		$file_id = $this->input->post('file_id', true);
		$file = $this->order_model->get_file($file_id, 0);
		if($file && isset($_FILES['plagiasm'.$file_id])) {
			$order_id = $file['order_id'];
			$receiver_id = $file['receiver_id'];
			$path = $this->config->item('order_upload_path').'/'.$order_id;
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['file_name'] = md5($_FILES['plagiasm'.$file_id]['name'].time());
			$this->load->library('upload', $config);
			if($this->upload->do_upload('plagiasm'.$file_id)) {
				$file = $this->upload->data();
				$data = array(
					'moderated' => 1,
					'support_id' => $_SESSION['user_id'],
					'order_id' => 0,
					'sender_id' => 1,
					'receiver_id' => $receiver_id,
					'date' => date("Y-m-d H:i:s"),
					'client_name' => $file['client_name'],
					'file_name' => $file['file_name'],
					'file_path' => $file['file_path'],
					'url_path' => $this->config->item('base_url').'order_files/'.$order_id,
					'raw_name' => $file['raw_name'],
					'file_ext' => $file['file_ext'],
					'type' => 0
				);
				$new_file_id = $this->order_model->save_file($data);
				$data = array(
					'second' => $new_file_id
				);
				$this->order_model->update_file($file_id, $data);
				$json = array('status' => 'ok');
			}
			else {
				//echo $this->upload->display_errors();
			}
		}
		echo json_encode($json);
	}
	public function moderate_file()
	{
		$json = array('status' => 'fail');
		$file_id = $this->input->post('file_id', true);
		$action = $this->input->post('action', true);
		$file = $this->support_model->get_file_by_id($file_id);
		$order = $this->order_model->get($file['order_id']);
		if($file && $order) {
			switch ($action) {
				case 'approve':
					$this->order_model->update($file['order_id'], array(
						'status' => 'File delivered',
						'status_client' => 'Delivered',
						'status_writer' => 'Delivered',
						'file_id' => $file_id
					));
					$data = array(
						'moderated' => 1
					);
					$this->order_model->update_file($file_id, $data);
					$user = $this->my_auth->get_by_id($order['customer_id']);
					$user['order_id'] = $order['order_id'];
			if($user['role'] == 'professor')
				$text = Mail::text_body('professor_writer_finished', $user);
			elseif($user['role'] == 'client')
				$text = Mail::text_body('client_writer_finished', $user);
			//$this->order_model->add_message(1, $user['user_id'], $user['order_id'], $text, 1, 1);
			if($user['role'] == 'professor')
				Mail::prepare('professor_writer_finished', $user);
			elseif($user['role'] == 'client')
				Mail::prepare('client_writer_finished', $user);
					$json = array('status' => 'ok');
				break;
				case 'reject':
					$data = array(
						'moderated' => 2
					);
					$this->order_model->update_file($file_id, $data);
					$this->order_model->update($order['order_id'], array('status_writer' => 'In progress'));
					$user = $this->my_auth->get_by_id($order['writer_id']);
					$user['order_id'] = $order['order_id'];
					$user['file_id'] = $file_id;
					Mail::prepare('writer_support_reject_file', $user);
					$json = array('status' => 'ok');
				break;
				default:
				break;
			}
		}
		echo json_encode($json);
	}
	public function get_preview($file_id)
	{
		if($file_id) {
			$file = $this->order_model->get_file($file_id, 0);
			if($file) {
				if(file_exists($file['file_path'].$file['raw_name'].'_preview.png')) {
					header('Content-Type: image/jpeg');
					header("Content-Transfer-Encoding: binary");
					header("Content-type: application/force-download"); 
					header('Content-Disposition: attachment; filename="'.$file['client_name'].'_preview.png"');
					echo file_get_contents($file['file_path'].$file['raw_name'].'_preview.png'); 
				}
				else {
					$this->create_preview($file_id);
					header('Content-Type: image/jpeg');
					header("Content-Transfer-Encoding: binary");
					header("Content-type: application/force-download"); 
					header('Content-Disposition: attachment; filename="'.$file['client_name'].'_preview.png"');
					echo file_get_contents($file['file_path'].$file['raw_name'].'_preview.png'); 
				}
			}			
		}
	}
	//----------MESSAGES--------------------
	public function send_message()
	{
		$json = array();
		$json['status'] = 'fail';
		$sender_id = $this->input->post('sender', true);

		if($sender_id == $_SESSION['user_id']) {

			$order_id = $this->input->post('order', true);
			$recipient_id = $this->input->post('recipient', true);
			$order = $this->order_model->get($order_id);
			$text = $this->input->post('text', true);
			if($order) {
				$recipient = $this->my_auth->get_by_id($recipient_id);
				if($recipient) {
					$this->order_model->add_message(1, $recipient['user_id'], $order['order_id'], $text, 1, $_SESSION['user_id']);
					$recipient['order_id'] = $order_id;
					if($order['customer_id'] == $recipient['user_id']) {
						if($recipient['role'] == 'professor')
							Mail::prepare('professor_support_commented', $recipient);
						elseif($recipient['role'] == 'client')
							Mail::prepare('client_support_commented', $recipient);

					}
					elseif($order['writer_id'] == $recipient['user_id']) {
						Mail::prepare('writer_support_commented', $recipient);
					}
					$json['status'] = 'ok';
				}
				else {
					$json['status'] = 'ok';
					$this->order_model->add_message(1, $recipient, $order['order_id'], $text, 1, $_SESSION['user_id']);
				}
			}
			else
				$json['status'] = 'recipient_fail';
		}
		echo json_encode($json);
	}
	//----------NEWS------------------------
	public function delete_news()
	{
		$json['status'] = 'fail';
		$news_id = $this->input->post('news_id', true);
		if($news_id) {
			$this->support_model->delete_news($news_id);
			$json['status'] = 'ok';
		}
		echo json_encode($json);
	}
	public function get_payments()
	{
		$data = array();
		$filter = array();
		$filters = $this->input->post('filter', true);
		if($filters)
			foreach ($filters as $key => $value) {
				if($value) {
					$filter[] = array(
						$key => $value
					);
				}
			}
		$field_sort = $this->input->post('sidx', true);
		$type_sort = $this->input->post('sord', true);
		$page = $this->input->post('page', true);
		$limit = $this->input->post('rows', true);

		$count = $this->support_model->count_payments();
		if($count)
			$total_pages = ceil($count/$limit);
		else
			$total_pages = 0;
		if($page > $total_pages)
			$page = $total_pages;

		$start = $limit * $page - $limit;
		if($start < 0)
			$start = 0;
		$data['page'] = $page;
		$data['total'] = $total_pages;
		$data['records'] = $count;
		$rows = $this->support_model->get_payments($field_sort, $type_sort, $start, $limit);
		$data['count'] = count($rows);
		$data['rows'] = $rows;
		foreach ($data['rows'] as $key => $value) {
			$order = $this->order_model->get($value['order_id']);
			$user['site_title'] = '';
			$user['order_date_title'] = '';
			$user['client_name'] = '';
			$user['topic'] = '';
			if($order) {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				if($user) {
					$user['client_name'] = $user['first_name'].' '.$user['last_name'];
					$user['topic'] = $order['topic'];
					if(isset($user['role'])) {
						switch ($user['role']) {
							case 'client':
								$user['site_title'] = 'Eagle-essays.com';
							break;
							case 'writer':
								$user['site_title'] = 'Writer-center.com';
							break;
							case 'professor':
								$user['site_title'] = 'Professor-essays.com';
							break;
							default:
							break;
						}
					}
				}
				else {
					$user['site_title'] = '';
					$user['order_date_title'] = '';
					$user['client_name'] = '';
					$user['topic'] = '';
				}
				$user['order_date_title'] = date("M j H:i", strtotime($order['date_add']));
			}
			$data['rows'][$key]['site_title'] = $user['site_title'];
			$data['rows'][$key]['order_date_title'] = $user['order_date_title'];
			$data['rows'][$key]['client_name'] = $user['client_name'];
			$data['rows'][$key]['topic'] = $user['topic'];
			$data['rows'][$key]['amount_title'] = '$'.$value['amount'];
			$data['rows'][$key]['date_title'] = date("M j H:i", strtotime($data['rows'][$key]['date']));
		}
		echo json_encode($data);
	}
	public function payments_export_xls()
	{
		$xls = "Number;";
		$xls .= "Site;";
		$xls .= "Order date;";
		$xls .= "Client name;";
		$xls .= "Order ID;";
		$xls .= "Topic;";
		$xls .= "Amount;";
		$xls .= "Receive date\r\n";
		$rows =  $this->support_model->get_payments('date', 'desc', 0, 65534);
		if(count($rows)) {
			$i = 1;
			foreach ($rows as $row) {
			$order = $this->order_model->get($row['order_id']);
			$user['site_title'] = '';
			$user['order_date_title'] = '';
			$user['client_name'] = '';
			$user['topic'] = '';
			$user['order_id'] = '';
			if($order) {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				if($user) {
					$user['client_name'] = $user['first_name'].' '.$user['last_name'];
					switch ($user['role']) {
						case 'client':
							$user['site_title'] = 'Eagle-essays.com';
						break;
						case 'writer':
							$user['site_title'] = 'Writer-center.com';
						break;
						case 'professor':
							$user['site_title'] = 'Professor-essays.com';
						break;
						default:
						break;
					}
				}
				else {
					$user['site_title'] = '';
					$user['order_date_title'] = '';
					$user['client_name'] = '';
					$user['topic'] = '';
					$user['order_id'] = '';
				}
				$user['order_id'] = $order['order_id'];
				$user['order_date_title'] = date("M j H:i", strtotime($order['date_add']));
				
				$user['topic'] = $order['topic'];
			}

			$xls .= $i.";";
			$xls .= $user['site_title'].";";
			$xls .= $user['order_date_title'].";";
			$xls .= $user['client_name'].";";
			$xls .= $user['order_id'].";";
			$xls .= $user['topic'].";";
			$xls .= '$'.$row['amount'].";";
			$xls .= date("M j H:i", strtotime($row['date']))."\r\n";
			$i++;
			}
		}

		$filename = 'Payment_statistics_'.date("Y-m-d H:i:s");
		header('Content-Disposition: attachment; filename="'.$filename.'.xls"');
		header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
		echo iconv("utf-8", "utf-8//TRANSLIT", $xls);
	}
	//----------SUPPORT-FUNCTIONS-----------
    public function create_preview($file_id)
    {
        $file = $this->order_model->get_file($file_id, 0);
        if($file){
            //$order = $this->order_model->get($file['order_id']);
            @unlink($file['file_path']."/".$file['file_name']."_preview.png");
            @unlink($file['file_path']."/".$file['raw_name'].".pdf");
            exec("/usr/local/bin/unoconv -f pdf ".$file['file_path']."/".$file['file_name']);
            $image = new imagick($file['file_path']."/".$file['raw_name'].".pdf");
            $pages = $image->getNumberImages();
            //if($pages > 10)
                //$pages = 10;
            for ($i=0; $i < $pages; $i++) { 
                $new_file = $file['file_path'].$file['raw_name'].'_preview'.$i.'.png';
                @unlink($new_file);
                $image = new imagick($file['file_path']."/".$file['raw_name'].".pdf[".$i."]");
                $image->writeImage($new_file);
            }
            unset($image);
            $this->split_images($file['client_name'], $file['file_path'], $file['raw_name'], $pages);
            $this->create_watermark($file['file_path'].$file['raw_name'].'_preview.png');
            @unlink($file['file_path'].$file['raw_name'].".pdf");
        }
    }
    public function split_images($name, $path, $raw_name, $pages)
    {
        $tmp_img = @imagecreatefrompng($path.$raw_name.'_preview0.png');
        $outfile_width = @imagesx($tmp_img);
        $outfile_height = 0;
        $outfile = $path.$raw_name.'_preview.png';
        @unlink($outfile);
        $sources = array();
        for ($i=0; $i < $pages ; $i++) { 
            $tmp_img = @imagecreatefrompng($path.$raw_name.'_preview'.$i.'.png');
            $outfile_height += @imagesy($tmp_img);
            $sources[] = $tmp_img;
            unset($tmp_img);
        }
        $image = @imagecreatetruecolor($outfile_width, $outfile_height);
        $x = 0;
        $y = 0;
        $index = 0;     
        for ($i=0; $i < $pages ; $i++) { 
            $width = @imagesx($sources[$index]);
            $height = @imagesy( $sources[$index]);
            @imagecopymerge($image, $sources[$index],$x, $y, 0, 0, $width, $height, 100);
            $y += $height;
            $index++;
        }
        @imagesavealpha($image,TRUE);
        @imagepng($image, $outfile);
        @imagedestroy($image);
        for ($i=0; $i < $pages ; $i++) { 
            @unlink($path.$raw_name.'_preview'.$i.'.png');
        }
        $image = null;
        $sources = null;
    }
    public function create_watermark($path)
    {
        $file = @file_get_contents($path);
        if(!$file)
            return;
        $image = @imagecreatefromstring($file);
        $w = @imagesx($image);
        $h = @imagesy($image);
        $watermark = @imagecreatefrompng('img/watermark.png');
        $ww = @imagesx($watermark);
        $wh = @imagesy($watermark);
        $img_paste_x = 0;
        while($img_paste_x < $w){
            $img_paste_y = 0;
            while($img_paste_y < $h){
                @imagecopy($image, $watermark, $img_paste_x, $img_paste_y, 0, 0, $ww, $wh);
                $img_paste_y += $wh;
            }
            $img_paste_x += $ww;
        }
        @imagepng($image, $path);
        @imagedestroy($image);
        @imagedestroy($watermark);      
    }
	public function check_updates()
	{
		$updates = 0;
		if($this->config->item('sitetype') == 'writer') {
			//if(!$updates) {
				//$user_files = $this->support_model->check_updates_user_files();
				//if($user_files)
					//$updates = 1;
			//}
			//if(!$updates) {
				//$user_testing = $this->support_model->check_updates_user_testing();
				//if($user_files)
					//$updates = 1;
			//}
		}
		//if(!$updates) {
			//$files = $this->support_model->check_updates_files();
			//if($files)
				//$updates = 1;
		//}
		//if(!$updates) {
			//$messages = $this->support_model->check_updates_messages();
			//if($messages)
				//$updates = 1;
		//}
		if(!$updates) {
			$orders = $this->support_model->check_updates_orders();
			if($orders)
				$updates = 1;
		}
		$json['status'] = 'ok';
		$json['updates'] = $updates;
		echo json_encode($json);
	}
	/*
	function drawImage($img1, $img2, $file = false)
	{
	    if(!preg_match('~\.(jpe?g|png|gif)$~i', $img1) || !preg_match('~\.(jpe?g|png|gif)$~i', $img2))
	        return false;
	    if(!($info[] = getimagesize($img1)) || !($info[] = getimagesize($img2)))
	        return false;
	 
	    $info[0]['type'] = substr($info[0]['mime'], 6);
	    $info[1]['type'] = substr($info[1]['mime'], 6);
	    $info[0]['width'] = (int) $info[0][0];
	    $info[1]['width'] = (int) $info[1][0];
	    $info[0]['height'] = (int) $info[0][1];
	    $info[1]['height'] = (int) $info[1][1];
	 
	    $create1 = 'imagecreatefrom' . $info[0]['type'];
	    $create2 = 'imagecreatefrom' . $info[1]['type'];
	    if( !function_exists($create1) || !function_exists($create1) )
	        return false;
	 
	    $width = ($info[0]['width'] > $info[1]['width']) ? $info[0]['width'] : $info[1]['width'];
	    $height = $info[0]['height'] + 1 + $info[1]['height']; //зазор в 1 пиксель между картинками (можно убрать, конечно)
	    if(empty($width) || empty($height))
	        return false;
	    
	    $image1 = $create1($img1); //create images
	    $image2 = $create2($img2); //create images
	 
	    $dst = imagecreatetruecolor($width, $height);
	 
	    if( in_array($info[0]['type'], array('gif','png')) || in_array($info[1]['type'], array('gif','png')) )
	    {
	        imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
	        imagealphablending($dst, false);
	        imagesavealpha($dst, true);
	        $type = 'png';
	    }
	    else
	        $type = 'jpeg';
	    
	    imagecopyresampled($dst, $image1, 0, 0, 0, 0, $width, $info[0]['height'], $info[0]['width'], $info[0]['height']);
	    imagecopyresampled($dst, $image2, 0, $info[0]['height'] + 1, 0, 0, $width, $info[1]['height'], $info[1]['width'], $info[1]['height']);
	    
	    $save = 'image' . $type;
	    header('Content-type: image/' . $type);
	    return false !== $file ? $save($dst, $file) : $save($dst);
	}//*/
	//----------END-AJAX-FILE---------------
}