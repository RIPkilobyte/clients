<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mails extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('support_model');
	}
	public function index()
	{
		$data = array();
		$tpl['active_item'] = 'mails';
		$tpl['title'] = $this->config->config['tp_title']."Mails";
		$tpl['content'] = $this->load->view('support/mail_list.tpl', $data, TRUE);
		$this->load->view('support/template.tpl', $tpl);
	}
	public function details()
	{
	}
}