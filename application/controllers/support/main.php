<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
	}
	public function index()
	{
		$data = array();
		$tpl['title'] = $this->config->config['tp_title']."Dashboard";
		$tpl['content'] = $this->load->view('support/main.tpl', $data, TRUE);
		$this->load->view('support/template.tpl', $tpl);
	}
}