<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('support_model');
	}
	public function index()
	{
		$data = array();
		$news = $this->support_model->get_all_news();
		if($news)
			foreach ($news as $key => $value)
				$news[$key]['date'] = date("M j h:i A", strtotime($news[$key]['date_add']));
		$data['news'] = $news;
		$tpl['active_item'] = 'news';
		$tpl['title'] = $this->config->config['tp_title']."News";
		$tpl['content'] = $this->load->view('support/news.tpl', $data, TRUE);
		$this->load->view('support/template.tpl', $tpl);
	}
	public function edit($news_id = '')
	{
		$data = array();
		if($news_id) {
			$tpl['title'] = $this->config->config['tp_title']."Edit news #".$news_id;
			$news = $this->support_model->get_news_by_id($news_id);
			if($news) {
				$news_id = $this->input->post('news_id', true);
				if($news_id) {//save
					$update = array(
						'title' => $this->input->post('title', true),
						'text' => $this->input->post('text', true)
					);
					if($update['title'] == '' || $update['text'] == '') {
						$data['title'] = $update['title'];
						$data['text'] = $update['text'];
						$data['errors'] = '<p>Please fill in the title and text messages</p>';
					}
					else {
						$this->support_model->add_edit_news($update, $news_id);
						$data['status'] = 'saved';
					}
					$data['news_id'] = $news_id;
				}
				else {//edit. show
					$data = array(
						'news_id' => $news['news_id'],
						'title' => $news['title'],
						'text' => $news['text'],
						'date' => date("M j h:i A", strtotime($news['date_add']))
					);
					$data['status'] = 'show';
				}
			}
			$tpl['content'] = $this->load->view('support/news_details.tpl', $data, TRUE);
		}
		else {
			$title = $this->input->post('title', true);
			$text = $this->input->post('text', true);
			if($title != '' && $text != '') {
				$insert = array(
					'title' => $title,
					'text' => $text,
					'date_add' => date("Y-m-d H:i:s")
				);
				$this->support_model->add_edit_news($insert);
				$data = array(
					'title' => $title,
					'text' => $text
				);				
			}
			else {
				$data = array(
					'title' => $title,
					'text' => $text
				);
				$data['errors'] = '<p>Please fill in the title and text messages</p>';
				$tpl['title'] = $this->config->config['tp_title']."Add news";
			}
			$tpl['content'] = $this->load->view('support/news_details.tpl', $data, TRUE);
		}

		$tpl['active_item'] = 'news';
		$this->load->view('support/template.tpl', $tpl);
	}
}