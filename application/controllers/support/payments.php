<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('support_model');
		$this->load->model('order_model');
		$this->load->model('payment_model');
	}
	public function index()
	{
		$data = array();
		$tpl['active_item'] = 'payments';
		$tpl['content'] = $this->load->view('support/payments.tpl', $data, TRUE);
		$tpl['title'] = $this->config->config['tp_title']."Payments";
		$this->load->view('support/template.tpl', $tpl);
	}
}