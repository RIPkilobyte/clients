<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Writers extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('support_model');
		$this->load->model('order_model');
		$this->load->model('assist_model');
		$this->load->model('user_model');
	}
	public function index()
	{
		$data = array();
		$tpl['active_item'] = 'writers';
		$tpl['title'] = $this->config->config['tp_title']."Writers";
		$tpl['content'] = $this->load->view('support/writers.tpl', $data, TRUE);
		$this->load->view('support/template.tpl', $tpl);
	}
	public function writer_details($id = '')
	{
		if($id) {
			$user = $this->my_auth->get_by_id($id);
			$user_meta = $this->my_auth->get_user_meta($id);
			$disciplines = $this->order_model->get_subjects();
			$paper_formats = $this->order_model->get_paper_formats();
			if($user && $user_meta) {
				$data = array();
				$data['user'] = array_merge($user, $user_meta);
				$user_disciplines = explode(",", $data['user']['disciplines']);
				$user_paper_formats = explode(",", $data['user']['paper_formats']);
				switch ($data['user']['step']) {
					case 'one':
						$data['user_testing'] = $this->support_model->get_user_testing1($id, 'users_testing1');
						if($data['user_testing']) {
							$data['essay'] = $this->user_model->get_essay($data['user_testing']['essay_id']);
							if($data['user_testing']['diplom'])
								$data['user_testing']['diplom'] = $this->user_model->get_user_file($data['user_testing']['diplom']);
							else
								$data['diplom_id'] = '';
							if($data['user_testing']['passport'])
								$data['user_testing']['passport'] = $this->user_model->get_user_file($data['user_testing']['passport']);
							else
								$data['passport_id'] = '';
							if($data['user_testing']['essay'])
								$data['user_testing']['essay'] = $this->user_model->get_user_file($data['user_testing']['essay']);
							else
								$data['essay_file'] = '';
						}
					break;

					case 'three':
						$data['user_testing'] = $this->support_model->get_user_testing1($id, 'users_testing1');
						if($data['user_testing']) {
							$data['essay'] = $this->user_model->get_essay($data['user_testing']['essay_id']);
							if($data['user_testing']['diplom'])
								$data['user_testing']['diplom'] = $this->user_model->get_user_file($data['user_testing']['diplom']);
							else
								$data['diplom_id'] = '';
							if($data['user_testing']['passport'])
								$data['user_testing']['passport'] = $this->user_model->get_user_file($data['user_testing']['passport']);
							else
								$data['passport_id'] = '';
							if($data['user_testing']['essay'])
								$data['user_testing']['essay'] = $this->user_model->get_user_file($data['user_testing']['essay']);
							else
								$data['essay_file'] = '';
						}

						$data['user_testing_results'] = $this->user_model->get_user_testing($id);
						if($data['user_testing_results']) {
							$user_questions = $this->user_model->get_user_questions($id);
							$tests = array();
							foreach ($user_questions as $user_question) {
								$question = '';
								$answer = '';
								$correct = '';
								if(!isset($tests[$user_question['test_id']])) {
									$test = $this->user_model->get_test_by_id($user_question['test_id']);
									$tests[$user_question['test_id']]['title'] = $test['title'].' Test';
								}
								$question = $this->user_model->get_question($user_question['question_id']);
								$question = $question['value'];

								$answer = $this->user_model->get_answer($user_question['question_id'], $user_question['answer_id']);
								if($answer && $answer['correct'])
									$correct = 1;
								else
									$correct = 0;
								if($answer)
									$answer = $answer['value'];
								else
									$answer = 'No answer';

								$tests[$user_question['test_id']]['questions'][] = array(
									'question' => $question,
									'answer' => $answer,
									'correct' => $correct
								);
							}
							$data['tests'] = $tests;
						}
					break;

					case 'finished':
						$data['user_testing'] = $this->support_model->get_user_testing1($id, 'users_testing1');
						if($data['user_testing']) {
							$data['essay'] = $this->user_model->get_essay($data['user_testing']['essay_id']);
							if($data['user_testing']['diplom'])
								$data['user_testing']['diplom'] = $this->user_model->get_user_file($data['user_testing']['diplom']);
							else
								$data['diplom_id'] = '';
							if($data['user_testing']['passport'])
								$data['user_testing']['passport'] = $this->user_model->get_user_file($data['user_testing']['passport']);
							else
								$data['passport_id'] = '';
							if($data['user_testing']['essay'])
								$data['user_testing']['essay'] = $this->user_model->get_user_file($data['user_testing']['essay']);
							else
								$data['essay_file'] = '';
						}

						$data['user_testing_results'] = $this->user_model->get_user_testing($id);
						if($data['user_testing_results']) {
							$questions = $this->user_model->get_user_questions($id);
							foreach ($questions as $key => $value) {
								$answer = $this->user_model->get_answer($value['question_id'], $value['answer_id']);
								if($answer && $answer['correct'])
									$questions[$key]['correct'] = 1;
								else
									$questions[$key]['correct'] = 0;
								if($answer)
									$questions[$key]['answer'] = $answer['value'];
								else
									$questions[$key]['answer'] = 'No answer';
								$question = $this->user_model->get_question($value['question_id']);
								$questions[$key]['question'] = $question['value'];
							}
							$data['questions'] = $questions;
						}
					break;
					default:
					break;
				}
				$orders = $this->support_model->get_user_orders($id);
				$data['user']['orders'] = count($orders);
				$data['user']['grades'] = 0;
				foreach ($orders as $k => $v) {
					$data['user']['grades'] = $data['user']['grades'] + $v['rating_writer'];
				}
				if(count($orders))
					$data['user']['grades'] = round($data['user']['grades']/count($orders), 2);
				else
					$data['user']['grades'] = 0;

				$data['user']['disciplines'] = '';
				foreach ($user_disciplines as $k2 => $v2) {
					foreach ($disciplines as $k1 => $v1) {
						if($v2 == $v1['subject_id']) {
							if(isset($user_disciplines[$k2+1]))
								$data['user']['disciplines'] .= $v1['title'].', ';
							else
								$data['user']['disciplines'] .= $v1['title'];
						}
					}
				}
				$data['user']['paper_formats'] = '';
				foreach ($user_paper_formats as $k2 => $v2) {
					foreach ($paper_formats as $k1 => $v1) {
						if($v2 == $v1['paper_format_id']) {
							if(isset($user_disciplines[$k2+3]))
								$data['user']['paper_formats'] .= $v1['title'].', ';
							else
								$data['user']['paper_formats'] .= $v1['title'];
						}
					}
				}				
				$country = $this->assist_model->get_country($data['user']['country']);
				$data['user']['country_title'] = $country['title'];
				$data['user']['phone_prefix'] = $country['phone_prefix'];

				$tpl['title'] = $this->config->config['tp_title']."Writer #".$id." details";
				$tpl['content'] = $this->load->view('support/writer_details.tpl', $data, TRUE);
				$this->load->view('support/template.tpl', $tpl);
			}
			else 
				redirect($this->config->item('base_url').'/support');
		}
		else
			redirect($this->config->item('base_url').'/support');
	}
	public function timeintervals($user_id = '')
	{
		$user = $this->my_auth->get_by_id($user_id);
		if($user) {
			$timeintervals = $this->input->post('timeinterval', true);
			$this->support_model->delete_user_testing_dates($user_id);
			foreach ($timeintervals as $interval) {
				if($interval)
					$this->support_model->add_user_testing_date($user_id, $interval);
			}
			redirect('/support/writers/writer_details/'.$user_id);
		}
		else
			redirect('/support/writers');
	}
}