<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inquiries extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
	}
	public function index()
	{
		$data = array();
		$tpl['active_item'] = 'inquiries';
		$tpl['content'] = $this->load->view('support/inquiries.tpl', $data, TRUE);
		$tpl['title'] = $this->config->config['tp_title']."Inquiries";
		$this->load->view('support/template.tpl', $tpl);
	}
}