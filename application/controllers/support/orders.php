<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('support_model');
		$this->load->model('order_model');
	}
	function myCmp($a, $b) 
	{ 
		if ($a['date'] === $b['date']) 
			return 0; 
		return $a['date'] < $b['date'] ? 1 : -1; 
	}	
	public function index()
	{
		$data = array();
		$tpl['active_item'] = 'orders';
		$tpl['content'] = $this->load->view('support/orders.tpl', $data, TRUE);
		$tpl['title'] = $this->config->config['tp_title']."Orders";
		$this->load->view('support/template.tpl', $tpl);
	}
	public function order_details($order_id = '')
	{
		if($order_id) {
			$data = array();
			$tpl['title'] = $this->config->config['tp_title']."Order #".$order_id;
			$data['order'] = $this->order_model->get($order_id);
			if($data['order']) {

				$academic_level = $this->order_model->get_academic_level_by_id($data['order']['academic_level']);
				if($academic_level)
					$data['order']['academic_level_title'] = $academic_level['title'];
				else
					$data['order']['academic_level_title'] = '';
				 
				$deadline = $this->order_model->get_deadline_by_id($data['order']['deadline']);
				if($deadline)
					$data['order']['deadline_title'] = $deadline['title'];
				else
					$data['order']['deadline_title'] = '';
				 
				$paper_type = $this->order_model->get_paper_type_by_id($data['order']['paper_type']);
				if($paper_type)
					$data['order']['paper_type_title'] = $paper_type['title'];
				else
					$data['order']['paper_type_title'] = '';

				$subject = $this->order_model->get_subject_by_id($data['order']['subject']);
				if($subject)
					$data['order']['subject_title'] = $subject['title'];	
				else
					$data['order']['subject_title'] = '';

				$paper_format = $this->order_model->get_paper_format_by_id($data['order']['paper_format']);
				if($paper_format)
					$data['order']['paper_format_title'] = $paper_format['title'];
				else
					$data['order']['paper_format_title'] = '';	
				
				$data['expired'] = 0;
				if(strtotime("now") > strtotime($data['order']['date_end'])) {
					$data['expired'] = 1;
				}
				
				$date = explode(' ', $data['order']['date_end']);
				$first = explode('-', $date[0]);
				$second = explode(':', $date[1]);
				$data['timer'] = array(
					'y' => $first[0],
					'm' => $first[1],
					'd' => $first[2],
					'H' => $second[0],
					'i' => $second[1],
					's' => $second[2]
				);
				$date = explode(' ', $data['order']['date_end_writer']);
				$first = explode('-', $date[0]);
				$second = explode(':', $date[1]);
				$data['writer_timer'] = array(
					'y' => $first[0],
					'm' => $first[1],
					'd' => $first[2],
					'H' => $second[0],
					'i' => $second[1],
					's' => $second[2]
				);				

				$messages_from_customer = $this->support_model->get_order_messages_from($data['order']['customer_id'], $data['order']['order_id']);
				$files_from_customer = $this->support_model->get_order_files_from($data['order']['customer_id'], $data['order']['order_id']);
				$from_customer = array_merge($messages_from_customer, $files_from_customer);
				uasort($from_customer, array($this,"myCmp"));
				//var_dump($from_customer);
				foreach ($from_customer as $key => $customer) {
					//from
					if($customer['sender_id'] == '1')
						$from = 'SUPPORT';
					elseif($customer['sender_id'] == '0')
						$from = 'GENERAL';
					elseif($customer['sender_id'] == $data['order']['customer_id'])
						$from = 'Customer #'.$customer['sender_id'];
					elseif($customer['sender_id'] == $data['order']['writer_id'])
						$from = 'Writer #'.$customer['sender_id'];
					else
						$from = 'User #'.$customer['sender_id'];
					//to
					if($customer['receiver_id'] == '1')
						$to = 'SUPPORT';
					elseif($customer['receiver_id'] == '0')
						$to = 'GENERAL';
					elseif($customer['receiver_id'] == $data['order']['customer_id'])
						$to = 'Customer #'.$customer['receiver_id'];
					elseif($customer['receiver_id'] == $data['order']['writer_id'])
						$to = 'Writer #'.$customer['receiver_id'];
					else
						$to = 'User #'.$customer['receiver_id'];
					$from_customer[$key]['from'] = $from;
					$from_customer[$key]['to'] = $to;

					if(isset($from_customer[$key]['attached_file']) && $from_customer[$key]['attached_file']) {
						$from_customer[$key]['file'] = $this->order_model->get_file($from_customer[$key]['attached_file'], 0);
						$img = '';
						switch ($from_customer[$key]['file']['file_ext']) {
							case '.jpg':
							case '.jpeg':
							case '.png':
							case '.bmp':
								$img = 'img.png';
							break;
							case '.xls':
							case '.xlsx':
								$img = 'xls.png';
							break;
							case '.doc':
							case '.docx':
								$img = 'doc.png';
							break;
							case '.pdf':
								$img = 'pdf.png';
							break;
							default:
								$img = '';
								break;
						}	
						$from_customer[$key]['file']['image'] = $img;
					}
				}
				$data['from_customer'] = $from_customer;

				if($data['order']['writer_id'] > 0)
					$messages_from_writer = $this->support_model->get_order_messages_from($data['order']['writer_id'], $data['order']['order_id']);
				else
					$messages_from_writer = $this->support_model->get_order_messages_from_writers($data['order']['order_id'], '0,1,'.$data['order']['customer_id']);
					$files_from_writer = $this->support_model->get_order_files_from($data['order']['writer_id'], $data['order']['order_id']);
					$from_writer = array_merge($messages_from_writer, $files_from_writer);
					uasort($from_writer, array($this,"myCmp"));
					foreach ($from_writer as $key => $writer) {
						//from
						if($writer['sender_id'] == '0' || $writer['sender_id'] == '1')
							$from = 'SUPPORT';
						elseif($writer['sender_id'] == $data['order']['customer_id'])
							$from = 'Customer #'.$writer['sender_id'];
						elseif($writer['sender_id'] == $data['order']['writer_id'])
							$from = 'Writer #'.$writer['sender_id'];
						else
							$from = 'Writer #'.$writer['sender_id'];
						//to
						if($writer['receiver_id'] == '0' || $writer['receiver_id'] == '1')
							$to = 'SUPPORT';
						elseif($writer['receiver_id'] == $data['order']['customer_id'])
							$to = 'Customer #'.$writer['receiver_id'];
						elseif($writer['receiver_id'] == $data['order']['writer_id'])
							$to = 'Writer #'.$writer['receiver_id'];
						else
							$to = 'Writer #'.$writer['receiver_id'];
						$from_writer[$key]['from'] = $from;
						$from_writer[$key]['to'] = $to;

						if(isset($from_writer[$key]['attached_file']) && $from_writer[$key]['attached_file']) {
							$from_writer[$key]['file'] = $this->order_model->get_file($from_writer[$key]['attached_file'], 0);
							$path = $from_writer[$key]['file']['file_path'].$from_writer[$key]['file']['raw_name'].'_preview.png';
							$from_writer[$key]['file']['exist_preview'] = 0;
							if(file_exists($path))
								$from_writer[$key]['file']['exist_preview'] = 1;
							//var_dump($from_writer[$key]);
							
							$img = '';
							switch ($from_writer[$key]['file']['file_ext']) {
								case '.jpg':
								case '.jpeg':
								case '.png':
								case '.bmp':
									$img = 'img.png';
								break;
								case '.xls':
								case '.xlsx':
									$img = 'xls.png';
								break;
								case '.doc':
								case '.docx':
									$img = 'doc.png';
								break;
								case '.pdf':
									$img = 'pdf.png';
								break;
								default:
									$img = '';
									break;
							}	
							$from_writer[$key]['file']['image'] = $img;
						}
						if(isset($from_writer[$key]['file_id']) && $from_writer[$key]['file_id']) {
							$path = $from_writer[$key]['file_path'].$from_writer[$key]['raw_name'].'_preview.png';
							$from_writer[$key]['exist_preview'] = 0;
							if(file_exists($path))
								$from_writer[$key]['exist_preview'] = 1;
							
							$img = '';
							switch ($from_writer[$key]['file_ext']) {
								case '.jpg':
								case '.jpeg':
								case '.png':
								case '.bmp':
									$img = 'img.png';
								break;
								case '.xls':
								case '.xlsx':
									$img = 'xls.png';
								break;
								case '.doc':
								case '.docx':
									$img = 'doc.png';
								break;
								case '.pdf':
									$img = 'pdf.png';
								break;
								default:
									$img = '';
									break;
							}	
							$from_writer[$key]['image'] = $img;
						}
				}
				$data['from_writer'] = $from_writer;

				$messages_from_support = $this->support_model->get_order_messages_from(1, $data['order']['order_id']);
				$files_from_support = $this->support_model->get_order_files_from(1, $data['order']['order_id']);
				$from_support = array_merge($messages_from_support, $files_from_support);
				uasort($from_support, array($this,"myCmp"));
				foreach ($from_support as $key => $support) {
					//from
					if($support['sender_id'] == '1')
						$from = 'SUPPORT';
					elseif($support['sender_id'] == $data['order']['customer_id'])
						$from = 'Customer #'.$support['sender_id'];
					else
						$from = 'Writer #'.$support['sender_id'];
					//to
					if($support['receiver_id'] == '1')
						$to = 'SUPPORT';
					elseif($support['receiver_id'] == $data['order']['customer_id'])
						$to = 'Customer #'.$support['receiver_id'];
					elseif($support['receiver_id'] == '0')
						$to = 'GENERAL';
					else
						$to = 'Writer #'.$support['receiver_id'];
					$from_support[$key]['from'] = $from;
					$from_support[$key]['to'] = $to;

					if(isset($from_support[$key]['file_id'])) {
						$img = '';
						switch ($from_support[$key]['file_ext']) {
							case '.jpg':
							case '.jpeg':
							case '.png':
							case '.bmp':
								$img = 'img.png';
							break;
							case '.xls':
							case '.xlsx':
								$img = 'xls.png';
							break;
							case '.doc':
							case '.docx':
								$img = 'doc.png';
							break;
							case '.pdf':
								$img = 'pdf.png';
							break;
							default:
								$img = '';
								break;
						}	
						$from_support[$key]['file']['image'] = $img;					
					}					
				}
				$data['from_support'] = $from_support;

				$data['order_statuses'] = $this->support_model->get_order_statuses();
				$data['order_current_status'] = 0;

				foreach ($data['order_statuses'] as $k1 => $v1) {
					if(
						$data['order']['status'] == $v1['status'] &&
						$data['order']['status_client'] == $v1['status_client'] &&
						$data['order']['status_writer'] == $v1['status_writer']
					) {
						$data['order_current_status'] = $v1['order_status_id'];
						break;
					}
				}
				//var_dump($data['order']);
				//die();
				$data['academic_levels'] = $this->order_model->get_academic_levels();
				$data['paper_types'] = $this->order_model->get_paper_types();
				$data['deadlines'] = $this->order_model->get_deadlines();
				$data['subjects'] = $this->order_model->get_subjects();
				$data['paper_formats'] = $this->order_model->get_paper_formats();
				$data['institution_types'] = $this->order_model->get_institution_types();
				//$data['countries'] = $this->assist_model->get_countries();

				$data['requests'] = $this->support_model->get_requests_by_order($order_id);
				$data['payment'] = $this->support_model->get_order_payment($data['order']['payment_id']);
				$tpl['content'] = $this->load->view('support/order_details.tpl', $data, TRUE);
			}
			else
				$tpl['content'] = '';
			$this->load->view('support/template.tpl', $tpl);
		}
		else
			redirect('/support');
	}
}