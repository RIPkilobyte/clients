<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if(!Check::support())
			redirect('/user/login');
		$this->load->model('assist_model');
		$this->load->model('support_model');
	}
	public function index()
	{
		redirect('/support');
	}
	public function profile($user_id = '')
	{
		if(!$user_id)
			redirect('/support');
		$data = array();
		$user = $this->my_auth->get_by_id($user_id);
		if($user && $user['role'] == 'writer') {
			$user['meta'] = $this->my_auth->get_user_meta($user_id);
			$academic_level = $this->assist_model->get_academic_level($user['meta']['academic_level']);
			$user['meta']['academic_level_title'] = $academic_level['title'];

			$paper_format = $this->assist_model->get_paper_format($user['meta']['paper_formats']);
			$tmp = array();
			foreach ($paper_format as $paper)
				array_push($tmp, $paper['title']);
			$user['meta']['paper_format_title'] = implode(', ', $tmp);			

			$disciplines = $this->assist_model->get_subjects($user['meta']['disciplines']);
			$tmp = array();
			foreach ($disciplines as $discipline)
				array_push($tmp, $discipline['title']);
			$user['meta']['disciplines_title'] = implode(', ', $tmp);			
		}
		
		$country = $this->assist_model->get_country($user['country']);
		$user['country_title'] = $country['title'];
		$user['phone_prefix'] = $country['phone_prefix'];

		$data['user'] = $user;
		$data['previous'] = $this->support_model->get_customer_orders($user_id);
		$tpl['title'] = $this->config->config['tp_title']."Profile #".$user_id;
		$tpl['content'] = $this->load->view('support/profile.tpl', $data, TRUE);
		$this->load->view('support/template.tpl', $tpl);
	}
}