<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Articles_model');
		$this->load->model('order_model');
		$this->load->model('other_model');
	}	
	public function index() 
	{
		$solo = $this -> Articles_model -> get_article_solo('main');
		if (!empty($solo)) {
            $solo['breadcrumbs'][] = array(
                'title' => 'Main page',
                'href' => '/'
            );
			$tpl['title'] = $this->config->config['tp_title'].$solo['title'];
	        $data = array(
    	    	'academic_levels' => $this->order_model->get_academic_levels(),
        		'paper_types' => $this->order_model->get_paper_types(),
        		'deadlines' => $this->order_model->get_deadlines()
        	);
        	foreach ($data['paper_types'] as $key => $value) {
        		if($value['position'] == '3')
        			$data['paper_types'][$key]['class'] = 'indent';
        	}

        	$prices = $this->order_model->get_prices();
        	$data['prices'] = array();
        	foreach ($data['academic_levels'] as $key => $academic_level) {
        		foreach ($data['deadlines'] as $key => $deadline) {
        			foreach ($prices as $key => $price) {
        				if($price['type'] == 'new' && $price['academic_level_id'] == $academic_level['academic_level_id'] && $price['deadline_id'] == $deadline['deadline_id'])
        					$data['prices']['new'][$academic_level['academic_level_id']][$deadline['deadline_id']] = $price['value'];
        				if($price['type'] == 'edit' && $price['academic_level_id'] == $academic_level['academic_level_id'] && $price['deadline_id'] == $deadline['deadline_id'])
        					$data['prices']['edit'][$academic_level['academic_level_id']][$deadline['deadline_id']] = $price['value'];
        			}
        		}
        	}

			$tpl['calc'] = $this->load->view('home/calc.tpl', $data, TRUE);
			$tpl['main_page_text'] = $this->other_model->get_main_page_text();
			$tpl['content'] = $this->load->view('common/article.tpl', $solo, TRUE);
			$tpl['description'] = $solo['meta_description'];
			$tpl['keywords'] = $solo['meta_keyword'];
			$tpl['video'] = $solo['video'];
			$tpl['video_class'] = 'main_video';
			$this->load->view('template.tpl', $tpl);
		}		
		/*
		$tpl['title'] = $this->config->config['tp_title']."Main";
		
        $tpl['content'] = $this->load->view('home/home.tpl', FALSE, TRUE);
        $data = array(
        	'academic_levels' => $this->order_model->get_academic_levels(),
        	'paper_types' => $this->order_model->get_paper_types(),
        	'deadlines' => $this->order_model->get_deadlines()
        	);
        
        $tpl['calc'] = $this->load->view('home/calc.tpl', $data, TRUE);
		
		$tpl['module1'] = $this->load->view('common/slidercontent/body.tpl', FALSE, TRUE);
		
		// Слайдер контента
		$tpl['scripts'][0] = $this->load->view('common/slidercontent/head.tpl', FALSE, TRUE);
		$tpl['description'] = $this->other_model->get_description();
		$tpl['description'] = $tpl['description']['value'];
		$tpl['keywords'] = $this->other_model->get_keywords();
		$tpl['keywords'] = $tpl['keywords']['value'];		
		
		$this->load->view('template.tpl', $tpl);
		//*/
	}
	
}