<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	
		$this->load->model('assist_model');
		$this->load->model('order_model');
		$this->load->model('user_model');
		$this->load->model('payment_model');
	}
	
	function mail_test()
	{
		$data = array(
			'first_name' => 'name',
			'last_name' => 'last',
			'email' => 'ripkilobyte@gmail.com',
			'password' => 'pass',
			'user_id' => '971',
			'order_id' => '1',
			'subject_title' => 'subject_title',
			'topic' => 'topic',
			'paper_type_title' => 'paper_type_title',
			'academic_level_title' => 'academic_level_title',
			'pages' => 'pages',
			'deadline_title' => 'deadline_title',
			'hours' => 4,
			'reason' => 'reason',
		);
		Mail::prepare('client_writer_finished', $data);
		/*
		client_register
		client_place_order
		client_receive_payment
		client_writer_assign
		client_writer_finished
		client_request_revision
		client_cancel_order
		client_approve_order
		client_found_writer
		client_writer_commented
		client_support_commented
		client_change_password
		client_forgot_password
		//*/
	}
	//*/
	/*
	public function all_mails_script()
	{
		$mails = array(
			'professor_register',
			'professor_receive_payment',
			'professor_writer_assign',
			'professor_writer_finished',
			'professor_request_revision',
			'professor_cancel_order',
			'professor_approve_order',
			'professor_writer_commented',
			'professor_support_commented',
			'professor_change_password',
			'professor_forgot_password',
			'professor_place_order',
			'professor_found_writer',
			'client_register',
			'client_place_order',
			'client_receive_payment',
			'client_writer_assign',
			'client_writer_finished',
			'client_request_revision',
			'client_cancel_order',
			'client_approve_order',
			'client_found_writer',
			'client_writer_commented',
			'client_support_commented',
			'client_change_password',
			'client_forgot_password',
			'writer_register',
			'writer_all_files_uploaded',
			'writer_files_confirm_ok',
			'writer_files_confirm_fail',
			'writer_test_complete',
			'writer_test_complete_ok',
			'writer_test_complete_fail',
			'writer_email_verification_code',
			'writer_email_verification_ok',
			'writer_apply_case',
			'writer_assigned_to_case',
			'writer_support_reject_file',
			'writer_customer_request_revision',
			'writer_half_time_deadline',
			'writer_two_hours_deadline',
			'writer_order_cancelled',
			'writer_customer_approved',
			'writer_choose_paypal',
			'writer_choose_payoner',
			'writer_choose_payment_method',
			'writer_change_password',
			'writer_support_commented',
			'writer_client_commented',
			'writer_forgot_password',
			'writer_dismiss_order',
			'support_client_place_order',);
		$data = array(
			'user_id' => '1',
			'order_id' => '1',
			'first_name'=>'first_name',
			'last_name'=>'last_name',
			'email'=>'email',
			'password'=>'password',
			'reason'=>'reason',
			'link'=>'link',
			'subject_title'=>'subject_title',
			'topic'=>'topic',
			'paper_type_title'=>'paper_type_title',
			'academic_level_title'=>'academic_level_title',
			'pages'=>'pages',
			'deadline_title'=>'deadline_title',
			'timeleft'=>'timeleft',
			'verify_code'=>'verify_code',
			'amount'=>'amount',
			'paypal_email'=>'paypal_email',
			);
		$arrayData = array();
		$arrayString = array();
		$arrayString[] = 'System name';
		$arrayString[] = 'Title';
		$arrayString[] = 'Text';
		$arrayData[] = $arrayString;
		foreach($mails as $mail) {
			$arrayString = array();
			$arrayString[] = $mail;
			$title = Mail::text_title($mail,$data);
			$text = Mail::text_body($mail,$data);
			$text = str_replace('<br>', '  ', $text);
			$arrayString[] = $title;
			$arrayString[] = $text;
			$arrayData[] = $arrayString;
		}
		require_once '/home/webadmin/eagle-essays.com/assets/PHPExcel/PHPExcel.php';
		//require_once 'c:/openserver/domains/clients/assets/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("PHP Export Tool")
					 ->setLastModifiedBy("PHP Export Tool")
					 ->setTitle("Office 2007 XLSX Document")
					 ->setSubject("Office 2007 XLSX Document")
					 ->setDescription("Export document for Office 2007 XLSX, generated using PHP classes.")
					 ->setKeywords("office 2007 openxml php")
					 ->setCategory("PHP Export file");
		$objPHPExcel->getActiveSheet()->fromArray($arrayData, NULL);
		$objPHPExcel->getActiveSheet()->setTitle(date('d-m-Y'));
		$objPHPExcel->setActiveSheetIndex(0);
				

		$name = "export-".date('d-m-Y').'.xls';
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$name.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	//*/
	/*
	public function export_clients()
	{
		$arrayData = array();
		$arrayString = array();
		$countries = $this->assist_model->get_countries();
		$arrayString[] = 'User site';
		$arrayString[] = 'User ID';
		$arrayString[] = 'First name';
		$arrayString[] = 'Last name';
		$arrayString[] = 'Email';
		$arrayString[] = 'Country';
		$arrayString[] = 'Phone';
		$arrayData[] = $arrayString;

		$roles = array('client', 'professor');
		$this->db->order_by('user_id asc');
		//$this->db->where_in('role', $roles);
		$users = $this->db->get('users')->result_array();
		foreach($users as $user) {
			$arrayString = array();
			if($user['role'] == 'client')
				$arrayString[] = 'Eagle-Essays';
			elseif($user['role'] == 'professor')
				$arrayString[] = 'Professor-Essays';
			else
				$arrayString[] = 'Writer-center';

			$arrayString[] = $user['user_id'];
			$arrayString[] = $user['first_name'];
			$arrayString[] = $user['last_name'];
			$arrayString[] = $user['email'];
			foreach($countries as $country)
				if($country['country_id'] == $user['country'])
					$arrayString[] = $country['title'];
			$arrayData[] = $arrayString;
		}
		require_once '/home/webadmin/eagle-essays.com/assets/PHPExcel/PHPExcel.php';
		//require_once 'c:/openserver/domains/clients/assets/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("PHP Export Tool")
					 ->setLastModifiedBy("PHP Export Tool")
					 ->setTitle("Office 2007 XLSX Document")
					 ->setSubject("Office 2007 XLSX Document")
					 ->setDescription("Export document for Office 2007 XLSX, generated using PHP classes.")
					 ->setKeywords("office 2007 openxml php")
					 ->setCategory("PHP Export file");
		$objPHPExcel->getActiveSheet()->fromArray($arrayData, NULL);
		$objPHPExcel->getActiveSheet()->setTitle(date('d-m-Y'));
		$objPHPExcel->setActiveSheetIndex(0);
				

		$name = "export-".date('d-m-Y').'.xls';
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$name.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	//*/
	function myCmp($a, $b) 
	{ 
		if ($a['date'] === $b['date']) 
			return 0; 
		return $a['date'] < $b['date'] ? 1 : -1; 
	}
	public function index() 
	{
		if(!Check::login())
			redirect('/user/login');		
		$tpl['title'] = $this->config->config['tp_title']."Home";
		$data = array();
		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value) {
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		}
        $tpl['content'] = $this->load->view('user/lk.tpl', $data, TRUE);
		$this->load->view('user_template.tpl', $tpl);		
	}
	public function logintry()
	{
		$email = $this->input->post('email', true);
		$password = $this->input->post('password', true);
		$order_id = $this->input->post('order_id', true);
		if($email && $password && $order_id) {
			if($this->my_auth->logintry($email, $password)) {
				redirect('/cart/checkout/'.$order_id);
			}
			else {
				redirect('/user/login');
			}
		}
		redirect('/user/login');
		//echo $this->db->last_query();
		//var_dump($_SESSION);
		//var_dump($_POST);
		//die();
	}
	public function login()
	{
		if(Check::login())
			redirect('/user');			
		if(isset($_POST['login'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('login', 'Login', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$login = $this->input->post('login', true);
			$password = $this->input->post('password', true);
			if($this->form_validation->run() == true) {
				if($this->my_auth->login($login, $password))
					redirect('/user');
				else {
					$data = array(
						'login' => $login,
						'password' => $password,
						'errors' => 'Login or Password incorrect'
					);
				}
			}
			else {
				$data = array(
					'login' => $login,
					'password' => $password,
					'errors' => 'Login or Password incorrect'
				);
			}
		}
		else {
			$data = array(
				'login' => '',
				'password' => ''
			);
		}
		$tpl['title'] = $this->config->config['tp_title']."Login";
		$tpl['content'] = $this->load->view('user/login.tpl', $data, TRUE);
		$this->load->view('login_template.tpl', $tpl);
	}
	public function forgot_password($code = '')
	{
		if(Check::login())
			redirect('/user');
		$data['login'] = '';
		if($code) {
			$user = $this->my_auth->get_user_by_forgot_code($code);
			if($user) {
				if(isset($_POST['code'])) {
					$this->load->library('form_validation');
					$this->form_validation->set_rules('new', 'New password', 'required|matches[confirm]');
					$this->form_validation->set_rules('confirm', 'Confirm password', 'required');

					if($this->form_validation->run()) {
						$this->my_auth->restore_password($user['user_id'], $this->input->post('new', true));
						redirect('user/login');
					}
					else {
						$data['errors'] = validation_errors();
					}
				}
				else {
					$data['success'] = '<div>Password restored successfully.<br />Please type new password.</div>';
				}
				$tpl['title'] = $this->config->config['tp_title']."Restore password";
				$tpl['content'] = $this->load->view('user/forgot_password_code.tpl', $data, TRUE);
				$this->load->view('login_template.tpl', $tpl);
			}
			else {
				$tpl['title'] = $this->config->config['tp_title']."Restore password";
				$tpl['content'] = $this->load->view('user/forgot_password_code_error.tpl', $data, TRUE);
				$this->load->view('login_template.tpl', $tpl);
			}
		}
		else {
			if(isset($_POST['login'])) {
				$login = $this->input->post('login', true);
				$user = $this->my_auth->get_by_email($login);
				if($user) {

					$data['name'] = $user['first_name'];
					$data['to'] = $login;
					$code = md5($user['email'].time());
					$user['link'] = $this->config->item('client_base_url').'user/forgot_password/'.$code;

					$this->my_auth->set_forgot_code($user['user_id'], $code);
					
					Mail::prepare('client_forgot_password', $user);
					$data['success'] = 'For further instructions, please check your email';
					$tpl['title'] = $this->config->config['tp_title']."Forgot password";
					$tpl['content'] = $this->load->view('user/forgot_password.tpl', $data, TRUE);
					$this->load->view('login_template.tpl', $tpl);
				}
				else {
					$data['errors'] = 'Invalid user email';
					$tpl['title'] = $this->config->config['tp_title']."Forgot password";
					$tpl['content'] = $this->load->view('user/forgot_password.tpl', $data, TRUE);
					$this->load->view('login_template.tpl', $tpl);
				}
			}
			else {
				$tpl['title'] = $this->config->config['tp_title']."Forgot password";
				$tpl['content'] = $this->load->view('user/forgot_password.tpl', $data, TRUE);
				$this->load->view('login_template.tpl', $tpl);
			}
		}
	}
	public function confirm($code = '')
	{
		if($code) {
			$user = $this->my_auth->get_user_by_confirmation($code);
			if($user) {
				$this->my_auth->set_user_verified($user['user_id']);
				//PrepareMail::send('client_after_verify_email', $user['email'], $user['first_name']);
				redirect('/user');
			}
			else
				show_404();
		}
		else
			redirect('/');
	}	
	public function profile()
	{
		if(!Check::login())
			redirect('/user/login');			
		if(isset($_SESSION['userdata']['country'])) {
			$country = $this->assist_model->get_country($_SESSION['userdata']['country']);
			$_SESSION['userdata']['country_title'] = '';
			if(count($country))
				$_SESSION['userdata']['country_title'] = $country['title'];
		}
		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value) {
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		}
		$tpl['title'] = $this->config->config['tp_title']."Profile";
		$tpl['content'] = $this->load->view('user/profile.tpl', false, TRUE);
		$this->load->view('user_template.tpl', $tpl);
	}
	public function change_password()
	{
		if(!Check::login())
			redirect('/user/login');			
		$this->load->library('form_validation');
		$this->form_validation->set_rules('old', 'Current password', 'required');
		$this->form_validation->set_rules('new', 'New password', 'required|matches[confirm]');
		$this->form_validation->set_rules('confirm', 'Re-type new password', 'required');
		if($this->form_validation->run()) {
			if($this->my_auth->change_password($this->input->post('old', true),$this->input->post('new', true))) {
				$user = $this->my_auth->get_by_id($_SESSION['user_id']);
				$user['password'] = $this->input->post('new', true);
				//var_dump($user);
				Mail::prepare('client_change_password', $user);
				redirect('/user');
			}
			else {
				$data['errors'] = 'Old password incorrect';
			}
		}
		else {
			$data['errors'] = validation_errors();
		}		
		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value) {
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		}
		$tpl['title'] = $this->config->config['tp_title']."Change password";
		$tpl['content'] = $this->load->view('user/change_password.tpl', $data, TRUE);
		$this->load->view('user_template.tpl', $tpl);
	}
	public function edit_profile()
	{
		if(!Check::login())
			redirect('/user/login');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_user_email');
		if($this->form_validation->run()) {
			$data = array(
				'phone' => $this->input->post('phone', true),
				'email' => $this->input->post('email', true),
			);
			$this->my_auth->update_user($data, $_SESSION['user_id']);
			redirect('/user');
		}
		else {
			$data['errors'] = validation_errors();
		}
		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value) {
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		}
		//$data['payment_orders'] = $this->order_model->check_payment_orders($_SESSION['user_id']);
		$tpl['title'] = $this->config->config['tp_title']."Change password";
		$tpl['content'] = $this->load->view('user/edit_profile.tpl', $data, TRUE);
		$this->load->view('user_template.tpl', $tpl);
	}
	public function logout()
	{
		$this->my_auth->logout();
		redirect($this->config->item('base_url'));
	}
	public function logoutall()
	{
		$data = array(
			'salt' => md5($_SESSION['user_id'].time())
			);
		$this->my_auth->update_user($data, $_SESSION['user_id']);
		$user = $this->my_auth_model->get_by_id($_SESSION['user_id']);
		$this->my_auth->set_user_session($user);
		redirect($_SERVER['HTTP_REFERER']);
	}
	public function order_details($order_id = null)
	{
		if(!Check::login())
			redirect('/user/login');			
		if($order_id) {
			$data = array();
			$data['order'] = $this->order_model->get($order_id);
			if($data['order']) {
				if($data['order']['customer_id'] != $_SESSION['user_id'])
					redirect('/user');
				if($data['order']['file']) {
					$data['order']['file'] = $this->order_model->get_file($data['order']['file']);
				}
				if($data['order']['payment_id'])
					$data['payment'] = $this->payment_model->get($data['order']['payment_id']);
				else
					$data['payment'] = '';

				$data['order']['client_date_end'] = date("M j h:i A", strtotime($data['order']['date_end']));
                if($data['order']['payment_id']) {
                    switch ($data['order']['status_client']) {
                        case 'Writer’s Review':
                            $data['order']['status_client'] = 'Work in progress';
                            break;
                        case 'In progress':
                            $data['order']['status_client'] = 'Work in progress';
                            break;
                        case 'Delivered':
                            $data['order']['status_client'] = 'Draft submitted';
                            break;
                        case 'Revision in progress':
                            $data['order']['status_client'] = 'Revision';
                            break;
                        case 'Canceled':
                            $data['order']['status_client'] = 'Canceled';
                            break;
                        case 'Approved':
                            $data['order']['status_client'] = 'Completed';
                            break;
                        default:
                            $data['order']['status_client'] = '';
                            break;
                    }
                }
                else {
                    if($data['order']['status_client'] == 'Canceled')
                        $data['order']['status_client'] = 'Canceled';
                    else
                        $data['order']['status_client'] = 'Waiting for payment';
                }
				//$this->order_model->set_read_order_messages($order_id);
				$deadline = $this->order_model->get_deadline_by_id($data['order']['deadline']);
				if($deadline)
					$data['order']['deadline'] = $deadline['title'];
				else
					$data['order']['deadline'] = '';

				if($data['order']['status'] != 'Completed' && $data['order']['status'] != 'Canceled') {
					if($deadline) {
						if(!$data['order']['payment_id'])
							$data['order']['date_end'] = date("Y-m-d H:i:s",time()+$deadline['time']);
					}
					$date = explode(' ', $data['order']['date_end']);
					$first = explode('-', $date[0]);
					$second = explode(':', $date[1]);
					$data['timer'] = array(
						'y' => $first[0],
						'm' => $first[1],
						'd' => $first[2],
						'H' => $second[0],
						'i' => $second[1],
						's' => $second[2]
					);
				}
				$academic_level = $this->order_model->get_academic_level_by_id($data['order']['academic_level']);
				$data['order']['academic_level'] = $academic_level['title'];
				 
				$paper_type = $this->order_model->get_paper_type_by_id($data['order']['paper_type']);
				$data['order']['paper_type'] = $paper_type['title'];

				$subject = $this->order_model->get_subject_by_id($data['order']['subject']);
				$data['order']['subject'] = $subject['title'];
				
				$paper_format = $this->order_model->get_paper_format_by_id($data['order']['paper_format']);
				if($paper_format)
					$data['order']['paper_format'] = $paper_format['title'];
				else
					$data['order']['paper_format'] = '';

				$messages = $this->order_model->get_order_messages($data['order']['order_id'], $_SESSION['user_id']);
				//$user_messages = $this->order_model->get_user_order_messages($data['order']['order_id'], $_SESSION['user_id']);
				//$messages = array_merge($messages, $user_messages);
				$files = $this->order_model->get_user_order_files($data['order']['order_id'], $_SESSION['user_id']);
				$records = array_merge($messages, $files);
				uasort($records, array($this,"myCmp"));
				foreach ($records as $key => $record) {
					//from
					if($record['sender_id'] == $_SESSION['user_id'])
						$from = 'Me';
					elseif($record['sender_id'] == '1')
						$from = 'SUPPORT';
					elseif($record['sender_id'] == '0')
						$from = 'GENERAL';
					else
						$from = 'Writer #'.$record['sender_id'];

					//to
					if($record['receiver_id'] == $_SESSION['user_id'])
						$to = 'Me';
					elseif($record['receiver_id'] == '1')
						$to = 'SUPPORT';
					elseif($record['receiver_id'] == '0')
						$to = 'GENERAL';
					else
						$to = 'Writer #'.$record['receiver_id'];
					$records[$key]['from'] = $from;
					$records[$key]['to'] = $to;

					if(isset($records[$key]['attached_file']) && $records[$key]['attached_file']) {
						$records[$key]['file'] = $this->order_model->get_file($records[$key]['attached_file'], 0);
						$img = '';
						switch ($records[$key]['file']['file_ext']) {
							case '.jpg':
							case '.jpeg':
							case '.png':
							case '.bmp':
								$img = 'img.png';
							break;
							case '.xls':
							case '.xlsx':
								$img = 'xls.png';
							break;
							case '.doc':
							case '.docx':
							$img = 'doc.png';
							break;
							case '.pdf':
								$img = 'pdf.png';
							break;
							default:
								$img = '';
							break;
						}	
						$records[$key]['file']['image'] = $img;
					}
					if(isset($records[$key]['file_id'])) {
						$img = '';
						switch ($records[$key]['file_ext']) {
							case '.jpg':
							case '.jpeg':
							case '.png':
							case '.bmp':
								$img = 'img.png';
							break;
							case '.xls':
							case '.xlsx':
								$img = 'xls.png';
							break;
							case '.doc':
							case '.docx':
							$img = 'doc.png';
							break;
							case '.pdf':
								$img = 'pdf.png';
							break;
							default:
								$img = '';
							break;
						}	
						$records[$key]['image'] = $img;
					}
				}
				$data['order']['records'] = $records;
/*
				$files = $this->order_model->get_order_files($data['order']['order_id']);
				$user_files = $this->order_model->get_user_order_files($data['order']['order_id'], $_SESSION['user_id']);
				$files = array_merge($files, $user_files);
				uasort($files, array($this,"myCmp"));
				if($files) {
					foreach ($files as $file) {
						//from
						if($file['sender_id'] == $data['order']['customer_id'])
							$from = 'Me';
						elseif($file['sender_id'] == '1')
							$from = 'SUPPORT';
						elseif($file['sender_id'] == $data['order']['writer_id'])
							$from = 'Writer #'.$file['sender_id'];
						else
							$from = 'SUPPORT';
						//to
						if($file['receiver_id'] == $data['order']['customer_id'])
							$to = 'Me';
						elseif($file['receiver_id'] == '1')
							$to = 'SUPPORT';
						elseif($file['receiver_id'] == $data['order']['writer_id'])
							$to = 'Writer #'.$file['receiver_id'];
						else
							$to = 'General';

						$img = '';
						switch ($file['file_ext']) {
							case '.jpg':
							case '.jpeg':
							case '.png':
							case '.bmp':
								$img = 'img.png';
							break;
							case '.xls':
							case '.xlsx':
								$img = 'xls.png';
							break;
							case '.doc':
							case '.docx':
								$img = 'xls.png';
							break;
							case '.pdf':
								$img = 'pdf.png';
							break;
							default:
								$img = '';
								break;
						}

						$data['order']['files'][] = array(
							'file_id' => $file['file_id'],
							'sender_id' => $file['sender_id'],
							'receiver_id' => $file['receiver_id'],
							'from' => $from,
							'to' => $to,
							'image' => $img,
							'client_name' => $file['client_name'],
							'filetype' => $file['file_ext'],
							'filename' => $file['file_name'],
							'date' => date("M j h:i A", strtotime($file['date'])),
							'file' => $file['file_path']
						);
					}
				}
				else
					$data['order']['files'] = array();
//*/					
			}
			else
				redirect('/user');
			$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
			foreach ($tpl['sidebar_orders'] as $key => $value) {
				$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
			}
			$tpl['content'] = $this->load->view('user/order_details.tpl', $data, TRUE);
			$tpl['title'] = $this->config->config['tp_title']."Order #".$order_id." details";
			$this->load->view('user_template.tpl', $tpl);		
		}
	}
}