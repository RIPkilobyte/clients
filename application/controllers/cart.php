<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller {
	private $redirect_url = 'http://eagleediting.co/cart/paypal/';
	private $return_url = 'http://eagleediting.co/cart/complete_paypal/';
	private $whitehorse_url = 'http://eagle-essays.com/';
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('order_model');
		$this->load->model('payment_model');
		$this->load->model('assist_model');
	}	
	public function index()
	{
		redirect('/user');
	}
	public function paypal($order_id = '')
	{
		redirect($this->redirect_url.''.$order_id);
		/*
		if($order_id) {
			$order = $this->order_model->get($order_id);
			if($order) {
				$price = $order['price'];
				$requestParams = array(
					'RETURNURL' => $this->return_url,
					'CANCELURL' => $this->return_url,
					'PAYMENTREQUEST_0_AMT' => $price,
					'PAYMENTREQUEST_0_ITEMAMT' => $price,
					'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
					'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
					'L_PAYMENTREQUEST_0_NAME0' => 'Order #'.$order_id,
					'L_PAYMENTREQUEST_0_AMT0' => $price,
				);
				require_once ('assets/paypal.php');
				$paypal = new Paypal();
				$response = $paypal->request('SetExpressCheckout', $requestParams);
				if(is_array($response) && $response['ACK'] == 'Success') {
					$token = $response['TOKEN'];
					$this->order_model->update($order_id, array('token'=>$token));
					header('Location: https://www.paypal.com/webscr?cmd=_express-checkout&useraction=commit&token='.urlencode($token));
				}
			}
		}
		if($_SERVER['HTTP_REFERER'])
			redirect($_SERVER['HTTP_REFERER']);
		//*/
	}
	public function complete_paypal()
	{
		$this->assist_model->save_log(array('text' => serialize($_POST)));
		$this->assist_model->save_log(array('text' => serialize($_GET)));
		if(isset($_GET['token']) && !empty($_GET['token']) && !empty($_GET['PayerID'])) {
			require_once ('assets/paypal.php');
			$paypal = new Paypal();
			$token = $_GET['token'];

			$checkoutDetails = $paypal -> request('GetExpressCheckoutDetails', array('TOKEN' => $token));

			if(!isset($checkoutDetails['PAYMENTREQUEST_0_AMT']))
				$checkoutDetails['PAYMENTREQUEST_0_AMT'] = 0;
			$requestParams = array(
				'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
				'PAYERID' => $_GET['PayerID'],
				'TOKEN' => $token,
				'PAYMENTREQUEST_0_AMT' => $checkoutDetails['PAYMENTREQUEST_0_AMT'],
			);

			$response = $paypal->request('DoExpressCheckoutPayment',$requestParams);
			$request = array();
			$request['GetExpressCheckoutDetails'] = '';
			foreach ($checkoutDetails as $key => $value) {
				$request['GetExpressCheckoutDetails'] .= "&$key=$value";
			}
			$request['DoExpressCheckoutPayment'] = '';
			foreach ($response as $key => $value) {
				$request['DoExpressCheckoutPayment'] .= "&$key=$value";
			}

			$order = $this->order_model->get_by_token($token);
			if($order) {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				if(!isset($checkoutDetails['PAYMENTREQUEST_0_TRANSACTIONID']))
					$checkoutDetails['PAYMENTREQUEST_0_TRANSACTIONID'] = $token;
				if(!isset($response['PAYMENTINFO_0_TRANSACTIONTYPE']))
					$response['PAYMENTINFO_0_TRANSACTIONTYPE'] = 'expresscheckout';
				if(!isset($checkoutDetails['AMT']))
					$checkoutDetails['AMT'] = $order['price'];
				$status = 0;
				if(is_array($response) && $response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning')
					$status = 1;
				$data = array(
					'system' => 'Paypal',
					'type' => $response['PAYMENTINFO_0_TRANSACTIONTYPE'],
					'transaction_id' => $checkoutDetails['PAYMENTREQUEST_0_TRANSACTIONID'],
					'status' => $status,
					'user_id' => $order['customer_id'],
					'order_id' => $order['order_id'],
					'amount' => $checkoutDetails['AMT'],
					'date' => date('Y-m-d H:i:s'),
					'details' => $request['GetExpressCheckoutDetails'].$request['DoExpressCheckoutPayment']
				);
				$payment_id = $this->payment_model->save($data);
				if(is_array($response) && $response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning') {
					$user['order_id'] = $order['order_id'];
					$this->pay_order($order['order_id'], $payment_id);

					$loyality = round($order['price'] - $data['amount'],2);
					$this->order_model->update($order['order_id'], array('loyality'=>$loyality));

					$amount = $user['amount'];	
					$amount = round($amount - $loyality, 2);
					$discount = array('amount' => $amount);
					$user['amount'] = $amount;
					$this->my_auth->update_user($discount, $user['user_id']);

					if($order['discount_code'])
						$this->give_user_money($order);
					$text = Mail::text_body('client_receive_payment', $user);
					redirect('/cart/completed/'.$order['order_id']);
				}
			}
			redirect('/cart/not_completed/'.$order['order_id']);
		}
		redirect('/');
	}
	public function completed($order_id = '')
	{
		if($order_id) {
			$order = $this->order_model->get($order_id);
			if($order) {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				if($user['role'] == $this->config->item('sitetype')) {
					$this->my_auth->logintry($user['email'], $user['password']);
					$data = array();
					$data['order'] = $order;
					$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
					foreach ($tpl['sidebar_orders'] as $key => $value) {
						$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
					}
						
					$tpl['title'] = $this->config->config['tp_title']."Payment successfull";
					$tpl['content'] = $this->load->view('payment/complete.tpl', $data, true);
					$this->load->view('user_template.tpl', $tpl);
					return null;
				}
				else {
					redirect($this->whitehorse_url.'cart/completed/'.$order_id);
				}
			}
		}
		redirect('/');
	}
	public function not_completed($order_id = '')
	{
		if($order_id) {
			$order = $this->order_model->get($order_id);
			if($order) {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				if($user['role'] == $this->config->item('sitetype')) {
					$this->my_auth->logintry($user['email'], $user['password']);
					$data = array();
					$data['order'] = $order;
					$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
					foreach ($tpl['sidebar_orders'] as $key => $value) {
						$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
					}
						
					$tpl['title'] = $this->config->config['tp_title']."Payment not completed";
					$tpl['content'] = $this->load->view('payment/not_completed.tpl', $data, true);
					$this->load->view('user_template.tpl', $tpl);
					return null;
				}
				else {
					redirect($this->whitehorse_url.'cart/not_completed/'.$order_id);
				}
			}
		}
		if(Check::login()) {
			$data = array();
			$data['order'] = $order;
			$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
			foreach ($tpl['sidebar_orders'] as $key => $value) {
				$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
			}
			$tpl['title'] = $this->config->config['tp_title']."Payment not completed";
			$tpl['content'] = $this->load->view('payment/not_completed.tpl', $data, true);
			$this->load->view('user_template.tpl', $tpl);
			return null;
		}
		redirect('/');
	}
	public function pay($order_id)
	{
		if(!Check::login())
			redirect('/user/login');
		$order = $this->order_model->get($order_id);
		if($order && $order['customer_id'] == $_SESSION['user_id']) {
			$tmp_price = $price = $order['price'];
			$amount = $_SESSION['userdata']['amount'];
			
			if($amount > $tmp_price) {
				$payment = array(
					'system' => 'Loyality Code',
					'type' => 'Code',
					'transaction_id' => 0,
					'status' => 1,
					'user_id' => $order['customer_id'],
					'order_id' => $order_id,
					'amount' => $price,
					'date' => date('Y-m-d H:i:s'),
					'details' => 'discount_code='.$order['discount_code']
				);
				$this->load->model('payment_model');
				$payment_id = $this->payment_model->save($payment);
				$this->payment_model->pay_order($order_id, $payment_id);

				$amount = round($amount - $price,2);
				$discount = array(
					'amount' => $amount
				);
				$_SESSION['userdata']['amount'] = $amount;
				$this->my_auth->update_user($discount, $_SESSION['user_id']);
				
				redirect('/user/order_details/'.$order_id);
			}
			elseif($amount == $price) {
				$payment = array(
					'system' => 'Loyality Code',
					'type' => 'Code',
					'transaction_id' => 0,
					'status' => 1,
					'user_id' => $order['customer_id'],
					'order_id' => $order_id,
					'amount' => $price,
					'date' => date('Y-m-d H:i:s'),
					'details' => 'discount_code='.$order['discount_code']
				);
				$this->load->model('payment_model');
				$payment_id = $this->payment_model->save($payment);
				$this->payment_model->pay_order($order_id, $payment_id);

				$amount = round($amount - $price,2);
				$discount = array(
					'amount' => 0
				);
				$_SESSION['userdata']['amount'] = $amount;
				$this->my_auth->update_user($discount, $_SESSION['user_id']);
				
				redirect('/user/order_details/'.$order_id);
			}
			elseif($amount < $tmp_price) {
				$price = round($tmp_price - $amount,2);
			}
				
			$requestParams = array(
				'RETURNURL' => $this->return_url,
				'CANCELURL' => $this->return_url,
				'PAYMENTREQUEST_0_AMT' => $price,
				'PAYMENTREQUEST_0_ITEMAMT' => $price,
				'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
				'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
				'L_PAYMENTREQUEST_0_NAME0' => 'Order #'.$order_id,
				'L_PAYMENTREQUEST_0_AMT0' => $price,
			);
			require_once ('assets/paypal.php');
			$paypal = new Paypal();
			$response = $paypal->request('SetExpressCheckout', $requestParams);
			if(is_array($response) && $response['ACK'] == 'Success') {
				$token = $response['TOKEN'];
				$this->order_model->update($order_id, array('token'=>$token));
				header('Location: https://www.paypal.com/webscr?cmd=_express-checkout&useraction=commit&token='.urlencode($token));
			}
			else
				die('000');
		}
		else
			die('1111');
	}
	public function pay_card()
	{
		$json['status'] = 'fail';
		if(Check::login()) {
			$order_id = $this->input->post('order_id', true);
			$order = $this->order_model->get($order_id);
			if($order && $order['customer_id'] == $_SESSION['user_id']) {
				$user = $this->my_auth->get_by_id($_SESSION['user_id']);

				$country = $this->assist_model->get_country($user['country']);

			$tmp_price = $price = $order['price'];
			$amount = $_SESSION['userdata']['amount'];
			
			if($amount > $tmp_price) {
						$payment = array(
							'system' => 'Loyality Code',
							'type' => 'Code',
							'transaction_id' => 0,
							'status' => 1,
							'user_id' => $order['customer_id'],
							'order_id' => $order_id,
							'amount' => $price,
							'date' => date('Y-m-d H:i:s'),
							'details' => 'discount_code='.$order['discount_code']
						);
						$this->load->model('payment_model');
						$payment_id = $this->payment_model->save($payment);
						$this->payment_model->pay_order($order_id, $payment_id);

				$amount = round($amount - $price,2);
				$discount = array(
					'amount' => $amount
				);
				$_SESSION['userdata']['amount'] = $amount;
				$this->my_auth->update_user($discount, $_SESSION['user_id']);
				
				$json['status'] = 'ok';
				echo json_encode($json);
				exit(0);
			}
			elseif($amount == $price) {
						$payment = array(
							'system' => 'Loyality Code',
							'type' => 'Code',
							'transaction_id' => 0,
							'status' => 1,
							'user_id' => $order['customer_id'],
							'order_id' => $order_id,
							'amount' => $price,
							'date' => date('Y-m-d H:i:s'),
							'details' => 'discount_code='.$order['discount_code']
						);
						$this->load->model('payment_model');
						$payment_id = $this->payment_model->save($payment);
						$this->payment_model->pay_order($order_id, $payment_id);

				$amount = round($amount - $price,2);
				$discount = array(
					'amount' => 0
				);
				$_SESSION['userdata']['amount'] = $amount;
				$this->my_auth->update_user($discount, $_SESSION['user_id']);
				
				$json['status'] = 'ok';
				echo json_encode($json);
				exit(0);

			}
			elseif($amount < $tmp_price) {
				$price = round($tmp_price - $amount,2);
			}

				$requestParams = array(
					'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
					'PAYMENTACTION' => 'Sale',

					'CREDITCARDTYPE' => $this->input->post('card_type', true),
					'ACCT' => $this->input->post('card_number', true),
					'EXPDATE' => $this->input->post('card_exp_month', true).$this->input->post('card_exp_year', true),
					'CVV2' => $this->input->post('card_cvv2', true),

					'FIRSTNAME' => $user['first_name'],
					'LASTNAME' => $user['last_name'],
					'COUNTRYCODE' => trim($this->input->post('country', true)),
					'STATE' => trim($this->input->post('state', true)),
					'CITY' => trim($this->input->post('city', true)),
					'STREET' => trim($this->input->post('street', true)),
					'ZIP' => trim($this->input->post('zip', true)),

					'AMT' => $price,
					'ITEMAMT' => $price,
					'SHIPPINGAMT' => '0',
					'CURRENCYCODE' => 'USD',

					'L_NAME0' => 'Order #'.$order['order_id'],
					'L_DESC0' => '',
					'L_AMT0' => $price,
					'L_QTY0' => '1',
				);
				require_once ('assets/paypal.php');
				$paypal = new Paypal();
				$response = $paypal->request('DoDirectPayment', $requestParams);

				$request = '';
				foreach ($response as $key => $value) {
					$request .= "&$key=$value";
				}
				if(!isset($response['TRANSACTIONID']))
					$response['TRANSACTIONID'] = $response['CORRELATIONID'];
				if(!isset($response['AMT']))
					$response['AMT'] = $order['price'];
				$status = 0;
				if(is_array($response) && $response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning')
					$status = 1;
				$data = array(
					'system' => 'Paypal Pro',
					'type' => 'Paypal Pro Card',
					'transaction_id' => $response['TRANSACTIONID'],
					'status' => $status,
					'user_id' => $order['customer_id'],
					'order_id' => $order['order_id'],
					'amount' => $response['AMT'],
					'date' => date('Y-m-d H:i:s'),
					'details' => $request
				);
				$payment_id = $this->payment_model->save($data);

				if(is_array($response) && $response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning') { 
					$user['order_id'] = $order['order_id'];
					$this->pay_order($order['order_id'], $payment_id);

					$loyality = round($order['price'] - $data['amount'],2);
					$this->order_model->update($order['order_id'], array('loyality'=>$loyality));

				$amount = $_SESSION['userdata']['amount'];	
				$amount = round($amount - $loyality,2);
				$discount = array(
					'amount' => $amount
				);
				$_SESSION['userdata']['amount'] = $amount;
				$this->my_auth->update_user($discount, $_SESSION['user_id']);

					if($order['discount_code'])
						$this->give_user_money($order);
					$text = Mail::text_body('client_receive_payment', $user);
					$json['status'] = 'ok';
				}
				elseif(is_array($response) && $response['ACK'] == 'Failure' || $response['ACK'] == 'FailureWithWarning') {
					$json['status'] = 'failure';
				}
			}

		}
		echo json_encode($json);
	}
	public function complete()
	{
		$this->assist_model->save_log(array('text' => serialize($_POST)));
		$this->assist_model->save_log(array('text' => serialize($_GET)));

		if(isset($_GET['token']) && !empty($_GET['token']) && !empty($_GET['PayerID'])) {
			require_once ('assets/paypal.php');
			$paypal = new Paypal();
			$token = $_GET['token'];

			$checkoutDetails = $paypal -> request('GetExpressCheckoutDetails', array('TOKEN' => $token));

			if(!isset($checkoutDetails['PAYMENTREQUEST_0_AMT']))
				$checkoutDetails['PAYMENTREQUEST_0_AMT'] = 0;
			$requestParams = array(
				'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
				'PAYERID' => $_GET['PayerID'],
				'TOKEN' => $token,
				'PAYMENTREQUEST_0_AMT' => $checkoutDetails['PAYMENTREQUEST_0_AMT'],
			);

			$response = $paypal -> request('DoExpressCheckoutPayment',$requestParams);
			$request = array();
			$request['GetExpressCheckoutDetails'] = '';
			foreach ($checkoutDetails as $key => $value) {
				$request['GetExpressCheckoutDetails'] .= "&$key=$value";
			}
			$request['DoExpressCheckoutPayment'] = '';
			foreach ($response as $key => $value) {
				$request['DoExpressCheckoutPayment'] .= "&$key=$value";
			}

			$order = $this->order_model->get_by_token($token);
			if($order) {
				$user = $this->my_auth->get_by_id($order['customer_id']);
				if(!isset($checkoutDetails['PAYMENTREQUEST_0_TRANSACTIONID']))
					$checkoutDetails['PAYMENTREQUEST_0_TRANSACTIONID'] = $token;
				if(!isset($response['PAYMENTINFO_0_TRANSACTIONTYPE']))
					$response['PAYMENTINFO_0_TRANSACTIONTYPE'] = 'expresscheckout';
				if(!isset($checkoutDetails['AMT']))
					$checkoutDetails['AMT'] = $order['price'];
				$status = 0;
				if(is_array($response) && $response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning')
					$status = 1;
				$data = array(
					'system' => 'Paypal',
					'type' => $response['PAYMENTINFO_0_TRANSACTIONTYPE'],
					'transaction_id' => $checkoutDetails['PAYMENTREQUEST_0_TRANSACTIONID'],
					'status' => $status,
					'user_id' => $order['customer_id'],
					'order_id' => $order['order_id'],
					'amount' => $checkoutDetails['AMT'],
					'date' => date('Y-m-d H:i:s'),
					'details' => $request['GetExpressCheckoutDetails'].$request['DoExpressCheckoutPayment']
				);
				$payment_id = $this->payment_model->save($data);
				if(is_array($response) && $response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning') {
					$user['order_id'] = $order['order_id'];
					$this->pay_order($order['order_id'], $payment_id);

					$loyality = round($order['price'] - $data['amount'],2);
					$this->order_model->update($order['order_id'], array('loyality'=>$loyality));

				$amount = $_SESSION['userdata']['amount'];	
				$amount = round($amount - $loyality, 2);
				$discount = array(
					'amount' => $amount
				);
				$_SESSION['userdata']['amount'] = $amount;
				$this->my_auth->update_user($discount, $_SESSION['user_id']);

					if($order['discount_code'])
						$this->give_user_money($order);
					$text = Mail::text_body('client_receive_payment', $user);
					if(!Check::login())
						redirect('/user/login');
					$data = array();
					$data['order'] = $order;
					$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
					foreach ($tpl['sidebar_orders'] as $key => $value) {
						$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
					}
					
					$tpl['title'] = $this->config->config['tp_title']."Payment successfull";
					$tpl['content'] = $this->load->view('payment/complete.tpl', $data, true);
					$this->load->view('user_template.tpl', $tpl);
					return null;
				}
			}
		}

		if(!Check::login())
			redirect('/user/login');
		$data = array();
		$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
		foreach ($tpl['sidebar_orders'] as $key => $value) {
			$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
		}
		$tpl['title'] = $this->config->config['tp_title']."Payment not completed";
		$tpl['content'] = $this->load->view('payment/not_completed.tpl', $data, true);
		$this->load->view('user_template.tpl', $tpl);
	}
	public function wepaycallback()
	{
		$this->assist_model->save_log(array('text' => 'cb-post:'.serialize($_POST)));
		$this->assist_model->save_log(array('text' => 'cb-get:'.serialize($_GET)));
	}
	public function checkout($order_id)
	{
		redirect('/user/order_details/'.$order_id);
		$this->assist_model->save_log(array('text' => 'c-post'.serialize($_POST)));
		$this->assist_model->save_log(array('text' => 'c-get'.serialize($_GET)));
		$data = array();
		if(!Check::login())
			redirect('/user/login');
		$order = $this->order_model->get($order_id);
		if($order_id && $order && $order['customer_id'] == $_SESSION['user_id']) {


			if($order['payment_id'])
				redirect('/user/order_details/'.$order_id);
			
			$data['order_id'] = $order_id;
			$data['order'] = $order;
	
			if($data['order']['file']) {
				$data['order']['file'] = $this->order_model->get_file($data['order']['file']);
			}

			$academic_level = $this->order_model->get_academic_level_by_id($data['order']['academic_level']);
			if($academic_level)
				$data['order']['academic_level'] = $academic_level['title'];
			else
				$data['order']['academic_level'] = '';

			$paper_type = $this->order_model->get_paper_type_by_id($data['order']['paper_type']);
			if($paper_type)
				$data['order']['paper_type'] = $paper_type['title'];
			else
				$data['order']['paper_type'] = '';

			$subject = $this->order_model->get_subject_by_id($data['order']['subject']);
			if($subject)
				$data['order']['subject'] = $subject['title'];
			else
				$data['order']['subject'] = '';
			
			$paper_format = $this->order_model->get_paper_format_by_id($data['order']['paper_format']);
			if($paper_format)
				$data['order']['paper_format'] = $paper_format['title'];
			else
				$data['order']['paper_format'] = '';

			$deadline = $this->order_model->get_deadline_by_id($data['order']['deadline']);
			if($deadline)
				$data['order']['deadline'] = $deadline['title'];
			else
				$data['order']['deadline'] = '';	

			$data['countries'] = $this->assist_model->get_countries();
			$data['user_country'] = $this->assist_model->get_country($_SESSION['userdata']['country']);

			$tpl['sidebar_orders'] = $this->order_model->get_order_by_type($_SESSION['user_id'], "'Needs confirmation', 'Confirmed', 'In progress','File submitted','File delivered','Revision in progress'");
			foreach ($tpl['sidebar_orders'] as $key => $value) {
				$tpl['sidebar_orders'][$key]['unread_messages'] = $this->order_model->count_unread_messages($value['order_id'], $_SESSION['user_id']);
			}
			$tpl['title'] = $this->config->config['tp_title']."Checkout Order #".$order_id;
			if($order_id == 1353) {
				$tpl['content'] = $this->load->view('payment/checkout_iframe.tpl', $data, true);
			}
			else {
				$tpl['content'] = $this->load->view('payment/checkout.tpl', $data, true);
			}
			$this->load->view('user_template.tpl', $tpl);
		}
		else
			redirect('/user');
	}
	public function ipn()
	{
		$this->assist_model->save_log(array('text' => serialize($_POST)));
		//$this->assist_model->save_log(array('text' => 'ipn1307'.serialize($_GET)));
		if(!$_POST)
			exit(0);
		$this->assist_model->save_log(array('text' => 'ipn'.serialize($_POST)));
		//$this->assist_model->save_log(array('text' => serialize($_GET)));
		define("DEBUG", 0);
		define("USE_SANDBOX", 0);
		$amount = '';
		$transaction_id = '';
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($_POST as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			if($key == 'txn_id')
				$transaction_id = $value;
			if($key == 'payment_gross')
				$amount = $value;
			$req .= "&$key=$value";
		}
		if(USE_SANDBOX == true) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}

		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		$this->assist_model->save_log(array('text' => serialize($req)));
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if(DEBUG == true) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		$res = curl_exec($ch);
		curl_close($ch);

		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		$this->assist_model->save_log(array('text' => 'ipn'.serialize($res)));
		$status = 0;
		if(strcmp ($res, "VERIFIED") == 0) {
			$status = 1;
		}
		$this->assist_model->save_log(array('text' => 'ipn'.serialize($status)));
		$order_id = $this->input->post('custom', true);
		$this->assist_model->save_log(array('text' => 'ipn'.serialize($order_id)));
		if($order_id) {
			$order = $this->order_model->get($order_id);
			if($order) {
				$data = array(
					'system' => 'Paypal Pro',
					'type' => 'Paypal Pro Card',
					'transaction_id' => $transaction_id,
					'status' => $status,
					'user_id' => $order['customer_id'],
					'order_id' => $order['order_id'],
					'amount' => $amount,
					'date' => date('Y-m-d H:i:s'),
					'details' => $res
				);
				$payment_id = $this->payment_model->save($data);
				if($status)
					$this->pay_order($order['order_id'], $payment_id);
				//redirect('/user/order_details/'+$order['order_id']);
			}
			//redirect('/user');
		}
		//redirect('/user/login');
	}
	private function pay_order($order_id, $payment_id)
	{
		$this->payment_model->pay_order($order_id, $payment_id);
	}
	private function give_user_money($order)
	{
		$code = $order['discount_code'];
		if($code) {
			$cod = substr($code, -2);
			if($cod == '02') {
				$user_id = substr($code, 0, -2);
				$user = $this->my_auth->get_by_id($user_id);
				if($user) {
					$discount = array(
						'amount' => $amount
					);			
					$this->my_auth->update_user($discount, $user_id);
				}
			}
		}
	}
	public function receipt_card()
	{
		if(!$_POST)
			exit(0);
		$this->assist_model->save_log(array('text' => serialize($_POST)));
		//$this->assist_model->save_log(array('text' => serialize($_GET)));
		define("DEBUG", 0);
		define("USE_SANDBOX", 0);
		/*
		$raw_post_data = $_POST;
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		//*/
		$amount = '';
		$transaction_id = '';
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($_POST as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			if($key == 'txn_id')
				$transaction_id = $value;
			if($key == 'payment_gross')
				$amount = $value;
			$req .= "&$key=$value";
		}
		if(USE_SANDBOX == true) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}

		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		//$this->assist_model->save_log(array('text' => serialize($req)));
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if(DEBUG == true) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		$res = curl_exec($ch);
		curl_close($ch);

		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		//$this->assist_model->save_log(array('text' => serialize($res)));
		$status = 0;
		if(strcmp ($res, "VERIFIED") == 0) {
			$status = 1;
		}
		//$this->assist_model->save_log(array('text' => serialize($status)));
		$order_id = $this->input->post('invoice', true);
		if($order_id) {
			$order = $this->order_model->get($order_id);
			if($order) {
				$data = array(
					'system' => 'Paypal Pro',
					'type' => 'Paypal Pro',
					'transaction_id' => $transaction_id,
					'status' => $status,
					'user_id' => $order['customer_id'],
					'order_id' => $order['order_id'],
					'amount' => $amount,
					'date' => date('Y-m-d H:i:s'),
					'details' => $res
				);
				$payment_id = $this->payment_model->save($data);
				if($status)
					$this->pay_order($order['order_id'], $payment_id);
				redirect('/user/order_details/'+$order['order_id']);
			}
			redirect('/user');
		}
		redirect('/user/login');
	}
}