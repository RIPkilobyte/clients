
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class AdminUsers_model extends CI_Model {
    
    function get($login) {
        $res = $this -> db -> where('login', $login) -> get('adminusers') -> row_array();
		if (count($res) > 0) return $res;
		return FALSE;
    }
	
	function update($login, $data) {
			$this->db->where('login', $login)->update('adminusers', $data);
    }
        
    function create($data) {
		if ($this->get($data['login']) == FALSE)
		{
			$this->db->insert('adminusers', $data);	
			return TRUE;
		}
		return FALSE;
    }
    
}