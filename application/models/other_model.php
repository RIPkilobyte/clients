<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Other_model extends CI_Model {
    
    function get_settings() {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> get('settings') -> result_array();
    }
    
    function save_settings() {
        $this->db->where('name', 'title')->where('site', $this->config->item('sitetype'))->update('settings', array('value'=>$this->input->post('title', TRUE)));
        $this->db->where('name', 'meta_keyword')->where('site', $this->config->item('sitetype'))->update('settings', array('value'=>$this->input->post('meta_keyword', TRUE)));
        $this->db->where('name', 'meta_description')->where('site', $this->config->item('sitetype'))->update('settings', array('value'=>$this->input->post('meta_description', TRUE)));
        $this->db->where('name', 'main_page_text')->where('site', $this->config->item('sitetype'))->update('settings', array('value'=>$this->input->post('main_page_text', TRUE)));
        //$this->db->where('name', 'video')->where('site', $this->config->item('sitetype'))->update('settings', array('value'=>$this->input->post('video')));
    }
    function get_description()
    {
    	$this->db->where('name', 'meta_description');
    	$record = $this->db->where('site', $this->config->item('sitetype'))->get('settings')->result_array();
    	if($record)
    		return $record[0];
    	else
    		return false;
    }
    function get_keywords()
    {
    	$this->db->where('name', 'meta_keyword');
    	$record = $this->db->where('site', $this->config->item('sitetype'))->get('settings')->result_array();
    	if($record)
    		return $record[0];
    	else
    		return false;
    }
    function get_main_page_text()
    {
        $this->db->where('name', 'main_page_text');
        $record = $this->db->where('site', $this->config->item('sitetype'))->get('settings')->result_array();
        if($record)
            return $record[0]['value'];
        else
            return false;
    }
    function get_video()
    {
        $this->db->where('name', 'video');
        $record = $this->db->where('site', $this->config->item('sitetype'))->get('settings')->result_array();
        if($record)
            return $record[0]['value'];
        else
            return false;
    }   
}