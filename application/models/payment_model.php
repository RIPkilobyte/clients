<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model {
	public function get($payment_id)
	{
		$this->db->where('payment_id', $payment_id);
		$records = $this->db->get('payments')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function save($data)
	{
		$this->db->insert('payments', $data);
		return $this->db->insert_id();
	}
	public function update($payment_id, $data)
	{
		$this->db->where('payment_id', $payment_id);
		$records = $this->db->get('payments')->result_array();
		if($records) {
			$this->db->where('payment_id', $payment_id);
			$this->db->update('payments', $data);
		}
		else {
			$data['payment_id'] = $payment_id;
			$this->db->insert('payments', $data);
		}
	}
	public function pay_order($order_id, $payment_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('orders', array('payment_id' => $payment_id));
	}
	public function save_currency($code, $rate)
	{
		$this->db->where('code', $code);
		$records = $this->db->get('currency')->result_array();
		if(count($records)) {
			$this->db->where('code', $code);
			$this->db->update('currency', array('rate' => $rate));
		}
		else {
			$this->db->insert('currency', array('code' => $code, 'rate' => $rate));
		}
	}
	public function get_currency($code)
	{
		$this->db->where('code', $code);
		$records = $this->db->get('currency')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();	
	}
	public function get_all_currencies()
	{
		$records = $this->db->get('currency')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
}