<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Assist_model extends CI_Model {
	function save_log($data)
    {
        $this->db->insert('logs', $data);
    }
    function get_countries()
    {
        $this->db->order_by('position asc, title asc');
    	return $this->db->get('countries')->result_array();
    }
    function get_country($id)
    {
    	$this->db->where('country_id', $id);
		$record = $this->db->get('countries')->result_array();
		if(count($record))
			return $record[0];
		else
			return false;    	
    }
    function get_academic_level($id)
    {
        $this->db->where('academic_level_id', $id);
        $record = $this->db->get('orders_academic_level')->result_array();
        if(count($record))
            return $record[0];
        else
            return false;       
    }
    function get_paper_format($id)
    {
        $query = 'SELECT title FROM cp_orders_paper_format WHERE paper_format_id IN ('.$id.')';
        //$this->db->where_in('paper_format_id', $id, FALSE);
        //$records = $this->db->get('orders_paper_format')->result_array();
        $records = $this->db->query($query)->result_array();
        if(count($records))
            return $records;
        else
            return false;       
    }
    function get_subjects($id)
    {
        $query = 'SELECT title FROM cp_orders_subject WHERE subject_id IN ('.$id.')';
        //$this->db->where_in('paper_format_id', $id, FALSE);
        //$records = $this->db->get('orders_paper_format')->result_array();
        $records = $this->db->query($query)->result_array();
        if(count($records))
            return $records;
        else
            return false;       
    } 
    function add_captcha($data)
    {
        $expire = time()-600;
        $this->db->where('captcha_time <', $expire);
        $this->db->delete('captcha');

        $this->db->insert('captcha', array(
            'captcha_time' => $data['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $data['word']
            ));
        $this->db->insert('captcha', array(
            'captcha_time' => $data['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => strtoupper($data['word'])
            ));        
        $this->db->insert('captcha', array(
            'captcha_time' => $data['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => strtolower($data['word'])
            ));        
    }
    function check_captcha($word)
    {
        $expire = time()-600;
        $this->db->where('captcha_time <', $expire);
        $this->db->delete('captcha');

        $this->db->where('word', $word);
        $this->db->where('ip_address', $this->input->ip_address());
        $this->db->where('captcha_time >', $expire);
        $records = $this->db->get('captcha')->result_array();
        if(count($records))
            return true;
        else
            return false;
    }
} 