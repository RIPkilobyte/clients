<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model {
	public function add($data)
	{
		$data['site'] = $this->config->item('sitetype');
		$this->db->insert('blog', $data);
		return $this->db->insert_id();
	}
	public function update($id, $data)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->update('blog', $data);
	}
	public function get($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$records = $this->db->get('blog')->result_array();
		if($records)
			return $records[0];
		else
			return array();
	}
	public function delete($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->delete('blog');
	}
	//-------------------------------------
	public function get_list()
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$records = $this->db->get('blog')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function count_records()
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->from('blog');
		return $this->db->count_all_results();
	}
	public function get_last_records($limit, $offset = 0,$order = 'date')
	{
		$this->db->where('site', $this->config->item('sitetype'));
		//$offset = $page * $limit;
		$this->db-> order_by($order.' desc');
		$this->db-> limit($limit, $offset);
		$records = $this->db->get('blog')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_by_href($href)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->limit(1);
		$this->db->where('href', $href);
		$records = $this->db->get('blog')->result_array();
		if($records)
			return $records[0];
		else
			return array();
	}
	//-------------------------------------
	public function add_comment($data)
	{
		$data['site'] = $this->config->item('sitetype');
		$this->db->insert('comments', $data);
		return $this->db->insert_id();
	}
	public function update_comment($id, $data)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('comment_id', $id);
		$this->db->update('comments', $data);
	}
	public function get_comment($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('comment_id', $id);
		$records = $this->db->get('comments')->result_array();
		if($records)
			return $records[0];
		else
			return array();
	}
	public function delete_comment($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('comment_id', $id);
		$this->db->delete('comments');
	}
	//-------------------------------------
	public function get_blog_comments($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$records = $this->db->get('comments')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function count_blog_comments($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->from('comments');
		return $this->db->count_all_results();
	}
	public function get_moderated_blog_comments($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->where('moderated = 1');
		$records = $this->db->get('comments')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function count_moderated_blog_comments($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->where('moderated = 1');
		$this->db->from('comments');
		return $this->db->count_all_results();
	}	
	public function get_unmoderated_blog_comments($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->where('moderated = 0');
		$records = $this->db->get('comments')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function count_unmoderated_blog_comments($id)
	{
		$this->db->where('site', $this->config->item('sitetype'));
		$this->db->where('blog_id', $id);
		$this->db->where('moderated = 0');
		$this->db->from('comments');
		return $this->db->count_all_results();
	}
}