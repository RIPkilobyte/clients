<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model {
	public function add($data)
	{
		$this->db->insert('orders', $data);
		return $this->db->insert_id();
	}
	public function get($id)
	{
		$this->db->where('order_id', $id);
		$record = $this->db->get('orders')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function update($order_id, $data)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('orders', $data);
	}
	public function delete($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->delete('orders');
	}

	public function get_my_orders($id)
	{
		$this->db->where('customer_id', $id);
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_discount_orders($discount)
	{
		$this->db->where('discount_code', $discount);
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}

	public function get_by_token($token)
	{
		$this->db->where('token', $token);
		$record = $this->db->get('orders')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function count_c_user_orders($user_id, $status)
	{
		$this->db->where('customer_id', $user_id);
		if($status != 'all')
			$this->db->where("status IN(".$status.")");
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return count($records);
		else
			return 0;
	}
	public function count_w_user_orders($user_id, $status)
	{
		$this->db->where('writer_id', $user_id);
		if($status == 'active')
			$this->db->where("status IN('In progress', 'File submitted', 'File delivered', 'Revision in progress')");
		else
			$this->db->where("status IN('Completed', 'Canceled')");
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return count($records);
		else
			return 0;
	}
	public function get_c_user_orders($user_id, $status, $field = 'order_id', $type = 'desc', $start, $limit)
	{
		$this->db->where('customer_id', $user_id);
		if($status != 'all')
			$this->db->where("status IN(".$status.")");
		$this->db->order_by($field, $type);
		$this->db->limit($limit, $start);
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}	
	public function get_w_user_orders($user_id, $status, $field = 'order_id', $type = 'desc', $start, $limit)
	{
		$this->db->where('writer_id', $user_id);
		if($status == 'active')
			$this->db->where("status IN('In progress', 'File submitted', 'File delivered', 'Revision in progress')");
		else
			$this->db->where("status IN('Completed', 'Canceled')");
		$this->db->order_by($field, $type);
		$this->db->limit($limit, $start);
		$records = $this->db->get('orders')->result_array();
		//echo $this->db->last_query();
		if(count($records))
			return $records;
		else
			return array();
	}	
	public function save_file($data)
	{
		$this->db->insert('files', $data);
		return $this->db->insert_id();
	}
	public function get_file($file_id, $moderated = 1)
	{
		$this->db->where('file_id', $file_id);
		if($moderated)
			$this->db->where('moderated', $moderated);
		$record = $this->db->get('files')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_last_file($order_id, $writer_id)//none
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('sender_id', $writer_id);
		$this->db->where('moderated = 1');
		$this->db->order_by('date desc');
		$record = $this->db->get('files')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}	
	public function update_file($file_id, $data)
	{
		$this->db->where('file_id', $file_id);
		$this->db->update('files', $data);
	}
	public function get_orders_for_search($field = 'order_id', $type = 'desc', $filter)
	{
		if($filter['filter_order'])
			$this->db->where('order_id', $filter['filter_order']);

		if($filter['filter_work'] == 'new')
			$this->db->where('work_type', 'new');
		elseif($filter['filter_work'] == 'edit')
			$this->db->where('work_type', 'edit');
		if($filter['filter_paper'])
			$this->db->where('paper_type', $filter['filter_paper']);
		if($filter['filter_subject'])
			$this->db->where('subject', $filter['filter_subject']);
		if($filter['filter_p_from'])
			$this->db->where('pages >', $filter['filter_p_from']);
		if($filter['filter_p_to'])
			$this->db->where('pages <', $filter['filter_p_to']);
		if($filter['filter_d_from'])
			$this->db->where('date_end_writer >', $filter['filter_d_from']);
		if($filter['filter_d_to'])
			$this->db->where('date_end_writer <', $filter['filter_d_to']);

		$this->db->where('moderated', '1');
		$this->db->where('writer_id', '0');
		$this->db->where("status IN('Confirmed')");
		$this->db->order_by($field, $type);
		$record = $this->db->get('orders')->result_array();
		if(count($record))
			return $record;
		else
			return array();
	}
	public function get_request_order($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('user_id', $user_id);
		$record = $this->db->get('orders_requests')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();		
	}
	public function accept_order($order_id, $writer_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('orders', array('writer_id'=>$writer_id));
	}
	public function apply_order($user_id, $order_id, $text, $price)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('user_id', $user_id);
		$record = $this->db->get('orders_requests')->result_array();
		if(count($record))
			return;
		else {
			$insert = array(
				'order_id' => $order_id,
				'user_id' => $user_id,
				'price' => $price,
				'text' => $text
			);
			$this->db->insert('orders_requests', $insert);
			return;
		}
	}
	public function get_order_by_type($customer_id, $status)
	{
		$this->db->where('customer_id', $customer_id);
		$this->db->where("status IN(".$status.")");
		$this->db->order_by('date_add desc');
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function add_order($data)
	{
		$this->db->insert('orders', $data);
		return $this->db->insert_id();
	}
	public function get_total_user_payment_orders($user_id)
	{
		$sql = "SELECT SUM(writer_price) as total FROM ".$this->db->dbprefix('orders')."
			WHERE 
				writer_id = ".$user_id." AND
				writer_payment_id = 0 AND
				status IN('Completed')
		";
		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return $records[0]['total'];
		else
			return 0;
	}
	public function count_user_payment_orders($user_id)
	{
		$this->db->select('order_id');
		$this->db->where('writer_id', $user_id);
		$this->db->where("status IN('Completed')");
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return count($records);
		else
			return 0;
	}
	public function get_user_payment_orders($user_id, $field = 'order_id', $type = 'desc', $start, $limit)
	{
		$this->db->where('writer_id', $user_id);
		$this->db->where("status IN('Completed')");
		$this->db->order_by($field, $type);
		$this->db->limit($limit, $start);
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function count_wuser_orders($user_id, $status)
	{
		$this->db->where('writer_id', $user_id);
		if($status == 'active')
			$this->db->where("status IN('In progress', 'File submitted', 'File delivered', 'Revision in progress')");
		else
			$this->db->where("status IN('Completed', 'Canceled')");
		$records = $this->db->get('orders')->result_array();
		//echo $this->db->last_query();
		if(count($records))
			return count($records);
		else
			return 0;
	}	
	public function get_user_orders($user_id, $status, $field = 'order_id', $type = 'desc', $start, $limit)
	{
		$this->db->where('writer_id', $user_id);
		if($status == 'active')
			$this->db->where("status IN('In progress', 'File submitted', 'File delivered', 'Revision in progress')");
		else
			$this->db->where("status IN('Completed', 'Canceled')");
		$this->db->order_by($field, $type);
		$this->db->limit($limit, $start);
		$records = $this->db->get('orders')->result_array();
		//echo $this->db->last_query();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_academic_level_by_id($id)
	{
		$this->db->where('academic_level_id', $id);
		$record = $this->db->get('orders_academic_level')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_academic_levels()
	{
		$this->db->order_by('position asc, title asc');
		$records = $this->db->get('orders_academic_level')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function update_academic_level($id, $data)
	{
		$this->db->where('academic_level_id', $id);
		$this->db->update('orders_academic_level', $data);
	}
	public function delete_academic_level($id)
	{
		$this->db->where('academic_level_id', $id);
		$this->db->delete('orders_academic_level');
	}
	public function add_academic_level($data)
	{
		$this->db->insert('orders_academic_level', $data);
		return $this->db->insert_id();
	}
	public function get_deadline_by_id($id)
	{
		$this->db->where('deadline_id', $id);
		$record = $this->db->get('orders_deadline')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_deadlines()
	{
		$this->db->order_by('position asc, title asc');
		$records = $this->db->get('orders_deadline')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function update_deadline($id, $data)
	{
		$this->db->where('deadline_id', $id);
		$this->db->update('orders_deadline', $data);
	}
	public function delete_deadline($id)
	{
		$this->db->where('deadline_id', $id);
		$this->db->delete('orders_deadline');
	}
	public function add_deadline($data)
	{
		$this->db->insert('orders_deadline', $data);
		return $this->db->insert_id();
	}
	public function get_paper_type_by_id($id)
	{
		$this->db->where('paper_type_id', $id);
		$record = $this->db->get('orders_paper_type')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_paper_types()
	{
		$this->db->order_by('position asc, title asc');
		$records = $this->db->get('orders_paper_type')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function update_paper_type($id, $data)
	{
		$this->db->where('paper_type_id', $id);
		$this->db->update('orders_paper_type', $data);
	}
	public function delete_paper_type($id)
	{
		$this->db->where('paper_type_id', $id);
		$this->db->delete('orders_paper_type');
	}
	public function add_paper_type($data)
	{
		$this->db->insert('orders_paper_type', $data);
		return $this->db->insert_id();
	}
	public function get_subject_by_id($id)
	{
		$this->db->where('subject_id', $id);
		$record = $this->db->get('orders_subject')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_subjects()
	{
		$this->db->order_by('position asc, title asc');
		$records = $this->db->get('orders_subject')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function update_subject($id, $data)
	{
		$this->db->where('subject_id', $id);
		$this->db->update('orders_subject', $data);
	}
	public function delete_subject($id)
	{
		$this->db->where('subject_id', $id);
		$this->db->delete('orders_subject');
	}
	public function add_subject($data)
	{
		$this->db->insert('orders_subject', $data);
		return $this->db->insert_id();
	}
	public function get_paper_format_by_id($id)
	{
		$this->db->where('paper_format_id', $id);
		$records = $this->db->get('orders_paper_format')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}	
	public function get_paper_formats()
	{
		$this->db->order_by('position asc, title asc');
		$records = $this->db->get('orders_paper_format')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function update_paper_format($id, $data)
	{
		$this->db->where('paper_format_id', $id);
		$this->db->update('orders_paper_format', $data);
	}
	public function delete_paper_format($id)
	{
		$this->db->where('paper_format_id', $id);
		$this->db->delete('orders_paper_format');
	}
	public function add_paper_format($data)
	{
		$this->db->insert('orders_paper_format', $data);
		return $this->db->insert_id();
	}	
	public function get_institution_type_by_id($id)
	{
		$this->db->where('institution_type_id', $id);
		$records = $this->db->get('orders_institution_type')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}	
	public function get_institution_types()
	{
		$this->db->order_by('position asc, title asc');
		$records = $this->db->get('orders_institution_type')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function add_message($from, $to, $order, $text, $moderated = 1, $support_id = 0, $file = 0)
	{
		$this->db->where('sender_id',$from);
		$this->db->where('receiver_id',$to);
		$this->db->where('order_id',$order);
		$this->db->where('text',$text);
		$record = $this->db->get('messages')->result_array();
		if(!count($record)) {
			$data = array(
				'moderated' => $moderated,
				'support_id' => $support_id,
				'sender_id' => $from,
				'receiver_id' => $to,
				'order_id' => $order,
				'date' => date("Y-m-d H:i:s"),
				'text' => $text,
				'attached_file' => $file
			);
			//if($from != '1')
				//s$data['message'] = 1;
			$this->db->insert('messages', $data);
		}
	}
	public function get_message($id)
	{
		$this->db->where('message_id',$id);
		$records = $this->db->get('messages')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}
	public function get_order_messages_mts($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('sender_id', $user_id);
		$this->db->where('receiver_id = 1');
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_order_messages_stm($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('sender_id = 1');
		$this->db->where('receiver_id', $user_id);
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_order_messages_wtc($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('sender_id != 1');
		$this->db->where('receiver_id', $user_id);
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_order_messages_ctw($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('sender_id', $user_id);
		$this->db->where('receiver_id != 1');
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_order_messages_stw($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('sender_id = 1');
		$this->db->where('receiver_id = 0');
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
//////////////////////////////////////////////////////	
	public function get_all_order_messages($order)
	{
		//$this->db->where('moderated = "1"');
		$this->db->where('order_id', $order);
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_order_messages_from_customer_and_support($order_id, $user_id)
	{
		$sql = "SELECT * from ".$this->db->dbprefix('messages')."";
		$sql .= " WHERE ";
		//$sql .= "moderated = 1 AND ";
		$sql .= "order_id = '".$order_id."' AND ";
		$sql .= "receiver_id IN(0,1,3) ";
		$sql .= "ORDER BY date desc ";
		$records = $this->db->query($sql)->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_order_messages($order_id, $user_id)
	{
		$sql = "SELECT * from ".$this->db->dbprefix('messages')."";
		$sql .= " WHERE ";
		//$sql .= "moderated = 1 AND ";
		$sql .= "order_id = '".$order_id."' AND ";
		$sql .= "(sender_id = '".$user_id."' OR receiver_id = '".$user_id."') ";
		$sql .= "ORDER BY date desc ";
		$records = $this->db->query($sql)->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_user_order_messages($order_id, $user_id)
	{
		//$this->db->where('moderated = "0"');
		$this->db->where('sender_id', $user_id);
		$this->db->where('order_id', $order_id);
		$this->db->order_by('date desc');
		$records = $this->db->get('messages')->result_array();
		if($records)
			return $records;
		else
			return array();
	}	
	public function count_unread_messages($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('receiver_id', $user_id);
		$this->db->where('viewed = 0');
		$this->db->from('messages');
		return $this->db->count_all_results();
	}
	public function update_message($message_id, $data)
	{
		$this->db->where('message_id', $message_id);
		$this->db->update('messages', $data);
	}
	public function update_order_messages($order_id, $data)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('messages', $data);
	}
	public function set_read_order_messages($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('messages', array('viewed' => 1));
	}	
	public function get_order_files($order)
	{
		$this->db->where('moderated = "1"');
		$this->db->where('order_id', $order);
		$this->db->order_by('date desc');
		$records = $this->db->get('files')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_customer_order_files($order, $user_id)
	{
		//$this->db->where('moderated = "0"');
		$this->db->where('sender_id', $user_id);
		$this->db->where('order_id', $order);
		$this->db->order_by('date desc');
		$records = $this->db->get('files')->result_array();
		if($records)
			return $records;
		else
			return array();
	}
	public function get_user_order_files($order, $user_id)
	{
		//$this->db->where('moderated = "0"');
		$this->db->where('sender_id', $user_id);
		$this->db->where('order_id', $order);
		$this->db->order_by('date desc');
		$records = $this->db->get('files')->result_array();
		if($records)
			return $records;
		else
			return array();
	}	
	public function publish_inquiry($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('orders', array(
			'type' => 'order', 
			'status' => 'Confirmed',
			'status_client' => 'Writer’s Review',
			'status_writer' => 'Case published'
		));
	}	
	public function get_approved_requests($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('approved = 1');
		$records = $this->db->get('orders_requests')->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function get_order_request($order_request_id)
	{
		$this->db->where('order_request_id', $order_request_id);
		$records = $this->db->get('orders_requests')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function get_user_order_request($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('user_id', $user_id);
		$records = $this->db->get('orders_requests')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function clear_order_requests($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->delete('orders_requests');
	}	
	public function delete_order_request($order_request_id)
	{
		$this->db->where('order_request_id', $order_request_id);
		$this->db->delete('orders_requests');
	}
	public function get_price_by_id($id)
	{
		$this->db->where('price_id', $id);
		$records = $this->db->get('price')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}
	public function get_price_by_filter($type, $level, $deadline)
	{
		$this->db->where('type', $type);
		$this->db->where('academic_level_id', $level);
		$this->db->where('deadline_id', $deadline);
		$records = $this->db->get('price')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}
	public function get_prices()
	{
		$records = $this->db->get('price')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function update_price($id, $data)
	{
		$this->db->where('price_id', $id);
		$this->db->update('price', $data);
	}
	public function add_price($data)
	{
		$this->db->insert('price', $data);
		return $this->db->insert_id();
	}
	public function check_payment_orders($id)
	{
		$this->db->where('customer_id', $id);
		$this->db->where('payment_id != 0');
		$this->db->from('orders');
		return $this->db->count_all_results();		
	}
}