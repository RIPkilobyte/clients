<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articles_model extends CI_Model {
    
    function get_articles() {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> get('articles') -> result_array();
    }
    
    function get_articles_filter($filter_artcat = NULL) {
        if ($filter_artcat) {
            if ($filter_artcat == 'solo_only') {
                return $this -> db -> where('site', $this->config->item('sitetype')) -> where('solo', '1') -> get('articles') -> result_array();
            }else{
                return $this -> db -> where('site', $this->config->item('sitetype')) -> where('artcat_id', $filter_artcat) -> get('articles') -> result_array();
            }
        }else{
            return $this -> db -> where('site', $this->config->item('sitetype')) -> get('articles') -> result_array();
        }
    }
    
    function get_article_solo($segment) {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> where('solo', '1') -> where('alias', $segment) -> get('articles') -> row_array();
    }
    function get_article_fralias($segment) {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> where('alias', $segment) -> get('articles') -> row_array();
    }
    
    function get_article($article_id) {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> where('article_id', $article_id) -> get('articles') -> row_array();
    }
    
    function save_article($article_id = NULL) {
        
        if ($this->input->post('status', TRUE) == 'true') {
            $status = 1;
        }else{
            $status = 0;
        }
        
        if ($this->input->post('solo', TRUE) == 'true') {
            $solo = 1;
        }else{
            $solo = 0;
        }
        $video = $this->input->post('video');
        if($video) {
            preg_match_all('!width="(.*?)"!si', $video, $matchh);
            preg_match_all('!height="(.*?)"!si', $video, $matchw);
            $video = str_replace($matchh[1][0], 273, $video);
            $video = str_replace($matchw[1][0], 154, $video);
        }
        $data = array(
            'title'     => $this->input->post('title', TRUE),
            'alias'     => $this->input->post('alias', TRUE),
            'artcat_id' => $this->input->post('artcat_id', TRUE),
            'status'    => $status,
            'solo'      => $solo,
            'site'      => $this->config->item('sitetype'),
            'text'      => $this->input->post('text'),
            'pre'       => $this->input->post('pre'),
            'video'       => $video,
            'meta_description'  => $this->input->post('meta_description', TRUE),
            'meta_keyword'      => $this->input->post('meta_keyword', TRUE),
            'meta_title'      => $this->input->post('meta_title', TRUE)
        );
        
        if ($article_id!=0) {
            $this->db->where('article_id', $article_id)->update('articles', $data);
            return $article_id;
        }else{
            $this->db->insert('articles', $data);
            return $this->db->insert_id();
        }
    }
    
    function update_article($article_id, $field, $value) {
        $this->db->where('article_id', $article_id)->update('articles', array($field => $value));
    }
    
    function delete_article($article_id) {
        $this->db->delete('articles', array ('article_id' => $article_id));
    }
    
    function get_artcats() {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> get('artcats') -> result_array();
    }
    
    function get_artcat($artcat_id) {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> where('artcat_id', $artcat_id) -> get('artcats') -> row_array();
    }
    
    function get_artcat_fralias($alias) {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> where('alias', $alias) -> get('artcats') -> row_array();
    }
    
    function get_artcles_frartcat($artcat_id) {
        return $this -> db -> where('site', $this->config->item('sitetype')) -> where('artcat_id', $artcat_id) -> get('articles') -> result_array();
    }
    
    function update_artcat($artcat_id, $field, $value) {
        $this->db->where('artcat_id', $artcat_id)->update('artcats', array($field => $value));
    }
    
    function delete_artcat($artcat_id) {
        $this->db->delete('artcats', array ('artcat_id' => $artcat_id));
    }
    
    function save_artcat($artcat_id = NULL) {
        if ($this->input->post('status', TRUE) == 'true') {
            $status = 1;
        }else{
            $status = 0;
        }
        
        $data = array(
            'title'             => $this->input->post('title', TRUE),
            'alias'             => $this->input->post('alias', TRUE),
            'status'            => $status,
            'site'              => $this->config->item('sitetype'),
            'meta_description'  => $this->input->post('meta_description', TRUE),
            'meta_keyword'      => $this->input->post('meta_keyword', TRUE)            
        );
        
        if ($artcat_id!=0) {
            $this->db->where('artcat_id', $artcat_id)->update('artcats', $data);
            return $artcat_id;
        }else{
            $this->db->insert('artcats', $data);
            return $this->db->insert_id();
        }
    }
    
}