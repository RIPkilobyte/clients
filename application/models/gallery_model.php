<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends CI_Model {
	function get_gallery_by_limit($limit = 30, $offset = 0)
	{
		$this->db->limit($limit, $offset);
		return $this->db->get('gallery')->result_array();
	}
	function save_image($data)
	{
		if(isset($data['gallery_id'])) {
			$this->db->where('gallery_id', $data['gallery_id']);
			$this->db->update('gallery', $data);
		}
		else
			$this->db->insert('gallery', $data);
	}
	function get_by_id($id)
	{
		$this->db->where('gallery_id', $id);
		return $this->db->get('gallery')->result_array();
	}
	function delete($id)
	{
		$this->db->delete('gallery', array('gallery_id' => $id)); 
	}
}