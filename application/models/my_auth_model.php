<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class My_auth_model extends CI_Model {

	private $users_table = 'users';
	private $users_meta_table = 'users_meta';
	private $users_table_id = 'user_id';
	private $email_field = 'email';
	private $username_field = 'username';
	private $forgot_code_field = 'forgot_code';
	private $verify_code_field = 'verify_code';

	public function get_by_id($id)
	{
		//$this->db->where('role', $this->config->item('sitetype'));
		//$this->db->select($this->users_table.'.*, '.$this->users_meta_table.'.*');
		$this->db->where($this->users_table.'.'.$this->users_table_id, $id);
		//$this->db->join($this->users_meta_table, $this->users_table.'.'.$this->users_table_id.' = '.$this->users_meta_table.'.'.$this->users_table_id, 'left');
		$record = $this->db->get($this->users_table)->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_by_email($email)
	{
		if($email != 'support@writer-center.com')
			$this->db->where('role', $this->config->item('sitetype'));
		//$this->db->select($this->users_table.'.*, '.$this->users_meta_table.'.*');
		$this->db->where($this->users_table.'.'.$this->email_field, $email);
		//$this->db->join($this->users_meta_table, $this->users_table.'.'.$this->users_table_id.' = '.$this->users_meta_table.'.'.$this->users_table_id, 'left');
		$record = $this->db->get($this->users_table)->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_by_username($username)
	{
		//$this->db->where('role', $this->config->item('sitetype'));
		//$this->db->select($this->users_table.'.*, '.$this->users_meta_table.'.*');
		$this->db->where($this->users_table.'.'.$this->username_field, $username);
		//$this->db->join($this->users_meta_table, $this->users_table.'.'.$this->users_table_id.' = '.$this->users_meta_table.'.'.$this->users_table_id, 'left');		
		$record = $this->db->get($this->users_table)->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_user_by_confirmation($code)
	{
		//$this->db->where('role', $this->config->item('sitetype'));
		$this->db->where($this->verify_code_field, $code);
		$record = $this->db->get($this->users_table)->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function set_user_verified($user_id)
	{
		$this->db->where($this->users_table_id, $user_id);
		$this->db->update($this->users_table, array($this->verify_code_field => null));
	}
	public function get_by_forgot_code($code)
	{
		//$this->db->where('role', $this->config->item('sitetype'));
		$this->db->where($this->forgot_code_field, $code);
		$record = $this->db->get($this->users_table)->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}	
	public function register($data)
	{
		$this->db->insert($this->users_table, $data);
		return $this->db->insert_id();
	}
	public function update_usermeta($data, $user_id)
	{
		$this->db->where($this->users_table_id, $user_id);
		$record = $this->db->get($this->users_meta_table)->result_array();
		if($record) {
			$this->db->where($this->users_table_id, $user_id);
			$this->db->update($this->users_meta_table, $data);
		}
		else {
			$data['user_id'] = $user_id;
			$this->db->insert($this->users_meta_table, $data);
		}
	}
	public function get_user_meta($user_id)
	{
		$this->db->where($this->users_table_id, $user_id);
		$record = $this->db->get($this->users_meta_table)->result_array();
		if(count($record))
			return $record[0];
		else
			return array();	
	}
	public function check_email($email)
	{
		$this->db->where('role', $this->config->item('sitetype'));
		$this->db->where($this->email_field, $email);
		$this->db->from($this->users_table);
		return $this->db->count_all_results();
	}
	public function update_user($id, $data)
	{
		$this->db->where($this->users_table_id, $id);
		$this->db->update($this->users_table, $data);
	}
	public function delete_user($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('users_meta');

		$this->db->where('user_id', $user_id);
		$this->db->delete('users');
	}
}