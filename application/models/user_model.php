<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public function count_essays()
	{
		return $this->db->from('users_essay')->count_all_results();
	}
	public function get_step1($user_id)
	{
		$this->db->where('user_id', $user_id);
		$record = $this->db->get('users_testing1')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function set_user_essay($user_id, $essay_id)
	{
		$data = array(
			'user_id' => $user_id,
			'essay_id' => $essay_id
			);
		$this->db->insert('users_testing1', $data);
		return $this->db->insert_id();
	}
	public function get_essay($id)
	{
		$this->db->where('essay_id', $id);
		$records = $this->db->get('users_essay')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}
	public function get_user_file($id)
	{
		$this->db->where('file_id', $id);
		$record = $this->db->get('users_files')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_user_testing_dates($id)
	{
		$this->db->where('user_id', $id);
		$this->db->order_by('selected desc, datetime asc');
		$records = $this->db->get('users_testing_dates')->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function get_testing_interval($testing_date_id)
	{
		$this->db->where('testing_date_id', $testing_date_id);
		$records = $this->db->get('users_testing_dates')->result_array();
		if(count($records))
			return $records;
		else
			return array();			
	}
	public function choose_testing_date($testing_date_id)
	{
		$this->db->where('testing_date_id', $testing_date_id);
		$this->db->update('users_testing_dates', array('selected' => 1));
	}

	public function get_test_by_id($test_id)
	{
		$this->db->where('test_id', $test_id);
		$records = $this->db->get('tests')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function get_testing_by_test_and_user_id($user_id, $test_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('test_id', $test_id);
		$this->db->order_by('users_testing_id desc');
		$records = $this->db->get('users_testing')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}	
	public function get_user_testing($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->order_by('users_testing_id desc');
		$records = $this->db->get('users_testing')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function get_user_tests($ids)
	{
		//$this->db->where_in('paper_format_id', $ids);
		//$records = $this->db->get('tests')->result_array();
		$records = $this->db->query("SELECT * from ".$this->db->dbprefix('tests')." WHERE paper_format_id IN (".$ids.")")->result_array();
		if(count($records))
			return $records;
		else
			return array();	
	}
	public function get_test_by_paper_format($paper_format)
	{
		$this->db->where('paper_format_id', $paper_format);
		$records = $this->db->get('tests')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();	
	}
	public function get_random_questions($test_id, $group_id)
	{
		$this->db->where('test_id', $test_id);
		$this->db->where('group_id', $group_id);
		$this->db->limit(1);
		$this->db->order_by('RAND()');
		$records = $this->db->get('questions')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function get_test_questions($test_id)
	{
		$this->db->where('test_id', $test_id);
		$records = $this->db->get('questions')->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function get_question($question_id)
	{
		$this->db->where('question_id', $question_id);
		$records = $this->db->get('questions')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();			
	}
	public function get_answers($question_id)
	{
		$this->db->where('question_id', $question_id);
		$this->db->order_by('answer_id asc');
		$records = $this->db->get('answers')->result_array();
		if(count($records))
			return $records;
		else
			return array();			
	}
	public function get_answer($question_id, $answer_id)
	{
		$this->db->where('question_id', $question_id);
		$this->db->where('answer_id', $answer_id);
		$records = $this->db->get('answers')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();			
	}
	public function set_answer($question_id, $user_id, $answer_id)
	{
		$this->db->where('question_id', $question_id);
		$this->db->where('user_id', $user_id);
		$this->db->update('users_questions', array('answer_id' => $answer_id));
	}
	public function assign_questions_to_user($user_id, $test_id, $question_id)
	{
		$this->db->insert('users_questions', array('user_id'=>$user_id, 'test_id'=>$test_id, 'question_id'=>$question_id));
	}
	public function start_user_test($data)
	{
		$this->db->insert('users_testing', $data);
	}
	public function get_user_questions($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->order_by('question_id asc');
		$records = $this->db->get('users_questions')->result_array();
		if(count($records))
			return $records;
		else
			return array();			
	}	
	public function get_assigned_questions($user_id, $test_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('test_id', $test_id);
		$this->db->order_by('question_id asc');
		$records = $this->db->get('users_questions')->result_array();
		if(count($records))
			return $records;
		else
			return array();			
	}
	public function get_users_testing_by_id($users_testing_id)
	{
		$this->db->where('users_testing_id', $users_testing_id);
		$records = $this->db->get('users_testing')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function complete_test($users_testing_id, $data)
	{
		$this->db->where('users_testing_id', $users_testing_id);
		$this->db->update('users_testing', $data);
	}
	public function delete_user_questions($user_id, $test_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('test_id', $test_id);
		$this->db->delete('users_questions');
	}
	public function add_user_file($data)
	{
		$this->db->insert('users_files', $data);
		return $this->db->insert_id();		
	}
	public function update_user_diplom_file($user, $id)
	{
		$this->db->where('user_id', $user);
		$this->db->update('users_testing1', array('diplom' => $id));
	}
	public function update_user_passport_file($user, $id)
	{
		$this->db->where('user_id', $user);
		$this->db->update('users_testing1', array('passport' => $id));
	}
	public function update_user_essay_file($user, $id)
	{
		$this->db->where('user_id', $user);
		$this->db->update('users_testing1', array('essay' => $id));
	}
	public function update_user_step($user_id, $step)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('users_meta', array('step' => $step));
	}
	public function get_news($limit = 30, $offset = 0)
	{
		$this->db->limit($limit, $offset);
		$this->db->order_by('news_id desc');
		$records = $this->db->get('news')->result_array();
		if(count($records))
			return $records;
		else
			return array();			
	}
	public function check_read_news($user_id, $news_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('news_id', $news_id);
		$this->db->from('news_read');
		return $this->db->count_all_results();
	}
	public function set_read_news($user_id, $news_id)
	{
		$this->db->insert('news_read', array('news_id'=>$news_id, 'user_id'=>$user_id));
	}
	public function check_unread_news($user_id)
	{
		$this->db->select('news_id');
		$records = $this->db->get('news')->result_array();
		if($records) {
			foreach ($records as $record) {
				if(!$this->check_read_news($user_id, $record['news_id']))
					return true;
			}
			return false;
		}
		else
			return false;
	}
	public function update_last_action($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('orders', array('last_action'=>date("Y-m-d H:i:s"), 'attention'=>1));
	}
	public function update_order_message($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update('orders', array('message'=>1));
	}
	public function save_email_hash($data)
	{
		$this->db->insert('email', $data);
	}
	public function get_by_hash($hash)
	{
		$this->db->where('hash', $hash);
		$records = $this->db->get('email')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function check_user_email($user_id, $email)
	{
		$this->db->where('user_id !=', $user_id);
		$this->db->where('email', $email);
		$this->db->from('users');
		return $this->db->count_all_results();
	}
	public function count_users($email = '', $id = '')
	{
		if($email)
			$this->db->like('email', $email);
		if($id)
			$this->db->where('user_id', $id);

		$this->db->from('users');
		return $this->db->count_all_results();
	}
	public function get_users($limit = 30, $offset = 0, $email = '', $id = '')
	{
		if($email)
			$this->db->like('email', $email);
		if($id)
			$this->db->where('user_id', $id);

		$this->db->limit($limit, $offset);
		$records = $this->db->get('users')->result_array();
		if(count($records))
			return $records;
		else
			return array();	
	}
	public function delete_user($id)
	{
		$this->db->where('user_id', $id);
		$this->db->delete('users');
	}
	public function update_user($id, $update)
	{
		$this->db->where('user_id', $id);
		$this->db->update('users', $update);
	}
}