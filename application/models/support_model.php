<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Support_model extends CI_Model {
//-----------------------PAYMENTS-----------------------------------------------------
	public function count_payments()
	{
		$this->db->from('payments');
		return $this->db->count_all_results();
	}
	public function get_payments($field, $type, $start=0, $limit = 20)
	{
		$sql = "SELECT * from ".$this->db->dbprefix('payments')."";

		$sql .= " ORDER BY ".$field." ".$type;
		$sql .= " LIMIT ".$start.", ".$limit." ";

		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}	
//-----------------------ORDER--------------------------------------------------------
	public function get_orders($status = 'Needs confirmation')//none
	{
		$this->db->where('status', $status);
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function count_ajax_orders($status = 'Needs confirmation', $filters = array())
	{
		$sql = "SELECT * from ".$this->db->dbprefix('orders')."";
		$sql .= " WHERE type='order'";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				$sql .= " AND ".$key." = ".$value." ";
			}
		}
		switch ($status) {
			case 'new':
				$sql .= " AND status IN('Needs confirmation', 'Confirmed')";
			break;
			case 'progress':
				$sql .= " AND status IN('In progress','File submitted','File delivered','Revision in progress')";
			break;
			case 'attention':
				$sql .= " AND attention = '1'";
			break;
			case 'due':
				//$sql .= " AND status IN('Needs confirmation', 'Confirmed', 'In progress', 'File submitted', 'File delivered', 'Revision in progress')";
				$sql .= " AND UNIX_TIMESTAMP(date_end) - UNIX_TIMESTAMP() < 28800";
			break;
			case 'completed':
				$sql .= " AND status IN('Completed')";
			break;
			case 'cancelled':
				$sql .= " AND status IN('Canceled')";
			break;
			case 'all':
			break;
		}
		/*
		if($status != 'all') {
			if($status != 'Needs confirmation')
				$sql .= " AND moderated = 1";
			$sql .= " AND status IN(".$status.")";
		}
		//*/
		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return count($records);
		else
			return 0;
	}	
	public function get_ajax_orders($status = 'Needs confirmation',  $field, $type, $start=0, $limit = 20, $filters = array())
	{
		$sql = "SELECT * from ".$this->db->dbprefix('orders')."";
		$sql .= " WHERE type='order'";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				$sql .= " AND ".$key." = ".$value." ";
			}
		}
		switch ($status) {
			case 'new':
				$sql .= " AND status IN('Needs confirmation', 'Confirmed')";
			break;
			case 'progress':
				$sql .= " AND status IN('In progress','File submitted','File delivered','Revision in progress')";
			break;
			case 'attention':
				$sql .= " AND attention = '1'";
			break;
			case 'due':
				$sql .= " AND status IN('Needs confirmation', 'Confirmed', 'In progress', 'File submitted', 'File delivered', 'Revision in progress')";
				$sql .= " AND UNIX_TIMESTAMP(date_end) - UNIX_TIMESTAMP() < 28800";
			break;
			case 'completed':
				$sql .= " AND status IN('Completed')";
			break;
			case 'cancelled':
				$sql .= " AND status IN('Canceled')";
			break;
			case 'all':
			break;
		}		
		/*
		if($status != 'all') {
			if($status != 'Needs confirmation')
				$sql .= " AND moderated = 1";
			$sql .= " AND status IN(".$status.")";
		}
		//*/
		$sql .= " ORDER BY ".$field." ".$type;
		$sql .= " LIMIT ".$start.", ".$limit." ";

		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function count_ajax_inquiries($filters = array())
	{
		$sql = "SELECT * from ".$this->db->dbprefix('orders')."";
		$sql .= " WHERE type='inquiry'";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				$sql .= " AND ".$key." = ".$value." ";
			}
		}
		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return count($records);
		else
			return 0;
	}	
	public function get_ajax_inquiries($field, $type, $start=0, $limit = 20, $filters = array())
	{
		$sql = "SELECT * from ".$this->db->dbprefix('orders')."";
		$sql .= " WHERE type='inquiry'";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				$sql .= " AND ".$key." = ".$value." ";
			}
		}
		$sql .= " ORDER BY moderated asc, ".$field." ".$type;
		$sql .= " LIMIT ".$start.", ".$limit." ";

		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_order_statuses()
	{
		$records = $this->db->get('orders_status')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_order_status_by_id($id)
	{
		$this->db->where('order_status_id', $id);
		$records = $this->db->get('orders_status')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}
	public function count_order_requests($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->from('orders_requests');
		return $this->db->count_all_results();
	}
	public function get_requests_by_order($order_id)
	{
		$this->db->where('order_id', $order_id);
		$records = $this->db->get('orders_requests')->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function update_user_request($request_id, $data)
	{
		$this->db->where('order_request_id', $request_id);
		$this->db->update('orders_requests', $data);
	}
	public function remove_assigned_writers($id)
	{
		$this->db->where('order_id', $id);
		$this->db->update('orders_requests', array('approved' => 0));
	}
	public function assign_user_request($request_id)
	{
		//$this->db->where('order_request_id', $request_id);
		//$this->db->update('orders_requests', array('approved' => 0));
		$this->db->where('order_request_id', $request_id);
		$this->db->update('orders_requests', array('approved' => 1, 'date_approve' => date("Y-m-d H:i:s")));
	}
	public function check_unmoderate_messages($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('moderated', "'0'", false);
		$this->db->from('messages');
		return $this->db->count_all_results();
	}	
	public function check_unmoderate_files($order_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('moderated', "'0'", false);
		$this->db->from('files');
		return $this->db->count_all_results();
	}
	public function get_request_order_and_user($order_id, $user_id)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('user_id', $user_id);
		$record = $this->db->get('orders_requests')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();		
	}
	public function get_order_payment($id)
	{
		$this->db->where('payment_id', $id);
		$record = $this->db->get('payments')->result_array();
		if(count($record))
			return $record[0];
		else
			return array();
	}
	public function get_customer_orders($customer_id)
	{
		$this->db->where('customer_id', $customer_id);
		$this->db->order_by('order_id desc');
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
//-----------------------WRITER-------------------------------------------------------
	public function count_ajax_writers($status, $filters = array())
	{
		$sql = "";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				//$this->db->where($key, $value);
				$sql .= "AND u.".$key." = ".$value." ";
			}
		}
		if($status == 'all') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				WHERE
					u.role = 'writer'
					".$sql."
			")->result_array();
		}
		elseif($status == 'new') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'one'
			")->result_array();
		}
		elseif($status == 'files') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					(
						ut.diplom = '' OR
						ut.passport = '' OR
						ut.essay = ''
					) AND
					step = 'one'
			")->result_array();
		}
		elseif($status == 'files_complete') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					ut.diplom != '' AND
					ut.passport != '' AND
					ut.essay != '' AND
					step = 'one'
			")->result_array();
		}		
		elseif($status == 'test') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'two'
			")->result_array();
		}
		elseif($status == 'test_complete') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'three'
			")->result_array();
		}
		elseif($status == 'approved') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'finished' AND
					status = '1'
			")->result_array();
		}
		elseif($status == 'rejected') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step IN('reject_files','reject_test')
			")->result_array();
		}		

		//echo $this->db->last_query();
		if(count($records))
			return count($records);
		else
			return 0;
	}	

	public function get_ajax_writers($status, $field, $type, $start = 0, $limit = 20, $filters = array())
	{
		$sql = "";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				//$this->db->where($key, $value);
				$sql .= "AND u.".$key." = ".$value." ";
			}
		}
		if($status == 'all') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer'
					".$sql."
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."				
			")->result_array();
		}
		elseif($status == 'new') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'one'
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}
		elseif($status == 'files') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					(
						ut.diplom = '' OR
						ut.passport = '' OR
						ut.essay = ''
					) AND
					step = 'one'
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}
		elseif($status == 'files_complete') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					ut.diplom != '' AND
					ut.passport != '' AND
					ut.essay != '' AND
					step = 'one'
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}
		elseif($status == 'test') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'two'
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}
		elseif($status == 'test_complete') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'three'
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}
		elseif($status == 'approved') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'finished' AND
					status = '1'
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}
		elseif($status == 'rejected') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step IN('reject_files','reject_test')
				ORDER BY u.".$field." ".$type."
				LIMIT ".$start.", ".$limit."
			")->result_array();
		}		
		//echo $this->db->last_query();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function export_writers($status)
	{
		if($status == 'all') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer'
				ORDER BY u.user_id desc
			")->result_array();
		}
		elseif($status == 'new') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'one'
				ORDER BY u.user_id desc
			")->result_array();
		}
		elseif($status == 'files') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					(
						ut.diplom = '' OR
						ut.passport = '' OR
						ut.essay = ''
					) AND
					step = 'one'
				ORDER BY u.user_id desc
			")->result_array();
		}
		elseif($status == 'files_complete') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					ut.diplom != '' AND
					ut.passport != '' AND
					ut.essay != '' AND
					step = 'one'
				ORDER BY u.user_id desc
			")->result_array();
		}
		elseif($status == 'test') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'two'
				ORDER BY u.user_id desc
			")->result_array();
		}
		elseif($status == 'test_complete') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'three'
				ORDER BY u.user_id desc
			")->result_array();
		}
		elseif($status == 'approved') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step = 'finished' AND
					status = '1'
				ORDER BY u.user_id desc
			")->result_array();
		}	
		elseif($status == 'rejected') {
			$records = $this->db->query("
				SELECT * from ".$this->db->dbprefix('users_meta')." um
				LEFT JOIN ".$this->db->dbprefix('users')." u ON (um.user_id = u.user_id)
				LEFT JOIN ".$this->db->dbprefix('users_testing1')." ut ON (ut.user_id = u.user_id)
				WHERE
					u.role = 'writer' AND
					step IN('reject_files','reject_test')
				ORDER BY u.user_id desc
			")->result_array();
		}
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_user_testing1($user_id, $table = 'users_testing1')
	{
		$this->db->where('user_id', $user_id);
		$records = $this->db->get($table)->result_array();
		if(count($records))
			return $records[0];
		else
			return array();
	}
	public function update_user_step1($user_id, $data)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('users_testing1', $data);
	}
	public function go_user_to_step($user, $step, $status = 0)
	{
		$this->db->where('user_id', $user);
		$this->db->update('users_meta', array('step' => $step));
		if($step == 'finished') {
			$this->db->where('user_id', $user);
			$this->db->update('users', array('status' => $status));
		}
	}
	public function delete_user_testing_dates($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('users_testing_dates');
	}
	public function add_user_testing_date($user_id, $date)
	{
		$this->db->insert('users_testing_dates', array('user_id' => $user_id, 'datetime' => $date));
	}
	public function moderate_testing($users_testing_id, $status)
	{
		$this->db->where('users_testing_id', $users_testing_id);
		$this->db->update('users_testing', array('status'=> $status));
	}
	public function delete_user_files($user_id)
	{
		$this->db->where('user_id', $user_id);
		$data = array(
			'diplom' => '',
			'diplom_confirm' => '0',
			'passport' => '',
			'passport_confirm' => '0',
			'essay' => '',
			'essay_confirm' => '0'
		);
		$this->db->update('users_testing1', $data);
	}
	public function delete_user_test($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('users_testing');

		$this->db->where('user_id', $user_id);
		$this->db->delete('users_questions');
	}
	public function get_users_learn()
	{
		$this->db->select('learned_from');
		$records = $this->db->get('users_meta')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_users_countries()
	{
		$this->db->where('role', 'writer');
		$this->db->select('country');
		$records = $this->db->get('users')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_users_disciplines()
	{
		$this->db->select('disciplines');
		$records = $this->db->get('users_meta')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
	public function get_user_orders($user_id)
	{
		$this->db->where('writer_id', $user_id);
		$records = $this->db->get('orders')->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
//-----------------------FILE---------------------------------------------------------
	public function get_order_files_from($user_id, $order_id)
	{
		//$records = $this->db->query("SELECT * FROM (`".$this->db->dbprefix('files')."`) WHERE `order_id` = ".$order." AND (`sender_id` = ".$user." OR `receiver_id` = ".$user.") ")->result_array();
		$records = $this->db->query("SELECT * FROM (`".$this->db->dbprefix('files')."`) WHERE `order_id` = ".$order_id." AND (`sender_id` = ".$user_id.") ")->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function get_file_by_id($file_id)
	{
		$this->db->where('file_id', $file_id);
		$records = $this->db->get('files')->result_array();
		if(count($records))
			return $records[0];
		else
			return false;		
	}
	public function moderate_file($id, $support_id, $moderated = '0')
	{
		$this->db->where('file_id', $id);
		$this->db->update('files', array('moderated' => $moderated, 'support_id' => $support_id));
	}
//-----------------------MESSAGE------------------------------------------------------
	public function get_new_messages($field = 'date', $type = 'asc', $limit = 10)//none
	{
		$this->db->order_by($field, $type);
		$this->db->where('moderated', "'0'", false);
		$this->db->where('support_id = 0');
		$this->db->limit($limit);
		$records = $this->db->get('messages')->result_array();
		if(count($records))
			return $records;
		else
			return false;		
	}
	public function get_order_messages_from($user_id, $order_id)
	{
		//$records = $this->db->query("SELECT * FROM (`".$this->db->dbprefix('messages')."`) WHERE `order_id` = ".$order." AND (`sender_id` = ".$user." OR `receiver_id` = ".$user.") ")->result_array();
		$records = $this->db->query("SELECT * FROM (`".$this->db->dbprefix('messages')."`) WHERE `order_id` = ".$order_id." AND (`sender_id` = ".$user_id.") ")->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
	public function get_order_messages_from_writers($order_id, $user_ids)
	{
		$records = $this->db->query("SELECT * FROM (`".$this->db->dbprefix('messages')."`) WHERE `order_id` = ".$order_id." AND (`sender_id` NOT IN(".$user_ids.")) ")->result_array();
		if(count($records))
			return $records;
		else
			return array();		
	}
//-----------------------NEWS---------------------------------------------------------
	public function get_all_news()
	{
		$this->db->order_by('date_add desc');
		$records = $this->db->get('news')->result_array();
		if(count($records))
			return $records;
		else
			return false;		
	}
	public function get_news_by_id($news_id)
	{
		$this->db->where('news_id', $news_id);
		$records = $this->db->get('news')->result_array();
		if(count($records))
			return $records[0];
		else
			return array();		
	}
	public function delete_news($news_id)
	{
		$this->db->where('news_id', $news_id);
		$this->db->delete('news');
	}
	public function edit_news($data, $news_id = '')
	{
		if($news_id) {
			$this->db->where('news_id', $news_id);
			$this->db->update('news', $data);
		}
		else
			$this->db->insert('news', $data);
	}
//-----------------------MAILS--------------------------------------------------------
	public function count_ajax_mails($filters)
	{
		$sql = "SELECT * from ".$this->db->dbprefix('email')." AS e";
		$sql .= " LEFT JOIN ".$this->db->dbprefix('users')." u ON (e.user_id = u.user_id) ";
		$sql .= " WHERE 1=1";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				if($key == 'search_site' && $value != '')
					$sql .= " AND u.role = ".$value." ";
				if($key == 'search_email' && $value != '')
					$sql .= " AND u.email LIKE '%".$value."%' ";
				if($key == 'search_title' && $value != '')
					$sql .= " AND e.title LIKE '%".$value."%' ";
				if($key == 'search_date_from' && $value != '')
					$sql .= " AND e.date >= '".$value."' ";
				if($key == 'search_date_to' && $value != '')
					$sql .= " AND e.date <= '".$value."' ";
			}
		}
		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return count($records);
		else
			return 0;
	}
	public function get_ajax_mails($field, $type, $start=0, $limit = 20, $filters = array())
	{
		$sql = "SELECT * from ".$this->db->dbprefix('email')." AS e";
		$sql .= " LEFT JOIN ".$this->db->dbprefix('users')." u ON (e.user_id = u.user_id) ";
		$sql .= " WHERE 1=1";
		foreach ($filters as $filter) {
			foreach ($filter as $key => $value) {
				if($key == 'search_site')
					$sql .= " AND u.role = ".$value." ";
				if($key == 'search_email')
					$sql .= " AND u.email LIKE '%".$value."%' ";
				if($key == 'search_title')
					$sql .= " AND e.title LIKE '%".$value."%' ";
				if($key == 'search_date_from')
					$sql .= " AND e.date >= '".$value."' ";
				if($key == 'search_date_to')
					$sql .= " AND e.date <= '".$value."' ";
			}
		}
		$sql .= " ORDER BY ".$field." ".$type;
		$sql .= " LIMIT ".$start.", ".$limit." ";

		$records = $this->db->query($sql)->result_array();
		if(count($records))
			return $records;
		else
			return array();
	}
//-----------------------AUDIO-UPDATES------------------------------------------------
	public function check_updates_orders()
	{
		$this->db->where('moderated', '0');
		$this->db->from('orders');
		return $this->db->count_all_results();
	}
	public function check_updates_user_files()
	{
		$this->db->where('users_testing1.diplom !=""');
		$this->db->where('users_testing1.passport !=""');
		$this->db->where('users_testing1.essay !=""');
		$this->db->where('users_meta.step = "one"');
		$this->db->from('users_testing1');
		$this->db->join('users_meta', 'users_meta.user_id = users_testing1.user_id');
		return $this->db->count_all_results();
	}	
	public function check_updates_user_testing()
	{
		$this->db->where('users_meta.step = "three"');
		$this->db->from('users_meta');
		return $this->db->count_all_results();
	}
	public function check_updates_files()
	{
		$this->db->where('moderated', '0');
		$this->db->from('files');
		return $this->db->count_all_results();		
	}
	public function check_updates_messages()
	{
		$this->db->where('moderated', '0');
		$this->db->from('messages');
		return $this->db->count_all_results();		
	}
//-----------------------END----------------------------------------------------------
}