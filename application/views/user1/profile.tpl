<div class="site-content-middle" role="main">
  <div class="site-content-middle-in">
        
    <div class="main_blocks main_blocks_order">
    	Profile
    </div>
    <div class="main_blocks main_blocks_profile main_blocks_payments">
			<table class="table_name_value">
				<tr>
					<td>Client ID:</td>
					<td><?php echo $_SESSION['user_id']; ?></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><?php echo $_SESSION['userdata']['first_name']; ?></td>
				</tr>
				<tr>
					<td>Last name:</td>
					<td><?php echo $_SESSION['userdata']['last_name']; ?></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><?php echo $_SESSION['userdata']['email']; ?></td>
				</tr>
				<tr>
					<td>Country:</td>
					<td><?php echo $_SESSION['userdata']['country_title']; ?></td>
				</tr>
				<tr>
					<td>Phone:</td>
					<td><?php echo $_SESSION['userdata']['phone']; ?></td>
				</tr>
				<tr>
					<td colspan="2" class="td_button"><a href="/user/change_password"><input type="button" class="btn btn_gray" value="Change password" /></a></td>
				</tr>
			</table>
    </div>

  </div><!-- .site-content-middle-in -->
</div><!-- .site-content-middle -->