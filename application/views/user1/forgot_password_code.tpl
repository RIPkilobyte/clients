<div class="login_box">
	<div class="block_element_head">Restore password</div>
	<div class="error_box">
		<?php
		if(isset($errors) && count($errors))
			echo $errors;
		?>
        <?php
            if(isset($success))
                echo '<div class="success">'.$success.'</div>';
        ?>
	</div>
	<div class="subblock">
		<form action="/user/forgot_password/<?php echo $this->uri->segment(3); ?>" method="post">
			<input type="hidden" name="code" value="<?php echo $this->uri->segment(3); ?>" />
			<h3>New password</h3>
			<input type="password" name="new" size="40" placeholder="Type here" />
			<h3>Re-type new password</h3>
			<input type="password" name="confirm" size="40" placeholder="Type here" />
			<div class="submit">
                <span>&nbsp;</span><input type="submit" class="btn btn-primary" value="Submit" />
            </div>
		</form>
	</div>
</div>

