<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
<script type="text/javascript" src="/js/nicEdit.js"></script>
<link href="/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />

      <div class="site-content-middle" role="main">
        <div class="site-content-middle-in">
        <?php if(isset($order) && count($order)): ?>

        <div class="main_blocks main_blocks_order">
          Orders # <span><?php echo $order['order_id']; ?></span> 
           <span class="icons_blocks">
                  <?php if(isset($order['unread_messages']) && $order['unread_messages']): ?><!-- надо подсветить картинку-->
                    <a id="top_blink1_<?php echo $order['order_id']; ?>" href="" class="icons_p  blinker" title="You have a <?php echo $orders['unread_messages']; ?> new messages"></a>
                  <?php else: ?><!-- картинка не подсвечивается-->
                    <a id="top_blink1_<?php echo $order['order_id']; ?>" href="" class="icons_p" title="You have no new messages"></a>
                  <?php endif; ?>

                  <?php if($order['status'] == 'File delivered' && isset($order['file_id']) && $order['file_id']): ?><!-- надо подсветить картинку-->
                    <a id="top_blink2_<?php echo $order['order_id']; ?>" href="" class="icons_p icons_l  blinker" title="You have a file"></a>
                  <?php else: ?><!-- картинка не подсвечивается-->
                    <a id="top_blink2_<?php echo $order['order_id']; ?>" href="" class="icons_p icons_l" title="You have no file"></a>
                  <?php endif; ?>

                  <?php if(!$order['payment_id']): ?><!-- надо подсветить картинку-->
                    <a id="top_blink3_<?php echo $order['order_id']; ?>" href="" class="icons_p icons_m  blinker" title="Waiting for a payment"></a>
                  <?php else: ?><!-- картинка не подсвечивается-->
                    <a id="top_blink3_<?php echo $order['order_id']; ?>" href="" class="icons_p icons_m" title="Payment has been already made"></a>
                  <?php endif; ?>
                  </span>
        </div>
        <div class="contleft_side main_blocks float-left">

          <?php if(isset($timer)): ?>
            <div class="time_title cont_titles">Time left:</div>
            <div class="time_sides float-left">
              <span id="timer_days">0</span>DAYS
            </div>
            <div class="time_sides float-left">
              <span id="timer_time">00:00</span>HOURS
            </div>
            <div class="clear"></div>
          <?php endif; ?>

          <div class="wait_p"><?php echo $order['status_client']; ?></div>
           <div class="ord_inf">
            
            <?php if($order['status'] != 'Completed' && $order['status'] != 'Canceled'): ?>
             <div class="row">
               <span>Deadline:</span>
               <span><?php echo date("M j h:i A", strtotime($order['date_end'])); ?></span>
             </div>
            <?php endif; ?>

             <div class="row">
               <span>Price:</span><span>$ <?php echo $order['price']; ?></span>
             </div>

           </div>
           <?php if($order['status'] == 'Completed' && $order['file_id']): ?>
            <a class="oreng_btn pointer" onclick="download_file(<?php echo $order['order_id']; ?>, <?php echo $order['rating']; ?>)">Download file</a>
           <?php elseif($order['status'] == 'File delivered' && $order['file_id']): ?>
            <a class="oreng_btn pointer" onclick="preview_file(<?php echo $order['file_id']; ?>)">Preview</a>
            <a class="oreng_btn pointer" id="approve_button">Approve</a>
            <?php if($order['revisions'] >= '3'):?>
            <?php else: ?>
              <a class="oreng_btn pointer" id="revision_button">Revision</a>
            <?php endif; ?>
           <?php endif; ?>

        </div>

        <div class="con_right_side float-left">
          <div class="con_right_side_in">

            <div class="main_blocks main_blocks_payments">
               <div class="title_p cont_titles float-left">PAYMENTS: <span> No payment has been made
              </span>  </div>
              <div class="title_p cont_titles cont_titles2 float-left"> Amount: <span>USD <?php echo $order['price']; ?></span></div>
               <?php if($payment): ?>
                <span>Created:</span>
                <span><?php echo date("M j H:i", strtotime($order['date_add'])); ?></span>
                <span>Paid:</span>
                <span><?php echo date("M j H:i", strtotime($payment['date'])); ?></span>
                <span>Amount:</span>
                <span>$ <?php echo $payment['amount']; ?></span>
               <?php else: ?>
                  <a class="green_btn float-right pointer" onclick="order_checkout(<?php echo $order['order_id']; ?>)">Pay</a>
               <?php endif; ?>
            </div>

            <div class="main_blocks main_blocks_inform">
              <div class="title_i cont_titles">INFORMATION</div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Number of pages:
                </div>
                <div class="cells_bl"><?php echo $order['pages']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Number of slides:
                </div>
                <div class="cells_bl"><?php echo $order['slides']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Academic level: 
                </div>
                <div class="cells_bl"><?php echo $order['academic_level']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Deadline:
                </div>
                <div class="cells_bl"><?php echo $order['deadline']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Type of work:  
                </div>
                <div class="cells_bl">
                  <?php
                    if($order['work_type'] == 'new')
                      echo 'Writing from scratch';
                    elseif($order['work_type'] == 'edit')
                      echo 'Editing/proofreading';
                  ?>
                </div>
                <div class="cells_bl cells_bl_title">
                  Type of paper: 
                </div>
                <div class="cells_bl"><?php echo $order['paper_type']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Subject: 
                </div>
                <div class="cells_bl"><?php echo $order['subject']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Plagiarism report: 
                </div>
                <div class="cells_bl">
                <?php
                  if($order['plagiarism_report'])
                    echo 'Yes';
                  else
                    echo 'No';
                ?>
                </div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Abstract page:
                </div>
                <div class="cells_bl">
                <?php
                  if($order['abstract_page'])
                    echo 'Yes';
                  else
                    echo 'No';
                ?>
                </div>
                <div class="cells_bl cells_bl_title">
                  Referencing style:
                </div>
                <div class="cells_bl"><?php echo $order['paper_format']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Number of Sources:
                </div>
                <div class="cells_bl"><?php echo $order['sources']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Topic:   
                </div>
                <div class="cells_bl"><?php echo $order['topic']; ?></div>
              </div>
              <div class="last_row row_i">
                <div class="cells_bl cells_bl_title">
                  Paper details: 
                </div>
                <div class="cells_bl"><?php echo $order['paper_details']; ?></div>
              </div>
            </div>

            <div class="main_blocks main_blocks_messages">
              <div class="cont_titles title_mes">MESSAGES / FILES</div>

              <div class="mes_body">
                <div class="title_side">
                  <span class="date">
                  to our supportteam:
                  <a class="green_btn float-right pointer" onclick="add_message()">Add new message / file</a>
                  <br /><br />
                  </span>
                </div>
                <div id="new_message" class="mes_body_text hidden">
                  <div class="thame">
                    <span class="interface_field_caption" style="float:none;width:600px;">Message:</span><br />
                    <textarea id="message_text" style="width:600px; height:150px;float:none;"></textarea>
                    <form method="POST" id="fileform" style="float:none;" enctype="multipart/form-data">
                      <input type="hidden" name="order_id" id="order_id" value="<?php echo $order['order_id']; ?>"/>
                      <input type="hidden" name="sender" id="sender_id" value="<?php echo $_SESSION['user_id']; ?>" />
                      <input type="hidden" name="recipient" id="recipient" value="1" />
                      <p style="margin:10px;"><input type="file" style="float:none;" name="file" id="file" /></p>
                    </form>
                    <div class="interface_buttons" id="submit_result">
                      <a class="green_btn float-left pointer" onclick="cancel_message()">Cancel</a>
                      <a class="green_btn float-right pointer" onclick="nicEditors.findEditor('message_text').saveContent();submit_file(<?php echo $order['order_id']; ?>)">Submit</a>
                    </div>
                    <br />
                  </div>
                </div>
              </div>

              <?php if(isset($order['records']) && count($order['records'])): ?>
                <?php foreach($order['records'] as $record): ?>
              <div class="mes_body">

                <div class="title_side">
                  <span class="from">
                    <?php echo $record['from']; ?>
                  </span>
                  <span class="to_h">
                    : <?php echo $record['to']; ?>
                  </span>
                  <div class="date">
                    <?php echo date("M j h:i A", strtotime($record['date'])); ?>
                  </div>
                </div>

                <div class="mes_body_text">
                  <div class="thame">

                    <?php if(isset($record['message_id'])): ?>
                      <?php echo $record['text']; ?>

                      <?php if(isset($record['file']) && $record['file']): ?>
                        <br />
                        <div class="pointer" onClick="get_file(<?php echo $record['file']['file_id']; ?>)">
                        <?php if(isset($record['file']['image']) && $record['file']['image']): ?>
                          <img src="/img/icon/<?php echo $record['file']['image']; ?>" height="30px" />
                        <?php endif; ?>
                        <?php echo $record['file']['client_name']; ?>
                        </div>
                      <?php endif; ?>

                    <?php else: ?>
                      <div class="pointer" onClick="get_file(<?php echo $record['file']['file_id']; ?>)">
                      <?php if(isset($record['file']['image']) && $record['file']['image']): ?>
                        <img src="/img/icon/<?php echo $record['file']['image']; ?>" height="30px" />
                      <?php endif; ?>
                      <?php echo $record['file']['client_name']; ?>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>

              </div>
                <?php endforeach; ?>
              <?php endif; ?>


            </div>
          </div>
        </div>

      <?php endif; ?>
    </div><!-- .site-content-middle-in -->
   </div><!-- .site-content-middle -->

<div id="approve_dialog" title="Attention!">
  <p>You have clicked the "<b>Approve</b>" button. Bear in mind, that after clicking the "<b>Submit</b>" button you will not be able to request for a revision or refund. After you click on this button, you will receive the editable file of your order.</p>
</div>
<div id="revision_dialog" title="Revision">
  <p>You have clicked on the <b>Revision</b> button. That means, that you are not satisfied with the paper that the writer has prepared. Please provide as many details as possible, explaining what must be changed in the written document.</p>
  <p><b>Revision details:</b></p>
  <p>
    <textarea id="revision_details" cols="60" rows="4"></textarea>
  </p>
  <p><b>Revision deadline:</b></p>
  <p>
    Add hours to the deadline: <input type="text" size="6" id="add_hours" value="0" /> hours
  </p>
  <p><b>Note:</b> give an adequate amount of time for a writer to revise your paper. It will be added to the main deadline’s time. If there is substantial amount of time until the main deadline, please choose +0 hours option. Bear in your mind, that you will not able to request for a full refund after you click the <b>Submit</b> button.
  </p>
  <p>Revisions left: <span class="success"><?php echo 3-$order['revisions'];?></span></p>
</div>
<div id="alert_dialog">
  <p id="alert_dialog_text"></p>
</div>

<div id="rating_dialog" class="hidden center_blocks">
  <h3>THANK YOU!</h3>
  <h3>PLEASE RATE OUR SERVICE</h3>
  <p>YOUR WRITER'S WORK
    <label><input type="radio" name="rating_writer" value="-10"><img src="/img/icon/angry.png" width="40px" /></label>
    <label><input type="radio" checked="checked" name="rating_writer" value="0"><img src="/img/icon/neutral.png" width="40px" /></label>
    <label><input type="radio" name="rating_writer" value="10"><img src="/img/icon/great.png" width="40px" /></label>
  </p>
  <p>OUR CUSTOMER SUPPORT SERVICE
    <label><input type="radio" name="rating_support" value="-10"><img src="/img/icon/angry.png" width="40px" /></label>
    <label><input type="radio" checked="checked" name="rating_support" value="0"><img src="/img/icon/neutral.png" width="40px" /></label>
    <label><input type="radio" name="rating_support" value="10"><img src="/img/icon/great.png" width="40px" /></label>
  </p>
  <p>WOULD YOU LIKE TO LEAVE FEEDBACK?</p>
  <p>
    <textarea style="width:100%; height:100%" name="rating_text" id="rating_text"></textarea>
  </p>
</div>

<script type="text/javascript">
$().ready(function(){
  $("#rating_dialog").dialog({
    autoOpen: false,
    height: 600,
    width: 600,
    modal: true,
    buttons: {
      "Confirm": function(){
        $.post('/ajax/send_rating',{
          order_id: <?php echo $order['order_id']; ?>,
          rating_writer: $("input[name=rating_writer]:checked").val(),
          rating_support: $("input[name=rating_support]:checked").val(),
          rating_text: $("#rating_text").val(),
        },function(data){
          if(data.status == 'ok') {
          }
          else {
          }
          $("#rating_dialog").dialog( "close" );
        }, 'json');
      }
    },
  });   
  $("#approve_dialog").dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      Cancel: function(){
        $( this ).dialog( "close" );
      },
      "Submit": function(){
        $("#approve_dialog").html('<p>Please wait… Processing your order…<img src="/img/icon/great.png" width="40px" /></p>');
        $.post('/ajax/order_action',{id:<?php echo $order['order_id']; ?>, type:'approve'},function(data){
          if(data.status == 'ok') {
            $("#approve_dialog").dialog("close");
            $("#alert_dialog_text").html('Order approved successfully!');
            $("#alert_dialog").dialog("open");
          }
          else {
            $("#approve_dialog").dialog("close");
            $("#alert_dialog_text").html('An error occurred. Please try again later');
            $("#alert_dialog").dialog("open");
          }
        }, 'json');
      }
    },
  }); 
  $("#revision_dialog").dialog({
    autoOpen: false,
    height: 600,
    width: 500,
    modal: true,
    buttons: {
      Cancel: function(){
        $(this).dialog("close");
      },
      "Submit": function(){
        $("#revision_dialog").html('<p>Please wait… Processing your order…<img src="/img/icon/great.png" width="40px" /></p>');
        $.post('/ajax/order_action',{id:<?php echo $order['order_id']; ?>, type:'revision', text:$("#revision_details").val(), hours:$("#add_hours").val()},function(data){
          if(data.status == 'ok') {
            $("#revision_dialog").dialog("close");
            $("#alert_dialog_text").html('You have successfully requested a revision!');
            $("#alert_dialog").dialog("open");
          }
          else {
            $("#revision_dialog").dialog("close");
            $("#alert_dialog_text").html('An error occurred. Please try again later');
            $("#alert_dialog").dialog("open");
          }
        }, 'json');
      }
    },
  }); 
  $("#alert_dialog").dialog({
    autoOpen: false,
    height: 200,
    width: 350,
    modal: true,
    close: function(){
      location.reload();
    }
  }); 
  $("#revision_button").click(function(){
    $("#revision_dialog").dialog("open");
  });
  $("#approve_button").click(function(){
    $("#approve_dialog").dialog("open");
  }); 
  $("#rating_button").click(function(){
    $("#rating_dialog").dialog("open");
  });  
  <?php
  if(isset($timer))
    echo 'order_timer('.$timer['y'].','.($timer['m']-1).','.$timer['d'].','.$timer['H'].','.$timer['i'].','.$timer['s'].');';
  ?>
  $("#add_hours").numberMask({beforePoint:4});
  var myNicEditor = new nicEditor();
  myNicEditor.addInstance('message_text');
});
</script>