<div class="site-content-middle" role="main">
  <div class="site-content-middle-in">
        
    <div class="main_blocks main_blocks_order">
    	Change password
    </div>
    <div class="main_blocks main_blocks_profile main_blocks_payments">
		<div class="error_box">
			<?php
			if(isset($errors) && count($errors))
				echo $errors;
			?>
		</div>
		<form action="/user/change_password" method="post">
			<h3>Current password</h3>
			<input type="password" name="old" size="40" placeholder="Type here" />
			<h3>New password</h3>
			<input type="password" name="new" size="40" placeholder="Type here" />
			<h3>Re-type new password</h3>
			<input type="password" name="confirm" size="40" placeholder="Type here" />
	        <div class="submit">
	            <span>&nbsp;</span><input type="submit" class="btn btn-primary" value="Submit" />
	        </div>
		</form>
    </div>

  </div><!-- .site-content-middle-in -->
</div><!-- .site-content-middle -->
