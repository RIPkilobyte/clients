<div class="site-content-middle" role="main">
  <div class="site-content-middle-in">
    <div class="main_blocks main_blocks_order">
    	Your discounts
    </div>
    <div class="main_blocks main_blocks_profile main_blocks_payments">
<div id="tabs">
	<div class="block_head">
	<ul>
		<li><a id="block_discounts" class="blue_btn" href="#discounts">ABOUT OUR DISCOUNT & REWARDS</a></li>
		<li><a id="block_rewards" class="blue_btn" href="#rewards">YOUR DISCOUNTS AND REWARDS</a></li>
	</ul>
	</div>
	<div id="discounts">
	<div class="block_content">
		<table class="disc_bb_wrap">
			<tr>
				<td>
<h4 style="margin:0;">Discounts & Rewards</h4>
We value our customers, and we want them to stay with us for a long time. We also want our customers to introduce us to their friends. 
So, we give back as much as it is practically possible using our discount & reward mechanism.  
Currently, there are two types of discounts available:  
 
 <br/><br/>
<h4 style="margin:0;">Loyalty discount</h4>  
We give you 20% off from all new orders that you place with us. 
A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. 
Please note that this is your personal discount code that will only work when it matches with your registered email address.
<p style="margin-top:15px;">
	<b>Your loyalty discount code:</b><br />
<span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span><br /><br />
</p>
</td>
				<td>
<h4 style="margin:0;">Introduction reward & discount</h4> 
Every time you introduce us to your friends we give you 10% reward credit from any amount they spend with us on their first orders. Such credit might be used against any payment you make to us in the future.
And we still give 10% discount to your friends too! So, 10% reward to you and 10% discount to them. This is a real “Win – Win” scenario! 
In the box below you can find your Introduction discount code. You just pass this code to your friends. Your friends will be requested to provide this code when they place their orders with us. Our system will recognise your code and will automatically add credit to your account.  
The balance of your credit is shown below. It always increases when people introduced by you approve their first orders. Your available credit will be used automatically against any payment you make to us in the future. 
<p style="margin-top:15px;">
<b>Your introduction discount code:</b><br />
 <span class="weight_sp1">	<?php echo $_SESSION['user_id']; ?>02</span><br /><br />	
</p>
				</td>
			</tr>
			<tr>
				<td>

				</td>
				<td>

				</td>
			</tr>
			<tr>
				<td style="text-align:justify;" colspan="2">
An example: You tell about us to your friends – Bill, Mary and George, and you give them an Introduction discount code. Let’s say each of them places 10 pages essay of bachelor level with 3 days deadline that will amount to 229.90 USD. They insert this code into discount box on their registration panels, and that gives them 10% discount to their orders (- 22.90 USD). At the same time you will automatically get 10% reward from each of their orders (+22.90 USD each), summarizing to handsome 68.70 USD of your credit. And you can use this credit against any payment you make to use in the future. Please do not forget that at the same time you still get your loyalty 20% discount on all your new orders. So, your next order will work by this formula: Your order price – 20% loyalty discount – 68.70 your introduction credit. This is the best offer on the market! And you can introduce as many people as you wish! So what are you waiting for.
				</td>
			</tr>
		</table>
	</div>
	</div>
	<div id="rewards">
		<table class="disc_bb_wrap">
			<tr>
				<td>
Your loyalty discount:  
As our valued customer, you are entitled to 20% discount on all of your orders after your first order. Please insert your loyalty discount code every time you place your new order.
				</td>
				<td>
Your Introduction Rewards: 
We do not reveal our customers’ names, but you can see orders below that were placed under your introduction code. The current summary is your credit that you can use against your next purchase with us.
 				</td>
			</tr>
			<tr>
				<td>
<b> Your loyalty discount code:</b><br />
<span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span><br /><br />
Your discount history:
<table class="small_table">
	<tr>
		<th>Orders</th>
		<th>Discounts <br/>%</th>
		<th>Discounts USD</th>
	</tr>
	<?php $total = '0.00'; ?>
	<?php foreach($user_orders as $order): ?>
		<?php $total = $total+$order['discount_save']; ?>
		<tr>
			<td><?php echo $order['order_id']; ?></td>
			<td><?php echo $order['discount']; ?></td>
			<td><?php echo $order['discount_save']; ?></td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="2"><strong>Your total savings</strong></td>
		<td><strong><?php echo number_format($total, 2, '.', ''); ?></strong></td>
	</tr>
</table>
				</td>
				<td>
<b>Your introduction discount code:</b><br />
<span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>02</span><br /><br />
Your Introduction credit history:
<table>
	<tr>
		<th style="width:33%;vertical-align:top;padding: 0 0 8px">Orders</th>
		<th style="width:33%;vertical-align:top;padding: 0 0 8px">Your credit<br> %</th>
		<th style="width:33%;vertical-align:top;padding: 0 0 8px">Your credit<br> USD</th>
	</tr>
	<?php $total = '0.00'; ?>
	<?php foreach($discount_orders as $order): ?>
		<?php $total = $total+$order['discount_save']; ?>
		<tr>
			<td><?php echo $order['order_id']; ?></td>
			<td><?php echo $order['discount']; ?></td>
			<td><?php echo $order['discount_save']; ?></td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="2" style="padding-right:0;"><strong>Your total current credit</strong> </td>
		<td style="padding-left:10px;"><strong><?php echo number_format($total, 2, '.', ''); ?></strong></td>
	</tr>
</table>
<br />
				</td>
			</tr>
		</table>
	</div>
</div>
    </div>

  </div><!-- .site-content-middle-in -->
</div><!-- .site-content-middle -->


<script type="text/javascript">
$().ready(function(){
	$("#tabs").tabs();
});
</script>
