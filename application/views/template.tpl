<!DOCTYPE html>
<?php 
if (!isset($content))
    $content = '<h2>Пустая страница</h2>';
?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <?php /* <meta name="viewport" content="width=1000" />  */ ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title ?></title>
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="icon" href="/img/favicon.ico" type="image/x-icon">
    <link href="/css/template.css" rel="stylesheet" media="all" />
    <?php /*<link href="/css/pda.css" rel="stylesheet" media="all" />*/ ?>
    
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/functions.js"></script>
    <link rel="stylesheet" href="/css/cusel.css" type="text/css" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,700,600' rel='stylesheet' type='text/css'>




    
    <script type="text/javascript" src="/js/jquery-1.6.1.js"></script>
    <script type="text/javascript" src="/js/cusel.js"></script>
    <script type="text/javascript" src="/js/cusel_set.js"></script>
    <script type="text/javascript" src="/js/jScrollPane.js"></script>
    <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/js/calc.js"></script>
    <?php // Подключение скриптов из контроллера
    if (!empty($scripts)) {
        foreach ($scripts as $script) { echo $script;
        }
    }
    ?>
    
    <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/css/ie.css" />
    <![endif]-->
    <script>
        function mobileVideoResize()
        {
            var h = $(window).height();
            var w = $(window).width();
            if(h > w)
            {
                $(".mobilevid img").width($(document).width());
                $(".mobilevid img").css('height','auto');
            }
            else
            {
                $(".mobilevid img").height(document.documentElement.clientHeight);
                $(".mobilevid img").css('width','auto');
            }
        }

        $(window).resize(function() {
            var h = $(window).height();
            var w = $(window).width();

            ratio = w/h;
            if(ratio < 1.72){
                $("#bgvid").css('height', '100%');
                $("#bgvid").css('width', 'auto');
            }
            else {
                $("#bgvid").css('height', 'auto');
                $("#bgvid").css('width', '100%');
            }
            mobileVideoResize();


        });

        $().ready(function(){
            var h = $(window).height();
            var w = $(window).width();
            ratio = w/h;
            if(ratio < 1.48){
                $("#bgvid").css('height', '100%');
                $("#bgvid").css('width', 'auto');
            }
            else {
                $("#bgvid").css('height', 'auto');
                $("#bgvid").css('width', '100%');
            }

            mobileVideoResize();

        });
    </script>
    <?php 
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '/main'){ ?>
<style>
#header {
    height: 70vh;
}
</style>
         <?php } ?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2TzXmB9ZRIjV7KryWVSKLzYNYybsFD54";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<script>

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69580922-1', 'auto');

ga('send', 'pageview');

</script>
</head>

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-48444080-1', 'eagle-essays.com');
  ga('send', 'pageview');
</script>
    <?php
        if ($_SERVER['REQUEST_URI'] == '/') {
            echo "<style>
    #header{
        height: 100vh;
    }


@media all and (max-width: 1000px) {
    #header {
    height: 50vh;
}
}

    </style>";
        }
    ?>

    <div id="nav">
        <div class="cont">
            <a href="/"><div class="logo"></div></a>
            <div class="menu">
                
                <div class="toggle">
                    <ul>
                       <!-- <li><a href="/"><span>Home</span></a></li>-->
                        <li class="l1">
                            <a href="/services"><span>Services</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul>
                                    <li><a href="/how-it-works"><span>How it works</span></a></li>
                                    <li><a href="/faq-top-essay"><span>FAQ</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="l2">
                            <a href="/prices"><span>Prices</span></a>

                        </li>
                        <li class="l3">
                            <a href="/about-writing-services"><span>About us</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul>
                                    <li><a href="/contact-us"><span>Contact us</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <!--
                        <li>
                            <a href="/company" onclick="return false;"><span>Company</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul>
                                </ul>
                            </div>
                        </li>
                        -->
                        <li><a href="/testimonials"><span>Testimonials</span></a></li>
                        <li class="l4">
                            <a href="/free-essays"><span>Free Essays</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul >
                                    <li><a href="/free-essay-tips"><span>Essay writing tips</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <!--<li><a href="/blog"><span>Blog</span></a></li>-->
                        <?php if(isset($_SESSION['user_id']) && $_SESSION['userdata']['role'] == 'client'): ?>
                            <li><a href="/user"><span><?php echo $_SESSION['userdata']['first_name']; ?></span></a></li>
                        <?php elseif(isset($_SESSION['user_id']) && $_SESSION['userdata']['role'] == 'support'):?>
                            <li><a href="/support"><span>Support</span></a></li>
                        <?php else: ?>
                            <li><a href="/user/login"><span>Sign in</span></a></li>
                        <?php endif;?>                
                    </ul>
                
                <!--
                <div class="chat"><a href="#">Chat now</a>
                </div>
                -->
                </div>

                <div class="buttons">

                    <div class="social"><span class="show-menu" href="#"><!-- --></span></div>
                    <div class="social"><a href="https://www.facebook.com/theeagleessays?ref=hl"><img src="/img/facebook45.png" alt="Eagle-essays.com  Buy  Essay"/></a></div>
                        
  

                    <div class="tell">
                        <a href="tel:+442032868140">
                            <img src="/img/phone45.png" title="+442032868140" alt="+442032868140" style="margin-top: 2px;"/>
                        </a>
                    </div>
                    <div class="skype">
                        <a href="skype:writemypapers?call">
                            <img src="/img/skype45.png" style="margin-top: 2px;" alt="Eagle-essays.com Writing essay"/>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
  <div id="videoblock">
        <video autoplay loop muted id="bgvid">
            <source src="/video/eagle.mp4" type="video/mp4" id="videomp4" />
        </video>

<div id="header">

                    <div class="btn_order">
                        <a href="/order">Order now</a>
                    </div>
                    <!-- <div class="btn_free">
                        <a href="/inquiry">Free inquiry</a>
                    </div> -->

<div class="timer"><div class="title">Live Stats</div>
<script type="text/javascript">
$().ready(function(){
    var time = "<?php echo time(); ?>";
    var one = time.substr(0,1);
    var two = time.substr(1,1);
    var three = time.substr(2,1);
    var four = time.substr(3,1);
    var five = time.substr(4,1);
    var six = time.substr(5,1);
    $("#live_stats_table").html('<tr><td>'+one+'</td><td>'+two+'</td><td>'+three+'</td><td>'+four+'</td><td>'+five+'</td><td>'+six+'</td></tr>');
});
</script>
<table id="live_stats_table">
    <tr>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
    </tr>
</table>
<div class="title_bottom">Orders Delivered</div></div>

<?php if(isset($calc)) echo $calc; ?>

                </div>

    </div>


<?php /*
    <script>
       $("#nav .show-menu").click(function(){
            $("#nav .toggle").slideToggle();
            return false;
        });

       function pdaMenu() {
            var w = $("body").width();
            if (w < 896) {
                $("body").addClass("pda");
                $("#nav .toggle").hide();
            } else {
                $("body").removeClass("pda");
                $("#nav .toggle").show();
            }
            if (w < 424) {
                $("body").addClass("pdaM");
            } else {
                $("body").removeClass("pdaM");
            }
        }
    
        $("#nav").css("position", "absolute");
        $(window).resize(function(){
            pdaMenu();
        });

        $(window).scroll(function() {
            pdaMenu();
        });

       pdaMenu();
    </script>
*/ ?>

<div id="wrapper">
    
    <div id="big_cont">
    <div class="cont">
    
    <div class="clear"></div>
    <?php 
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '/main'){
            echo '<div id="left_col">';
            /*echo '<img src="../img/left_discount.jpg">';*/
            if(isset($left_block))
                echo $left_block;
            echo '</div>';
        }
    ?>
    
    <?php 
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '/main'){
            echo '<div id="content">'; 
        }
        else {
            echo '<div id="content" class="contenthome">'; 
        }  
    ?>

    <?=$content ?>

    </div>
    
    <div class="clear"></div>
   </div> 
   <div id="ft_m">
   <div class="block_ft_m">
    <div class="cont">
            <ul>
                <li><a href="/disclaimer"><span>Disclaimer</span></a></li>
                <li><a href="/terms-of-use" class="unfirst"><span>Terms of use</span></a></li>
                <li><a href="/privacy-policy" class="unfirst"><span>Privacy policy</span></a></li>
                <li><a href="/revision-policy" class="unfirst"><span>Revision policy</span></a></li>
                <li><a href="/money-back" class="unfirst"><span>Money back guarantee</span></a></li>
                <li><a href="/contact-us"><span>Contact us</span></a></li>
                <li><a href="/blog"><span>Blog</span></a></li>
            </ul>
            </div>
</div></div>
    <div id="footer">
        <div class="cont">
            
            <div class="ftt_m_c">
            <?php /*<div class="ftt_m">
                <ul>
                    <li><a href="/architecture-essays">Architecture Essays</a></li>
                    <li><a href="/art-essays">Art Essays</a></li>
                    <li><a href="/astronomy-essays">Astronomy Essays</a></li>
                    <li><a href="/biology-essays">Biology Essays</a></li>
                    <li><a href="/business-essays">Business Essays</a></li>
                    <li><a href="/chemistry-essays">Chemistry Essays</a></li>
                    <li><a href="/criminology-essays">Criminology Essays</a></li>
                    <li><a href="/economics-essays">Economics Essays</a></li>
                </ul>
            </div>
                        <div class="ftt_m">
                <ul>
                    <li><a href="/engineering-papers.co.uk">Engineering Papers</a></li>
                    <li><a href="/english-essays">English Essays</a></li>
                    <li><a href="/essays-in-literature">Essays in Literature</a></li>
                    <li><a href="/finance-essays">Finance Essays</a></li>
                    <li><a href="/genetics-help">Genetics Help</a></li>
                    <li><a href="/geography-essays">Geography Essays</a></li>
                    <li><a href="/history-essays">History Essays</a></li>
                    <li><a href="/linguistics-essays">Linguistics Essays</a></li>
                </ul>
            </div>
                        <div class="ftt_m">
                <ul>
                    <li><a href="/marketing-essays">Marketing Essay</a></li>
                    <li><a href="/philosophy-essays">Philosophy Essays</a></li>
                    <li><a href="/politics-essays">Politics Essays</a></li>
                    <li><a href="/psychology-essays">Psychology Essays</a></li>
                    <li><a href="/sociology-essays">Sociology Essays</a></li>
                    <li><a href="/technology-essays">Technology Essays</a></li>
                    <li><a href="/write-my-report">Write My Report</a></li>
                </ul>
            </div>
            </div> */ ?>
            
            <div class="ftt_m">
                <ul>
                    <li><a href="/architecture-paper">Architecture Essays</a></li>
                    <li><a href="/art-paper">Art Essays</a></li>
                    <li><a href="/cheap-essays">Astronomy Essays</a></li>
                    <li><a href="/biology-paper">Biology Essays</a></li>
                    <li><a href="/business-essay-samples">Business Essays</a></li>
                </ul>
            </div>
                        <div class="ftt_m">
                <ul>
                    <li><a href="/chemistry-paper">Chemistry Essays</a></li>
                    <li><a href="/criminlology-paper">Criminology Essays</a></li>
                    <li><a href="/economic-essays">Economics Essays</a></li>
                    <li><a href="/engineering-paper">Engineering Papers</a></li>
                    <li><a href="/english-essays">English Essays</a></li>
                </ul>
            </div>
                        <div class="ftt_m">
                <ul>
                    <li><a href="/essay-literature">Essays in Literature</a></li>
                    <li><a href="/finance-essays">Finance Essays</a></li>
                    <li><a href="/genetics-essay">Genetics</a></li>
                    <li><a href="/geography-sample-essays">Geography Essays</a></li>
                    <li><a href="/history-sample-papers">History Essays</a></li>
                </ul>
                </div>
                <div class="ftt_m">
                <ul>
                    <li><a href="/linguistics-essays">Linguistics Essays</a></li>
                    <li><a href="/free-marketing-assignments">Marketing Essay</a></li>
                    <li><a href="/philosophy-essays">Philosophy Essays</a></li>
                    <li><a href="/politics-paper">Politics Essays</a></li>
                    <li><a href="/psychology-essays">Psychology Essays</a></li>
                </ul>
                </div>
                <div class="ftt_m">
                <ul>
                    <li><a href="/sociology-essays">Sociology Essays</a></li>
                    <li><a href="/technology-paper">Technology Essays</a></li>
                    <li><a href="/technical-writing">Help for My Report</a></li>
                </ul>
            </div>
            </div>
            
            <div class="clear"></div>
            <div class="acc_2"></div>
            <div class="to_top"><a href="#">Back to top</a></div>
            <div class="clear"></div>
            
        </div>
        <div class="copy_r">
                <div class="cont">
                <div class="copy">
                    Copyright © <a href="/">Eagle Essays</a> | <a href="http://neopulse.ru">Site development</a> NeoPulse |
                    All Rights Reserved.
                </div>
                </div>
            </div>
    </div>
    </div>

</div>

</body>
</html>