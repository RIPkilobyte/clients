    <div class="right_block">
      <div class="right_side">

        <div class="contacts">
          <div class="blocks_title">
            CONTACT US:
          </div>
          <div class="line_step">
            <div class="note_block">Email:</div>
            info@eagle-essays.com
          </div>
          <div class="line_step line_step1">
            <div class="note_block">Skype:</div>
            writemypapers
          </div>
          <div class="line_step line_step2">
            <div class="note_block">Phone:</div>
            +442032868140
          </div>
        </div>
        <div class="contacts contacts_1">
            <div class="note_block title_dis title_dis11">Loyalty discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
            <div class="bottom_block_wr">
            <div class="note_block title_dis">Referral discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                 Help your friend get 10% discount by giving them your introduction code, all they need to do is enter the code in the discount box whenever they place their order, not only that, you will also earn 10% reward credit every time they use the introductory code , it is a Win-Win scenario! The credit that you will earn can be used to pay for your next order. The more friend you refer the more credit you can get.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1 weight_sp111">  <?php echo $_SESSION['user_id']; ?>02</span>   
          </div>
      </div>
      </div>
    </div><!-- .right_block -->


       <div class="block_center">
     <div class="block_center_in">
      <div class="order_top">
        Profile
      </div>
 
     </div>
   </div><!-- .block_center -->


    <div class="block_center">
      <div class="block_center_in">
        <div class="center_blocks" id="order_list_center">
            <div class="cell_block">
              <div class="services_line">
                <span class="note_block">Client ID:</span>  <?php echo $_SESSION['user_id']; ?>
              </div>
              <div class="services_line">
                <span class="note_block">Name:</span>  <?php echo $_SESSION['userdata']['first_name']; ?>
              </div>
              <div class="services_line">
                <span class="note_block">Last name:</span>  <?php echo $_SESSION['userdata']['last_name']; ?>
              </div>
              <div class="services_line">
                <span class="note_block">Email:</span>  <?php echo $_SESSION['userdata']['email']; ?>
              </div>
              <div class="services_line">
                <span class="note_block">Country:</span>  <?php echo $_SESSION['userdata']['country_title']; ?>
              </div>
              <div class="services_line">
                <span class="note_block">Phone:</span>  <?php echo $_SESSION['userdata']['phone']; ?>
              </div>
              <div class="services_line">
                <a href="/user/change_password" class="blue_btn">Change password</a>
              </div>
              <div class="services_line">
                <a href="/user/edit_profile" class="blue_btn">Change contact information</a>
              </div>
            </div>
        </div><!-- .center_blocks -->      
      </div>
    </div>