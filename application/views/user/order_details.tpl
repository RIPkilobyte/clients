<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
<script type="text/javascript" src="/js/nicEdit.js"></script>
<link href="/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
<style>
#approve_dialog.ui-dialog-content,
#revision_dialog.ui-dialog-content {
    height: auto!important;
}
.ui-draggable {
    width: 90%!important;
    max-width: 600px!important;
}
#revision_details {
    width: 100%;
}
</style>
<?php if(isset($order) && count($order)): ?>

    <div class="block_center">
      <div class="block_center_in">

       <div class="sw">
     <div class="s1">
      <div class="order_top">
        Your order: <?php echo $order['order_id']; ?>
      </div>
      <div class="center_ico float-left ">
       <div class="icons_wrap icons_wrap_b">
                    <?php if(isset($order['unread_messages']) && $order['unread_messages']): ?><!-- надо подсветить картинку-->
                     <a id="top_blink1_<?php echo $order['order_id']; ?>" href="#" class="blinker">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have a <?php echo $order['unread_messages']; ?> new messages</span>
                        </span>
                     </a>
                    <?php else: ?><!-- картинка не подсвечивается-->
                     <a id="top_blink1_<?php echo $order['order_id']; ?>" href="#">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have no new messages</span>
                        </span>
                     </a>
                    <?php endif; ?>

                    <?php if($order['status'] == 'File delivered' && isset($order['file_id']) && $order['file_id']): ?><!-- надо подсветить картинку-->
                     <a id="top_blink2_<?php echo $order['order_id']; ?>" href="#" class="blinker">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have a file</span>
                        </span>
                     </a>
                    <?php else: ?><!-- картинка не подсвечивается-->
                     <a id="top_blink2_<?php echo $order['order_id']; ?>" href="#">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have no file</span>
                        </span>
                     </a>
                    <?php endif; ?>

                    <?php if(!$order['payment_id']): ?><!-- надо подсветить картинку-->
                     <a id="top_blink3_<?php echo $order['order_id']; ?>" href="#" class="blinker">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>Waiting for a payment</span>
                        </span>
                     </a>
                    <?php else: ?><!-- картинка не подсвечивается-->
                     <a id="top_blink3_<?php echo $order['order_id']; ?>" href="#">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>Payment has been already made</span>
                        </span>
                     </a>
                    <?php endif; ?>
       </div>
      </div> 
      <div class="clear"></div> 
     </div>
   </div><!-- .block_center -->
        <div class="center_blocks" id="payment_blok">
          <div class="blocks_title">Payment</div>
          <?php if($payment): ?>
          <div class="payment_panel">
            <div class="cell_block">
              <span class="note_block">Payment:</span> Has been already made
            </div>
              <div class="cell_block">
                <span class="note_block">Amount: </span> USD <?php echo $payment['amount']; ?>
                <?php if($payment['system'] == 'Loyality Code'): ?>
                <div class="text_amount">
                  Paid by Introduction credits
                </div>
                <?php endif; ?>
                <?php if($order['loyality']): ?>
                  <div class="text_amount">
                   USD <?php echo $order['loyality']; ?> Paid by Introduction credits
                  </div>
                  
                <?php endif; ?>

              </div>
            <div class="cell_block">
              <span class="note_block">Paid:</span> <?php echo date("M j H:i", strtotime($payment['date'])); ?>
            </div>
          </div> 
          <?php else: ?>
          <div class="payment_panel">
            <div class="cell_block">
              <span class="note_block">Payment:</span> No payment has been made
            </div>
              <div class="cell_block">
                <span class="note_block">Amount:</span> USD 
                <?php 
                  if($_SESSION['userdata']['amount']) {
                    $amount = $_SESSION['userdata']['amount'];
                    $price = $order['price'];
                    if($amount > $price)
                      echo '0.00';
                    elseif($amount < $price)
                      echo round($price - $amount,2);
                    elseif($amount == $price)
                      echo '0.00';
                  }
                  else
                    echo $order['price'];
                ?>
                <?php //echo $order['price']; ?>
              </div>
            <div class="cell_block">
                
                <!--<form method="post" action="http://eagleediting.co/cart/checkout/<?php echo $order['order_id']; ?>">
                  <input type="hidden" value="<?php echo $_SESSION['user_id']; ?>" name="id" />
                  <input type="hidden" value="<?php echo $_SESSION['userdata']['password']; ?>" name="password" />
                  <input type="submit" value="Pay Now" id="blink_btn" class="blink blinker order_now_btn" />
                </form>-->
                
              <a href="/cart/paypal/<?php echo $order['order_id']; ?>" id="blink_btn" class="blinker  order_now_btn">Pay Now</a>
            </div>
          </div> 
          <?php endif; ?>
        </div><!-- .center_blocks -->

        <div class="center_blocks" id="order_list_center">
          <div class="cell_block">
            <div class="services_line">
             <span class="note_block">Type of service:</span>  
              <?php if($order['work_type'] == 'new'): ?>
                Writing from scratch
              <?php else: ?>
                Editing/Proofreading
              <?php endif; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Academic level:</span> <?php echo $order['academic_level']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Type of paper:</span> <?php echo $order['paper_type']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Subject:</span> <?php echo $order['subject']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">No. of pages:</span>  <?php echo $order['pages']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">No. of slides:</span> <?php echo $order['slides']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Referencing:</span> <?php echo $order['paper_format']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Topic:</span> <?php echo substr($order['topic'], 0, 400); ?>
            </div>
            <div class="services_line">
            <span class="note_block">Paper details:</span> <?php echo $order['paper_details']; ?>
            </div>
          </div>
          <div class="cell_block">
             <div class="services_line">
             <span class="note_block">No. of sources: </span> <?php echo $order['sources']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Abstract page:</span> 
        <?php if($order['abstract_page']): ?>
          Yes
        <?php else: ?>
          No
        <?php endif; ?>            
            </div>
            <div class="services_line">
            <span class="note_block">Plagiarism rep.:</span>
        <?php if($order['plagiarism_report']): ?>
          Yes
        <?php else: ?>
          No
        <?php endif; ?>            
            </div>
            <div class="services_line">
            <span class="note_block">Paper details:</span> Submitted
            </div>
            <div class="services_line">
            <span class="note_block">Attachments:</span>
        <?php if($order['file']): ?>
          <?php
          $image = 'doc.png';
          if(isset($order['file']['file_ext']))
          switch ($order['file']['file_ext']) {
            case '.doc':
            case '.docx':
              $image = 'doc.png';
            break;
            case '.pdf':
              $image = 'pdf.png';
            break;
            case '.ppt':
            case '.pptx':
            case '.pps':
              $image = 'ppt.png';
            break;
            case '.xls':
            case '.xlss':
              $image = 'xls.png';
            break;
            case '.jpg':
            case '.jpeg':
            case '.bmp':
            case '.png':
              $image = 'jpg.png';
            break;
            default:
              $image = 'doc.png';
            break;
          }
          ?>
          <img src="/img/icon/<?php echo $image; ?>" width="15px"/> <span class="attach_name"><?php echo substr($order['file']['client_name'], 0, 20); ?></span>
        <?php else: ?>
          No
        <?php endif; ?>            
            </div>
            <div class="services_line">
            <span class="note_block">Full price:</span> USD <?php echo number_format($order['price']+$order['discount_save'], 2, '.', ''); ?>
            </div>
            <div class="services_line">
            <span class="note_block">Discounted price:</span> <?php echo $order['discount']; ?>% (USD <?php echo $order['price']; ?>)
            </div>
            <div class="services_line">
            <span class="note_block">Introduction credit:</span> USD <?php echo number_format($_SESSION['userdata']['amount'], 2, '.', '');  ?>
            </div>
<?php if(!$order['payment_id']): ?>
            <div class="services_line">
            <span class="note_block">Total price:</span> USD 
            <?php 
            if($_SESSION['userdata']['amount'] > $order['price'])
              echo '0.00';
            elseif($_SESSION['userdata']['amount'] == $order['price'])
              echo '0.00';
            else
              echo number_format($order['price']-$_SESSION['userdata']['amount'], 2, '.', ''); 
            ?>
            </div>
<?php else: ?>
            <div class="services_line">
            <span class="note_block">Total price:</span> USD <?php echo $payment['amount']; ?>
            </div>
<?php endif; ?>

          </div>
            <?php if(!$order['payment_id'] && in_array($order['status'], array('Needs confirmation', 'Confirmed'))): ?>
          <div>
            <div class="cell_block cell_block_l">
              <div class="note_block note_space">
              </div>
            </div>
            <div class="cell_block "  style="width:182px; float:right;padding-right:20px;">
              <a href="/order/edit/<?php echo $order['order_id']; ?>" class="pointer blue_btn">Edit information</a>
            </div>
          </div>
            <?php endif; ?>
        </div><!-- .center_blocks -->

        <div class="center_blocks" id="massage_panel">
          <div class="blocks_title">Messages / Files</div>
          <div>
            <div class="cell_block cell_block_l">
              <div class="note_block note_space">
                to / from our support team
              </div>
            </div>
            <div class="cell_block ">
              <a onclick="add_message()" class="pointer blue_btn">Add new message / file</a>
            </div>
          </div>
        <div class="hidden" id="new_message">
          <input type="hidden" value="1" id="recipient">
          <textarea id="message_text" style="width:550px; height:150px;float:none;"></textarea>
          <form method="POST" id="fileform" style="float:none;" enctype="multipart/form-data">
            <input type="hidden" name="order_id" id="order_id" value="<?php echo $order['order_id']; ?>"/>
            <input type="hidden" name="sender" id="sender" value="<?php echo $_SESSION['user_id']; ?>" />
            <p class="add_file_wrap">
              <input type="file" style="float:none;" name="file" id="file" /><br/><br/>
              Only one document can be downloaded at a time
            </p>
            <div class="interface_buttons" id="submit_result">
              <div class="cell_block"><a id="cancel" onClick="cancel_message()" class="pointer blue_btn">Cancel</a></div>
              <div class="cell_block"><a id="submit" onClick="nicEditors.findEditor('message_text').saveContent();submit_file(<?php echo $order['order_id']; ?>)" class="pointer blue_btn">Submit</a></div>
            </div>
          </form>
        </div>          
        </div><!-- .center_blocks -->

        <div class="center_blocks" id="massage_box">
          <div class="blocks_title">Message Box</div>
          <div>
           <div class="note_block note_space">
              messages received from / sent to support team
           </div>
           <?php if(isset($order['records']) && count($order['records'])): ?>
              <?php foreach ($order['records'] as $record): ?>
                <?php if(isset($record['message_id'])): ?>
           <div class="message_block">
             <div class="message_top">
               <div class="title_mess float-left">
                 <span class="s1"><?php echo $record['from']; ?> to <?php echo $record['to']; ?></span>
               </div>

                    <?php if($record['sender_id'] != $_SESSION['user_id']): ?>

                      <?php if($record['viewed']): ?>
                         <div class="float-right mess_data">
                         <div class="date_block">
                            
                             <?php echo date("M j H:i", strtotime($record['date'])); ?>
                           </div>
                          <div class="mess_readed">Read</div>
                           

                         </div>
                      <?php else: ?>
                         <div class="float-right mess_data" id="read<?php echo $record['message_id']; ?>">
                           <div class="date_block pointer" onclick="set_read(<?php echo $record['message_id']; ?>, <?php echo $record['order_id']; ?>)">
                             <?php echo date("M j H:i", strtotime($record['date'])); ?>
                             <a></a>
                           </div>
                         </div>
                      <?php endif; ?>

                    <?php else: ?>
                       <div class="float-right mess_data">
                         <div class="date_block">
                           <?php echo date("M j H:i", strtotime($record['date'])); ?>
                         </div>
                       </div>
                    <?php endif; ?>

             </div>
             <div class="message_body">
              <?php echo $record['text']; ?>
                    <?php if(isset($record['file']) && $record['file']): ?>
                      <br />
                      <div class="file_link" onClick="get_file(<?php echo $record['file']['file_id']; ?>)">
                        <?php if(isset($record['file']['image']) && $record['file']['image']): ?>
                          <img src="/img/icon/<?php echo $record['file']['image']; ?>" height="30px" />
                        <?php endif; ?>
                        <?php echo $record['file']['client_name']; ?>
                      </div>
                    <?php endif; ?>
              </div>
           </div>                  
                <?php elseif(isset($record['file_id'])): ?>
                <?php endif; ?>
              <?php endforeach; ?>
           <?php endif; ?>

          </div>
        </div><!-- .center_blocks -->
        
      </div>
    </div><!-- .block_center -->    
<div class="right_block">
      <div class="right_side">
        
        <?php if(isset($timer)): ?>
        <div class="right_blocks right_blocks_time">
          <div class="blocks_title">
            Time left:
          </div>
          <div class="time_left_block">
            <div class="row">
              <div class="cell"><span class="time_b" id="timer_days">0</span>Days</div>
              <div class="cell"><span class="time_b" id="timer_hours">0</span>Hours</div>
              <div class="cell"><span class="time_b last_child" id="timer_minutes">0</span>Min</div>
            </div>
          </div>
        </div>
        <?php endif; ?>

        <div class="right_blocks">
          <div class="blocks_title">
            CURRENT STATUS:
          </div>
           <div class="note_block">
             <?php echo $order['status_client']; ?>
             <a href="" class="inf_popr">
                  <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                   This is a status of your order at this moment.
                 </span>
               </span>
             </a>
           </div>
        </div>

        <div class="right_blocks">
          <div class="blocks_title">
            EXPECTED:
          </div>
           <div class="note_block">
             <?php echo $order['client_date_end']; ?>
             <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                   This is en expected time to deliver a first draft. If the work was under revision, this time indicates when your revised work will be submitted. Please note that substantial revisions might require more time and might affect the expected delivery time. 
                 </span>
                 </span>
             </a>
           </div>
        </div>

        <div class="right_blocks">
        <?php if($order['status'] == 'Completed' && $order['file_id']): ?>
          <a onclick="download_file(<?php echo $order['order_id']; ?>, <?php echo $order['rating']; ?>)" class="pointer blue_btn">Download File</a>
        <?php elseif($order['status'] == 'File delivered' && $order['file_id']): ?>
          <a onclick="preview_file(<?php echo $order['file_id']; ?>)" class="pointer blue_btn">Preview</a>
          <a class="pointer blue_btn" id="approve_button">Approve</a>
          <?php if($order['revisions'] >= '3'):?>
          <?php else: ?>
            <a class="pointer blue_btn" id="revision_button">Revision</a>
          <?php endif; ?>
        <?php endif; ?>
        </div>        

        <div class="contacts">
          <div class="blocks_title">
            CONTACT US:
          </div>
          <div class="line_step">
            <div class="note_block">Email:</div>
            info@eagle-essays.com
          </div>
          <div class="line_step line_step1">
            <div class="note_block">Skype:</div>
            writemypapers
          </div>
          <div class="line_step line_step2">
            <div class="note_block">Phone:</div>
            +442032868140
          </div>
        </div>
        <div class="contacts contacts_1">
            <div class="note_block title_dis title_dis11">Loyalty discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
            <div class="bottom_block_wr">
            <div class="note_block title_dis">Introduction discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                 Help your friend get 10% discount by giving them your introduction code, all they need to do is enter the code in the discount box whenever they place their order, not only that, you will also earn 10% reward credit every time they use the introductory code , it is a Win-Win scenario! The credit that you will earn can be used to pay for your next order. The more friend you refer the more credit you can get.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1 weight_sp111">  <?php echo $_SESSION['user_id']; ?>02</span>   
          </div>
      </div>
      </div>
    </div><!-- .right_block -->

<div id="approve_dialog" title="Attention!">
  <p>You have clicked the "<b>Approve</b>" button. Please bear in mind that after clicking the "<b>Submit</b>" button you will not be able to request a revision or a refund. After you click this button, you will receive the editable file of your order.</p>
</div>
<div id="alert_dialog">
  <p id="alert_dialog_text"></p>
</div>

<div id="revision_dialog" title="Revision">
  <p>You have clicked on the <b>Revision</b> button. That means, that you are not satisfied with the paper that the writer has prepared. Please provide as many details as possible, explaining what must be changed in the written document.</p>
  <p><b>Revision details:</b></p>
  <p>
    <textarea id="revision_details" cols="40" rows="4"></textarea>
  </p>
  <p><b>Revision deadline:</b></p>
  <p>
    Add hours to the deadline: <input type="text" size="6" id="add_hours" value="0" /> hours
  </p>
  <p><b>Note:</b> give an adequate amount of time for a writer to revise your paper. It will be added to the main deadline’s time. If there is substantial amount of time until the main deadline, please choose +0 hours option. Bear in your mind, that you will not able to request for a full refund after you click the <b>Submit</b> button.
  </p>
  <p>Revisions left: <span class="success"><?php echo 3-$order['revisions'];?></span></p>
</div>

<div id="rating_dialog" class="hidden center_blocks">
  <p>
  <span class="mod_cell"> YOUR WRITER'S WORK</span>
    <label><input type="radio" name="rating_writer" value="-10"><img src="/img/icon/angry.png" width="40px" /></label>
    <label><input type="radio" checked="checked" name="rating_writer" value="0"><img src="/img/icon/neutral.png" width="40px" /></label>
    <label><input type="radio" name="rating_writer" value="10"><img src="/img/icon/great.png" width="40px" /></label>
  </p>
  <p><span class="mod_cell"> OUR CUSTOMER SUPPORT SERVICE</span>
    <label><input type="radio" name="rating_support" value="-10"><img src="/img/icon/angry.png" width="40px" /></label>
    <label><input type="radio" checked="checked" name="rating_support" value="0"><img src="/img/icon/neutral.png" width="40px" /></label>
    <label><input type="radio" name="rating_support" value="10"><img src="/img/icon/great.png" width="40px" /></label>
  </p>
  <p class="wish_b">WOULD YOU LIKE TO LEAVE FEEDBACK?</p>
  <p class="txt_area">
    <textarea style="width:100%; height:100%" name="rating_text" id="rating_text"></textarea>
  </p>
</div>

<script type="text/javascript">

$().ready(function(){
  $("#rating_dialog").dialog({
    autoOpen: false,
    height: 600,
    width: 600,
    modal: true,
    buttons: {
      "Confirm": function(){
        $.post('/ajax/send_rating',{
          order_id: <?php echo $order['order_id']; ?>,
          rating_writer: $("input[name=rating_writer]:checked").val(),
          rating_support: $("input[name=rating_support]:checked").val(),
          rating_text: $("#rating_text").val(),
        },function(data){
          if(data.status == 'ok') {
          }
          else {
          }
          $("#rating_dialog").dialog( "close" );
        }, 'json');
      }
    },
  });   
  $("#approve_dialog").dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      Cancel: function(){
        $( this ).dialog( "close" );
      },
      "Submit": function(){
        $("#approve_dialog").html('<p>Please wait… Processing your order…<img src="/img/icon/great.png" width="40px" /></p>');
        $.post('/ajax/order_action',{id:<?php echo $order['order_id']; ?>, type:'approve'},function(data){
          if(data.status == 'ok') {
            $("#approve_dialog").dialog("close");
            $("#alert_dialog_text").html('Order approved successfully!');
            $("#alert_dialog").dialog("open");
          }
          else {
            $("#approve_dialog").dialog("close");
            $("#alert_dialog_text").html('An error occurred. Please try again later');
            $("#alert_dialog").dialog("open");
          }
        }, 'json');
      }
    },
  }); 
  
  var revisionDialog = $("#revision_dialog");

  revisionDialog.dialog({
    autoOpen: false,
    height: 200,
    width: 100,
    modal: true,
    buttons: {
      Cancel: function(){
        $(this).dialog("close");
      },
      "Submit": function(){
        $(".ui-dialog-buttonpane").addClass('hidden');
        text = $("#revision_details").val();
        $("#revision_dialog").html('<p>Please wait… Processing your order…<img src="/img/icon/great.png" width="40px" /></p>');
        $.post('/ajax/order_action',{id:<?php echo $order['order_id']; ?>, type:'revision', text:text, hours:$("#add_hours").val()},function(data){
          
          if(data.status == 'ok') {
            $("#revision_dialog").dialog("close");
            $("#alert_dialog_text").html('You have successfully requested a revision!');
            $("#alert_dialog").dialog("open");
          }
          else {
            $("#revision_dialog").dialog("close");
            $("#alert_dialog_text").html('An error occurred. Please try again later');
            $("#alert_dialog").dialog("open");
          }
          //$(".ui-dialog-buttonpane").removeClass('hidden');
        }, 'json');
      }
    },
  });

  $("#alert_dialog").dialog({
    autoOpen: false,
    height: 200,
    width: 350,
    modal: true,
    close: function(){
      location.reload();
    }
  }); 
  $("#revision_button").click(function(){
    $("#revision_dialog").dialog("open");
  });
  $("#approve_button").click(function(){
    $("#approve_dialog").dialog("open");
  }); 
  $("#rating_button").click(function(){
    $("#rating_dialog").dialog("open");
  });  
  <?php
  if(isset($timer))
    if($order['payment_id'])
      echo 'order_timer('.$timer['y'].','.($timer['m']-1).','.$timer['d'].','.$timer['H'].','.$timer['i'].','.$timer['s'].');';
    else
      echo 'order_timer_nopay('.$timer['y'].','.($timer['m']-1).','.$timer['d'].','.$timer['H'].','.$timer['i'].','.$timer['s'].');';
  ?>
  $("#add_hours").numberMask({beforePoint:4});
  var myNicEditor = new nicEditor({maxHeight : 150});
  myNicEditor.addInstance('message_text');
});
</script>
<?php endif; ?>