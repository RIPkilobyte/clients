    <div class="right_block">
      <div class="right_side">

        <div class="contacts">
          <div class="blocks_title">
            CONTACT US:
          </div>
          <div class="line_step">
            <div class="note_block">Email:</div>
            info@eagle-essays.com
          </div>
          <div class="line_step line_step1">
            <div class="note_block">Skype:</div>
            writemypapers
          </div>
          <div class="line_step line_step2">
            <div class="note_block">Phone:</div>
            +442032868140
          </div>
        </div>
        <div class="contacts contacts_1">
            <div class="note_block title_dis title_dis11">Loyalty discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
            <div class="bottom_block_wr">
            <div class="note_block title_dis">Referral discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                 Help your friend get 10% discount by giving them your introduction code, all they need to do is enter the code in the discount box whenever they place their order, not only that, you will also earn 10% reward credit every time they use the introductory code , it is a Win-Win scenario! The credit that you will earn can be used to pay for your next order. The more friend you refer the more credit you can get.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1 weight_sp111">  <?php echo $_SESSION['user_id']; ?>02</span> 

          </div>
      </div>
      </div>
    </div><!-- .right_block -->


       <div class="block_center">
     <div class="block_center_in">
      <div class="order_top">
        Your discounts
      </div>
 
     </div>
   </div><!-- .block_center -->
    <div class="block_center">
      <div class="block_center_in">
        <div class="center_blocks" id="order_list_center">
        <div class="tabss">
<div id="tabs">
  <div class="block_head">
  <ul class="default-list">
    <li><a id="block_discounts" class="blue_btn" href="#discounts">ABOUT OUR DISCOUNT & REWARDS</a></li>
    <li><a id="block_rewards" class="blue_btn" href="#rewards">YOUR DISCOUNTS AND REWARDS</a></li>
  </ul>
  </div>
  <div id="discounts">
  <div class="block_content">
    <table class="disc_bb_wrap">
      <tr>
        <td>
<h4 >Discounts & Rewards</h4>
We value our customers, and we want them to stay with us for a long time. We also want our customers to introduce us to their friends. 
So, we give back as much as it is practically possible using our discount & reward mechanism.  
Currently, there are two types of discounts available:  
 
 <br/><br/>
<h4 >Loyalty discount</h4>  
We give you 20% off from all new orders that you place with us. 
A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. 
Please note that this is your personal discount code that will only work when it matches with your registered email address.
<p>
<span class="weight_sp1 title_dis">Loyalty discount code:</span>
<span class="weight_sp1">  <?php echo $_SESSION['user_id']; ?>01</span> 
</p>
</td>
        <td>
<h4 >Referral reward & discount</h4> 
Help your friends get 10% discount by giving them your referral code, all they need to do is enter the code in the discount box whenever they place their orders, the best part is, you will also earn INSTANT 25% cash reward every time they use the referral code, it is truly a Win-Win scenario! The more friend you refer the more money you can earn
<p>
<span class="weight_sp1 title_dis">Referral discount code:</span>
  <span class="weight_sp1">  <?php echo $_SESSION['user_id']; ?>02</span>  </p>
        </td>
      </tr>
      <tr>
        <td style="text-align:justify;" colspan="2">
An example: You tell about us to your friends – Bill, Mary and George, and you give them an Introduction discount code. Let’s say each of them places 10 pages essay of bachelor level with 3 days deadline that will amount to 229.90 USD. They insert this code into discount box on their registration panels, and that gives them 10% discount to their orders (- 22.90 USD). At the same time you will automatically get 10% reward from each of their orders (+22.90 USD each), summarizing to handsome 68.70 USD of your credit. And you can use this credit against any payment you make to use in the future. Please do not forget that at the same time you still get your loyalty 20% discount on all your new orders. So, your next order will work by this formula: Your order price – 20% loyalty discount – 68.70 your introduction credit. This is the best offer on the market! And you can introduce as many people as you wish! So what are you waiting for.
        </td>
      </tr>
    </table>
  </div>
  </div>
  <div id="rewards">
    <table class="disc_bb_wrap">
      <tr>
        <td>
Your loyalty discount:  
As our valued customer, you are entitled to 20% discount on all of your orders after your first order. Please insert your loyalty discount code every time you place your new order.

        </td>
        <td>
Your Referral Rewards: 
We do not reveal our customer’s names, but you can see orders below that were placed under your referral code. 

        </td>
      </tr>
      <tr>
        <td>
        <p style="margin:0;">
<span class="weight_sp1 title_dis">Loyalty discount code:</span>
<span class="weight_sp1">  <?php echo $_SESSION['user_id']; ?>01</span> 
</p>
<div class="u_his">Your referral history::</div>

<table class="small_table">
  <tr>
    <th>Orders</th>
    <th>Discounts %</th>
    <th>Discounts USD</th>
  </tr>
  <?php $total = '0.00'; ?>
  <?php foreach($user_orders as $order): ?>
    <?php $total = $total+$order['discount_save']; ?>
    <tr class="rows_dis">
      <td><?php echo $order['order_id']; ?></td>
      <td><?php echo $order['discount']; ?></td>
      <td><?php echo $order['discount_save']; ?></td>
    </tr>
  <?php endforeach; ?>
  <tr>
    <td colspan="2"><strong>Your total savings</strong></td>
    <td><strong><?php echo number_format($total, 2, '.', ''); ?></strong></td>
  </tr>
</table>
        </td>
        <td>
<span class="s_b">Referral discount code:</span>
<span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>02</span>
<div class="u_his">Your Introduction credit history:</div>

<table>
  <tr>
    <th style="width: 25%;vertical-align:top;padding: 0 0 8px">Orders</th>
    <th style="width: 35%;vertical-align:top;padding: 0 0 8px">Your credit %</th>
    <th style="width: 40%;vertical-align:top;padding: 0 0 8px">Your credit USD</th>
  </tr>
  <?php $total = '0.00'; ?>
  <?php foreach($discount_orders as $order): ?>
    <?php $total = $total+$order['discount_save']; ?>
    <tr class="rows_dis">
      <td><?php echo $order['order_id']; ?></td>
      <td><?php echo $order['discount']; ?></td>
      <td><?php echo $order['discount_save']; ?></td>
    </tr>
  <?php endforeach; ?>
  <tr>
    <td colspan="2" style="padding-right:0;"><strong>Your total current credit</strong> </td>
    <td style="padding-left:10px;"><strong><?php echo number_format($total, 2, '.', ''); ?></strong></td>
  </tr>
  <tr>
    <td colspan="2"><strong>Your unused credit</strong></td>
    <td><strong><?php echo number_format($_SESSION['userdata']['amount'], 2, '.', ''); ?></strong></td>
  </tr>
</table>
<br />
        </td>
      </tr>
    </table>
  </div>
</div>
        </div>
        </div><!-- .center_blocks -->      
      </div>
    </div>   
<script type="text/javascript">
$().ready(function(){
  $("#tabs").tabs();
});
</script>