<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<div class="block_center">
     <div class="block_center_in block_center_in_right">
      <div class="order_top order_top_lk">
        My orders
      </div>
          <div id="tabs">
          <ul class="default-list list_lk">
            <li><a id="block_active" class="blue_btn" href="#active">Current orders</a></li>
            <li><a id="block_all" class="blue_btn" href="#all">All orders</a></li>
          </ul>
          <div id="active">
            <table id="list_active"></table>
          </div>
          <div id="all">
            <table id="list_all"></table>
          </div>
        </div>
     </div>
   </div><!-- .block_center -->
<script type="text/javascript">
var per_page = 20;
var row_list = [20,50,100];
var sort_name = "order_id";
var sort_order = "desc";
function build_grid(type)
{
  $("#list_"+type).jqGrid({
    url:'/ajax/get_orders/'+type,
    datatype: "json",
    mtype: 'POST',
    colNames:['Order id','Topic', 'Status', 'Deadline'],
    colModel:[
      {name:'order_id', align:'center', width:80},
      {name:'topic', align:'center'},
      {name:'status_client', align:'center',width:120},
      {name:'deadline_title', index:'date_end', align:'center', width:120}
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#pager_"+type),
    sortname: sort_name,
    sortorder: sort_order,        
    onSelectRow: function (row_id) {
      var rowData = jQuery(this).getRowData(row_id); 
      var id = rowData['order_id'];
      document.location.href = '/user/order_details/'+id;
    },
    autowidth: true,
    height: "auto",
    toppager: true
  }).trigger("reloadGrid");
}

$().ready(function(){
  $("#tabs").tabs();
  $("#block_active").click(function(){
    build_grid('active');
  }); 
  $("#block_all").click(function(){
    build_grid('all');
  });
  if(window.location.hash == '#all')
    $("#block_all").click();
  else
    $("#block_active").click();
  // ! this is workaround to remove strange right margin (10) that involves horizontal scroll (inspect class .ui-jqgrid but in Chrome)
  // i see it may be called at the end of each tab onClick()
  // see last tab (cancelled) on click - few lines above, and play with last tab
  $(".ui-jqgrid").width( $(".ui-jqgrid-bdiv").width() - 10); 
}); 
</script>   