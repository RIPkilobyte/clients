<div class="artcat">
	<h1><?php echo $title; ?></h1>

	<?php foreach($records as $record) { ?>
		
		<div class="globalnews">
			<div class="blog_record_title"><?php echo $record['title']; ?></div>
			<div class="blog_date"><?php echo $record['date']; ?></div>
			<div class="blog_image"></div>
			<div class="blog_text"><?php echo $record['text']; ?></div>
			<div class="blog_more"><a href="/blog/<?php echo $record['href']; ?>">More...</a></div>
			<div class="blog_more">Comments: <?php echo $record['comments']; ?></div>
		</div>
	<?php } ?>
	<?php echo $pagination; ?>
</div>