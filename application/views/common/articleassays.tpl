<?php if($alias != 'main'): ?>
<h1><?=$title?></h1>
<?php endif; ?>
<div class="body_content">
    <?=$text?>
</div>
<style type="text/css">
ul.tabs {
  height: 28px;
  line-height: 25px;
  list-style: none;
  border-bottom: 1px solid #DDD;
  background: #FFF;
}
.tabs li {
  float: left;
  display: inline;
  margin: 0 1px -1px 0;
  padding: 0 13px 1px;
  color: #777;
  cursor: pointer;
  background: #FFFFFF;
  border: 1px solid #E4E4E4;
  border-bottom: 1px solid #F9F9F9;
  position: relative;
}
.tabs li:hover,
.vertical .tabs li:hover {
  color: #F70;
  padding: 0 13px;
  background: #FFFFDF;
  border: 1px solid #FFCA95;
}
.tabs li.current {
  color: #444;
  background: #EFEFEF;
  padding: 0 13px 2px;
  border: 1px solid #D4D4D4;
  border-bottom: 1px solid #EFEFEF;
}
.box {
  display: none;
  border-top: 1px solid #D4D4D4;
  border-width: 0 1px 1px;
  background: #ffffff;
  padding: 0 12px;
}
.box.visible {
  display: block;
}
.section.vertical {
  width: 440px;
  border-left: 160px solid #FFF;
}
.vertical .tabs {
  width: 160px;
  float: left;
  display: inline;
  margin: 0 0 0 -160px;
}
.vertical .tabs li {
  padding: 0 13px;
  margin: 0 0 1px;
  border: 1px solid #E4E4E4;
  border-right: 1px solid #F9F9F9;
  width: 132px;
  height: 25px;
}
.vertical .tabs li:hover {
  width: 131px;
}
.vertical .tabs li.current {
  width: 133px;
  color: #444;
  background: #EFEFEF;
  border: 1px solid #D4D4D4;
  border-right: 1px solid #EFEFEF;
  margin-right: -1px;
}
.vertical .box {
  border-width: 1px;
}
.benefit {
	width: 25%;
}
.benefit_image {
	background-image: url("/img/benefits.png");
	width: 50px;
	height: 50px;
	display: block;
	margin: 0 auto;
}
.benefit2 span {
	background-position: 0 -50px;
}
.benefit3 span {
	background-position: 0 -100px;
}
.benefit4 span {
	background-position: 0 -150px;
}
.benefit5 span {
	background-position: 0 -200px;
}
.benefit6 span {
	background-position: 0 -250px;
}
.benefit7 span {
	background-position: 0 -300px;
}
.benefit8 span {
	background-position: 0 -350px;
}
.benefit9 span {
	background-position: 0 -400px;
}
.benefit10 span {
	background-position: 0 -450px;
}
.benefit11 span {
	background-position: 0 -500px;
}
.benefit12 span {
	background-position: 0 -550px;
}
.benefit13 span {
	background-position: 0 -600px;
}
.benefit14 span {
	background-position: 0 -650px;
}
.benefit15 span {
	background-position: 0 -700px;
}
.benefit16 span {
	background-position: 0 -750px;
}
.benefit17 span {
	background-position: 0 -800px;
}
.benefit18 span {
	background-position: 0 -850px;
}
.discount {
	padding-top: 100px;
}
.discount_line {
	display: table;
	clear: both;
}
.disc5 {
	width: 25%;
	background: url("/img/disc_5.png") no-repeat scroll 56% 16px #F8F0F9;
}
.disc10 {
	width: 25%;
	background: url("/img/disc_10.png") no-repeat scroll 56% 16px #F8F0F9;
}
.disc15 {
	width: 25%;
	background: url("/img/disc_15.png") no-repeat scroll 56% 16px #F8F0F9;
}
</style>
<script type="text/javascript">
    var prices_table = new Array(4);
    for (j = 0; j < 4; j++) {
        prices_table[j] = new Array(42);
        for(var i = 0; i < 42; i++) {
            prices_table[j][i] = false;
        }
    }
$().ready(function(){
  $("input").each(function(){
    if($(this).val() == 'Order now') {
      $(this).bind("click", function(e){
        location.assign("http://"+window.location.hostname+"/order");
      });
    }
    if($(this).val() == 'Contact Support') {
      $(this).bind("click", function(e){
        location.assign("http://"+window.location.hostname+"/contact");
      });
    }
  });
        $("#currency_select").change(function() {
            var currency_rate = $(this).val().replace(/^....(.*)$/,"$1");
            var currency_code = $(this).val().replace(/^(...).*$/,"$1");
            if (currency_code == "GBP") {
                currency_code = "&pound;"
            } else if (currency_code == "EUR") {
                currency_code = "&euro;"
            } else {
                currency_code = "$"
            }
            for (table_id = 0; table_id < 4; table_id++) {
                var j = 0;
                $("#price_table_"+table_id).find("td").each(function() {
                    if ($(this).text().match(/^[^0-9]*[0-9.]+$/)) {
                        if (!prices_table[table_id][j]) {
                            prices_table[table_id][j] = $(this).text().replace(/[^0-9.]/,"");
                        }
                        if (currency_rate != 1) {
                            $(this).html(currency_code+Math.round(prices_table[table_id][j]*currency_rate));
                        } else {
                            $(this).html(currency_code+prices_table[table_id][j]*currency_rate);
                        }
                        j++;
                    }
                });                    
            }
        });
$('ul.tabs').delegate('li:not(.current)', 'click', function() {
  $(this).addClass('current').siblings().removeClass('current')
   .parents('div.tabs').find('div.box').hide().eq($(this).index()).fadeIn(150);
})

	$(".order_button").click(function(){
		location.assign("http://"+window.location.hostname+"/order");
	});
	$(".support_button").click(function(){
		location.assign("http://"+window.location.hostname+"/contact");
	});	
});
</script>	