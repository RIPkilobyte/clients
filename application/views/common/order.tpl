<div class="whiteblock">
<div class="cont">
<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
<script type="text/javascript" src="/js/order_scripts.js"></script>
<script src="/js/nicEdit.js" type="text/javascript"></script>
<?php if(isset($errors) && $errors): ?>
    <style type="text/css">
    #three_step {
        height: auto;
    }
.step_table input[type='text'].error_bb{
 border-color:#FF001D;
 background-color: #fff;
}
.note_bottom {
 color: #284a71;
 font-family: 'TornadoCyr-Regular';
}    
    </style>
<?php endif; ?>
<script type="text/javascript">
var phone_prefix = {};
<?php
    foreach ($countries as $key => $value) {
        echo 'phone_prefix['.$value['country_id'].'] = '.$value['phone_prefix'].';';
    }
?>
</script>
<form method="post" id="order_form" action="/order" enctype="multipart/form-data" autocomplete="off">
<?php
    //if(isset($error['login_error']))
        //echo '<div class="block_element">'.$error['login_error'].'</div>';
?>
<div id="first_step">
<div class="nav-steps order-form-header order-desktop-view">
    <div class="nav-step to-step-1 active-step">1. Paper details</div>
    <div class="nav-step to-step-2">2. Price calculator</div>
    <div class="nav-step to-step-3">3. Contact information</div>
</div>
<table class="step_table step_table1">
    <tr>
        <td><div class="block_element">
            <div class="block_element_head">Type of service<!-- hint -->
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                    <div class="somehint" style="width:300px;">
                        Writing from scratch implies that the writer will create a plagiarised free original paper for you. Editing/Proofreading implies we look over an existing paper and suggest ways to improve it
                        <img src="../img/bottomugol.png">
                    </div>
                </a>
                <!-- /hint --></div></td>
                <td>
        <select name="work_type" id="work_type" class="input_hints calc_selector" style="width:220px">
            <?php 
                $checked1 = '';
                $checked2 = '';
                if(isset($work_type) && $work_type == 'new')
                    $checked1 = 'selected="selected"';
                if(isset($work_type) && $work_type == 'edit')
                    $checked2 = 'selected="selected"';
            ?>
            <option value="new" <?php echo $checked1; ?>>Writing from scratch</option>
            <option value="edit" <?php echo $checked2; ?>>Editing/proofreading</option>
        </select>

        </div>
        </td>
<!--     </tr>
    <tr> -->
        <td style="width:150px;"><div class="block_element">
            <div class="block_element_head" >Type of paper
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                    <div class="somehint" style="width:300px;">
                        Please select the necessary type of paper
                        <img src="../img/bottomugol.png">
                    </div>
                </a>

            </div></td><td>
        <div class="error hidden" id="error_paper_type"> </div>
        <?php //echo form_error('paper_type'); ?>
        <select name="paper_type" id="paper_type" class="calc_selector" style="width:220px" onChange="change_paper_types()">
            <option value="0">Choose Type Of Paper</option>
            <?php
                foreach ($paper_types as $key => $value) {
            $selected = '';
            if(isset($paper_type) && $paper_type == $value['paper_type_id'])
            $selected = ' selected="selected"';
            $class = '';
            if(isset($value['class']) && $value['class'] == 'indent')
            $class = 'class="indent"';
            echo '<option '.$class.' value="'.$value['paper_type_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
            ?>
        </select>

        </div>

    </td>
    </tr>
    <tr>
        <td><div class="block_element" id="subject_block">
            <div class="block_element_head">Subject
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                    <div class="somehint" style="width:300px;">
                        Choose the subject field of your task
                        <img src="../img/bottomugol.png">
                    </div>
                </a>
            </div>
        </td><td>
        <div class="error hidden" id="error_subject"></div>
        <?php //echo form_error('subject'); ?>
        <select name="subject" id="subject" class="calc_selector" style="width:220px;">
            <option value="0">Choose Your Subject</option>
            <?php
                foreach ($subjects as $key => $value) {
            $selected = '';
            if(isset($subject) && $subject == $value['subject_id'])
            $selected = ' selected="selected"';
            echo '<option value="'.$value['subject_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
            ?>
        </select>

        </div></td>
 <!--    </tr> -->
    <td>

        <div class="block_element" id="paper_format_block">
            <div class="block_element_head" >Referencing Style
                <!-- hint -->
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                    <div class="somehint" style="width:300px;">
                      Please choose your referencing style that your paper has to be formatted into. If you do not know what style do you need – just choose “GENERAL”.

                        <img src="../img/bottomugol.png">
                    </div>
                </a>
                <!-- /hint -->
            </div>
    </td>
    <td>

        <div class="error hidden" id="error_paper_format"></div>
        <?php //echo form_error('paper_format'); ?>
        <select name="paper_format" id="paper_format" class="calc_selector" style="width:220px;">
            <option value="0">Choose Referencing Style</option>
            <?php
                foreach ($paper_formats as $key => $value) {
            $selected = '';
            if(isset($paper_format) && $paper_format == $value['paper_format_id'])
            $selected = ' selected="selected"';
            echo '<option value="'.$value['paper_format_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
            ?>
        </select>

        </div></td>
</tr>

    

   
    <input type="hidden" value="none" name="additional_materials" />
    <!--    
        <tr>
        
        
        
        
            <td>
              <div class="block_element_head">Additional materials:
    
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
            <div class="somehint" style="width:300px;">
            Please indicate whether or not you can provide additional materials now or not at all
            <img src="../img/bottomugol.png">
            </div>
            </a>
    
            </div>
            </td>
            <td>
            <?php 
                $checked1 = '';
                $checked2 = '';
                $checked3 = '';
                if(isset($additional_materials) && $additional_materials == 'none')
                    $checked1 = 'selected="selected"';
                if(isset($additional_materials) && $additional_materials == 'later')
                    $checked2 = 'selected="selected"';
                if(isset($additional_materials) && $additional_materials == 'needed')
                    $checked3 = 'selected="selected"';
            ?>    
            <select name="additional_materials" class="calc_selector" style="width:220px;" id="additional_materials" onChange="change_additional_materials()">
                <option value="none"<?php echo $checked1; ?>>Not needed</option>
                <option value="later"<?php echo $checked2; ?>>Needed, I will provide them later</option>
                <option value="needed"<?php echo $checked3; ?>>Needed, I won’t be able to provide them</option>
            </select>
            <div class="hidden" id="additional_materials_hint_for_2">Please upload your additional materials within 2 hours</div>
            <?php //echo form_error('additional_materials'); ?>
            </td>
            
            
            
            
        </tr>
    -->
</table>
<div class="wrapper_line">
   
  
   
    <div class="cell_fr cell_fr_title_h" style="margin-left:0;">
         <div class="block_element block_element_tt" style="width: 205px;">
                <div class="block_element_head block_element_head_bl" style="margin:7px 0 0;width: 201px;">
                    Upload a file   (max 3MB) <!-- hint -->
                    <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                        <div class="somehint" style="width:300px;">
                           Here  you can upload a document that might be helpful for your writer to use in his/her work. 
Please attach your document if it is necessary. We accept: Word, PDF, Powerpoint, JPG, or Excel. 
At this stage you can only download one document, but you can download as many documents as you want  when you are already registered. 

                            <img src="../img/bottomugol.png">
                        </div>
                    </a>
                    <!-- /hint -->
               
                   


                </div>
                    <div class="clear"></div>
            </div>
    </div>
     <div class="cell_fr">
         <?php
if(isset($file_error) && $file_error) {
    echo '<div class="error">There was a mistake, however all your information is saved but you have to upload your file again and then you can complete the order procedure.</div><br />';
            }
            ?>

            <div class="block_element block_element_bl">
                <div class="iconsglobal iconsglobal_bl">
                    <img src="/img/icon/doc.png" width="16px" alt="Word" title="Word" />
                    <img src="/img/icon/pdf.png" width="16px" alt="PDF" title="PDF" />
                    <img src="/img/icon/ppt.png" width="16px" alt="Powerpoint" title="Powerpoint" />
                    <img src="/img/icon/xls.png" width="16px" alt="Excel" title="Excel" />
                    <img src="/img/icon/jpg.png" width="16px" alt="Excel" title="JPG" /></div>
                <div class="uploadglobal uploadglobal_bl">  
                <input type="file" name="file" id="file" />
                <span>
                    You can upload only one document on this stage
                </span>
                </div>
            </div>
    </div>
    <div style="float:right;">
      <div class="cell_fr">
        <div class="block_element block_element_tt">
            <div class="block_element_head block_element_head_bl" style="margin:7px 0 0 ;">Sources needed
                <!-- hint -->
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                    <div class="somehint" style="width:300px; left: -17px;margin-left: 0;">
                        Indicate a number of sources you want your writer to base your paper on. If you require specific sources, please indicate what kind of sources you need in the paper details box below. If you do not have any requirements on a number of sources, jest leave this field blank. 

                        <img src="../img/bottomugol.png">
                    </div>
                </a>
                <!-- /hint -->
                </div>
            </div>
    </div>
     <div class="cell_fr">
        <?php //echo form_error('sources_needed'); ?>
        <div class="plusminusblock" style="margin-top:0;padding: 0 3px 0 0;">
            <div class="minus" id="source_minus">-</div>
            <div class="coint" id="sources_needed_div">0</div>
            <input type="hidden" name="sources_needed" value="<?php if(isset($sources_needed)) echo $sources_needed; else echo '0' ?>" id="sources_needed" />
            <div class="plus" id="source_plus">+</div>
        </div>
        </div>
 </div>
    <div class="clear"></div>
</div>

<div class="wrapper_line">
       
    <!-- </tr> -->

    <!--  <div class="block_element hidden" id="hint_for_mult_questions">
       <div class="">Please make sure you select "Problem solving" as a type of paper, if your assignment involves calculations</div>
   </div>
   <div class="block_element hidden" id="academic_level_not_applicable_block">
       Academic level is not applicable for this type of paper
   </div>
   <div class="block_element hidden" id="institution_type_block">
       <div class="block_element_head">Type of institution</div>
       <?php //echo form_error('institution_type'); ?>
       <select id="institution_type" name="institution_type" class="calc_selector" style="width:220px;">
           <option value="">Choose Institution type</option>
           <?php
               foreach ($institution_types as $key => $value) {
                   $selected = '';
                   if(isset($institution_type) && $institution_type == $value['institution_type_id'])
                       $selected = ' selected="selected"';
                   echo '<option value="'.$value['institution_type_id'].'"'.$selected.'>'.$value['title'].'</option>';
               }
           ?>
       </select>
       <div style="display:none" class="order_hints" id="institution_type_hint">Specify the type of educational institution you want to apply to</div>
   </div>
   <div class="block_element hidden" id="institution_name_block">
       <label>Institution (program) name</label>
       <?php //echo form_error('institution'); ?>
       <input type="text" name="institution" id="institution" value="<?php if(isset($institution)) echo $institution; ?>" />
       <div style="display:none" class="order_hints" id="institution_hint">Specify the name of educational institution or program you want to apply to</div>
   </div>
   <div class="block_element hidden" id="job_title_or_industry_block">
       <label>Job title or industry segment</label>
       <?php //echo form_error('job_title_or_industry'); ?>
       <input type="text" name="job_title_or_industry" id="job_title_or_industry" value="<?php if(isset($job_title_or_industry)) echo $job_title_or_industry; ?>" />
       <div style="display:none" class="order_hints" id="job_title_or_industry_hint">Specify the name of the position or industry you want to apply for</div>
   </div>
   <div class="block_element hidden" id="reasons_for_applying_block">
       <label>Reasons for applying</label>
       <?php //echo form_error('reasons_for_applying'); ?>
       <input type="text" name="reasons_for_applying" id="reasons_for_applying" value="<?php if(isset($reasons_for_applying)) echo $reasons_for_applying; ?>" />
       <div style="display:none" class="order_hints" id="reasons_for_applying_hint">Why you want to apply for this particular job or this industry segment</div>
   </div>-->


    <!-- <div class="block_element" id="general_format_block">
         General format: 275 words per page, legible font (e.g. Arial) 12 pt, double-spaced.
     </div> -->




    <!-- <tr> -->

 
        <div ><div class="block_element cell_fr cell_fr_title">
        <div class="block_element_head block_element_head_bl" >Topic
            <!-- hint -->
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                <div class="somehint" style="width:300px;">
                    Indicate the topic of your paper
                    <img src="../img/bottomugol.png">
                </div>
            </a>
            <!-- /hint -->
        </div>
    </div><div class="cell_fr">
    <div class="error hidden" id="error_topic"></div>
    <?php //echo form_error('topic'); ?>
    <input type="text" class="sbHolder sbHolder_bl" name="topic" id="topic" value="<?php if(isset($topic)) echo $topic; ?>" />
    </div>
</div>
<div class="clear"></div>
   </div>
   <div class="wrapper_line">

        <div class="cell_fr cell_fr_title">
            <div class="block_element_head block_element_head_bl"  >Paper Details
       
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                    <div class="somehint" style="width:300px;">
                        Indicate exact details of the requirements for your paper. Please note that the quality of the details will dictate the content of your assignment. Please be specific as possible
                        <img src="../img/bottomugol.png">
                    </div>
                </a>
             
            </div>
        </div>
    <div class="cell_fr cell_fr_blll">
            <div class="error hidden" id="error_paper_details"></div>
            <?php //echo form_error('paper_details'); ?>
            <div class="block_element nic">

                <textarea name="paper_details" id="paper_details" style="width:788px;height:150px;border:0px;"><?php if(isset($paper_details)) echo $paper_details; ?></textarea>
                <div style="display:none" class="order_errors" id="paper_details_error">Please enter at least 3 words</div>
                <div style="display:none" class="order_hints" id="paper_details_hint">Provide as many details of your assignment as you can</div>
            </div>
        </div>
<div class="clear"></div>
    </div>
       <div class="clear"></div>
 
        
    
    
    
    
    <div class="block_element hidden" id="paper_details_text_1">
        To ensure the highest quality of your paper that will show your personal characteristics and experience in the best way, please provide as many details as you can. Make sure to include the following information:
        <ul>
            <li>Academic background (name of educational institutions, years of graduation).</li>
            <li>Why you want to apply to this particular institution/ program.</li>
            <li>Overall information about yourself: skills, talents, your long-term and short-term goals.</li>
            <li>Other information or facts about your life that will be useful for the paper.</li>
        </ul>
    </div>
    <div class="block_element hidden" id="paper_details_text_2">
        To ensure the highest quality of your paper that will show your personal characteristics and experience in the best way, please provide as many details as you can. Make sure to include the following information:
        <ul>
            <li>Academic background (name of educational institutions, years of graduation).</li>
            <li>Professional experience.</li>
            <li>Professional skills.</li>
            <li>Personal characteristics.</li>
            <li>Information about certificates or courses that you have attended.</li>
            <li>Other information or facts about your life that will be useful for the paper.</li>
        </ul>
    </div>
   
   

    <div class="block_element block_element_btn">
        <input type="button" class="btn btn-primary" onclick="go_step_two()" value="GO to Step II" style="right: 0px;margin-top: 15px;margin-right: 22px;"/>
    </div>
    <!-- first -->
</div>
<div id="two_step" class="hidden">
<div class="nav-steps order-form-header order-desktop-view">
    <div class="nav-step to-step-1">1. Paper details</div>
    <div class="nav-step to-step-2 active-step">2. Price calculator</div>
    <div class="nav-step to-step-3">3. Contact information</div>
</div>
<table class="step_table step_table_left step_table1">
    <tr>
        <td>
        <div class="block_element hidden" id="academic_level_block">
                <div class="block_element_head" >Academic Level<!-- hint -->
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                <div class="somehint" style="width:300px;">
                Select your academic level
                <img src="../img/bottomugol.png">
                </div>
                </a>
                <!-- /hint --></div></td><td>
                <div class="error hidden" id="error_academic_level"></div>
                <?php //echo form_error('academic_level'); ?>
                <select name="academic_level" id="academic_level" class="calc_selector" style="width:220px;">
                    <option value="0">Choose Academic Level</option>
                    <?php
                        foreach ($academic_levels as $key => $value) {
                            $selected = '';
                            if(isset($academic_level) && $academic_level == $value['academic_level_id'])
                                $selected = ' selected="selected"';
                            echo '<option value="'.$value['academic_level_id'].'"'.$selected.'>'.$value['title'].'</option>';
                        }
                    ?>         
                </select>
                
            </div>
        </td>
        <td><div class="block_element" id="number_of_pages_block">
        <div class="block_element_head" style="padding-left:55px !important;" >Number of pages<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;left:-290px;">
        Please enter a desired number of pages. Should be a full number. Please note that a standard page is 275 words.
        <img src="../img/bottomugol.png" style="left:292px;">
        </div>
        </a>
        <!-- /hint --></div></td><td>
        <div class="error hidden" id="error_number_of_pages"></div>
        <?php //echo form_error('pages'); ?>
        <div class="plusminusblock plusminusblock_nn">
        <div class="minus" id="number_of_pages_minus">-</div>
        <div class="coint" id="number_of_pages_div"><?php if(isset($number_of_pages)) echo $number_of_pages; else echo '1' ?></div>
        <input type="hidden" name="number_of_pages" value="<?php if(isset($number_of_pages)) echo $number_of_pages; else echo '1' ?>" id="number_of_pages" />
        <div class="plus" id="number_of_pages_plus">+</div>
        </div>
        <!-- <div style="margin-top: 10px;">&nbsp;&nbsp;1 page = 275 words</div> -->
        <input type="hidden" name="spacing" value="Single" />
        </div>
        </td>
    </tr>
    <tr>
      <td><div class="block_element">
        <div class="block_element_head" >Deadline<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        Please choose your deadline date to receive the assignment. Please make note to give yourself enough time to review the work before handing it in
        <img src="../img/bottomugol.png">
        </div>
        </a>
        <!-- /hint --></div></td><td>
        <?php //echo form_error('deadline'); ?>
        <div class="error hidden" id="error_deadline"></div>
        <select name="deadline" id="deadline" class="" style="width:220px;">
        <option value="0">Choose Deadline</option>
        <?php
        foreach ($deadlines as $key => $value) {
        $selected = '';
        if(isset($deadline) && $deadline == $value['deadline_id'])
        $selected = ' selected="selected"';
        echo '<option value="'.$value['deadline_id'].'"'.$selected.'>'.$value['title'].'</option>';
        }
        ?>          
        </select>

        </div>
        </td>
        <td><div class="block_element" id="slides">
        <div class="block_element_head" style="padding-left:55px !important;">Number of slides<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;left:-290px;
        ">
        If you need Power Point Presentation, specify the number of slides you need. 1 slide=50% of the cost per page.
        <img src="../img/bottomugol.png" style="left:292px;">
        </div>
        </a>
        <!-- /hint --></div></td><td>
        <?php //echo form_error('slides'); ?>
        <div class="plusminusblock plusminusblock_nn">
        <div class="minus" id="slides_minus">-</div>
        <div class="coint" id="slides_div">0</div>
        <input type="hidden" name="slides" value="<?php if(isset($slides)) echo $slides; else echo '0' ?>" id="number_of_slides" />
        <div class="plus" id="slides_plus">+</div>
        </div>

        </div>
        </td>
      
</tr>
<!--
<tr>
<td><div class="block_element_head">Spacing
<a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        Please choose the line spacing that you want your paper to be structured in
        <img src="../img/bottomugol.png">
        </div>
        </a>
        <!-- /hint --><!--
</div></td>
<td style="padding-top: 6px;"><div class="block_element">
        
        <?php //echo form_error('spacing'); ?>
        <?php
            if(isset($spacing)) {
                $checked1 = '';
                $checked2 = '';
                if($spacing == 'Single')
                    $checked1 = 'checked="checked"';
                else
                    $checked2 = 'checked="checked"';
            }
            else
                $checked2 = 'checked="checked"';
        ?>    
        <label class="spacing_input_hints" onclick="change_spacing()" style="border-right: 1px solid rgb(187, 187, 187);
padding-right: 14px;
margin-right: 3px;"><input type="radio" name="spacing" value="Single"<?php echo $checked1; ?> style="" />&nbsp;Single Spaced</label>
        <label class="spacing_input_hints" onclick="change_spacing()"><input type="radio" name="spacing" value="Double"<?php echo $checked2; ?> />&nbsp;Double Spaced</label>
        <div>1 page = 550 words</div>
        <div style="" class="" id="spacing_hint_2">1 page = 275 words</div>
    </div></td></tr>
-->
   
    <!--
    <tr>
<td><div class="block_element preferred_writer_block">
        <div class="block_element_head">Preferred writer
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        If you have ordered from us before, you can request your previous writer
        <img src="../img/bottomugol.png">
        </div>
        </a>
        </div></td><td>
        <?php echo form_error('preferred_writer'); ?>
        <?php 
            $checked1 = '';
            $checked2 = '';
            $checked3 = '';
            if(isset($preferred_writer) && $preferred_writer == '1')
                $checked1 = 'selected="selected"';
            if(isset($preferred_writer) && $preferred_writer == '2')
                $checked2 = 'selected="selected"';
            if(isset($preferred_writer) && $preferred_writer == '3')
                $checked3 = 'selected="selected"';
        ?>     
        <select name="preferred_writer" id="preferred_writer" class="dynamic-hints" style="width:220px;" onChange="change_preferred_writer()">
            <option value="1"<?php echo $checked1; ?>>Any writer</option>
            <option value="3"<?php echo $checked3; ?>>My previous writer</option>
        </select>
        <div class='hidden' id="previous_writer_hint">If the preferred writer is not able to complete your order, it will be assigned to another equally proficient writer</div>
        
    </div></td></tr>
    <tr id="previous_writer_block">
    <td>
    <div class="block_element ">
        <div class="block_element_head">Writer's ID</div>
        
        </td><td>
        <?php echo form_error('previous_writer'); ?>
        <input type="text" name="previous_writer" id="previous_writer" value="<?php if(isset($previous_writer)) echo $previous_writer; ?>" />
       
    </div></td>
    </tr>
    -->

</table>
<table class="step_table step_table_left step_table_helf_cells mb_table step_table1">
     <tr>
            <td style="padding-bottom:10px; padding-right:21px !important;"><span id="deadline_calc">The deadline for the first draft is <b>7 Aug 02 PM</b>.</span> <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        This is the deadline for a draft of your paper to be ready. Please note that a payment has to be made within 15 minutes from finishing this registration. Otherwise the time for the deadline will be calculated from the moment when the payment was made. 
        <img src="../img/bottomugol.png">
        </div>
        </a>
        <input type="hidden" value="1" name="preferred_writer" />
        </td>
        <td>
                  <div class="block_element checkboxes plagiarism_report_block" style="padding-left:51px;">
            <div class="block_element_head"></div>
            <?php
            $checked = '';
            if(isset($plagiarism_report) && $plagiarism_report == '1')
            $checked = 'checked="checked"';
            ?>
            <label>
            <input type="checkbox" name="plagiarism_report" id="plagiarism_report" value="1"<?php echo $checked; ?> />&nbsp;<span >I want to receive official Plagiarism report</span> $<span id='plag_price'>0</span> 
            <!-- hint -->
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
            <div class="somehint" style="width:300px;
            left: -284px;">
            We guarantee that our papers are plagiarism-free. If you tick this option, in addition, we will provide you an official third party detailed plagiarism report. We will attach it to the final version of your writing. 
            <img src="../img/bottomugol.png" style="right: 18px!important;left: auto;">
            </div>
            </a>
            <!-- /hint -->
            </label>&nbsp;
            </div>
        </td>
    </tr>
            <tr>
           <td style="padding-right:9px !important;">
           <div class="block_element discount_code_block">
        <div class="block_element_head block_element_head_cell" style="margin-right:-2px;">Discount code<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        Please type in your discount code into this box if you have one. If you don’t have one then please leave it blank
        <img src="../img/bottomugol.png" style="">
        </div>
        </a>
        <!-- /hint --></div>
        <?php echo form_error('discount_code'); ?>
        <input type="text" style="width:285px !important;" id="discount_code" class="discount_code_inn" name="discount_code" value="<?php if(isset($discount_code)) echo $discount_code; ?>" />
        <input type="hidden" value="0" id="applied_discount" name="applied_discount" />
        </div>
        </td>

            <td>

      

            <div class="block_element checkboxes abstract_block" style="padding-left:51px;">
            <div class="block_element_head"></div>
            <?php
            $checked = '';
            if(isset($abstract_page) && $abstract_page == '1')
            $checked = 'checked="checked"';
            ?>    
            <label>
            <input type="checkbox" name="abstract_page" onChange="order_calc_selector()" id="abstract_page" value="1"<?php echo $checked; ?> />&nbsp;<span>Add an Abstract page to my paper</span> $14.99 
            <!-- hint -->
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
            <div class="somehint" style="width:300px;
            left: -284px;">
            Often academic papers must contain an abstract, particularly if the writing consists of more than two pages and APA formatting. Tick this option if you want your writing to include an abstract page.
            <img src="../img/bottomugol.png" style="right: 18px!important;left: auto;">
            </div>
            </a>
            <!-- /hint -->
            </label>&nbsp;
            </div>
            </td>
        </tr>
</table>
    
    
    
    
    
    
    
    
    
    
    <div class="block_element">
        <div class="block_element_head"></div>
        <?php
            $checked = '';
            if(isset($top_priority) && $top_priority == '1')
                $checked = 'checked="checked"';
        ?>    
       <?php /* <label>
            <input type="checkbox" name="top_priority" onChange="calc_selector()" id="top_priority" value="1"<?php echo $checked; ?> />&nbsp;I want to order TOP priority customer service for $14.99
        </label>&nbsp;
        <a href='#' rel='tooltip' id="top_priority_input_hints">?</a>*/ ?>
        <div class='order_hints hidden' id="top_priority_hint">Top priority customer support means that replying to your messages and answering your calls will be our first order of business. Additionally, you will receive an SMS notification when a writer is assigned to your order, and when it is completed. Make sure to provide us with the correct phone number if you want this feature to work properly.</div>
    </div>
    
    <div class="block_element">
        <input type="button" class="btn btn-primary back_btn" onclick="back_step_one()" value="Back to step I" style="float:left;margin-left: 22px;" />
    </div>
    <div class="block_element">
        <input type="button" class="btn btn-primary" onclick="go_step_three()" value="GO to Step III" style="right: 0px;margin-right: 22px;" />
    </div>
    <!-- two -->
</div>
<div id="three_step" class="hidden">
<div class="nav-steps order-form-header order-desktop-view">
    <div class="nav-step to-step-1">1. Paper details</div>
    <div class="nav-step to-step-2">2. Price calculator</div>
    <div class="nav-step to-step-3 active-step">3. Contact information</div>
</div>
<div class="left-side left_complite_wrap">
<table class="complete">
<tr>
<td class="gray">Type of service:</td>
<td class="black f1" id="preview_service_type">Writing from scratch</td>
<td class="gray gray_big">No. of sources:</td>
<td class="black" id="preview_sources">1</td>
</tr>
<tr>
<td class="gray">Academic Level:</td>
<td class="black f1" id="preview_academic_level"></td>
<td class="gray gray_big">Abstract page:</td>
<td class="black" id="preview_abstract">No</td>
</tr>
<tr>
<td class="gray">Type of paper:</td>
<td class="black f1" id="preview_paper_type"></td>
<td class="gray gray_big">Plagiarism rep.:</td>
<td class="black" id="preview_plagiarism">No</td>
</tr>
<tr>
<td class="gray">Subject:</td>
<td class="black f1" id="preview_subject"></td>
<td class="gray gray_big">Paper details:</td>
<td class="black" id="preview_details">No</td>
</tr>
</tr>
<tr>
<td class="gray">No. of pages:</td>
<td class="black f1" id="preview_pages">0</td>
<td class="gray gray_big">Attachment:</td>
<td class="black" id="preview_attachment">No</td>
</tr>
    <tr>
<td class="gray">No. of slides:</td>
<td class="black f1" id="preview_slides">0</td>
<td class="gray gray_big">Price:  <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                <div class="somehint" style="width:300px;font-family: 'TornadoCyr-Regular';
                ">
            
 The payment can be done in any currency <br>as it will be automatically converted<br> into USD.
                <img src="../img/bottomugol.png" style="">
                </div>
                </a></td>
<td class="black" id="final_price">
    USD 0.00
</td>
</tr> 
<tr>
    <td class="gray">Referencing:</td>
    <td class="black f1" id="preview_referencing"></td>
    <td class="gray">Discount:</td>
    <td class="black f1" id="final_discount">0 % (USD 0.00)</td>
</tr>
</table>
<span class="gray" style="width:auto; float:left;">Topic:</span> <span style="text-align:left; font-weight:bold;padding-right:12px;word-wrap:break-word;display:block;" class="blue blue_bbb" id="preview_topic"></span>
</div>
<ul class="right_side">
    <li>
      <div class="time_title">
          time left
      </div> 
      <div class="cells">
        <span class="silver_t" style="font-weight:normal;">
        days
        </span>
         <div class="day_left">
    <b>3</b>
            
         </div>
      </div>
       <div class="cells time_title">
        <span class="silver_t" style="font-weight:normal;">
        hours
        </span>
        <div class="day_left day_left_last">
          00  
        </div>
       </div> 
    </li>
    <li >
        <span class="silver_t blue_t_pb">
           current status:&nbsp; 
       </span>  
           <span class="blue_t blue_t_pb">
           <?php if(isset($_SESSION['user_id'])): ?>
                <b>Payment</b>
            <?php else: ?>
                <b>Registration & Payment</b>
            <?php endif; ?>
                <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                <div class="somehint" style="width:220px;left: -212px;
                ">
           This is a status of your order <br>at this moment.

                <img src="../img/bottomugol.png" style="left: 214px;">
                </div>
                </a>
           </span>
    </li>
    <li class="last">
        <span class="silver_t">
            Ecpected:&nbsp;
        </span>  
          <span class="blue_t">
            <b><span id="blue_t_date">31 oct. 18:15</span></b>
             <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
                <div class="somehint" style="width:220px;left: -212px;
                ">
         Please note that an online payment has to be made before we start working on your order. Upon completion of this registration you will be taken to Paypal page where you can make your payment.

                <img src="../img/bottomugol.png" style="left: 214px;">
                </div>
                </a>
        </span>
    </li>
</ul>
<?php 
            $checked1 = '';
            $checked2 = '';

            if(isset($user) && $user == '1')
                $checked1 = 'checked="checked"';
            if(isset($user) && $user == '2')
                $checked2 = 'checked="checked"';
            else
                $checked2 = 'checked="checked"';
        ?>   
        <table>
</table>
  <div class="clear"></div>
<?php if(!isset($_SESSION['user_id'])): ?>
<ul class="order-tabs">
    <input type="hidden" name="user" id="user_choise" value="<?php if($checked1) echo '1'; elseif($checked2) echo '2'; ?>" />
    
    <li class="title_user_select pointer<?php if($checked2) echo ' active';?>" id="title_login"><a onclick="change_user(2)">Sign in</a></li>
    <li class="title_user_select pointer<?php if($checked1) echo ' active';?>" id="title_reg"><a onclick="change_user(1)">I am new here</a></li>
    <span id="discount_fail"></span>
</ul>

    <fieldset class="hidden" id="client_login" style="margin-bottom:15px;">
           <table class="step_table step_table1">
<tr>
        <td > 
            <table>
            <tr>
            <td  class="login_b">
            <div class="block_element">
            <div class="block_element_head block_element_head_em">Email<!-- hint -->
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
            <div class="somehint" style="width:300px;">
            Please input the email of your existing account
            <img src="../img/bottomugol.png">
            </div>
            </a>
            <!-- /hint --></div>  
            </td>
            <td>

            <?php echo form_error('email_login'); ?>
            <?php
            if(isset($errors['login_error']))
            echo $errors['login_error'];
            ?>
            <input style="margin-bottom: 13px;"  type="text" class="error_bb_input <?php if(!isset($email_login) || form_error('email_login')) echo 'error_bb'; ?>" name="email_login" id="email_login" value="<?php if(isset($email_login)) echo $email_login; ?>" />

            </div>

            </td>
            </tr>
            <tr>
            <td ><div class="block_element">
            <div class="block_element_head">Password<!-- hint -->
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
            <div class="somehint" style="width:300px;">
            Please input the password to your existing account
            <img src="../img/bottomugol.png">
            </div>
            </a>
            <!-- /hint --></div></td>
            <td class="login_b">
            <?php echo form_error('password_login'); ?>
            <input style="margin-bottom: 13px;" type="password" class="error_bb_input <?php if(!isset($password_login) || form_error('password_login')) echo 'error_bb'; ?>" name="password_login" id="password_login" value="<?php if(isset($password_login)) echo $password_login; ?>" />

            </div> 
            </td>
            </tr>
            </table>
        </td> 
        <td>
 <table class="step_table step_table1">
    <tr>
    <td> <div class="block_element_head block_element_headpl">Please enter Captcha</div></td>
    <td>
    <?php echo form_error('captcha'); ?>
    <?php
            if(isset($captcha))
                echo $captcha['image'];
        ?>
        <input class="error_bb_input error_bb" name="captcha[]" type="text" style="
        width: 158px!important;
        margin-top: -43px;
        display: block;
        margin-left: 229px;
        "/></td>
    </tr>
    <tr>
    <td style="vertical-align: middle;padding-top:9px;">
    <input class="error_bb_chbx <?php if(!isset($agree) || form_error('agree')) echo 'error_bb'; ?>" type="checkbox" name="agree" value="1" />&nbsp;<span>I agree with:</span></td>
    <td style="vertical-align: middle;padding-top:9px;"> <div class="block_element checkboxes">
        <?php echo form_error('agree'); ?>
        <label>
           <span>
            <a href='/money-back' target='_blank'>Money Back Guarantee</a>, 
            <a href='/privacy-policy' target='_blank'>Privacy Policy</a>, 
            <a href='/terms-of-use' target='_blank'>Terms of Use</a>.</span>
        </label>
    </div></td>
    </tr>
    </table>
        </td>
    </tr>
</table>
   
    
    </fieldset>

    <fieldset class="hidden" id="client_create">
    <table class="step_table step_table_reg_stage step_table1" style="margin-top: 0px;">


    <tr>
    <td>
        <div class="block_element">
        <div class="block_element_head">Email<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        Please make sure you have entered the correct email address to receive notifications of order completion status.
        <img src="../img/bottomugol.png">
        </div>
        </a>
        <!-- /hint --></div>
      <div style="display:inline-block;margin-top: 13px;">  <?php echo form_error('email_reg'); ?>
        <?php
        if(isset($errors['email_check_fail']))
        echo $errors['email_check_fail'];
        ?>
        <?php
        if(isset($errors['email_check_fail']))
        echo 'Please click on the <a href="/user/forgot_password" target="_blank">link</a> below to restore your password.<br />';
        ?>
        <input type="text" class="error_bb_input <?php if(!isset($email_reg) || form_error('email_reg')) echo 'error_bb'; ?>" id="email_reg" name="email_reg" value="<?php if(isset($email_reg)) echo $email_reg; ?>" />
</div>
        </div>
    </td>
    <td style="text-align:right;">
       <div class="block_element">
        <div class="block_element_head">Password<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        Your password must contain at least 6 symbols (digits and letters only).
        <img src="../img/bottomugol.png">
        </div>
        </a>
        <!-- /hint --></div>
        <div style="display:inline-block;margin-top: 13px;"><?php echo form_error('password_reg'); ?>
        <input class="error_bb_input <?php if(!isset($password_reg) || form_error('password_reg')) echo 'error_bb'; ?>" type="password" id="password_reg" name="password_reg" value="<?php if(isset($password_reg)) echo $password_reg; ?>" /></div>
        
    </div> 
    </td>
    </tr>
    <tr>
    <td><div class="block_element">
        <div class="block_element_head">Confirm email</div>
        <div style="display:inline-block;margin-top: 13px;"><?php echo form_error('confirm_email'); ?>
        <?php
            //if(isset($errors['email_check_fail']))
                //echo $errors['email_check_fail'];
        ?>
        <input type="text" class="error_bb_input <?php if(!isset($confirm_email) || form_error('confirm_email')) echo 'error_bb'; ?>" name="confirm_email" value="<?php if(isset($confirm_email)) echo $confirm_email; ?>" /></div>
    </div></td>
        <td style="text-align:right;"><div class="block_element">
        <div class="block_element_head">Confirm password</div>
        <div style="display:inline-block;margin-top: 13px;"><?php echo form_error('confirm_password'); ?>
        <input type="password" class="error_bb_input <?php if(!isset($confirm_password) || form_error('confirm_password')) echo 'error_bb'; ?>" name="confirm_password" value="<?php if(isset($confirm_password)) echo $confirm_password; ?>" /></div>
    </div>
    </td>
    </tr>
    <tr>
    <td><div class="block_element">
        <div class="block_element_head">First Name</div>
        <div style="display:inline-block;margin-top: 13px;"><?php echo form_error('first_name'); ?>
        <input type="text" class="error_bb_input <?php if(!isset($first_name) || form_error('first_name')) echo 'error_bb'; ?>" name="first_name" maxlength="15" value="<?php if(isset($first_name)) echo $first_name; ?>" /></div>
    </div>
    </td>
    <td style="text-align:right;">
    <div class="block_element">
        <div class="block_element_head">Last name</div>
        <div style="display:inline-block;margin-top: 13px;"><?php echo form_error('last_name'); ?>
        <input type="text" class="error_bb_input <?php if(!isset($last_name) || form_error('last_name')) echo 'error_bb'; ?>" name="last_name" maxlength="15" value="<?php if(isset($last_name)) echo $last_name; ?>" /></div>
    </div>
    </td>
    </tr>

<tr>
<td>
     <div class="block_element block_element_bbb">
        <div class="block_element_head" style="margin-bottom: 10px;text-align:left;">Country</div>
       <div style="display:inline-block;margin-top: 13px;"> <?php echo form_error('country'); ?>
        <select name="country" id="country" style="width: 220px;" onChange="change_country()">
            <option value="">Choose country</option>
            <?php
                foreach ($countries as $key => $value) {
                    $selected = '';
                    if(isset($country) && $country == $value['country_id'])
                        $selected = ' selected="selected"';
                    echo '<option value="'.$value['country_id'].'"'.$selected.'>'.$value['title'].'</option>';
                }
            ?>         
        </select></div>
    </div>
</td>

<td style="text-align:right;">
<div class="block_element">

        <div class="block_element_head">Phone<!-- hint -->
        <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: -4px;">
        <div class="somehint" style="width:300px;">
        Please make sure you have entered the correct phone number, so that we could contact you to check on the details of your order and to verify your payment.
        <img src="../img/bottomugol.png" style="">
        
       
        </div>
        </a>
        <!-- /hint --></div>

       <div style="display:inline-block;margin-top: 13px;"> <?php echo form_error('phone_number'); ?>
        <!--<span class="add-on">+</span>-->
        <input disabled="disabled" size="2" id="phone_prefix" class="phone_prefix input-mini" type="text" value="" style="width:37px !important;height:20px;vertical-align: top;" />
        <input size="16" class="error_bb_input <?php if(!isset($phone_number) || form_error('phone_number')) echo 'error_bb'; ?>" maxlength="10" id="phone_number" name="phone_number" type="text" value="<?php if(isset($phone_number)) echo $phone_number; ?>" /></div>

    </div>
    </td>
 </tr> 

    </table>
<table class="step_table step_table1">
    <tr>
    <td> <div class="block_element_head block_element_headpl">Please enter Captcha</div></td>
    <td>
   <div style="margin-top:13px;"> <?php echo form_error('captcha'); ?>
    <?php
            if(isset($captcha))
                echo $captcha['image'];
        ?>
        <input class="error_bb_input error_bb" name="captcha[]" type="text" style="
        width: 158px!important;
        margin-top: -43px;
        display: block;
        margin-left: 229px;
        "/>
            <div style="width: 787px; margin-top:8px;">
                 <input class="error_bb_chbx <?php if(!isset($agree) || form_error('agree')) echo 'error_bb'; ?>" type="checkbox" name="agree" value="1" />&nbsp;<span>I agree with:</span>
                 <label>
           <span>
            <a href='/money-back' target='_blank'>Money Back Guarantee</a>, 
            <a href='/privacy-policy' target='_blank'>Privacy Policy</a>, 
            <a href='/terms-of-use' target='_blank'>Terms of Use</a>.</span>
        </label>
            </div></div>
        </td>
    </tr>
    <tr>
    </tr>
    </table>
    </fieldset>
<?php else: ?>
<table style="height:100px;">
<tr><td><span id="discount_fail"></span></td></tr>
</table>
<?php endif; ?>
   
    <div class="block_element">
        <input type="button" class="btn btn-primary back_btn" onclick="back_step_two()" value="Back to step II" style="float:left;margin-left: 22px;bottom: 67px;" />
    </div>
    <div class="block_element">
        <input type="button" class="btn btn-primary apply_dis" onclick="apply_discount()" value="Apply discount" style="right: 170px;margin-right: 22px;bottom: 67px;" />
        <input type="button" class="btn btn-primary" onclick="nicEditors.findEditor('paper_details').saveContent();fsubmitter_form()" value="Proceed" style="right: 0px;margin-right: 22px;bottom: 67px;" />
        <input type="submit" id="submitter_form" style="display:none;" />
    </div>
    <span class="bottom_text">Please note that an online payment has to be made before we start working on your order. Upon completion of this registration you will be taken to Paypal page where you can make your payment.</span>
</div>

<div class="block_element order-price" >
    FULL PRICE USD <span class='order_form_price' id="calc_result">0,00</span>  <!-- hint -->
            <a href="#" rel="tooltip" class="hints"><img src="img/znak.png" style="margin-bottom: 1px;">
            <div class="somehint" style="width:300px;
            left: -283px;bottom:41px;font-family: 'TornadoCyr-Regular';">
          The payment can be done in any currency as it will be automatically converted into USD.

            <img src="../img/bottomugol.png" style="right: 18px!important;left: auto;">
            </div>
            </a>
            <!-- /hint -->
    <div class='order_form_save hidden'>(Total save <span class="span_order_form_save">0</span>$)</div>
</div>
<!--
<div class="block_element hide" id="num_of_questions">
    <label>Questions</label>
    <input type="text" name="questions" maxlength="3" />
</div>

<div class="block_element hide" id="num_of_problems">
    <label>Problems</label>
    <input type="text" name="problems" maxlength="3" />
</div>
-->


<input type="hidden" name="submit" value="1" />
</form>
<script type="text/javascript">
$().ready(function(){
    //$("*").animate({ scrollTop: $("#order_form").offset().top - 130}, { duration: 'fast', easing: 'swing'});
    change_country();
    <?php if(isset($user) && $user == '1'): ?>
        change_user(1);
    <?php elseif(isset($user) && $user == '2'): ?>
        change_user(2);
    <?php else: ?>
        change_user(2);
    <?php endif; ?>
});
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor({maxHeight : 150});
    myNicEditor.addInstance('paper_details');
    <?php
    if(!isset($file_error) || !$file_error)
        if(isset($errors) && $errors)
            echo 'after_errors();';
    ?>    
});
</script>
</div>
</div>