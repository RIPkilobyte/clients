<div class="container">
    <div id="da-slider" class="da-slider">
        <div class="da-slide">
            <h2>Это CMS!</h2>
            <p>Модуль реализован как хелпер. Его можно включить и выключить в шаблоне</p>
            <a href="#" class="da-link">Узнать больше</a>
            <div class="da-img"><img src="img/contentslides/1.png" alt="image01" /></div>
        </div>
        <div class="da-slide">
            <h2>Easy management</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <a href="#" class="da-link">Узнать больше</a>
            <div class="da-img"><img src="img/contentslides/2.png" alt="image01" /></div>
        </div>
        <div class="da-slide">
            <h2>Revolution</h2>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <a href="#" class="da-link">Узнать больше</a>
            <div class="da-img"><img src="img/contentslides/3.png" alt="image01" /></div>
        </div>
        <div class="da-slide">
            <h2>Quality Control</h2>
            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
            <a href="#" class="da-link">Узнать больше</a>
            <div class="da-img"><img src="img/contentslides/4.png" alt="image01" /></div>
        </div>
        <nav class="da-arrows">
            <span class="da-arrows-prev"></span>
            <span class="da-arrows-next"></span>
        </nav>
    </div>
</div>

<script type="text/javascript" src="<?=$this -> config -> base_url(); ?>js/slideshowcontent/jquery.cslider.js"></script>

<script type="text/javascript">
    $(function() {
        $('#da-slider').cslider({
            autoplay	: true,
            bgincrement	: 450
        });
    });
</script>