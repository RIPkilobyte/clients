<div class="artcat">
    <h3><?=$category['title']?></h3>
    <ul>
        <?php foreach($articles as $article) { ?>
            <li>
                <a href="/<?php echo $category['alias'].'/'.$article['alias']; ?>">
                    <span><?=$article['title']?></span>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>