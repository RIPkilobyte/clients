<link rel="stylesheet" href="/css/lightbox.css" media="screen"/>
<script src="/js/lightbox-2.6.min.js"></script>
<div class="gallery">
    <?php
        foreach ($images as $k => $v) {
            echo '<div class="image">';
            echo '<a href="/gallery/'.$images[$k]['file'].'" rel="lightbox[gallery]" title="'.$images[$k]['description'].'" alt="'.$images[$k]['name'].'">';
            echo '<img src="/gallery/thumb_'.$images[$k]['file'].'" />';
            echo '</a>';
            echo '</div>';
        }
    ?>
</div>