<div id="mycontact">
    
    <div class="form-wrap">
    <form id="mycontact_form" method="POST">
        
        <table>
            <tr>
                <td>Имя</td>
                <td><input type="text" name="name" required="required" /></td>
            </tr>
            <tr>
                <td>Телефон</td>
                <td><input type="text" name="phone" required="required" /></td>
            </tr>
            <tr>
                <td>E-mail</td>
                <td><input type="email" name="email" required="required" /></td>
            </tr>
            <tr>
                <td>Сообщение</td>
                <td><textarea name="msg" required="required"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="button" name="send_data" class="send_data" value="Отправить" /></td>
            </tr>
        </table>
        
    </form>
    </div>
    
    <div class="msg-wrap" style="display:none; padding:10px;">
        <span>
            Ваше письмо отправлено и будет прочитано в ближайшее время
        </span>
    </div>
    
</div>



<script>
    $(document).ready(function(){
        $('.send_data').click(function(){
            $.ajax({
                url: "/index.php/ajax/mycontactsend",
                dataType: "html",
                type: "POST",
                
                data: $('#mycontact_form').serialize(),
                complete:  function(){
                    $('.form-wrap').hide();
                    $('.msg-wrap').show();
                }
            });
        });
    });
</script>