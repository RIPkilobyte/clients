<div style="clear:both"></div>
<script src="/js/nicEdit.js" type="text/javascript"></script>
<div class="artcat">
	<div class="globalnewsinsude">
		<div class="zap">
			<h1><?php echo $record['title']; ?></h1>
			<div class="blog_date"><?php echo $record['date']; ?></div>
			<div class="blog_pre_content"><?php echo $record['pre']; ?></div>
			<div class="blog_content"><?php echo $record['text']; ?></div>
		</div>
		<div style="clear:both"></div>
		
			<?php if(isset($comments) && $comments): ?>
			<div class="blog_comments">
				<div class="commentstitle">Comments:</div>
				<?php foreach ($comments as $k => $v): ?>
					
					<div class="comment">
						<div class="comment_name"><?php echo $v['name']; ?></div>
						<div class="comment_text"><?php echo $v['text']; ?></div>
					</div>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
		
		<div style="clear:both"></div>
		<div class="form">	
			<div class="commentstitle">Leave a comment</div>
			<div class="result"></div>
			<p>
				Name*:<br />
				<input required="required" type="text" id="name" size="51" />
			</p>
			<p>
				Email*:<br />
				<input required="required" type="text" id="email" size="51" />
			</p>
			<p>
				<div class="textdiv">Text*:<br />
				<textarea required="required" id="comment_text" cols="39" rows="10"></textarea>
				</div>
			</p>
			<p><input type="button" value="Submit" onclick="submit_comment(); return false;" /></p>
		</div>
	</div>
</div>
<script type="text/javascript">
function submit_comment()
{
	var id = <?php echo $record['blog_id']; ?>;
	var name = $("#name").val();
	var email = $("#email").val();
	var nicE = new nicEditors.findEditor('comment_text');
	var text = nicE.getContent();
	if(id && name && email) {
		$.post('/blog/add_comment', {id:id, name:name, email:email, text:text}, function(data){
			if(data.status == 'ok') {
				$(".result").removeClass('error');
				$(".result").addClass('success');
				$(".result").html('Comment successfully added');
				setTimeout('location.reload(false);', 1000);
			}
			else {
				$(".result").removeClass('success');
				$(".result").addClass('error');
				$(".result").html('An error was occured. Please try again later');
				setTimeout('location.reload(false);', 1000);
			}
		}, 'json');
	}
	else {
		$(".result").removeClass('success');
		$(".result").addClass('error');
		$(".result").html('Please fill all fields');
	}
}
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor();
    myNicEditor.addInstance('comment_text');
});
</script>