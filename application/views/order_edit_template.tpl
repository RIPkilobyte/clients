<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_desc']; ?>">
	<meta name="keywords" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_key']; ?>">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/css/styles.css">
	<link rel="stylesheet" href="/css/order_styles.css">
	<link rel="stylesheet" href="/css/cusel.css" type="text/css" media="screen">
	<link rel="stylesheet" href="/css/jquery.selectbox.css" media="all" />
	<link rel="shortcut icon" href="/img/favicon.png">
	<link rel="icon" href="/img/favicon.png">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
	<script type="text/javascript" src="/js/order_edit_scripts.js"></script>
	<script src="/js/nicEdit.js" type="text/javascript"></script>
	<script type="text/javascript" src="/js/user.js"></script>
	<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
	<script type="text/javascript" src="/js/nicEdit.js"></script>
	<script type="text/javascript" src="/js/jquery.selectbox-0.2.js"></script>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>

<script type="text/javascript">
var prices = {
	<?php
		foreach ($prices as $kt=>$type) {
			echo $kt.':{';
			foreach ($type as $kl => $level) {
				echo $kl.':{';
				foreach ($level as $kd => $dl)
					echo $kd.':'.$dl.',';
				echo '},';
			}
			echo '},';
		}
	?>
};
var phone_prefix = {};
<?php
	foreach ($countries as $key => $value) {
		echo 'phone_prefix['.$value['country_id'].'] = '.$value['phone_prefix'].';';
	}
?>
</script>
<div class="wrapper">
	<div class="site-wrapper_order">
		<div class="header_block">
			<div class="logo_form float-left"><a href="/"></a></div>
			<ul class="nav_steps default_list">
				<li><a id="step_1_link" class="nav_steps_links pointer current" onclick="back_step_one()">1. Paper details</a></li>
				<li><a id="step_2_link" class="nav_steps_links pointer" onclick="go_step_two()">2. Price calculator</a></li>
				<li><a id="step_3_link" class="nav_steps_links pointer" onclick="go_step_three()">3. Details</a></li>
			</ul>
			<div class="tabs_body">
<form method="post" id="order_form" enctype="multipart/form-data" autocomplete="off">	
	<input type="hidden" value="0" id="full_price" /> 
	<div class="step_1">
		<div class="row_wrap">
			<div class="cell float-left">
				<div class="title_side">Type of paper 
					<a href="javascript:void(0);">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">Please select the necessary type of paper</span>
						</span>
					</a>
				</div>
				<select name="paper_type" id="paper_type" class="calc_selector selectbox">
					<option value="0">Choose Type Of Paper</option>
					<?php
						foreach ($paper_types as $key => $value) {
							$selected = '';
							if(isset($order['paper_type']) && $order['paper_type'] == $value['paper_type_id'])
								$selected = ' selected="selected"';
							$class = '';
							if(isset($value['class']) && $value['class'] == 'indent')
								$class = 'class="indent"';
							echo '<option '.$class.' value="'.$value['paper_type_id'].'"'.$selected.'>'.$value['title'].'</option>';
						}
					?>
				</select>
				<div class="error hidden error_title" id="error_paper_type"></div>
			</div>
			
			<div class="cell float-left">
				<div class="title_side">Subject 
					<a href="javascript:void(0);">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">Choose the subject field of your task</span>
						</span>
					</a>
				</div>
				<select name="subject" id="subject" class="calc_selector selectbox">
					<option value="0">Choose Your Subject</option>
					<?php
						foreach ($subjects as $key => $value) {
							$selected = '';
							if(isset($order['subject']) && $order['subject'] == $value['subject_id'])
								$selected = ' selected="selected"';
							echo '<option value="'.$value['subject_id'].'"'.$selected.'>'.$value['title'].'</option>';
						}
					?>
				</select>
				<div class="error hidden error_title" id="error_subject"></div>
			</div>

			<div class="cell float-left">
				<div class="title_side">Referencing Style 
					<a href="javascript:void(0);">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">Please choose your referencing style that your paper has to be formatted into. If you do not know what style do you need – just choose "GENERAL".</span>
						</span>
					</a>
				</div>
				<select name="paper_format" id="paper_format" class="selectbox calc_selector">
					<option value="0">Choose Referencing Style</option>
					<?php
						foreach ($paper_formats as $key => $value) {
							$selected = '';
							if(isset($order['paper_format']) && $order['paper_format'] == $value['paper_format_id'])
								$selected = ' selected="selected"';
							echo '<option value="'.$value['paper_format_id'].'"'.$selected.'>'.$value['title'].'</option>';
						}
						?>
				</select>
				<div class="error hidden error_title" id="error_paper_format"></div>
			</div>

			<div class="cell float-left">
				<div class="title_side">Sources needed 
					<a href="javascript:void(0);" class="inf_popr">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">Indicate a number of sources you want your writer to base your paper on. If you require specific sources, please indicate what kind of sources you need in the paper details box below. If you do not have any requirements on a number of sources, jest leave this field blank. </span>
						</span>
					</a>
				</div>
				<div class="plusminusblock">
					<div class="minus" id="source_minus">-</div>
					<div class="coint" id="sources_needed_div"><?php if(isset($order['sources_needed'])) echo $order['sources_needed']; else echo '0' ?></div>
					<input type="hidden" name="sources_needed" value="<?php if(isset($order['sources_needed'])) echo $order['sources_needed']; else echo '0' ?>" id="sources_needed" />
					<div class="plus" id="source_plus">+</div>
				</div>
			</div>
		</div>
		<div class="row_wrap row_wrap_last_input">
			<div class="cell float-left">
				<div class="title_side">Upload a file (max 3MB)
					<a class="pop_up_top" href="javascript:void(0);">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">Here	you can upload a document that might be helpful for your writer to use in his/her work. Please attach your document if it is necessary. We accept: Word, PDF, Powerpoint, JPG, or Excel. At this stage you can only download one document, but you can download as many documents as you want	when you are already registered. </span>
						</span>
					</a>
				</div>
				<input type="file" name="file" id="file" />
				<div class="note_block">
					You can upload only one document on this stage
				</div>
				<?php
					if(isset($file_error) && $file_error) {
						echo '<div class="error error_title error_title_files">There was a mistake, however all your information is saved but you have to upload your file again and then you can complete the order procedure.</div><br />';
					}
				?>
			</div>
			<div class="cell_topic">
 				<div class="title_side">Topic 
 					<a href="javascript:void(0);">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">Indicate the topic of your paper</span>
						</span>
					</a>
				</div>
				<input type="text" name="topic" placeholder="Your text..." id="topic" value="<?php if(isset($order['topic'])) echo $order['topic']; ?>" />
				<div class="error hidden error_title" id="error_topic"></div>
			</div>
		</div>
		<div class="row_wrap row_wrap_last_area">
			<div class="title_side title_side_paper">Paper Details 
				<a href="javascript:void(0);">
					<span class="pop_up">
						<span class="bg_popup"></span>
						<span class="text">Indicate exact details of the requirements for your paper. Please note that the quality of the details will dictate the content of your assignment. Please be specific as possible</span>
					</span>
				</a>
			</div>
			<div class="text_area_bl">
				<textarea name="paper_details" placeholder="Your text..." id="paper_details"><?php if(isset($order['paper_details'])) echo $order['paper_details']; ?></textarea>
			</div>
			<div class="clear"></div>
			<div class="error hidden error_title" id="error_paper_details"></div>
		</div>
		
		<div class="bottom_panel">
			<a href="/user/order_details/<?php echo $order_id; ?>" class="pointer btn_blue_fr float-left"><span class="back">Cancel</span></a>
			<a onclick="go_step_two()" class="pointer btn_red_fr btn_red_fr_step2 float-right"><span class="next">Go to step II</span></a>
			<div class="prie_bottom">
				<div class="block_text">
					Total price USD <span class='order_form_price hidden total_price calc_result'>0,00</span>
					<a href="#">
						<span class="pop_up">
							<span class="bg_popup"></span>
							<span class="text">The payment can be done in any currency as it will be automatically converted into USD.</span>
						</span>
					</a>
				</div>
			</div>
		</div>

		</div><!-- .step_1 -->

		<div class="step_2 hidden">
			<div class="row_wrap">
				 
				<div class="cell float-left">
					<div class="title_side">Type of service 
						<a href="javascript:void(0);">
							<span class="pop_up">
								<span class="bg_popup"></span>
								<span class="text">
									<span class="weight_sent">Writing from scratch </span> implies that the writer will create a plagiarised free original paper for you.<br/> 
									<span class="weight_sent">Editing/Proofreading </span> implies we look over an existing paper and suggest ways to improve it
								</span>
						 	</span>
					 	</a>
					</div>
					<select name="work_type" id="work_type" class="input_hints selectbox calc_selector">
						<?php 
							$checked1 = '';
							$checked2 = '';
							if(isset($order['work_type']) && $order['work_type'] == 'new')
								$checked1 = 'selected="selected"';
							if(isset($order['work_type']) && $order['work_type'] == 'edit')
								$checked2 = 'selected="selected"';
						?>
						<option value="new" <?php echo $checked1; ?>>Writing from scratch</option>
						<option value="edit" <?php echo $checked2; ?>>Editing/proofreading</option>
					</select>
				</div>

				<div class="cell float-left">
					<div class="title_side">Academic Level 
						<a href="javascript:void(0);">
							<span class="pop_up">
								<span class="bg_popup"></span>
								<span class="text">Select your academic level</span>
						 	</span>
					 	</a>
					</div>
					<select name="academic_level" id="academic_level" class="calc_selector selectbox">
						<option value="0">Choose Academic Level</option>
						<?php
							foreach ($academic_levels as $key => $value) {
								$selected = '';
								if(isset($order['academic_level']) && $order['academic_level'] == $value['academic_level_id'])
									$selected = ' selected="selected"';
								echo '<option value="'.$value['academic_level_id'].'"'.$selected.'>'.$value['title'].'</option>';
							}
						?>
					</select>
					<div class="error hidden error_title" id="error_academic_level"></div>
				</div>

				<div class="cell float-left">
					<div class="title_side">Number of pages 
						<a href="javascript:void(0);">
							<span class="pop_up">
								<span class="bg_popup"></span>
								<span class="text">Please enter a desired number of pages. Should be a full number. Please note that a standard page is 275 words.</span>
							</span>
						</a>
					</div>
					<div class="plusminusblock">
						<div class="minus" id="number_of_pages_minus">-</div>
						<div class="coint" id="number_of_pages_div"><?php if(isset($order['pages'])) echo $order['pages']; else echo '1' ?></div>
						<input type="hidden" name="number_of_pages" value="<?php if(isset($order['pages'])) echo $order['pages']; else echo '1' ?>" id="number_of_pages" />
						<div class="plus" id="number_of_pages_plus">+</div>
					</div>
				</div>

				<div class="cell float-left">
					<div class="title_side">Number of slides 
						<a href="javascript:void(0);">
							<span class="pop_up pop_up_tt">
								<span class="bg_popup"></span>
								<span class="text">If you need Power Point Presentation, specify the number of slides you need. 1 slide=50% of the cost per page.</span>
							</span>
						</a>
					</div>
					<div class="plusminusblock">
						<div class="minus" id="slides_minus">-</div>
						<div class="coint" id="slides_div"><?php if(isset($order['slides'])) echo $order['slides']; else echo '0' ?></div>
						<input type="hidden" name="slides" value="<?php if(isset($order['slides'])) echo $order['slides']; else echo '0' ?>" id="number_of_slides" />
						<div class="plus" id="slides_plus">+</div>
					</div>
				 </div>

			 </div>

			<div class="row_wrap">
				<div class="cell float-left">
					<div class="title_side">Deadline 
						<a href="javascript:void(0);">
							<span class="pop_up">
								<span class="bg_popup"></span>
								<span class="text">Please choose your deadline date to receive the assignment. Please make note to give yourself enough time to review the work before handing it in</span>
							</span>
					 	</a>
					</div>
					<select name="deadline" id="deadline" class="selectbox calc_selector">
						<option value="0">Choose Deadline</option>
						<?php
							foreach ($deadlines as $key => $value) {
								$selected = '';
								if(isset($order['deadline']) && $order['deadline'] == $value['deadline_id'])
									$selected = ' selected="selected"';
								echo '<option value="'.$value['deadline_id'].'"'.$selected.'>'.$value['title'].'</option>';
							}
						?>					
					</select>
					<div class="error hidden error_title" id="error_deadline"></div>
				</div>

				<div class="cell float-left">
					<div class="title_side title_input title_input1">
						<?php
							$checked = '';
							if(isset($order['abstract_page']) && $order['abstract_page'] == '1')
								$checked = 'checked="checked"';
						?>
						<label>
							<input type="checkbox" name="abstract_page" onChange="order_calc_selector()" id="abstract_page" value="1"<?php echo $checked; ?> /> &nbsp; Add an Abstract page to my paper $ 14.99
						</label> 
						<a href="#">
							<span class="pop_up pop_up_tt">
								<span class="bg_popup"></span>
								<span class="text">Often academic papers must contain an abstract, particularly if the writing consists of more than two pages and APA formatting. Tick this option if you want your writing to include an abstract page.</span>
							 </span>
						</a>
					</div>
					
					<div class="title_side title_input ">
						<?php
							$checked = '';
							if(isset($order['plagiarism_report']) && $order['plagiarism_report'] == '1')
								$checked = 'checked="checked"';
						?>
						<label>
							<input type="checkbox" name="plagiarism_report" id="plagiarism_report" value="1"<?php echo $checked; ?> /> &nbsp; I want to receive official Plagiarism report $ 0
						</label>
						<a href="#">
							<span class="pop_up pop_up_tt">
								<span class="bg_popup"></span>
								<span class="text">We guarantee that our papers are plagiarism free. If you tick this option we will provide you an official third party detailed plagiarism report that will be attached to the final version of your writing. Please note that Proof Reading has no plagiarism report due to the nature of this service.</span>
							</span>
					 	</a>
					</div>
				</div>
			</div>
			
			<div class="bottom_panel">
				<a onclick="back_step_one()" class="pointer btn_blue_fr btn_blue_fr_step_2 float-left"><span class="back">Back to step I</span></a>
				<a onclick="go_step_three()" class="pointer btn_red_fr btn_red_fr_step2 float-right"><span class="next">Go to step III</span></a>
				<div class="prie_bottom">
					<div class="block_text">
						Total price USD <span class='order_form_price hidden total_price calc_result'>0,00</span>
						<a href="#">
							<span class="pop_up">
								<span class="bg_popup"></span>
								<span class="text">The payment can be done in any currency as it will be automatically converted into USD.</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div><!-- .step_2 -->

		<div class="step_3 hidden">
			 
			<div class="center_blocks" id="order_list_center">
				<div class="cell_block">
					<div class="services_line">
						<span class="note_block">Type of service:</span>
						<span id="preview_service_type">Writing from scratch</span>
					</div>
					<div class="services_line">
						<span class="note_block">Academic level:</span>
						<span id="preview_academic_level">Academic level</span>
					</div>
					<div class="services_line">
						<span class="note_block">Type of paper:</span>
						<span id="preview_paper_type">Type of paper</span>
					</div>
					<div class="services_line">
						<span class="note_block">Subject:</span>
						<span id="preview_subject">Subject</span>
					</div>
					<div class="services_line">
						<span class="note_block">No. of pages:</span>
						<span id="preview_pages">0</span>
					</div>
					<div class="services_line">
						<span class="note_block">No. of slides:</span>
						<span id="preview_slides">0</span>
					</div>
					<div class="services_line">
						<span class="note_block">Referencing:</span>
						<span id="preview_referencing">Referencing</span>
					</div>
				</div>
				<div class="cell_block">
					<div class="services_line">
						<span class="note_block">No. of sources: </span>
						<span id="preview_sources">0</span>
					</div>
					<div class="services_line">
						<span class="note_block">Abstract page:</span>
						<span id="preview_abstract">No</span>
					</div>
					<div class="services_line">
						<span class="note_block">Plagiarism rep.:</span>
						<span id="preview_plagiarism">No</span>
					</div>
					<div class="services_line">
						<span class="note_block">Paper details:</span>
						<span id="preview_details">Paper details</span>
					</div>
					<div class="services_line">
						<span class="note_block note_attachment">Attachments:</span>
						<span id="preview_attachment">No</span>
					</div>
					<div class="services_line">
						<span class="note_block">Price:</span> <span id="final_price">USD 0</span>
					</div>
					<div class="services_line">
						<span class="note_block">Discount:</span>
						<span id="final_discount">0 % (USD 0)</span>
					</div>
					<div class="services_line">
						<span class="note_block">Discounted price:</span>
						<span id="discounted_price"><span id="discount_price">USD 0</span></span>
					</div>
				</div>
				<div class="services_line">
					<span class="note_block note_block_bottom">Topic:</span>
					<span id="preview_topic">Topic</span>
				</div>
				<div class="services_line">
					<span class="note_block note_block_bottom">Paper details:</span>
					<span id="preview_full_paper_details">Paper details</span>
				</div>
			</div> <!-- .center_blocks -->
			<div class="float-right right_side_step3 right_side ">
				<div class="time_right_wrap_bb">
					<div class="blocks_title">
						Time left:
						<div class="time_left_block error_title">
							<div> 
							<!--время-->
							</div>
						</div>
					</div>
					<div class="time_left_bb">
						<div class="balls_row" id="time_left_balls">
							<div class="ball">1</div>
							<div class="ball">17</div>
							<div class="ball">45</div>
						</div>
						<div class="balls_row">
							<div class="ball_tit">Day</div>
							<div class="ball_tit">Hour</div>
							<div class="ball_tit">Min</div>
						</div>
					</div>
				</div>
				<div class="current_bb right_blocks">
					<div class="blocks_title">
						CURRENT STATUS:
					</div>
					<div class="note_block">
						<?php if(!$order['payment_id']): ?>
							Waiting for payment
						<?php endif; ?>
						<a href="" class="inf_popr">
							<span class="pop_up">
								<span class="bg_popup"></span>
								<span class="text">This is en expected time to deliver a first draft. If the work was under revision, this time indicates when your revised work will be submitted. Please note that substantial revisions might require more time and might affect the expected delivery time. </span>
							</span>
						</a>
					</div>
				</div>
				<div class="ecpected_time_bb right_blocks">
					<div class="blocks_title">
						EXPECTED:
					</div>
					<div class="note_block">
						<span id="blue_t_date">Dec 19 08:28 AM</span>
						<a href="" class="inf_popr">
							<span class="pop_up">
							<span class="bg_popup"></span>
								<span class="text">This is en expected time to deliver a first draft. If the work was under revision, this time indicates when your revised work will be submitted. Please note that substantial revisions might require more time and might affect the expected delivery time. </span>
							</span>
						</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>

			<div class="bottom_panel">
				<a onclick="back_step_two()" class="pointer btn_blue_fr btn_blue_fr_step_2 float-left"><span class="back">Back to step II</span></a>
				<span id="proceed_btn"><a onclick="nicEditors.findEditor('paper_details').saveContent();fsubmitter_form()" class="pointer proceed btn_red_fr float-right ">Save</a></span>

				<input type="submit" id="submitter_form" style="display:none;" />
				<div class="prie_bottom">
					<div class="block_text">
						Total price USD <span class='order_form_price hidden total_price calc_result'>0,00</span>
						<a href="#">
							<span class="pop_up pop_up_1">
								<span class="bg_popup"></span>
								<span class="text">The payment can be done in any currency as it will be automatically converted into USD.</span>
							</span>
						</a>
					</div>
				</div>
			</div>

		</div><!-- .step_3 -->	 
	<input type="hidden" name="submit" value="1" />
</form>			 
			</div><!-- .tabs_body -->
		</div>
	</div> <!-- .site-wrapper -->
</div>
<style type="text/css">
.nicEdit-main[placeholder]:empty:before {
		content: attr(placeholder);
		color: #9aa1b6; 
}
.nicEdit-main[placeholder]:empty:focus:before {
		content: "";
}
</style>
<script type="text/javascript">
$().ready(function(){
	change_country();
	$(".selectbox").selectbox({});
});
bkLib.onDomLoaded(function() {
	var myNicEditor = new nicEditor({maxHeight : 150});
	myNicEditor.addInstance('paper_details');
	$(".nicEdit-main").focusout(function(){
		var element = $(this);
		if (!element.text().replace(" ", "").length) {
			element.empty();
		}
	});
	$(".nicEdit-main").focusin(function(){
		var element = $(this);
		if (!element.text().replace(" ", "").length) {
			element.empty();
		}
	});
	<?php
		if(!isset($file_error) || !$file_error)
			if(isset($errors) && $errors)
				echo 'after_errors();';
	?>		
});
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2TzXmB9ZRIjV7KryWVSKLzYNYybsFD54";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>