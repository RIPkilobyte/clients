<!DOCTYPE html>
<?php 
if (!isset($content))
	$content = '<h2>Пустая страница</h2>';
?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$title ?></title>
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/css/template.css" rel="stylesheet" media="all" />
    
    <script src="/js/jquery-1.10.2.min.js"></script>
	<script src="/js/functions.js"></script>
	<link rel="stylesheet" href="/css/cusel.css" type="text/css" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,700,600' rel='stylesheet' type='text/css'>
    
    <script type="text/javascript" src="/js/jquery-1.6.1.js"></script>
    <script type="text/javascript" src="/js/cusel.js"></script>
    <script type="text/javascript" src="/js/cusel_set.js"></script>
    <script type="text/javascript" src="/js/jScrollPane.js"></script>
    <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/js/calc.js"></script>
	<?php // Подключение скриптов из контроллера
	if (!empty($scripts)) {
		foreach ($scripts as $script) { echo $script;
		}
	}
	?>
	
	<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/css/ie.css" />
    <![endif]-->

    <?php 
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '/main'){ ?>
<style>
#header_big {
height: 184px!important;
}
</style>
         <?php } ?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?22V4MpFbOXjQFaW38hEAMwo4J5bXEp9R';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-48444080-1', 'eagle-essays.com');
  ga('send', 'pageview');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24824639 = new Ya.Metrika({id:24824639,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24824639" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<div id="nav">
		<div class="cont">
			<a href="/"><div class="logo"></div></a>
        <div class="menu">
            <ul>
               <!-- <li><a href="/"><span>Home</span></a></li>-->
                <li class="l1">
                    <a href="/services"><span>Services</span></a>
                    <div class="visible">
                        <img src="../img/ygol.png" />
                        <ul>
                            <li><a href="/how-it-works"><span>How it works</span></a></li>
                            <li><a href="/faq-top-essay"><span>FAQ</span></a></li>
                        </ul>
                    </div>
                </li>

                <li class="l2">
                    <a href="/pricing-proofreading-and-essay-writing-service"><span>Prices</span></a>
                    <div class="visible">
                        <img src="../img/ygol.png" />
                        <ul>
                            <li><a href="/discounts"><span>Discounts</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="l3">
                    <a href="/about-writing-services"><span>About us</span></a>
                    <div class="visible">
                        <img src="../img/ygol.png" />
                        <ul>
                            <li><a href="/contact-us"><span>Contact us</span></a></li>
                        </ul>
                    </div>
                </li>
                <!--
                <li>
                    <a href="/company" onclick="return false;"><span>Company</span></a>
                    <div class="visible">
                        <img src="../img/ygol.png" />
                        <ul>
                        </ul>
                    </div>
                </li>
                -->
                <li><a href="/testimonials"><span>Testimonials</span></a></li>
                <li class="l4">
                    <a href="/free"><span>Sample Essays</span></a>
                    <div class="visible">
                        <img src="../img/ygol.png" />
                        <ul >
                            <li><a href="/tips"><span>Essay writing tips</span></a></li>
                        </ul>
                    </div>
                </li>
				<!--<li><a href="/blog"><span>Blog</span></a></li>-->
                <?php if(isset($_SESSION['user_id']) && $_SESSION['userdata']['role'] == 'client'): ?>
                    <li><a href="/user"><span><?php echo $_SESSION['userdata']['first_name']; ?></span></a></li>
                <?php elseif(isset($_SESSION['user_id']) && $_SESSION['userdata']['role'] == 'support'):?>
                    <li><a href="/support"><span>Support</span></a></li>
                <?php else: ?>
                    <li><a href="/user/login"><span>Sign in</span></a></li>
                <?php endif;?>                
            </ul>
        
        <!--<div class="chat"><a href="#">Chat now</a>-->


        </div>
        
        <div class="social"><a href="https://www.facebook.com/theeagleessays"><img src="/img/facebook45.png" /></a></div>
        <div class="social"><a href="https://twitter.com/EagleEssays"><img src="/img/twitter45.png" /></a></div>
        <div class="social"><a href="https://plus.google.com/b/104419076632602871749/104419076632602871749/about"><img src="/img/google45.png" /></a></div>          
        <div class="tell">

            <img src="/img/phone45.png" title="+442032868140" alt="+442032868140" style="margin-top: 2px;"/>
        </div>
        <div class="skype"><a href="skype:writemypapers?call"><img src="/img/skype45.png" style="margin-top: 2px;"/></a></div>
        </div>
        </div>
    </div>

<div id="wrapper">
		
		<div id="header_big">
				<div id="header">

					<div class="btn_order">
						<a href="/order">Order now</a>
					</div>
					<div class="btn_free">
						<a href="/inquiry">Free inquiry</a>
					</div>
					<div class="logo_2"><?php if(isset($main_page_text)) echo $main_page_text; ?></div>
<!--<div class="pod_logo_2">Top class professional writers. On-time delivery. Affordably priced.</div>-->
<div class="timer"><div class="title">Live Stats</div>
<script type="text/javascript">
$().ready(function(){
    var time = "<?php echo time(); ?>";
    var one = time.substr(0,1);
    var two = time.substr(1,1);
    var three = time.substr(2,1);
    var four = time.substr(3,1);
    var five = time.substr(4,1);
    var six = time.substr(5,1);
    $("#live_stats_table").html('<tr><td>'+one+'</td><td>'+two+'</td><td>'+three+'</td><td>'+four+'</td><td>'+five+'</td><td>'+six+'</td></tr>');
});
</script>
<table id="live_stats_table">
    <tr>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
    </tr>
</table>
<div class="title_bottom">Orders Delivered</div></div>

<?php if(isset($calc)) echo $calc; ?>

				</div>
			</div>
    
	<div id="big_cont">
	<div class="cont">
	
    <div class="clear"></div>
    <?php 
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '/main'){
            echo '<div id="left_col">';
	  	    /*echo '<img src="../img/left_discount.jpg">';*/
            if(isset($left_block))
                echo $left_block;
            echo '</div>';
        }
    ?>
    
    <?php 
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '/main'){
            echo '<div id="content">'; 
        }
        else {
            echo '<div id="content" class="contenthome">'; 
        }  
    ?>
    <?=$content ?>
    </div>
    
    <div class="clear"></div>
   </div> 
   <div id="ft_m">
   <div class="block_ft_m">
	<div class="cont">
	          <ul>
                <li><a href="/disclaimer"><span>Disclaimer</span></a></li>
                <li><a href="/terms-of-use" class="unfirst"><span>Terms of use</span></a></li>
                <li><a href="/privacy-policy" class="unfirst"><span>Privacy policy</span></a></li>
                <li><a href="/revision-policy" class="unfirst"><span>Revision policy</span></a></li>
                <li><a href="/money-back" class="unfirst"><span>Money back guarantee</span></a></li>
                <li><a href="/contact-us"><span>Contact us</span></a></li>
            </ul>
            </div>
</div></div>
    <div id="footer">
    	<div class="cont">
    		
    		<div class="ftt_m_c">
    		<?php /*<div class="ftt_m">
    			<ul>
    				<li><a href="/architecture-essays">Architecture Essays</a></li>
    				<li><a href="/art-essays">Art Essays</a></li>
    				<li><a href="/astronomy-essays">Astronomy Essays</a></li>
    				<li><a href="/biology-essays">Biology Essays</a></li>
    				<li><a href="/business-essays">Business Essays</a></li>
    				<li><a href="/chemistry-essays">Chemistry Essays</a></li>
    				<li><a href="/criminology-essays">Criminology Essays</a></li>
    				<li><a href="/economics-essays">Economics Essays</a></li>
    			</ul>
    		</div>
    		    		<div class="ftt_m">
    			<ul>
    				<li><a href="/engineering-papers.co.uk">Engineering Papers</a></li>
    				<li><a href="/english-essays">English Essays</a></li>
    				<li><a href="/essays-in-literature">Essays in Literature</a></li>
    				<li><a href="/finance-essays">Finance Essays</a></li>
    				<li><a href="/genetics-help">Genetics Help</a></li>
    				<li><a href="/geography-essays">Geography Essays</a></li>
    				<li><a href="/history-essays">History Essays</a></li>
    				<li><a href="/linguistics-essays">Linguistics Essays</a></li>
    			</ul>
    		</div>
    		    		<div class="ftt_m">
    			<ul>
					<li><a href="/marketing-essays">Marketing Essay</a></li>
    				<li><a href="/philosophy-essays">Philosophy Essays</a></li>
    				<li><a href="/politics-essays">Politics Essays</a></li>
    				<li><a href="/psychology-essays">Psychology Essays</a></li>
    				<li><a href="/sociology-essays">Sociology Essays</a></li>
    				<li><a href="/technology-essays">Technology Essays</a></li>
    				<li><a href="/write-my-report">Write My Report</a></li>
    			</ul>
    		</div>
    		</div> */ ?>
    		
    		<div class="ftt_m">
    			<ul>
    				<li><a href="/architecture-essays">Architecture Essays</a></li>
    				<li><a href="/art-essays">Art Essays</a></li>
    				<li><a href="/cheap-essays">Astronomy Essays</a></li>
    				<li><a href="/essay-help">Biology Essays</a></li>
    				<li><a href="/business-essays">Business Essays</a></li>
    			</ul>
    		</div>
    		    		<div class="ftt_m">
    			<ul>
    				<li><a href="/chemistry-essays">Chemistry Essays</a></li>
    				<li><a href="/essay-writing-service">Criminology Essays</a></li>
    				<li><a href="/economics-essays-writing">Economics Essays</a></li>
    				<li><a href="/engineering-papers">Engineering Papers</a></li>
    				<li><a href="/english-essays">English Essays</a></li>
    			</ul>
    		</div>
    		    		<div class="ftt_m">
    			<ul>
    				<li><a href="/literature-essay">Essays in Literature</a></li>
    				<li><a href="/finance-essays">Finance Essays</a></li>
    				<li><a href="/genetics-essay">Genetics</a></li>
    				<li><a href="/geography-essays">Geography Essays</a></li>
    				<li><a href="/history-essays">History Essays</a></li>
    			</ul>
    			</div>
    			<div class="ftt_m">
    			<ul>
    				<li><a href="/essays">Linguistics Essays</a></li>
					<li><a href="/marketing-essay">Marketing Essay</a></li>
    				<li><a href="/philosophy-essays">Philosophy Essays</a></li>
    				<li><a href="/politics-essays">Politics Essays</a></li>
    				<li><a href="/psychology-essays">Psychology Essays</a></li>
    			</ul>
    			</div>
    			<div class="ftt_m">
    			<ul>
    				<li><a href="/sociology-essays">Sociology Essays</a></li>
    				<li><a href="/technology-essays">Technology Essays</a></li>
    				<li><a href="/technical-writing">Help for My Report</a></li>
    			</ul>
    		</div>
    		</div>
    		
    		<div class="clear"></div>
    		<div class="acc_2"></div>
    		<div class="to_top"><a href="#">Back to top</a></div>
    		<div class="clear"></div>
    		
        </div>
        <div class="copy_r">
    			<div class="cont">
    			<div class="copy">
					Copyright © <a href="/">Eagle Essays</a> | <a href="http://neopulse.ru">Site development</a> NeoPulse |
					All Rights Reserved.
				</div>
				</div>
			</div>
    </div>
    </div>

</div>

</body>
</html>