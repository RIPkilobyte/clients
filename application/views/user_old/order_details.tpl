<script src="/js/jquery.form.min.js"></script>
<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
<script src="/js/nicEdit.js" type="text/javascript"></script>

<div class="block_container">
	<?php if(isset($order) && count($order)): ?>
	<div class="block_head"><span>Order #</span><?php echo $order['order_id']; ?></div>
	<div class="block_content">
	<input type="hidden" value="<?php echo $order['order_id']; ?>" id="order_id" />
	<input type="hidden" value="<?php echo $_SESSION['user_id']; ?>" id="sender_id" />
	<input type="hidden" value="<?php echo $order['writer_id']; ?>" id="writer_id" />
		<table class="table_order_details">
		<tr><td rowspan="4" class="td_order_controls">
			<div class="order_controls">
				
				<?php if(isset($timer)): ?>
					<div class="counter_block">
						<div class="counter">
							<div class="timer_title">Time left: &nbsp;</div>
							<!--<div class="timer"></div>-->
							
							<div id="span_timer" class="timer">
								<table class="count_table">
									<tr>
										<td id="timer_days" class="count_n">0</td>
										<td rowspan="2" class="dots2">:</td>
										<td id="timer_time" class="count_n">0:0</td>
									</tr>
									<tr>
										<td class="count_t">DAYS</td>
										<td class="count_t">HOURS</td>
									</tr>
								</table>
							</div>

						</div>
					</div>
				<?php endif; ?>
				<div class="status_title"><?php echo $order['status_client']; ?></div>
				<table>
					<?php if($order['status'] != 'Completed' && $order['status'] != 'Canceled'): ?>
						<tr>
							<td>Deadline</td>
							<td><?php echo date("M j h:i A", strtotime($order['date_end'])); ?></td>
						</tr>
					<?php endif; ?>
					<tr>
						<td>Price</td>
						<td>$ <?php echo $order['price']; ?></td>
					</tr>
				</table>
				
				<div class="order_buttons" id="main_order_buttons">
					<table>
						<tr>
							<td>
							<?php if($order['status'] == 'Completed' && $order['file_id']): ?>
								<input type="button" class="btn_green" value="Download file" onclick="download_file(<?php echo $order['order_id']; ?>)" />

							<?php elseif($order['status'] == 'File delivered' && $order['file_id']): ?>
								<input type="button" class="btn_orange" value="Preview" onclick="preview_file(<?php echo $order['file_id']; ?>)" />
								<input type="button" class="btn_green" value="Approve" id="approve_button" />
								<?php if($order['revisions'] >= '3'):?>
								<?php else: ?>
									<input type="button" class="btn_gray" value="Revision" id="revision_button" />
								<?php endif; ?>
							<?php endif; ?>
							</td>
						</tr>
					</table>
				</div>

			</div>
		</td>
		<td colspan="2" class="td_order_information">
			<div class="order_information subblock"><h3>Information</h3>
				<table>
					<tr>
						<th>Number of pages</th>
						<th>Number of slides</th>
						<th>Academic level</th>
						<th>Deadline</th>
						<th>Type of work</th>
						<th>Type of paper</th>
					</tr>
					<tr>
						<td><?php echo $order['pages']; ?></td>
						<td><?php echo $order['slides']; ?></td>
						<td><?php echo $order['academic_level']; ?></td>
						<td><?php echo $order['deadline']; ?></td>
						<td>
						<?php
					if($order['work_type'] == 'new')
						echo 'Writing from scratch';
					elseif($order['work_type'] == 'edit')
						echo 'Editing/proofreading';
					?></td>
						<td><?php echo $order['paper_type']; ?></td>
					</tr>
				</table>
				<table class="table_subject_topic">
					<tr>
						<th>Subject</th>
						<th>Plagiarism report</th>
						<th>Abstract page</th>
						<th>Referencing style</th>
						<th>Number of Sources</th>
					</tr>
					<tr>
						<td><?php echo $order['subject']; ?></td>

								<td>
								<?php
								if($order['plagiarism_report'])
									echo 'Yes';
								else
									echo 'No';
								?>
								</td>
								<td>
								<?php
								if($order['abstract_page'])
									echo 'Yes';
								else
									echo 'No';
								?>
								</td>
						<td><?php echo $order['paper_format']; ?></td>
						<td><?php echo $order['sources']; ?></td>
					</tr>
				</table>
				<div class="paper_details">
					<h4>Topic</h4>
					<p><?php echo $order['topic']; ?></p>
				</div>
				<div class="paper_details">
					<h4>Paper details</h4>
					<p><?php echo $order['paper_details']; ?></p>
				</div>
			</div>
		</td>
		</tr>
		<tr>
		<td class="td_order_payments">
			<div class="order_payments subblock"><h3>Payments</h3>
				<div class="subblock_btn_pad">
					<?php if($payment): ?>
						<table class="td_order_information" style="background-color: white;">
							<tr>
								<th><b>Created:</b></th>
								<th><b>Payment type:</b></th>
							</tr>
							<tr>
								<td><?php echo date("M j H:i", strtotime($order['date_add'])); ?></td>
								<td><?php echo $payment['system']; ?></td>
							</tr>
							<tr>
								<th><b>Paid:</b></th>
								<th><b>Amount:</b></th>
							</tr>
							<tr>
								<td><?php echo date("M j H:i", strtotime($payment['date'])); ?></td>
								<td>$<?php echo $payment['amount']; ?></td>
							</tr>
						</table>
					<?php else: ?>
						<input type="button" class="btn_green" value="Pay" onclick="order_checkout(<?php echo $order['order_id']; ?>)" style="margin:10px" />
					<?php endif; ?>
				</div>
			</div>
		</td>
		</tr>
		<tr><td colspan="2" class="td_order_information">	
			<div class="order_messages subblock"><h3>Messages / Files</h3>
				<div class="subblock_btn_pad">
					<input type="button" class="btn_green" value="Add new message / file" onclick="add_message()"/>
				</div>
				<div id="new_message" style="float:none;" class="order_interface hidden">
					<div class="interface_controls" style="min-height:270px;">
						<span style="float:none;" class="interface_field_caption">To Support</span>
						<input type="hidden" id="recipient" name="recipient" value="1" />
						<!--
						<select id="recipient" class="calc_selector" style="float:none;width: 180px;">
							<option value="" disabled>Choose recipient</option>
							<option value="1">SUPPORT</option>
							<?php if($order['writer_id']): ?>
								<option value="<?php echo $order['writer_id']; ?>">WRITER #<?php echo $order['writer_id']; ?></option>
							<?php endif; ?>
						</select>
						<span style="float:none" class="message_about_general hidden">Every writer will be able to see this message if you choose <b>GENERAL</b></span>
						<span style="float:none" class="message_about_support hidden">This message will only be seen by Eagle-Essays Support team</span>
						-->
						<span class="interface_field_caption" style="float:none;width:600px;">Message:</span><br />
						<textarea id="message_text" style="width:600px; height:150px;float:none;"></textarea>

					<form method="POST" id="fileform" style="float:none;" enctype="multipart/form-data">
						<input type="hidden" name="order_id" value="<?php echo $order['order_id']; ?>"/>
						<input type="hidden" name="sender" value="<?php echo $_SESSION['user_id']; ?>" />
						<p style="margin:10px;"><input type="file" style="float:none;" name="file" id="file" /></p>
					</form>

					</div>
					<div class="interface_buttons" id="submit_result">
						<input class="btn_gray" type="button" value="Cancel" id="cancel" onClick="cancel_message()" />
						<input class="btn_green" type="button" value="Submit" id="submit" onClick="nicEditors.findEditor('message_text').saveContent();submit_file(<?php echo $order['order_id']; ?>)" />
					</div>
				</div>
				<?php if(isset($order['records']) && count($order['records'])) {
					foreach ($order['records'] as $record) {
						if(isset($record['message_id'])) {
						echo '<div class="order_addon_pad message">';
							echo '<div class="message_title">';
								echo '<span class="message_sender">';
								echo $record['from'];
								echo '</span> : ';
								echo '<span class="message_receiver">';
								echo $record['to'];
								echo '</span><br />';
								echo '<span class="message_date">';
								echo $record['date'];
								echo '</span>';
							echo '</div>';
							echo '<div class="message_text">';
							echo $record['text'];
							echo '</div>';
							if(isset($record['file']) && $record['file']) {
								echo '<br /><div class="file_link" onClick="get_file('.$record['file']['file_id'].')">';
								if(isset($record['file']['image']) && $record['file']['image'])
									echo '<img src="/img/icon/'.$record['file']['image'].'" height="30px" />';
								echo $record['file']['client_name'];
								echo '</div>';
							}
						echo '</div>';
						}
						elseif(isset($record['file_id'])) {
							echo '<div class="order_addon_pad file">';
								echo '<div class="file_title">';
									echo '<span class="file_sender">';
									echo $record['from'];
									echo '</span> : ';
									echo '<span class="file_receiver">';
									echo $record['to'];
									echo '</span><br />';
									echo '<span class="file_date">';
									echo $record['date'];
									echo '</span>';
								echo '</div>';
								echo '<div class="file_link" onClick="get_file('.$record['file_id'].')">';
								if(isset($record['image']) && $record['image'])
									echo '<img src="/img/icon/'.$record['image'].'" height="30px" />';
								echo $record['client_name'];
								echo '</div>';
							echo '</div>';							
						}
					}
				}
				?>
			</div>
		</td>
		</tr>

		<!--
		<tr><td>
			<div class="order_files subblock"><h3>Files</h3>
			<div class="subblock_btn_pad">
				<input type="button" class="btn_green" value="Add new file" onclick="add_file()" style="margin:10px" />
			</div>			
			
			<div id="new_file" class="hidden order_interface">
				<form method="POST" id="fileform" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $order['order_id']; ?>"/>
					<input type="hidden" name="sender" value="<?php echo $_SESSION['user_id']; ?>" />
					<p style="margin:10px;"><input type="file" name="file" id="file" /></p>
					<div class="interface_buttons" id="file_message_result">
						<input class="btn_gray" type="button" value="Cancel" id="cancel" onClick="cancel_file()" />
						<input class="btn_green" type="button" value="Submit" id="submit" onClick="submit_file()" />
					</div>
				</form>
			</div>
				<?php if(isset($order['files']) && count($order['files'])) {
					foreach ($order['files'] as $file) {
						if($file['sender_id'] != $order['writer_id']) {
							echo '<div class="order_addon_pad file">';
								echo '<div class="file_title">';
									echo '<span class="file_sender">';
									echo $file['from'];
									echo '</span> : ';
									echo '<span class="file_receiver">';
									echo $file['to'];
									echo '</span><br />';
									echo '<span class="file_date">';
									echo $file['date'];
									echo '</span>';
								echo '</div>';
								echo '<div class="file_link" onClick="get_file('.$file['file_id'].')">';
								if(isset($file['image']) && $file['image'])
									echo '<img src="/img/icon/'.$file['image'].'" height="30px" />';
								echo $file['client_name'];
								echo '</div>';
							echo '</div>';
						}
					}
				}
				?>
			</div>
		</td></tr>
		-->
	</table>	
	</div>
	<?php else: ?>
		Blank page
	<?php endif; ?>
</div>
<div id="approve_dialog" title="Attention!">
	<p>You have clicked the "<b>Approve</b>" button. Bear in mind, that after clicking the "<b>Submit</b>" button you will not be able to request for a revision or refund. After you click on this button, you will receive the editable file of your order.</p>
</div>
<div id="revision_dialog" title="Revision">
	<p>You have clicked on the <b>Revision</b> button. That means, that you are not satisfied with the paper that the writer has prepared. Please provide as many details as possible, explaining what must be changed in the written document.</p>
	<p><b>Revision details:</b></p>
	<p>
		<textarea id="revision_details" cols="60" rows="4"></textarea>
	</p>
	<p><b>Revision deadline:</b></p>
	<p>
		Add hours to the deadline: <input type="text" size="6" id="add_hours" value="0" /> hours
	</p>
	<p><b>Note:</b> give an adequate amount of time for a writer to revise your paper. It will be added to the main deadline’s time. If there is substantial amount of time until the main deadline, please choose +0 hours option. Bear in your mind, that you will not able to request for a full refund after you click the <b>Submit</b> button.
	</p>
	<p>Revisions left: <span class="success"><?php echo 3-$order['revisions'];?></span></p>
</div>
<div id="alert_dialog">
	<p id="alert_dialog_text"></p>
</div>
<script type="text/javascript">
$().ready(function(){
	$("#cuselFrame-recipient").change(function(){
		add_message();
	});
	$("#approve_dialog").dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			Cancel: function(){
				$( this ).dialog( "close" );
			},
			"Submit": function(){
				$.post('/ajax/order_action',{id:<?php echo $order['order_id']; ?>, type:'approve'},function(data){
					if(data.status == 'ok') {
						$("#approve_dialog").dialog("close");
						$("#alert_dialog_text").html('Order approved successfully!');
						$("#alert_dialog").dialog("open");
					}
					else {
						$("#approve_dialog").dialog("close");
						$("#alert_dialog_text").html('An error occurred. Please try again later');
						$("#alert_dialog").dialog("open");
					}
				}, 'json');
			}
		},
	});	
	$("#revision_dialog").dialog({
		autoOpen: false,
		height: 600,
		width: 500,
		modal: true,
		buttons: {
			Cancel: function(){
				$(this).dialog("close");
			},
			"Submit": function(){
				$.post('/ajax/order_action',{id:<?php echo $order['order_id']; ?>, type:'revision', text:$("#revision_details").val(), hours:$("#add_hours").val()},function(data){
					if(data.status == 'ok') {
						$("#revision_dialog").dialog("close");
						$("#alert_dialog_text").html('You have successfully requested a revision!');
						$("#alert_dialog").dialog("open");
					}
					else {
						$("#revision_dialog").dialog("close");
						$("#alert_dialog_text").html('An error occurred. Please try again later');
						$("#alert_dialog").dialog("open");
					}
				}, 'json');
			}
		},
	});	
	$("#alert_dialog").dialog({
		autoOpen: false,
		height: 200,
		width: 350,
		modal: true,
		close: function(){
			location.reload();
		}
	});	
	$("#revision_button").click(function(){
		$("#revision_dialog").dialog("open");
	});
	$("#approve_button").click(function(){
		$("#approve_dialog").dialog("open");
	});	
	<?php
	if(isset($timer))
		echo 'order_timer('.$timer['y'].','.($timer['m']-1).','.$timer['d'].','.$timer['H'].','.$timer['i'].','.$timer['s'].');';
	?>
	$("#add_hours").numberMask({beforePoint:4});
	
});
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor();
    myNicEditor.addInstance('message_text');	
});
</script>
