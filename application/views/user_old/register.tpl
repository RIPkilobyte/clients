<div class="login_box">
    <div class="block_element_head">Register</div>
    <div class="error_box">
        <?php
            if(isset($errors))
                echo $errors;
        ?>
    </div>
    <div class="login_form">
        <form method="post" action="/user/register">
            <input type="hidden" name="register" value="1" />
                <input type="text" name="first_name" value="<?php echo $first_name; ?>" placeholder="First name">
                <input type="text" name="last_name" value="<?php echo $last_name; ?>" placeholder="Last name">
                <select id="registration_country" class="calc_selector" name="country" style="width: 270px;">
                    <option value="0" >Choose country</option>
                    <?php
                    foreach ($countries as $key => $value) {
                        $checked = '';
                        if($value['short_code'] == $country)
                            $checked = 'checked="checked"';
                        echo '<option value="'.$value['short_code'].'" '.$checked.'>'.$value['title'].'</option>';
                    }
                    ?>
                </select>
                <input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="Phone">
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="Email">
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="Password">
            <div class="submit">
                <span>&nbsp;</span><input type="submit" class="btn btn-primary" value="Register">
            </div>
        </form>
    </div>
</div>
