<div style="margin:0 auto; width:600px;"><table style="width:600px; color:#595959">
	<tr>
		<td>
			<?php if(isset($hash_email) && $hash_email): ?>
			<small>
				<center>
					Having trouble viewing this email? Try viewing in your <a href="http://eagle-essays.com/show_email/<?php echo $hash_email; ?>">browser.</a>
				</center>
			</small>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td>
		<center><img src="http://eagle-essays.com/img/email/Logo.png" style="width:600px;">
		</center></td>
	</tr>
</table>
<table cellpadding="15" style="width:600px;
-webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;
border:1px dashed black;
background-color:#f2f2f2;
">
	<tr>
		<td style="font-size:24px">
			<?php echo $title; ?>
		</td>
	</tr>
	<tr>
		<td>
			<?php echo $text; ?>
		</td>
	</tr>
	<tr>
		<td style="padding-top:40px;">Kind regards,
		<br>
		Customer Support
		<br>
		Eagle-Essays.com</td>
	</tr>
	<tr>
		<td style="padding-top: 20px;">
		<center><table>
			<tr>
				<td><img src="http://eagle-essays.com/img/email/Phoneicon.png"></td><td>
				<center>
					Call us, if you have some questions! +44 2032 868140
				</center></td><td><a href="#"><img src="http://eagle-essays.com/img/email/Facebookicon.png" ></a></td>
			</tr>
			<tr>
				<td style="padding-top:15px;"><img src="http://eagle-essays.com/img/email/Skypeicon.png"></td><td style="padding-top:15px;">
				<center>
					Or call via Skype for free: write-my-papers
				</center></td><td style="padding-top:15px;"><a href="#"><img src="http://eagle-essays.com/img/email/Twittericon.png"></a></td>
			</tr>
		</table></center></td>
	</tr>
	<tr>
		<td>
		<center>
			Your opinion is important to us! Send your ideas and feedback to:
			<br>
			<a href="mailto:manager@eagle-essays.com">manager@eagle-essays.com</a>
		</center></td>
	</tr>
</table>
<table style="width:600px; color:#595959">
	<tr>
		<td>
		<center>
			<small>Eagle-Essays.com is committed to protecting your privacy online. We will never email you directly requesting any personal data or details. If you believe you have received any such communication from us, do not respond. Please email support@eagle-essays.com for assistance.</small>
		</center></td>
	</tr>
</table>
</div>