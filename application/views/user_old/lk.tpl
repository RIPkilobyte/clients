<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<link rel="stylesheet" href="/css/ui.jqgrid.css" type="text/css" media="screen">

<div class="block_container">
	<div class="block_head">My Orders</div>
	<div class="block_content">
		<div id="tabs">
			<ul>
				<li><a id="block_active" href="#active">Current cases</a></li>
        <li><a id="block_previous" href="#previous">Previous</a></li>
			</ul>
			<div id="active">
				<table id="list_active"></table>
			</div>
			<div id="previous">
				<table id="list_previous"></table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var per_page = 20;
var row_list = [20,50,100];
var sort_name = "order_id";
var sort_order = "desc";
function build_grid(type)
{
  $("#list_"+type).jqGrid({
    url:'/ajax/get_orders/'+type,
    datatype: "json",
    mtype: 'POST',
    colNames:['Order id','Topic', 'Status', 'Deadline'],
    colModel:[
      {name:'order_id', align:'center', width:80},
      {name:'topic', align:'center'},
      {name:'status_client', align:'center',width:120},
      {name:'deadline_title', index:'date_end', align:'center', width:120}
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#pager_"+type),
    sortname: sort_name,
    sortorder: sort_order,        
    onSelectRow: function (row_id) {
      var rowData = jQuery(this).getRowData(row_id); 
      var id = rowData['order_id'];
      document.location.href = '/user/order_details/'+id;
    },
    autowidth: true,
    height: "auto",
    toppager: true
  }).trigger("reloadGrid");
}

$().ready(function(){
  $("#tabs").tabs();
  $("#block_active").click(function(){
    build_grid('active');
  }); 
  $("#block_previous").click(function(){
    build_grid('previous');
  });   
	$("#block_active").click();
	// ! this is workaround to remove strange right margin (10) that involves horizontal scroll (inspect class .ui-jqgrid but in Chrome)
	// i see it may be called at the end of each tab onClick()
	// see last tab (cancelled) on click - few lines above, and play with last tab
	$(".ui-jqgrid").width( $(".ui-jqgrid-bdiv").width() - 10); 
});	
</script>