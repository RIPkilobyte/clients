<div class="login_box">
    <div class="block_element_head">Login</div>
    <div class="error_box" style="color:red">
        <?php
            if(isset($errors))
                echo $errors;
        ?>
    </div> 
    <div class="login_form">
        <form method="post" action="/user/login">
            <input type="text" name="login" value="<?php echo $login; ?>" placeholder="Login">
            <input type="password" name="password" value="<?php echo $password; ?>" placeholder="Password">
            <div class="submit">
            	<span>
	                <a href="/user/forgot_password">Forgot password?</a>
				</span>
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
    </div>
</div>