<div class="login_box">
    <div class="block_element_head">Forgot password</div>
    <div class="error_box">
        <?php
            if(isset($errors))
                echo $errors;
        ?>
        <?php
            if(isset($success))
                echo '<div class="success">'.$success.'</div>';
        ?>
        <p>An error was occured. Please try again</p>
    </div>
    <div class="login_form">
        <form method="post" action="/user/forgot_password">
            <input type="text" name="login" value="<?php echo $login; ?>" placeholder="Email">
            <div class="submit">
	            <span>&nbsp;</span><input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>