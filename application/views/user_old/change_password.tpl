<div class="login_box">
	<div class="block_element_head">Change password</div>
	<div class="error_box">
		<?php
		if(isset($errors) && count($errors))
			echo $errors;
		?>
	</div>
	<form action="/user/change_password" method="post">
		<h3>Current password</h3>
		<input type="password" name="old" size="40" placeholder="Type here" />
		<h3>New password</h3>
		<input type="password" name="new" size="40" placeholder="Type here" />
		<h3>Re-type new password</h3>
		<input type="password" name="confirm" size="40" placeholder="Type here" />
        <div class="submit">
            <span>&nbsp;</span><input type="submit" class="btn btn-primary" value="Submit" />
        </div>
	</form>
</div>

