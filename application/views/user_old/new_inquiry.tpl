<script type="text/javascript" src="/js/calc.js"></script>
<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
<script type="text/javascript" src="/js/order_scripts.js"></script>
<script src="/js/nicEdit.js" type="text/javascript"></script>

<form method="post" id="order_form" action="/user/new_inquiry" autocomplete="off">

<?php
    if(isset($error['login_error']))
        echo '<div class="block_element">'.$error['login_error'].'</div>';
?>

<div class="block_container">
	<div class="block_head">New Inquiry</div>
	<div class="block_content">

<div class="block_element">
    <div class="block_element_head">Type of service</div>
    <select name="work_type" id="work_type" class="calc_selector" style="width:220px" onChange="change_work_type()">
        <?php 
            $checked1 = '';
            $checked2 = '';
            if(isset($work_type) && $work_type == 'new')
                $checked1 = 'selected="selected"';
            if(isset($work_type) && $work_type == 'edit')
                $checked2 = 'selected="selected"';
        ?>
        <option value="new" <?php echo $checked1; ?>>Writing from scratch</option>
        <option value="edit" <?php echo $checked2; ?>>Editing/proofreading</option>
    </select>
    <div style="display:none" class="order_hints" id="work_type_hint_new">"Writing from scratch" implies completion of a unique and plagiarism free paper.</div>
    <div style="display:none" class="order_hints" id="work_type_hint_edit">Please make sure you upload the file to be edited and proofread</div>
</div>

<div class="block_element">
    <div class="block_element_head">Type of paper</div>
    <?php echo form_error('paper_type'); ?>
    <select name="paper_type" id="paper_type" class="calc_selector" style="width:220px" onChange="change_paper_types()">
        <option value="">Choose Type of paper</option>
        <?php
            foreach ($paper_types as $key => $value) {
                $selected = '';
                if(isset($paper_type) && $paper_type == $value['paper_type_id'])
                    $selected = ' selected="selected"';
                echo '<option value="'.$value['paper_type_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
        ?>
    </select>
    <div style="display:none" class="order_hints" id="paper_type_hint">Please select the necessary type of paper</div>
</div>
	
<div class="block_element hidden" id="hint_for_mult_questions">
    <div class="">Please make sure you select "Problem solving" as a type of paper, if your assignment involves calculations</div>
</div>

<div class="block_element hidden" id="subject_block">
    <div class="block_element_head">Subject</div>
    <?php echo form_error('subject'); ?>
    <select name="subject" id="subject" class="calc_selector" style="width:220px;">
        <option value="">Choose...</option>
        <?php
            foreach ($subjects as $key => $value) {
                $selected = '';
                if(isset($subject) && $subject == $value['subject_id'])
                    $selected = ' selected="selected"';
                echo '<option value="'.$value['subject_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
        ?>        
    </select>
    <div style="display:none" class="order_hints" id="subject_hint">Choose the subject field of your task</div>
</div>
	
<div class="block_element hidden" id="institution_type_block">
    <div class="block_element_head">Type of institution</div>
    <?php echo form_error('institution_type'); ?>
    <select id="institution_type" name="institution_type" class="calc_selector" style="width:220px;">
        <option value="">Choose Institution type</option>
        <?php
            foreach ($institution_types as $key => $value) {
                $selected = '';
                if(isset($institution_type) && $institution_type == $value['institution_type_id'])
                    $selected = ' selected="selected"';
                echo '<option value="'.$value['institution_type_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
        ?>
    </select>
    <div style="display:none" class="order_hints" id="institution_type_hint">Specify the type of educational institution you want to apply to</div>
</div>
	
<div class="block_element hidden" id="institution_name_block">
    <label>Institution (program) name</label>
    <?php echo form_error('institution'); ?>
    <input type="text" name="institution" id="institution" value="<?php if(isset($institution)) echo $institution; ?>" />
    <div style="display:none" class="order_hints" id="institution_hint">Specify the name of educational institution or program you want to apply to</div>
</div>

<div class="block_element hidden" id="job_title_or_industry_block">
    <label>Job title or industry segment</label>
    <?php echo form_error('job_title_or_industry'); ?>
    <input type="text" name="job_title_or_industry" id="job_title_or_industry" value="<?php if(isset($job_title_or_industry)) echo $job_title_or_industry; ?>" />
    <div style="display:none" class="order_hints" id="job_title_or_industry_hint">Specify the name of the position or industry you want to apply for</div>
</div>

<div class="block_element hidden" id="reasons_for_applying_block">
    <label>Reasons for applying</label>
    <?php echo form_error('reasons_for_applying'); ?>
    <input type="text" name="reasons_for_applying" id="reasons_for_applying" value="<?php if(isset($reasons_for_applying)) echo $reasons_for_applying; ?>" />
    <div style="display:none" class="order_hints" id="reasons_for_applying_hint">Why you want to apply for this particular job or this industry segment</div>
</div>
<div class="block_element">
    <label>Topic</label>
    <?php echo form_error('topic'); ?>
    <input type="text" name="topic" id="topic" value="<?php if(isset($topic)) echo $topic; ?>" />
    <div style="display:none" class="order_hints" id="topic_hint">Indicate the topic of your paper</div>
</div>

<div class="block_element">
    <label>Paper Details</label>
    <?php echo form_error('paper_details'); ?>
    <textarea name="paper_details" id="paper_details" style="width:417px;height:300px;"><?php if(isset($paper_details)) echo $paper_details; ?></textarea>
    <div style="display:none" class="order_errors" id="paper_details_error">Please enter at least 3 words</div>
    <div style="display:none" class="order_hints" id="paper_details_hint">Provide as many details of your assignment as you can</div>
</div>
	
<div class="block_element hidden" id="paper_details_text_1">
    To ensure the highest quality of your paper that will show your personal characteristics and experience in the best way, please provide as many details as you can. Make sure to include the following information:
    <ul>
        <li>Academic background (name of educational institutions, years of graduation).</li>
        <li>Why you want to apply to this particular institution/ program.</li>
        <li>Overall information about yourself: skills, talents, your long-term and short-term goals.</li>
        <li>Other information or facts about your life that will be useful for the paper.</li>
    </ul>
</div>
<div class="block_element hidden" id="paper_details_text_2">
    To ensure the highest quality of your paper that will show your personal characteristics and experience in the best way, please provide as many details as you can. Make sure to include the following information:
    <ul>
        <li>Academic background (name of educational institutions, years of graduation).</li>
        <li>Professional experience.</li>
        <li>Professional skills.</li>
        <li>Personal characteristics.</li>
        <li>Information about certificates or courses that you have attended.</li>
        <li>Other information or facts about your life that will be useful for the paper.</li>
    </ul>
</div>
	
<div class="block_element" id="paper_format_block">
    <div class="block_element_head">Paper Format</div>
    <?php echo form_error('paper_format'); ?>
    <select name="paper_format" id="paper_format" class="calc_selector" style="width:220px;">
        <option value="">Choose...</option>
        <?php
            foreach ($paper_formats as $key => $value) {
                $selected = '';
                if(isset($paper_format) && $paper_format == $value['paper_format_id'])
                    $selected = ' selected="selected"';
                echo '<option value="'.$value['paper_format_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
        ?>         
    </select>
    <div style="display:none" class="order_hints" id="paper_format_hint">Choose the required formatting style or specify paper format in "Paper details" section </div>
</div>

<div class="block_element" id="general_format_block">
    General format: 275 words per page, legible font (e.g. Arial) 12 pt, double-spaced.
</div>
	
<div class="block_element">
    <label>Sources needed</label>
    <?php echo form_error('sources_needed'); ?>
    <input type="text" name="sources_needed" maxlength="2" id="sources_needed" value="<?php if(isset($sources_needed)) echo $sources_needed; ?>" />
    <div style="display:none" class="order_hints" id="sources_needed_hint">Indicate the number of sources on which you need to base your paper</div>
</div>

<div class="block_element hidden" id="academic_level_block">
    <div class="block_element_head">Academic Level</div>
    <?php echo form_error('academic_level'); ?>
    <select name="academic_level" id="academic_level" class="calc_selector" style="width:220px;">
        <option value="">Choose Academic Level</option>
        <?php
            foreach ($academic_levels as $key => $value) {
                $selected = '';
                if(isset($academic_level) && $academic_level == $value['academic_level_id'])
                    $selected = ' selected="selected"';
                echo '<option value="'.$value['academic_level_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
        ?>         
    </select>
    <div style="display:none" class="order_hints" id="academic_level_hint">Select your academic level</div>
</div>
	
<div class="block_element radioboxes dynamic-hints">
    <div class="block_element_head">Spacing</div>
    <?php echo form_error('spacing'); ?>
    <?php
        if(isset($spacing)) {
            $checked1 = '';
            $checked2 = '';
            if($spacing == 'Single')
                $checked1 = 'checked="checked"';
            else
                $checked2 = 'checked="checked"';
        }
        else
            $checked2 = 'checked="checked"';
    ?>    
    <label class="spacing_input_hints" onclick="change_spacing()"><input type="radio" name="spacing" value="Single"<?php echo $checked1; ?> />&nbsp;Single Spaced</label>
    <label class="spacing_input_hints" onclick="change_spacing()"><input type="radio" name="spacing" value="Double"<?php echo $checked2; ?> />&nbsp;Double Spaced</label>
    <div style="display:none" class="" id="spacing_hint_1">1 page = 550 words</div>
    <div style="display:none" class="" id="spacing_hint_2">1 page = 275 words</div>
</div>

<div class="block_element" id="number_of_pages_block">
    <label>Number of pages</label>
    <?php echo form_error('pages'); ?>
    <input type="text" name="pages" maxlength="3" id="number_of_pages" value="<?php if(isset($pages)) echo $pages; ?>" />
    <div style="display:none" class="order_hints" id="number_of_pages_hint">Please enter desired number of pages. Should be a full number.</div>
</div>
<div class="block_element" id="slides">
    <label>Number of slides</label>
    <?php echo form_error('slides'); ?>
    <input type="text" name="slides" id="number_of_slides" maxlength="3" value="<?php if(isset($slides)) echo $slides; ?>" />
    <div style="display:none" class="order_hints" id="number_of_slides_hint">If you need Power Point Presentation, specify the number of slides you need. 1 slide=50% of the cost per page</div>
</div>
<div class="block_element">
    <div class="block_element_head">Deadline</div>
    <?php echo form_error('deadline'); ?>
    <select name="deadline" id="deadline" class="calc_selector" style="width:220px;">
        <option value="">Choose deadline</option>
        <?php
            foreach ($deadlines as $key => $value) {
                $selected = '';
                if(isset($deadline) && $deadline == $value['deadline_id'])
                    $selected = ' selected="selected"';
                echo '<option value="'.$value['deadline_id'].'"'.$selected.'>'.$value['title'].'</option>';
            }
        ?>          
    </select>
    <div style="display:none" class="order_hints" id="deadline_hint">Please specify the deadline in advance, to have some time to review the paper</div>
</div>
<div class="block_element additional_materials_block">
    <div class="block_element_head">Additional Materials</div>
    <?php echo form_error('additional_materials'); ?>
    <?php 
        $checked1 = '';
        $checked2 = '';
        $checked3 = '';
        if(isset($additional_materials) && $additional_materials == 'none')
            $checked1 = 'selected="selected"';
        if(isset($additional_materials) && $additional_materials == 'later')
            $checked2 = 'selected="selected"';
        if(isset($additional_materials) && $additional_materials == 'needed')
            $checked3 = 'selected="selected"';
    ?>    
    <select name="additional_materials" class="dynamic-hints calc_selector" style="width:220px;" id="additional_materials" onChange="change_additional_materials()">
        <option value="none"<?php echo $checked1; ?>>Not needed</option>
        <option value="later"<?php echo $checked2; ?>>Needed, I will provide them later</option>
        <option value="needed"<?php echo $checked3; ?>>Needed, I won’t be able to provide them</option>
    </select>
    <div class="hidden" id="additional_materials_hint_for_2">Please upload your additional materials within 2 hours</div>
    <div style="display:none" class="order_hints" id="additional_materials_hint">Will the writer need any additional materials?</div>
</div>
	
<div class="block_element checkboxes plagiarism_report_block">
    <div class="block_element_head"></div>
    <?php
        $checked = '';
        if(isset($plagiarism_report) && $plagiarism_report == '1')
            $checked = 'checked="checked"';
    ?>
    <label>
        <input type="checkbox" name="plagiarism_report" id="plagiarism_report" value="1"<?php echo $checked; ?> />&nbsp;<span>I want to receive official Plagiarism report</span> $<span id='plag_price'>0</span> <a href='#' rel='tooltip' id="plagiarism_report_input_hints"><img src="../img/znak.png" style="margin-bottom: -4px;"></a>
    </label>&nbsp;
    
    <div class='order_hints hidden' id="plagiarism_report_hint" style="margin-left: 334px;">We guarantee that our papers are plagiarism-free. If you tick this option, in addition, we will provide you an official third party detailed plagiarism report. We will attach it to the final version of your writing. </div>
</div>

<div class="block_element checkboxes abstract_block">
    <div class="block_element_head"></div>
    <?php
        $checked = '';
        if(isset($abstract_page) && $abstract_page == '1')
            $checked = 'checked="checked"';
    ?>    
     <label>
        <input type="checkbox" name="abstract_page" onChange="calc_selector()" id="abstract_page" value="1"<?php echo $checked; ?> />&nbsp;<span>Add an Abstract page to my paper</span> $14.99 <a href='#' rel='tooltip' id="abstract_input_hints"><img src="../img/znak.png" style="margin-bottom: -4px;"></a>
    </label>&nbsp;
    
    <div class='order_hints hidden' id="abstract_hint" style="margin-left: 334px;">Often academic papers must contain an abstract, particularly if the writing consists of more than two pages and APA formatting. Tick this option if you want your writing to include an abstract page.</div>
</div>

<div class="block_element checkboxes top_priority_block">
    <div class="block_element_head"></div>
    <?php
        $checked = '';
        if(isset($top_priority) && $top_priority == '1')
            $checked = 'checked="checked"';
    ?>    
  <?php /* <label>
        <input type="checkbox" name="top_priority" onChange="calc_selector()" id="top_priority" value="1"<?php echo $checked; ?> />&nbsp;I want to order TOP priority customer service for $14.99
    </label>&nbsp;
    <a href='#' rel='tooltip' id="top_priority_input_hints">?</a>*/ ?>
    <div class='order_hints hidden' id="top_priority_hint">Top priority customer support means that replying to your messages and answering your calls will be our first order of business. Additionally, you will receive an SMS notification when a writer is assigned to your order, and when it is completed. Make sure to provide us with the correct phone number if you want this feature to work properly.</div>
</div>
	

<div class="block_element preferred_writer_block">
    <div class="block_element_head">Preferred writer</div>
    <?php echo form_error('preferred_writer'); ?>
    <?php 
        $checked1 = '';
        $checked2 = '';
        $checked3 = '';
        if(isset($preferred_writer) && $preferred_writer == '1')
            $checked1 = 'selected="selected"';
        if(isset($preferred_writer) && $preferred_writer == '2')
            $checked2 = 'selected="selected"';
        if(isset($preferred_writer) && $preferred_writer == '3')
            $checked3 = 'selected="selected"';
    ?>     
    <select name="preferred_writer" id="preferred_writer" class="dynamic-hints calc_selector" style="width:220px;" onChange="change_preferred_writer()">
        <option value="1"<?php echo $checked1; ?>>Any writer</option>
        <option value="2"<?php echo $checked2; ?>>TOP writer:+23% to the basic price</option>
        <option value="3"<?php echo $checked3; ?>>My previous writer</option>
    </select>
    <div class='hidden' id="previous_writer_hint">If the preferred writer is not able to complete your order, it will be assigned to another equally proficient writer</div>
    <div style="display:none" class="order_hints" id="preferred_writer_hint">You can request a specific writer or one of our TOP writers to work on your order</div>
</div>

<div class="block_element hidden" id="previous_writer_block">
    <label>Writer's ID</label>
    <?php echo form_error('previous_writer'); ?>
    <input type="text" name="previous_writer" id="previous_writer" value="<?php if(isset($previous_writer)) echo $previous_writer; ?>" />
    <div style="display:none" class="order_hints" id="previous_writer_hint_2">Enter the ID of your preferred writer or case number that was completed by him</div>
</div>

<div class="block_element discount_code_block">
    <label>Discount code</label>
    <?php echo form_error('discount_code'); ?>
    <input type="text" id="discount_code" name="discount_code" value="<?php if(isset($discount_code)) echo $discount_code; ?>" />
    <div style="display:none" class="order_hints" id="discount_code_hint">Please be aware that discounts cannot be applied to orders under $30.00</div>
</div>

<div class="block_element order-price">
    Price $<span class='order_form_price' id="calc_result">0</span>
    <div class='order_form_save' style='display:none'>(Total save <span class="span_order_form_save">0</span>$)</div>
</div>
<div class="block_element checkboxes">
    <div class="block_element_head"></div>
    <?php echo form_error('agree'); ?>
        <label>
            <input type="checkbox" name="agree" value="1" />&nbsp;<span>I agree with: 
            <a href='/money-back' target='_blank'>Money Back Guarantee</a>, 
            <a href='/privacy-policy' target='_blank'>Privacy Policy</a>, 
            <a href='/terms-of-use' target='_blank'>Terms of Use</a></span>.
        </label>
</div>
	<div class="block_element">
	    <input type="submit" class="btn btn-primary" onclick="nicEditors.findEditor('paper_details').saveContent();" value="Checkout" />
	</div>
	<input type="hidden" name="submit" value="1" />
	</form>

</div> <!-- /block_content -->
</div> <!-- /block_container -->
<script type="text/javascript">
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor();
    myNicEditor.addInstance('paper_details');
});
</script>
