<!DOCTYPE html>
<?php 

if (!isset($content))
	$content = '<h2>Empty page</h2>';
?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$title ?></title>

    <meta name="description" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_desc']; ?>">
    <meta name="keywords" content="<? if(isset($description)) echo $description; else echo $this->config->config['tp_meta_key']; ?>">
    <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/css/template.css" rel="stylesheet" media="all" />

    <script src="/js/jquery-1.8.3.js" type="text/javascript"></script>
	<script src="/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript" ></script>

    <link href="/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/js/functions.js"></script>
    <script type="text/javascript" src="/js/user.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,700,600' rel='stylesheet' type='text/css'>

   	<link rel="stylesheet" href="/css/cusel.css" type="text/css" media="screen">
    <script type="text/javascript" src="/js/cusel.js"></script>
    <script type="text/javascript" src="/js/cusel_set.js"></script>

	<?php 
	if(!empty($scripts))
		foreach ($scripts as $script)
			echo $script;
	?>
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-48444080-1', 'eagle-essays.com');
  ga('send', 'pageview');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24824639 = new Ya.Metrika({id:24824639,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24824639" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<div id="nav">
		<div class="cont">
			<a href="/user"><div class="logo"></div></a>
	        <div class="menu">
	            <ul>
	            	<!--<li><a href="/user/"><span>Home</span></a></li>-->
					<li><a href="/order"><span>New Order</span></a></li>
					<!--<li><a href="/user/new_inquiry"><span>New Inquiry</span></a></li>-->
					<li><a href="/user/logout"><span>Logout</span></a></li>
	            </ul>
	        </div>
	        <div class="welcome_user">Welcome, <?php echo $_SESSION['userdata']['first_name']; ?>!</div>
        </div>
    </div>

<div id="wrapper-user">
	
	<table class="table_wrapper"><tr>
	<td class="td_sidebar">
		<div id="sidebar"> 
			<div id="orders">
				<div class="orders_title active"><a href="/user/">Orders</a></div>
				<div class="orders_list">
					<?php
					if(isset($sidebar_orders) && $sidebar_orders)
						foreach ($sidebar_orders as $order) {
							echo '<div class="sidebar_order">';

							echo '<span class="order_id" id="order_'.$order['order_id'].'"><a href="/user/order_details/'.$order['order_id'].'">';
							if(isset($order['unread_messages']) && $order['unread_messages']) {
								echo '<span class="message_envelope" title="You have a '.$order['unread_messages'].' new messages">';
								echo '&nbsp;';
								echo '</span>';
							}
							if($order['status_client'] == 'Delivered' || $order['status_client'] == 'Approved')
							if(isset($order['file_id']) && $order['file_id']) {
								echo '<span class="file_envelope" title="You have a file">';
								echo '&nbsp;';
								echo '</span>';
							}							
							echo 'Order #'.$order['order_id'];
							echo '<br /><span class="sidebar_order_status">';
                if($order['payment_id']) {
                    switch ($order['status_client']) {
                        case 'Writer’s Review':
                            $order['status_client'] = 'Work in progress';
                            break;
                        case 'In progress':
                            $order['status_client'] = 'Work in progress';
                            break;
                        case 'Delivered':
                            $order['status_client'] = 'Draft submitted';
                            break;
                        case 'Revision in progress':
                            $order['status_client'] = 'Revision';
                            break;
                        case 'Canceled':
                            $order['status_client'] = 'Canceled';
                            break;
                        case 'Approved':
                            $order['status_client'] = 'Completed';
                            break;
                        default:
                            $order['status_client'] = '';
                            break;
                    }
                }
                else {
                    if($order['status_client'] == 'Canceled')
                        $order['status_client'] = 'Canceled';
                    else
                        $order['status_client'] = 'Waiting for payment';
                }
							echo $order['status_client'];
							echo '</span>';
							echo '</a></span>';
							echo '</div>';
						}
					?>
				</div>
			</div>
			<div id="profile">
				<a href="/user/profile">Profile</a>
			</div>
		</div>

	</td><td class="content_user">

		<div id="content-user">
			<?php echo $content; ?>
		</div>
	
	</td></tr></table>
</div>
<!-- Google Code for Purchases Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 974970579;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "RlpmCMmS3FYQ073z0AM";
var google_conversion_value = 1.00;
var google_conversion_currency = "GBP";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/974970579/?value=1.00&amp;currency_code=GBP&amp;label=RlpmCMmS3FYQ073z0AM&amp;guid=ON&amp;script=0
"/>
</div>
</noscript>

</body>
</html>

