<h2><?php echo $this->lang->line('Main_site_settings'); ?></h2>
<div id="settings">
	<h3><?php echo $this->lang->line('Settings_meta'); ?></h3>
	<hr>
    <form method="POST">
		<table>
            <tr>
                <td><span class="tdCaption"><strong>Title</strong><br>(<?php echo $this->lang->line('sitename'); ?>)</span></td>
                <td><input type="text" name="title" value="<?=$this->config->config['settings']['title']?>" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Meta-keyword</strong><br>(<?php echo $this->lang->line('meta_keywords'); ?>)</span></td>
                <td><textarea name="meta_keyword"><?=$this->config->config['settings']['meta_keyword']?></textarea></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Meta-description</strong><br>(<?php echo $this->lang->line('meta_description'); ?>)</span></td>
                <td><textarea name="meta_description"><?=$this->config->config['settings']['meta_description']?></textarea></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Text on main page</strong></span></td>
                <td><textarea name="main_page_text"><?php echo $main_page_text; ?></textarea></td>
            </tr>
        </table>
        <input type="submit" name="save_settings" value="<?php echo $this->lang->line('Save'); ?>" />
    </form>
	<h3><?php echo $this->lang->line('change_admin_password'); ?></h3>
	<hr>
	<span style="color:red"><?=@$msg ?></span>
	<form method="POST">
	<table>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Old_password'); ?></strong></span></td>
                <td><input type="password" name="pwd_old" value="" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('New_password'); ?></strong></span></td>
                <td><input type="password" name="pwd_new" value="" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Retype_password'); ?></strong></span></td>
                <td><input type="password" name="pwd_conf" value="" /></td>
            </tr>
        </table>
        <input type="submit" name="save_pwd" value="<?php echo $this->lang->line('Change'); ?>" />
	</form>
</div>