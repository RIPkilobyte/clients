<script type="text/javascript" src="/assets/editor/ckeditor.js"></script>
<?php if (isset($blog_id)): ?>
<h2>Edit record [ID:<?=$blog_id?>]</h2>
<?php else: ?>
<h2>Add record</h2>
<?php endif; ?>
<div id="article">
    <div class="edit">
        <div class="menu">
            <ul>
                <li><a onClick="save()"><span><?php echo $this->lang->line('Save');?></span></a></li>
                <li><a href="/admin/blog/"><span><?php echo $this->lang->line('Return');?></span></a></li>
            </ul>
        </div>
        <?php
        if(isset($errors)) {
        	echo '<div class="error">';
        	echo $errors;
        	echo '</div>';
        }
        ?>
        <form enctype="multipart/form-data" action="/admin/blog/edit/<?=(isset($blog_id)) ? $blog_id : "" ?>" method="post" id="my_form">
        <input type="hidden" name="blog_id" value="<?php echo $blog_id; ?>" />
        <input type="hidden" name="submit" value="1" />
        <input class="hidden" type="submit" id="submit" value="submit" />
        <hr>
        <table>
            <tr>
                <td><span class="tdCaption"><strong>Title</strong></span></td>
                <td><input type="text" name="title" value="<?php echo $title; ?>" size="50" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Link to record</strong></span></td>
                <td><input type="text" name="href" value="<?php echo $href; ?>" size="50" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Date</strong></span></td>
                <td><input type="text" name="date" value="<?php echo $date; ?>" size="50" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Comments includes</strong></span></td>
                <?php
                    $checked = '';
                    if($comments_on)
                        $checked = 'checked="checked"';
                ?>
                <td><input type="checkbox" <?php echo $checked; ?> name="comments_on" value="<?php echo $comments_on; ?>" size="50" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Comment moderation</strong></span></td>
                <?php
                    $checked = '';
                    if($moderate)
                        $checked = 'checked="checked"';
                ?>                
                <td><input type="checkbox" <?php echo $checked; ?> name="moderate" value="<?php echo $moderate; ?>" size="50" /></td>
            </tr>
        </table>
        <div class="clear"></div>
        <p><h4>Foreword</h4></p>
        <div style="margin-top: 15px;"><hr><textarea name="pre" class="pre" id="pre"><?php echo $pre; ?></textarea></div>
        <p><h4>Full text</h4></p>
        <div style="margin-top: 15px;"><hr><textarea name="text" class="text" id="text"><?php echo $text; ?></textarea></div>
        </form>
    </div>
</div>
<script type="text/javascript">
CKEDITOR.replace('text');
CKEDITOR.replace('pre');
function save() 
{
    for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
    $('#submit').click();
}
</script>





