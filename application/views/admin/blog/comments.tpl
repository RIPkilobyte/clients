<script type="text/javascript" src="/js/jquery-1.6.1.js"></script>
<script src="/js/jquery.form.min.js"></script>
<script src="/js/nicEdit.js" type="text/javascript"></script>
<?php if (isset($blog_id)): ?>
<h2>Edit record [ID:<?=$blog_id?>]</h2>
<?php else: ?>
<h2>Add record</h2>
<?php endif; ?>
<div id="article">
	<div class="edit">
        <div class="menu">
            <ul>
                <li><a href="/admin/blog/"><span><?php echo $this->lang->line('Return');?></span></a></li>
            </ul>
        </div>
        <?php
        if(isset($errors)) {
        	echo '<div class="error">';
        	echo $errors;
        	echo '</div>';
        }
        ?>
        <div class="success">
        </div>
        <input type="hidden" name="blog_id" value="<?php echo $blog_id; ?>" />
        <input type="hidden" name="submit" value="1" />
        <input class="hidden" type="submit" id="submit" value="submit" />
        <hr>
        
        <?php if(isset($records) && $records): ?>
        	<table>
        		<tr>
        			<th>Name</th>
        			<th>Email</th>
        			<th>Text</th>
        			<th></th>
                    <th></th>
        		</tr>
        		<?php foreach ($records as $record): ?>
        			<tr>
        				<td>
        					<?php echo $record['name']; ?>
        				</td>
        				<td>
        					<?php echo $record['email']; ?>
        				</td>
        				<td>
        					<!--<textarea cols="55" rows="5" id="text_<?php echo $record['comment_id']; ?>" class="comment_text"><?php echo $record['text']; ?></textarea>-->
        					<?php echo $record['text']; ?>
        				</td>
        				<td>
        					<div id="action_<?php echo $record['comment_id']; ?>">
        						<?php if($record['moderated']): ?>
        							<input type="button" value="Unmoderate" onclick="moderate(<?php echo $record['comment_id']; ?>)" />
        						<?php else: ?>
        							<input type="button" value="Moderate" onclick="moderate(<?php echo $record['comment_id']; ?>)" />
        						<?php endif; ?>
        					</div>
        				</td>
                        <td>
                            <a onclick="delete_comment(<?php echo $record['comment_id']; ?>)"><img src="/img/delete.png" title="Delete comment" class="file_link"></a>
                        </td>
        			</tr>
        		<?php endforeach; ?>
        	</table>
        <?php else: ?>
        	<p>Currently no comments yet</p>
        <?php endif; ?>
        
	</div>
</div>
<script type="text/javascript">
function moderate(id)
{
	$.post('/admin/blog/ajax_moderate_comment', {id: id}, function(data){
		if(data.status == 'ok') {
			$(".success").html('Success!');
			setTimeout('location.reload(false);', 1000);
		}
		else {
			$(".success").html('An error was occured. Please try again later');
			setTimeout('location.reload(false);', 1000);
		}
	}, 'json');
}
function delete_comment(id)
{
    if(confirm("Are you sure to delete this comment?")) {
        $.post('/admin/blog/ajax_delete_comment/'+id, {id: id}, function(data){
            if(data.status == 'ok') {
                $(".success").html('Success!');
                setTimeout('location.reload(false);', 1000);
            }
            else {
                $(".success").html('An error was occured. Please try again later');
                setTimeout('location.reload(false);', 1000);
            }
        }, 'json');
    }
}
function save() 
{
    $('#submit').click();
}
$().ready(function(){
});
/*
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor();
    $(".comment_text").each(function(){
    	var id = $(this).attr('id');
    	myNicEditor.addInstance(id);
    });
});
*/
</script>