<script type="text/javascript">
function delete_record(id)
{
    if(confirm("Are you sure to delete this record?")) {
        $.post('/admin/blog/json_delete/'+id, {id: id}, function(data){
            if(data.status == 'ok') {
                alert('<?php echo $this->lang->line('Record_successful');?>');
                location.reload();
            }
            else
                alert('<?php echo $this->lang->line('Error_with_deleting');?>');
        }, "json");
    }
}
$().ready(function(){
});
</script>
<h2>Blog</h2>
<div id='gallery_list'>

<a style="float: left;" class="button" href="/admin/blog/edit/">Add record</a>
    <div class="clear"></div>

    <table style="margin-top: 15px;" class="list" cellpadding="5">
        <tr>
            <th>№</th>
            <th>ID</th>
            <th>Title</th>
            <th>Link</th>
            <th>Date add</th>
            <th>Comments</th>
            <th>Unmoderated</th>
            <th>&nbsp;</th>
        </tr>
            <?php if(isset($records) && $records): ?>
                <?php $i = 1; ?>
                <?php foreach($records as $record): ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $record['blog_id']; ?></td>
                        <td><a href="/admin/blog/edit/<?php echo $record['blog_id']; ?>"><?php echo $record['title']; ?></a></td>
                        <td><?php echo $record['href']; ?></td>
                        <td><?php echo $record['date']; ?></td>
                        <td><a href="/admin/blog/comments/<?php echo $record['blog_id']; ?>"><?php echo $record['comments']; ?></a></td>
                        <td><a href="/admin/blog/unmoderated/<?php echo $record['blog_id']; ?>"><?php echo $record['unmoderate']; ?></a></td>
                        <td>
                            <a class="file_link" onClick="delete_record(<?php echo $record['blog_id']; ?>)">
                            <img src="/img/delete.png" title="Delete record" />
                            </a>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else: ?>
                <tr>
                    <td colspan="8">No added records</td>
                </tr>
            <?php endif; ?>
    </table>
</div>