<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$this->config->config['settings']['title']?> - <?php echo $this->lang->line('admin'); ?></title>
    <link href="<?=$this->config->base_url();?>img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="<?=$this->config->base_url();?>css/admin.css" rel="stylesheet" media="all" />
    <?php if (!empty($css)) { foreach ($css as $value) { ?>
        <link href="<?=$this->config->base_url();?>css/<?=$value?>" rel="stylesheet" media="all" />
    <?php }} ?>
    <script src="<?=$this -> config -> base_url(); ?>js/jquery-min-1.9.0.js"></script>
    <script src="<?=$this -> config -> base_url(); ?>js/functions.js"></script>
</head>

<body>
    
<div id="wrapper">
    
    <div id="header">
        <div class="menu">
            <ul class="adminmenu">
                <li<?=Menu::Check_active('/admin') ?>><a href="/admin"><span><?php echo $this->lang->line('Main'); ?></span></a></li>
                <li<?=Menu::Check_active('/admin/articles') ?>><a href="/admin/articles"><span><?php echo $this->lang->line('Articles'); ?></span></a></li>
                <li<?=Menu::Check_active('/admin/artcats') ?>><a href="/admin/artcats"><span><?php echo $this->lang->line('Categories'); ?></span></a></li>
                <li<?=Menu::Check_active('/admin/gallery') ?>><a href="/admin/gallery"><span><?php echo $this->lang->line('Gallery'); ?></span></a></li>
                <li<?=Menu::Check_active('/admin/blog') ?>><a href="/admin/blog"><span>Blog</span></a></li>
                <li<?=Menu::Check_active('/admin/order') ?>><a href="/admin/order"><span>Order settings</span></a></li>
                <li<?=Menu::Check_active('/admin/users') ?>><a href="/admin/users"><span>Users</span></a></li>
                <!--<li><a href="/admin/main/logoutsupport"><span>Logout support</span></a></li>-->
            </ul>
        </div>
    </div>
    
    <div id="content">
        <?=$content?>
    </div>
    
    <div id="footer">

        <a href="<?=$this->config->base_url();?>" target="_blank"><?php echo $this->lang->line('Frontend'); ?></a> |
        <a href="<?=$this->config->base_url();?>admin/login/logout" target="_blank"><?php echo $this->lang->line('Exit'); ?></a>
    </div>
    
</div>

</body>
</html>