<h2>List of Users</h2>
<span style="float: left;">
	<table>
		<tr>
			<td>Find by email:</td>
			<td><input type="text" name="user_email" id="user_email" value="" /></td>
		</tr>
		<tr>
			<td>Find by ID:</td>
			<td><input type="text" name="user_id" id="user_id" value="" /></td>
		</tr>
		<tr>
			<td><input type="button" value="Reset" onclick="reset_filter(0)" /></td>
			<td><input type="button" value="Search!" onclick="apply_filter(0)" /></td>
		</tr>
	</table>
</span>
<span style="float: right;">
	<table>
		<tr>
			<td>Per page:</td>
			<td>
				<select name="limit" id="limit" onchange="apply_filter(0)">
					<option value="30">30</option>
					<option value="50">50</option>
					<option value="100">100</option>
				</select>
			</td>
		</tr>
	</table>
</span>
<div class="clear"></div>

<table id="table_list" style="margin-top: 15px; width:100%" class="list" cellpadding="5">
<tr>
	<td>ID</td>
	<td>Email</td>
	<td>Site</td>
	<td>Name</td>
	<td>New password</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td colspan="7">No records yet</td>
</tr>	
</table>
<script type="text/javascript">
function apply_filter(page)
{
	var limit = $("#limit").val();
	var email = $("#user_email").val();
	var user_id = $("#user_id").val();
	$.post('/admin/users/ajax_get_users', {page:page, limit:limit, email:email, user_id:user_id}, function(data){
		$('#table_list tr').not(':first').remove();
		var text_html = '';
		if(data.rows.length > 0) {
			for (var i=0; i < data.rows.length; i++) {
				text_html += '<tr>';
				
				text_html += '<td>';
				text_html += data.rows[i].user_id;
				text_html += '</td>';

				text_html += '<td>';
				text_html += data.rows[i].email;
				text_html += '</td>';


				text_html += '<td>';
				text_html += data.rows[i].role;
				text_html += '</td>';

				text_html += '<td>';
				text_html += data.rows[i].first_name+' '+data.rows[i].last_name;
				text_html += '</td>';

				text_html += '<td>';
				text_html += '<input type="text" name="pass_'+data.rows[i].user_id+'" id="pass_'+data.rows[i].user_id+'" value="" />';
				text_html += '</td>';

				text_html += '<td>';
				text_html += '<a title="Change password" style="cursor:pointer;"onclick="change_password('+data.rows[i].user_id+')"><img src="/img/publish_yes.png"></a>';
				text_html += '</td>';

				text_html += '<td>';
				text_html += '<a title="Delete user" style="cursor:pointer;"onclick="delete_user('+data.rows[i].user_id+')"><img src="/img/delete.png"></a>';
				text_html += '</td>';

				text_html += '</tr>';
			}
		}
		else {
			text_html += '<tr><td colspan="7">No records found</td></tr>';
		}
		$('#table_list').append(text_html);
	}, 'json');
}
function reset_filter(page)
{
	$("#user_email").val('');
	$("#user_id").val('');
	apply_filter(0);
}
function change_password(user_id)
{
	if(confirm("Are you sure to change password for this user?")) {
		var password = $("#pass_"+user_id).val();
		$.post('/admin/users/ajax_update_user', {user_id:user_id, password: password}, function(data){
			apply_filter(0);
		}, 'json');
	}
}
function delete_user(user_id)
{
	if(confirm("Are you sure to delete this user?")) {
		$.post('/admin/users/ajax_delete_user', {user_id:user_id}, function(data){
			apply_filter(0);
		}, 'json');
	}
}
$().ready(function(){
	apply_filter(0);
});
</script>