<h2><?php echo $this->lang->line('Categories');?></h2>
<div id="artcats">
	<a style="float: left;" class="button" href="/admin/artcats/edit/"><?php echo $this->lang->line('Create_category');?></a>
    <div class="clear"></div>
    <table style="margin-top: 15px;" class="list" cellpadding="5">
        <tr>
            <th><?php echo $this->lang->line('Topic');?></th>
            <th><?php echo $this->lang->line('Status');?></th>
            <th><?php echo $this->lang->line('Alias');?></th>
            <th><?php echo $this->lang->line('Position');?></th>
            <th>ID</th>
        </tr>
        <?php
        foreach ($list as $category) {
            if (@$category['status']==0) {
                $class['status'] = 'publish_no';
            }else{
                $class['status'] = 'publish_yes';
            }
            ?>
            <tr>
                <td class="title">
                    <a href="/admin/artcats/edit/<?=$category['artcat_id']?>">
                        <span><?=@$category['title']?></span>
                    </a>
                </td>
                <td><a href="/admin/artcats/changestatus/<?=@$category['artcat_id']?>"><span class="<?=$class['status']?>"></span></a></td>
                <td class="alias">
                    <span><?=@$category['alias']?></span>
                </td>
                <td class="position"><span><?=$category['position']?></span></td>
                <td class="artcat_id"><span><?=$category['artcat_id']?></span></td>
            </tr>
        <?php } ?>
    </table>
    
</div>