<?php if (isset($artcat_id)): ?>
<h2><?php echo $this->lang->line('Edit_category');?> [ID:<?=@$artcat_id?>]</h2>
<?php else: ?>
<h2><?php echo $this->lang->line('New_category');?></h2>
<?php endif; ?>
<div id="artcat">
    <div class="edit">
        
        <div class="menu">
            <ul>
                <li><a onclick="save('save');"><span><?php echo $this->lang->line('Save');?></span></a></li>
                <li><a onclick="save('close');"><span><?php echo $this->lang->line('Save_and_close');?></span></a></li>
                <li><a href="/admin/artcats"><span><?php echo $this->lang->line('Close');?></span></a></li>
                <li><a href="/admin/artcats/delete/<?=@$artcat_id?>"><span><?php echo $this->lang->line('Delete');?></span></a></li>
                <li class="saved" style="display:none;"><span style="color:red;"><?php echo $this->lang->line('Saved');?></span></li>
            </ul>
        </div>
		<form method="POST" id="editform">
	        <h3><?php echo $this->lang->line('Display_options');?></h3>
			<hr>
			<table>
	            <tr>
	                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Topic');?>*</strong></span></td>
	                <td><input type="text" name="title" value="<?=@$title?>" size="92" /></td>
	            </tr>
	            <tr>
	                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Alias');?>*</strong></span></td>
	                <td><input style="width: 575px;" class="alias" type="text" name="alias" value="<?=@$alias?>" /></td>
	            </tr>
				<tr>
	                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Options');?></strong></span></td>
	                <td>
						<?php if (@$status == 1)   $checked['status'] = ' CHECKED '; ?>
	                	<input type="checkbox" name="status" value="1" <?=@$checked['status']?> /><?php echo $this->lang->line('Included');?>?
					</td>
	            </tr>
	        </table>
			<div class="spoiler" >
            <div class="sphead"><?php echo $this->lang->line('Image_download');?></div>
            <div class="spbody" <?php if(@$image != ""): ?> style="display:block"<?php endif; ?>>
				<div id="imgContainer" <?php if(@!($image != "")): ?> style="display:none"<?php endif; ?>>
					<img src="/upload/images/<?=@$image ?>" />
					<div style="margin-top: 15px;" class="button" id="imgDelete"><?php echo $this->lang->line('Delete');?></div>
					<hr>
					<span style="color: red;"><?php echo $this->lang->line('For_replace_image_select_new_old_deleted');?></span>
				</div>
				<div>
					<input type="file" name="image" id="imageFile" value=""/>
				</div>
			</div>
        </div>	
		</form>
        
    </div>
</div>

<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<script>
   var Gartcat_id = '<?=@$artcat_id?>';
   $(document).ready(function() {
	    $('#editform').ajaxForm(); 
		
		$("#imgDelete").click(function()
		{
			$.post("/index.php/admin/artcats/ajax_deleteImage/<?=@$artcat_id?>", function(data)
			{
  				if (data.length > 0)
				{
					$("#imgContainer").hide();
				}
			});
		});
		
	}); 
   
   
   
   function save(method) {
        
        if ($("#imageFile").val() != "")
		{
			var file = $("#imageFile")[0].files[0];
			var fileName = file.name;
			var fileSize = file.size;
			
			if((fileSize > 2000000) || (fileSize < 100))
			{
				alert("<?php echo $this->lang->line('Error_filesize');?> ("+fileSize+"<?php echo $this->lang->line('bytes');?>)");
				return 0;
			}
		}
		
		// if save copy
        if (method == 'copy') {
            var artcat_id = '';
        }else{
            var artcat_id = '<?=@$artcat_id?>';
        }
		
		$("#editform").ajaxSubmit(
		{ 
	        beforeSubmit:  function(){$('#content .menu ul, #content .saved').hide();},
	        url:       "/index.php/admin/artcats/ajax_artcat_save/"+artcat_id,
	        type:      'post',
	        dataType:  'json',
			data:({
				title :     $('input[name="title"]').val(),
                alias :     $('input[name="alias"]').val(),
                status :    $('input[name="status"]').prop('checked')
			}),
			success: function(data) {
				if(data.qstatus == 1) {
                    if (method == 'save') {
                        if (Gartcat_id == '') window.location.assign('/admin/artcats/edit/'+data.aid);
						
						$('#content .menu ul').show();
                        $('#content .saved').show();
						console.log(data.image);
						if (data.image != undefined)
						{
							$("#imgContainer img").attr("src","/upload/images/"+data.image);	
							$("#imgContainer").show();
						}
						Gartcat_id = data.aid;
                    }
                    if (method == 'close') {
                        window.location.assign('/admin/artcats');
                    }
                }
                else {
                    err = data.error;
					$('#content .menu ul').show();
                    $('#content .saved').html('<span style="color:red;"><?php echo $this->lang->line('An_error_occurred_when_saving');?></span>');
                    $('#content .saved').show();
                }
            }
	    });
		
		return 0;
		
		/*
		$('#content .menu ul').hide();
        $('#content .saved').hide();
        
        // Если сохранить копию
        if (method == 'copy') {
            var artcat_id = '';
        }else{
            var artcat_id = '<?=@$artcat_id?>';
        }
        
        $.ajax({
            url: "/index.php/admin/ajaxadmin/artcat_save/"+artcat_id,
            dataType: "html",
            type: "POST",
            data: ({
                title :     $('input[name="title"]').val(),
                alias :     $('input[name="alias"]').val(),
                status :    $('input[name="status"]').prop('checked'),
            }),
            success:  function(){
                if (method == 'save') {
                    $('#content .menu ul').show();
                    $('#content .saved').show();
                }
                if (method == 'close') {
                    window.location.assign('/admin/artcats');
                }
            }
        });*/
        
    }
</script>