<?php if (isset($image['file'])): ?>
<h2><?php echo $this->lang->line('Change_file');?> [ID:<?=$image['gallery_id']?>]</h2>
<?php else: ?>
<h2><?php echo $this->lang->line('Add_new_file');?></h2>
<?php endif; ?>
<div id="article">
    <div class="edit">
        <div class="menu">
            <ul>
                <li><a onClick="$('#form_gallery').submit()"><span><?php echo $this->lang->line('Save');?></span></a></li>
                <li><a href="/admin/gallery/"><span><?php echo $this->lang->line('Return');?></span></a></li>
            </ul>
        </div>
        <?php
        if(isset($errors)) {
        	echo '<div class="error">';
        	echo $errors;
        	echo '</div>';
        }
        ?>
        <form enctype="multipart/form-data" action="/admin/gallery/edit/<?=(isset($image['gallery_id'])) ? $image['gallery_id'] : "" ?>" method="post" id="form_gallery">
        	<input type="hidden" name="gallery_id" value="<?=(isset($image['gallery_id'])) ? $image['gallery_id'] : "0" ?>" />
			<hr>
			<table>
	            <tr>
	                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Title');?></strong></span></td>
	                <td><input type="text" name="name" value="<?=(isset($image['name'])) ? $image['name'] : '' ?>" size="50" /></td>
	            </tr>
	            <tr>
	                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Description');?></strong></span></td>
	                <td><textarea name="description" cols="37" rows="10"><?=(isset($image['description'])) ? $image['description'] : '' ?></textarea></td>
	            </tr>
				<tr>
	                <td><span class="tdCaption"><strong><?php echo $this->lang->line('File');?>*:</strong></span></td>
	                <td>
						<?php
       					if(isset($image['file'])) {
       						echo '<p>';
       						echo '<small><em>'.$this->lang->line('File_located_in').':</em><br /></small>';
       						echo '<strong>'.$image['full_path'].'</strong><br /><br />';
       						echo '<input type="file" name="file" size="50" />*<br /><br />';
							echo '<span style="font-weight:bold; color:red;">*'.$this->lang->line('If_you_replace_file_select_new').'</span>';
       						echo '</p>';
       					}
       					else {
       						echo '<input type="file" name="file" size="50" />';
       					}
       				?>
					</td>
	            </tr>
	        </table>
			
			
			
			<?php /*
			<div class="line">
        		<div class="left">
        			<nobr>
        				<span>Название:</span>
        				<input type="text" name="name" value="<?=(isset($image['name'])) ? $image['name'] : '' ?>" size="50" />
        			</nobr>
        		</div>
        		<div class="left">
        			<nobr>
        				<span style="vertical-align: top">Описание:</span>
        				<textarea name="description" cols="46" rows="10"><?=(isset($image['description'])) ? $image['description'] : '' ?></textarea>
        			</nobr>
        		</div>        		
        		<div class="left">
        			<nobr>
       				<span>Файл*:</span>
       				<?php
       					if(isset($image['file'])) {
       						echo '<p>';
       						echo 'Файл находится в:<br />';
       						echo $image['full_path'].'<br />';
       						echo 'Если вы хотите заменить файл, выберите новый (старый будет удален)<br />';
       						echo '<input type="file" name="file" size="50" />';
       						echo '</p>';
       					}
       					else {
       						echo '<input type="file" name="file" size="50" />';
       					}
       				?>
       				
       				</nobr>
        		</div>

        	</div> */?>
        </form>
	</div>
</div>