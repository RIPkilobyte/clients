<script type="text/javascript">
function delete_image(id)
{
    if(confirm("<?php echo $this->lang->line('Are_you_sure_delete_record_with_files');?>?")) {
        $.post('/admin/gallery/delete/'+id, {gallery_id: id}, function(data){
            if(data.status == 'ok') {
                alert('<?php echo $this->lang->line('Record_successful');?>');
                location.reload();
            }
            else
                alert('<?php echo $this->lang->line('Error_with_deleting');?>');
        }, "json");
    }
}
$().ready(function(){
});
</script>
<h2><?php echo $this->lang->line('Gallery');?></h2>
<div id='gallery_list'>

<a style="float: left;" class="button" href="/admin/gallery/edit/"><?php echo $this->lang->line('Add_image');?></a>
    <div class="clear"></div>

    <table style="margin-top: 15px;" class="list" cellpadding="5">
        <tr>
        	<th>№</th>
            <th>ID</th>
            <th><?php echo $this->lang->line('Title');?></th>
            <th><?php echo $this->lang->line('File');?></th>
            <th><?php echo $this->lang->line('Date_add');?></th>
            <th>&nbsp;</th>
        </tr>
        <?php
        if(isset($images) && $images) {
        	$i = 1;
        	foreach ($images as $k => $v) {
        		echo '<tr>';
        		
        		echo '<td>';
                echo '<a href="/admin/gallery/edit/'.$images[$k]['gallery_id'].'">';
        		echo '<span>'.$i.'</span>';
                echo '</a>';
        		echo '</td>';
        		
        		echo '<td>';
                echo '<a href="/admin/gallery/edit/'.$images[$k]['gallery_id'].'">';
                echo '<span>'.$images[$k]['gallery_id'].'</span>';
                echo '</a>';
        		echo '</td>';
        		
        		echo '<td>';
                echo '<a href="/admin/gallery/edit/'.$images[$k]['gallery_id'].'">';
                echo '<span>'.$images[$k]['name'].'</span>';
                echo '</a>';
        		echo '</td>';
        		
        		echo '<td>';
                echo '<a href="/admin/gallery/edit/'.$images[$k]['gallery_id'].'">';
                echo '<span>'.$images[$k]['file'].'</span>';
                echo '</a>';
        		echo '</td>';
        		
        		echo '<td>';
                echo '<a href="/admin/gallery/edit/'.$images[$k]['gallery_id'].'">';
                echo '<span>'.$images[$k]['date_add'].'</span>';
                echo '</a>';
        		echo '</td>';

                echo '<td>';
                echo '<a onClick="delete_image('.$images[$k]['gallery_id'].')">';
                echo '<img src="/img/delete.png" title="'.$this->lang->line('Delete').'" />';
                echo '</a>';
                echo '</td>';                

        		echo '</tr>';
        		$i++;
        	}
        }
        else {
        	echo '<tr>';
        	echo '<td colspan="6">';
        	echo '<span>'.$this->lang->line('No_added_records').'</span>';
        	echo '</td>';
        	echo '</tr>';
        }
        ?>
	</table>
</div>