<h2><?php echo $this->lang->line('Materials');?></h2>
<div id="articles">
   
    <a style="float: left;" class="button" href="/admin/articles/edit/"><?php echo $this->lang->line('Create_material');?></a>

    <span style="float: right;">
        <form name="form_filter_artcat" method="POST">
            <?php $filter_selected[@$filter_artcat] = ' SELECTED '; ?>
            <select name="filter_artcat" onchange="document.forms['form_filter_artcat'].submit()">
                <option value="no_filter">--<?php echo $this->lang->line('category_filter');?>--</option>
                <option <?=@$filter_selected['solo_only']?> value="solo_only">- <?php echo $this->lang->line('Elect');?> -</option>
                <?php foreach($categories as $category) { ?>
                    <option <?=@$filter_selected[$category['artcat_id']]?> value="<?=$category['artcat_id']?>"><?=$category['title']?></option>
                <?php } ?>
            </select>
        </form>
    </span>
    
    <div class="clear"></div>
    <table style="margin-top: 15px;" class="list" cellpadding="5">
        <tr>
            <th><?php echo $this->lang->line('Topic');?></th>
            <th><?php echo $this->lang->line('State');?></th>
            <th><?php echo $this->lang->line('Elect');?></th>
            <th><?php echo $this->lang->line('Category');?></th>
            <th><?php echo $this->lang->line('Date');?></th>
            <th>ID</th>
        </tr>
        <?php
        
        foreach ($list as $article) {
            if (@$article['status']==0) {
                $class['status'] = 'publish_no';
            }else{
                $class['status'] = 'publish_yes';
            }
            if (@$article['solo']==0) {
                $class['solo'] = 'publish_no';
            }else{
                $class['solo'] = 'publish_yes';
            }
        ?>
            <tr>
                <td class="title">
                    <a href="/admin/articles/edit/<?php echo $article['article_id']; ?>">
                        <span><?php echo @$article['title']; ?></span>
                    </a>
                </td>
                <td><a href="/admin/articles/changestatus/<?=@$article['article_id']?>"><span class="<?=$class['status']?>"></span></a></td>
                <td><a href="/admin/articles/changesolo/<?=@$article['article_id']?>"><span class="<?=$class['solo']?>"></span></a></td>
                <td class="category">
                    <span><?=@$article['category']['title']?></span>
                </td>
                <td class="date"><span><?=$article['date']?></span></td>
                <td class="article_id"><span><?=$article['article_id']?></span></td>
            </tr>
        <?php } ?>
    </table>
    
</div>