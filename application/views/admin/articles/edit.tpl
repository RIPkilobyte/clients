<script type="text/javascript" src="/assets/editor/ckeditor.js"></script>
<?php if (isset($article_id)): ?>
<h2><?php echo $this->lang->line('Edit_material');?> [ID:<?=@$article_id?>]</h2>
<?php else: ?>
<h2><?php echo $this->lang->line('New_material');?></h2>
<?php endif; ?>
<div id="article">
    <div class="edit">

        <div class="menu">
            <ul>
                <li><a onclick="save('save');"><span><?php echo $this->lang->line('Save');?></span></a></li>
                <li><a onclick="save('close');"><span><?php echo $this->lang->line('Save_and_close');?></span></a></li>
                <li><a onclick="save('copy');"><span><?php echo $this->lang->line('Save_copy');?></span></a></li>
                <li><a href="/admin/articles"><span><?php echo $this->lang->line('Close');?></span></a></li>
                <li><a href="/admin/articles/delete/<?=@$article_id?>"><span><?php echo $this->lang->line('Delete');?></span></a></li>
                <li class="saved" style="display:none;"><span style="color:red;"><?php echo $this->lang->line('Saved');?></span></li>
            </ul>
        </div>
        
        <form method="POST" id="editform" enctype="multipart/form-data" action="/index.php/admin/ajaxadmin/article_save/2" >
		<h3><?php echo $this->lang->line('Display_options');?></h3>
		<hr>
		<table>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Meta_title');?>*</strong></span></td>
                <td><input type="text" name="meta_title" value="<?=@$meta_title?>" size="92" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Topic');?>*</strong></span></td>
                <td><input type="text" name="title" value="<?=@$title?>" size="92" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Alias');?>*</strong></span></td>
                <td><input size="92" class="alias" type="text" name="alias" value="<?=@$alias?>" /></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Category');?></strong></span></td>
                <td>
					<select style="width: 300px;" name="artcat_id">
	                    <option value="">-- <?php echo $this->lang->line('select_category');?> --</option>
	                    <?php
	                    foreach($categories as $category) {
	                        $active_cat[$artcat_id] = ' SELECTED ';
	                        ?>
	                        <option <?=@$active_cat[$category['artcat_id']]?> value="<?=$category['artcat_id']?>"><?=$category['title']?></option>
	                    <?php } ?>
	                </select>
				</td>
            </tr>
			<tr>
                <td><span class="tdCaption"><strong><?php echo $this->lang->line('Options');?></strong></span></td>
                <td>
					<?php
	                if (@$status == 1)   $checked['status'] = ' CHECKED ';
	                if (@$solo == 1)     $checked['solo'] = ' CHECKED ';
	                ?>
	                <label><input type="checkbox" name="status" value="1" <?=@$checked['status']?> /><?php echo $this->lang->line('Included');?>?</label>
	                <br />
	                <label><input type="checkbox" name="solo" value="1" <?=@$checked['solo']?> /><?php echo $this->lang->line('Independent');?></label>
				</td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Video</strong></span></td>
                <td><textarea style="width: 545px;" name="video"><?=@$video?></textarea></td>
            </tr>            
        </table>
		<h3><?php echo $this->lang->line('Settings_meta');?></h3>
		<hr>
		<table>
            <tr>
                <td><span class="tdCaption"><strong>Meta-keyword</strong><br>(<?php echo $this->lang->line('meta_keywords');?>)</span></td>
                <td><textarea style="width: 545px;" name="meta_keyword"><?=@$meta_keyword?></textarea></td>
            </tr>
            <tr>
                <td><span class="tdCaption"><strong>Meta-description</strong><br>(<?php echo $this->lang->line('meta_description');?>)</span></td>
                <td><textarea style="width: 545px;" name="meta_description"><?=@$meta_description?></textarea></td>
            </tr>
        </table>
		
        <div class="clear"></div>
        
        <div style="margin-top: 15px;"><hr><textarea  name="text" class="text" id="text"><?=@$text?></textarea></div>
        
        <div class="spoiler">
            <div class="sphead"><?php echo $this->lang->line('Foreword');?></div>
            <div class="spbody">
                <textarea name="pre" id="pre"><?=@$pre?></textarea>
            </div>
        </div>
		
		<div class="spoiler">
            <div class="sphead"><?php echo $this->lang->line('Image_download');?></div>
            <div class="spbody" <?php if(@$image != ""): ?> style="display:block"<?php endif; ?>>
				<div id="imgContainer" <?php if(@!($image != "")): ?> style="display:none"<?php endif; ?>>
					<img src="/upload/images/<?=@$image ?>" />
					<div style="margin-top: 15px;" class="button" id="imgDelete"><?php echo $this->lang->line('Delete');?></div>
					<hr>
					<span style="color: red;"><?php echo $this->lang->line('For_replace_image_select_new_old_deleted');?></span>
				</div>
				<div>
					<input type="file" name="image" id="imageFile" value=""/>
				</div>
			</div>
        </div>			
		</form>
        
    </div>
</div>
<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<script>
 	
	var Gartcat_id = '<?=@$article_id?>';
    
	CKEDITOR.replace('text');
    CKEDITOR.replace('pre');
    	
	$(document).ready(function() {
	    $('#editform').ajaxForm(); 
		
		$("#imgDelete").click(function()
		{
			$.post("/index.php/admin/articles/ajax_deleteImage/<?=@$article_id?>", function(data)
			{
  				if (data.length > 0)
				{
					$("#imgContainer").hide();
				}
			});
		});
		
	}); 
	
	
    function save(method) {
        
        if ($("#imageFile").val() != "")
		{
			var file = $("#imageFile")[0].files[0];
			var fileName = file.name;
			var fileSize = file.size;
			
			if((fileSize > 2000000) || (fileSize < 100))
			{
				alert("<?php echo $this->lang->line('Error_filesize');?> ("+fileSize+"<?php echo $this->lang->line('bytes');?>)");
				return 0;
			}
		}
		
		// if save copy
        if (method == 'copy') {
            var article_id = '';
        }else{
            var article_id = '<?=@$article_id?>';
        }
		
		
		$("#editform").ajaxSubmit(
		{ 
	        beforeSubmit:  function(){$('#content .menu ul, #content .saved').hide();},
	        success:       function(){$('#content .menu ul, #content .saved').show();},
	        url:       "/index.php/admin/articles/ajax_article_save/"+article_id,
	        type:      'post',
	        dataType:  'json',
			data:({
				title :     $('input[name="title"]').val(),
	            alias :     $('input[name="alias"]').val(),
	            artcat_id : $('select[name="artcat_id"]').val(),
	            status :    $('input[name="status"]').prop('checked'),
	            solo :      $('input[name="solo"]').prop('checked'),
	            text :      CKEDITOR.instances.text.getData(),
	            pre :       CKEDITOR.instances.pre.getData(),
	            meta_description :  $('textarea[name="meta_description"]').val(),
	            meta_keyword :      $('textarea[name="meta_keyword"]').val()
			}),
			success: function(data) {
				if(data.qstatus == 1) {
                    if (method == 'save') {
						if (Gartcat_id == '') window.location.assign('/admin/articles/edit/'+data.aid);
						
                        $('#content .menu ul').show();
                        $('#content .saved').show();
						if (data.image != undefined)
						{
							$("#imgContainer img").attr("src","/upload/images/"+data.image);	
							$("#imgContainer").show();
						}
						Gartcat_id = data.aid;
                    }
                    if (method == 'close') {
                        window.location.assign('/admin/articles');
                    }
                    if (method == 'copy') {
                        window.location.assign('/admin/articles');
                    }
                }
                else {
                    err = data.error;
					$('#content .menu ul').show();
                    $('#content .saved').html('<span style="color:red;"><?php echo $this->lang->line('An_error_occurred_when_saving');?></span>');
                    $('#content .saved').show();
                }
            }
	    });
		
		return 0;
		/*
		$('#content .menu ul').hide();
        $('#content .saved').hide();
        
        // Если сохранить копию
        if (method == 'copy') {
            var article_id = '';
        }else{
            var article_id = '<?=@$article_id?>';
        }
		imageFile = "";
        if ($("#imageFile").val() != "")
		{
			//imageFile = $('#imageFile')[0].files[0];
			var file = $("#imageFile")[0].files[0];
			var fileName = file.name;
			var fileSize = file.size;
			
			if((fileSize > 2000000) || (fileSize < 100))
			{
				alert("Ошибка! Р файла ("+fileSize+"байт)");
			}
		}
        $.ajax({
            url: "/index.php/admin/ajaxadmin/article_save/"+article_id,
            
            type: "POST",
            data: ({
                title :     $('input[name="title"]').val(),
                alias :     $('input[name="alias"]').val(),
                artcat_id : $('select[name="artcat_id"]').val(),
                status :    $('input[name="status"]').prop('checked'),
                solo :      $('input[name="solo"]').prop('checked'),
                text :      CKEDITOR.instances.text.getData(),
                pre :       CKEDITOR.instances.pre.getData(),
                meta_description :  $('textarea[name="meta_description"]').val(),
                meta_keyword :      $('textarea[name="meta_keyword"]').val(),
            }),
            complete: function(data) {
                if(data.responseText.length > 0) {
                    if (method == 'save') {
                        $('#content .menu ul').show();
                        $('#content .saved').show();
                    }
                    if (method == 'close') {
                        window.location.assign('/admin/articles');
                    }
                    if (method == 'copy') {
                        window.location.assign('/admin/articles');
                    }
                }
                else {
                    $('#content .menu ul').show();
                    $('#content .saved').html('<span style="color:red;">Произошла ошибка при сохранении</span>');
                    $('#content .saved').show();
                }
            },

        });*/
        
    }
</script>