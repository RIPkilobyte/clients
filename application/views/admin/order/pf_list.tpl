<h2>List of Referencing styles</h2>
<div class="menu">
	<ul>
		<li><a style="float: left;" class="button" href="/admin/order/pf/edit">Create new</a></li>
	</ul>
</div>
<div class="clear"></div>

<table style="margin-top: 15px;" class="list" cellpadding="5">
<tr>
	<td>Number</td>
	<td>Title</td>
	<td>Sort</td>
	<td>&nbsp;</td>
</tr>
<?php if(isset($records) && $records): ?>
	<?php $i = 1; ?>
	<?php foreach($records as $record): ?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><a href="/admin/order/pf/edit/<?php echo $record['paper_format_id']; ?>"><?php echo $record['title']; ?></a></td>
			<td><?php echo $record['position']; ?></td>
			<td><a href="/admin/order/pf/delete/<?php echo $record['paper_format_id']; ?>"><img src="/img/delete.png"></a></td>
		</tr>
		<?php $i++; ?>
	<?php endforeach; ?>
<?php else: ?>
	<tr>
		<td colspan="3">No records yet</td>
	</tr>	
<?php endif; ?>
</table>