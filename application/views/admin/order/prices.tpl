<h2>List of Prices</h2>
<div class="menu">
	<ul>
		<li><a href="/admin/order"><span>Return</span></a></li>
	</ul>
</div>
<?php
	if(isset($errors)) {
		echo '<div class="error">';
		echo $errors;
		echo '</div>';
	}
?>
<h3>Custom Essay Prices Per Page</h3>
<?php

$width = count($deadlines);
?>
<table style="width:100%;" border="1px black">
	<tr>
		<th>Level</th>
		<?php for($i=0; $i<$width; $i++) { ?>
			<th><?php echo $deadlines[$i]['title']; ?></th>
		<?php } ?>
	</tr>
	<?php foreach($academic_levels as $level) { ?>
		<tr>
			<td><?php echo $level['title']; ?></td>
			<?php for($i=0; $i<$width; $i++) { ?>
				<td>
					<?php $order_price = 0; ?>
					<?php foreach($prices as $price) {
						if($price['type'] == 'new' && $price['academic_level_id'] == $level['academic_level_id'] && $price['deadline_id'] == $deadlines[$i]['deadline_id'])
							$order_price = $price['value'];
					} ?>
					$ <input type="text" class="order_price" id="new_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>" value="<?php echo $order_price; ?>" size="4" />
					<a onclick="save_price('new_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>')" id="link_new_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>" style="cursor:pointer;" title="Save" class="order_price_button"><img id="img_new_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>" width="16px" src="/img/publish_yes.png" /></a>
				</td>
			<?php } ?>
		</tr>
	<?php } ?>
</table>
<div class="clear"></div>

<h3>Editing Prices Per Page</h3>
<table style="width:100%;" border="1px black">
	<tr>
		<th>Level</th>
		<?php for($i=0; $i<$width; $i++) { ?>
			<th><?php echo $deadlines[$i]['title']; ?></th>
		<?php } ?>
	</tr>
	<?php foreach($academic_levels as $level) { ?>
		<tr>
			<td><?php echo $level['title']; ?></td>
			<?php for($i=0; $i<$width; $i++) { ?>
				<td>
					<?php $order_price = 0; ?>
					<?php foreach($prices as $price) {
						if($price['type'] == 'edit' && $price['academic_level_id'] == $level['academic_level_id'] && $price['deadline_id'] == $deadlines[$i]['deadline_id'])
							$order_price = $price['value'];
					} ?>
					$ <input type="text" class="order_price" id="edit_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>" value="<?php echo $order_price; ?>" size="4" />
					<a onclick="save_price('edit_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>')" id="link_edit_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>" style="cursor:pointer;" title="Save" class="order_price_button"><img id="img_edit_<?php echo $level['academic_level_id']; ?>_<?php echo $deadlines[$i]['deadline_id']; ?>" width="16px" src="/img/publish_yes.png" /></a>
				</td>
			<?php } ?>
		</tr>
	<?php } ?>
</table>
<div class="clear"></div>

<script type="text/javascript">
function save_price(id)
{
	$("#img_"+id).attr('src', '/img/loading.gif');
	var price = $("#"+id).val();
	price = parseFloat(price);
	if(isNaN(price))
		price = 0;
	$("#"+id).val(price);
	$.post('/admin/order/ajax_save_price', {price: price, id: id}, function(data){
		if(data.status == 'ok') {
			$("#link_"+id).css('display', 'none');
			$("#img_"+id).attr('src', '/img/publish_yes.png');
		}
		else {
		}
		//setTimeout('location.reload(false)', 2000);
	}, 'json');
}
function save()
{
	$(".saved").show();
	setTimeout('location.reload(false)', 2000);
}
$().ready(function(){
	$(".order_price_button").css('display', 'none');
	$(".order_price").keyup(function(){
		var id = $(this).attr('id');
		$("#link_"+id).css('display', '');
	});
});
</script>