<?php if (isset($record_id) && $record_id): ?>
	<h2>Edit record [ID:<?php echo $record_id; ?>]</h2>
<?php else: ?>
	<h2>Add record</h2>
<?php endif; ?>
<div class="menu">
	<ul>
		<li><a onclick="save('close');"><span><?php echo $this->lang->line('Save_and_close');?></span></a></li>
		<li><a href="/admin/order/subject/list"><span>Close</span></a></li>

		<?php if (isset($record_id) && $record_id): ?>
			<li><a href="/admin/order/subject/delete/<?php echo $record_id; ?>"><span>Delete</span></a></li>
		<?php endif; ?>
	</ul>
</div>
<?php
	if(isset($errors)) {
		echo '<div class="error">';
		echo $errors;
		echo '</div>';
	}
?>
<form method="POST" id="editform" enctype="multipart/form-data">
	<input type="hidden" value="1" name="submit" />
	<table>
		<tr>
			<td><span class="tdCaption"><strong>Title</strong></span></td>
			<td><input type="text" name="title" value="<?php echo $record['title']; ?>" /></td>
		</tr>
		<tr>
			<td><span class="tdCaption"><strong>Position</strong></span></td>
			<td><input type="text" name="position" value="<?php echo $record['position']; ?>" placeholder="99" /></td>
		</tr>
	</table>
	<div class="clear"></div>
	<input type="submit" id="submit" class="hidden" />
</form>
<script type="text/javascript">
function save()
{
	$("#submit").click();
}
$().ready(function(){
});
</script>