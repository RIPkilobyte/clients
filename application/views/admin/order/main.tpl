<h1>Orders settings</h1>

<h2>Academic levels</h2>
    <div class="menu">
        <ul>
            <li><a style="float: left;" class="button" href="/admin/order/al/list">List all</a></li>
            <li><a style="float: left;" class="button" href="/admin/order/al/edit">Create new</a></li>
        </ul>
    </div>
    <div class="clear"></div>

<h2>Paper types</h2>
    <div class="menu">
        <ul>
            <li><a style="float: left;" class="button" href="/admin/order/pt/list">List all</a></li>
            <li><a style="float: left;" class="button" href="/admin/order/pt/edit">Create new</a></li>
        </ul>
    </div>
    <div class="clear"></div>

<h2>Referencing style</h2>
    <div class="menu">
        <ul>
            <li><a style="float: left;" class="button" href="/admin/order/pf/list">List all</a></li>
            <li><a style="float: left;" class="button" href="/admin/order/pf/edit">Create new</a></li>
        </ul>
    </div>
    <div class="clear"></div>

<h2>Subjects</h2>
    <div class="menu">
        <ul>
            <li><a style="float: left;" class="button" href="/admin/order/subject/list">List all</a></li>
            <li><a style="float: left;" class="button" href="/admin/order/subject/edit">Create new</a></li>
        </ul>
    </div>
    <div class="clear"></div>

<h2>Deadlines</h2>
    <div class="menu">
        <ul>
            <li><a style="float: left;" class="button" href="/admin/order/deadline/list">List all</a></li>
            <li><a style="float: left;" class="button" href="/admin/order/deadline/edit">Create new</a></li>
        </ul>
    </div>
    <div class="clear"></div>

<h2>Prices</h2>
    <div class="menu">
        <ul>
            <li><a style="float: left;" class="button" href="/admin/order/prices">View and editing</a></li>
        </ul>
    </div>
    <div class="clear"></div>

<h2>Discount codes</h2>
    <div class="menu">
        <p>Coming soon</p>
    </div>
<div class="clear"></div>