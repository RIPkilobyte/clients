<!doctype html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <title><?php echo $title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name="description" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_desc']; ?>">
  <meta name="keywords" content="<?php if(isset($keywords)) echo $keywords; else echo $this->config->config['tp_meta_key']; ?>">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
  <link rel="stylesheet" href="/css/styles_lk.css">
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="/js/functions.js"></script>
  <script type="text/javascript" src="/js/user.js"></script>
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<div class="wrapper_main">
<div class="site-wrapper">
    <div class="left_bg"></div>

  <div class="top_block">
    <div class="block_left">
      <div class="logo_block">
      <a href="/" ><img src="/img/logo_lk.png" alt="logo"></a>
      </div>
    </div>
    <div class="float-left top_link top_link_h">
      <a href="/order" class="blue_btn pay_btn_new">New order</a>
    </div>
        <div class="float-left top_link top_link_h qqq">
      <p class="pol">NO CHARGEBACK POLICY</p>
      <div class="pol_info">
        <p>As we use an Anti-Fraud Management system, we practice NO CHARGEBACK POLICY against payments that are claimed as “Unauthorized”.</p>
      </div>
    </div>
    <div class="right_block right_block_h">
      <div class="usser_out">
        <span>
          Welcome, <?php echo $_SESSION['userdata']['first_name']; ?>!
        </span>
        <span>
          <a title="Exit" href="/user/logout">
            <img src="/img/logout.png"  alt="">
          </a>
        </span>
      </div>
    </div>
  </div>

  <div class="middle_block">

<div class="block_left" id="leftAside">
      <ul class="default_list">
        <li>
            <?php
              $active = '';
              if($_SERVER['REQUEST_URI'] == '/user')
                $active = 'btn_left_active';
            ?>
          <!--<a href="/user" class="btn_left btn_left_order <?php echo $active; ?>">Orders</a>-->

          <?php if(isset($sidebar_orders) && $sidebar_orders): ?>
            <?php foreach($sidebar_orders as $orders): ?>

              <?php if(isset($order) && count($order) && $order['order_id'] == $orders['order_id']): ?>
                <div class="orders_list btn_left_active">
              <?php else: ?>
                <div class="orders_list">
              <?php endif; ?>
                  <span><a href="/user/order_details/<?php echo $orders['order_id']; ?>">Order #<?php echo $orders['order_id']; ?></a></span>

                  <div class="icons_wrap icons_wrap_b">
                    <?php if(isset($orders['unread_messages']) && $orders['unread_messages']): ?><!-- надо подсветить картинку-->
                     <a id="left_blink1_<?php echo $orders['order_id']; ?>" href="#" class="blinker">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have a <?php echo $orders['unread_messages']; ?> new messages</span>
                        </span>
                     </a>
                    <?php else: ?><!-- картинка не подсвечивается-->
                     <a id="left_blink1_<?php echo $orders['order_id']; ?>" href="#">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have no new messages</span>
                        </span>
                     </a>
                    <?php endif; ?>

                    <?php if($orders['status'] == 'File delivered' && isset($orders['file_id']) && $orders['file_id']): ?><!-- надо подсветить картинку-->
                     <a id="left_blink2_<?php echo $orders['order_id']; ?>" href="/user/order_details/<?php echo $orders['order_id']; ?>" class="blinker">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have a file</span>
                        </span>
                     </a>
                    <?php else: ?><!-- картинка не подсвечивается-->
                     <a id="left_blink2_<?php echo $orders['order_id']; ?>" href="#">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>You have no file</span>
                        </span>
                     </a>
                    <?php endif; ?>

                    <?php if(!$orders['payment_id']): ?><!-- надо подсветить картинку-->
                     <a id="left_blink3_<?php echo $orders['order_id']; ?>" href="/cart/checkout/<?php echo $orders['order_id']; ?>" class="blinker">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>Waiting for a payment</span>
                        </span>
                     </a>
                    <?php else: ?><!-- картинка не подсвечивается-->
                     <a id="left_blink3_<?php echo $orders['order_id']; ?>" href="#">
                        <span class="popin_block">
                        <span class="bg_popbl"></span>
                          <span>Payment has been already made</span>
                        </span>
                     </a>
                    <?php endif; ?>
                  </div>
                </div>
            <?php endforeach; ?>
          <?php endif; ?>


        </li>

        <li><a href="/user#all" class="btn_left" id="all_link">All orders</a></li>
        <?php
          $active = '';
          if($_SERVER['REQUEST_URI'] == '/user/profile')
            $active = 'btn_left_active';
        ?>
        <li><a href="/user/profile" class="btn_left <?php echo $active; ?>">Your profile</a></li>
        <?php
          $active = '';
          if($_SERVER['REQUEST_URI'] == '/discounts')
            $active = 'btn_left_active';
        ?>
        <li><a href="/discounts" class="btn_left <?php echo $active; ?>">Your discounts</a></li>
      </ul>
          <div class="top_link_l top_link">
      <a href="/order" class="blue_btn pay_btn_new">New order</a>
    </div>
    <div class="right_block right_block_l">
      <div class="usser_out">
        <span>
          Welcome, <?php echo $_SESSION['userdata']['first_name']; ?>!
        </span>
        <span>
          <a title="Exit" href="/user/logout">
            <img src="/img/logout.png"  alt="">
          </a>
        </span>
      </div>
    </div>
    </div><!-- .block_left -->

<?php 
  if(isset($content) && $content)
    echo $content;
?>
    
  </div><!-- .middle_block -->
   <div id="screen"></div>
  <a href="javascript:void(0);" class="btn-menu" title="Left side"><img src="/img/left_side_btn.png" alt=""></a>

</div> <!-- .site-wrapper -->
</div>
<script type="text/javascript">
var timeOuts = new Array();
function blink(id)
{
  if(!$('#'+id).is(":hover")) {
  $('#'+id).fadeOut('slow',function() {
    $('#'+id).fadeIn('slow',function() {
      clearTimeout(timeOuts[id]);
      timeOuts[id] = setTimeout('blink("'+id+'")', 700);
    });
  });
  }
}
$().ready(function(){
  $(".blinker").each(function(){
    blink(""+$(this).attr('id')+"");
  });
  $(".blinker").hover(
    function() {
      if($(this).hasClass("icons_red1")) {
        $(this).removeClass('icons_red1');
        $(this).addClass('icons_blue1');
      }
      else if($(this).hasClass("icons_red2")) {
        $(this).removeClass('icons_red2');
        $(this).addClass('icons_blue2');
      }
      else if($(this).hasClass("icons_red3")) {
        $(this).removeClass('icons_red3');
        $(this).addClass('icons_blue3');
      }
      
      var id = $(this).attr('id');
      clearTimeout(timeOuts[id]);
      $('#'+id).fadeIn('slow',function() {
        clearTimeout(timeOuts[id]);
      });
    },
    function() {
      if($(this).hasClass("icons_blue1")) {
        $(this).addClass('icons_red1');
        $(this).removeClass('icons_blue1');
      }
      else if($(this).hasClass("icons_blue2")) {
        $(this).addClass('icons_red2');
        $(this).removeClass('icons_blue2');
      }
      else if($(this).hasClass("icons_blue3")) {
        $(this).addClass('icons_red3');
        $(this).removeClass('icons_blue3');
      }      
      var id = $(this).attr('id');
      timeOuts[id] = setTimeout('blink("'+id+'")', 700);
    }
  );
  if(window.location.hash) {
    var s = window.location.hash.substr(1);
    if(s == 'past')
      $("#previous_link").addClass('btn_left_active');
  };




    var btn_win = $('.btn-menu'), w = $(window).width();
      btn_win.hide();
     $(window).resize(function(){
      var w = $(window).width();
            if(w < 964) {
              btn_win.show().attr('id', 'click_me');
              $('#leftAside').addClass('toggle_side');
      }else{
             btn_win.hide().removeAttr('id', 'click_me'); 
             $('#leftAside').removeClass('toggle_side');
             $('#screen').hide();
            }
        });
      if(w < 964){
        btn_win.show().attr('id', 'click_me');
              $('#leftAside').addClass('toggle_side');
      }else{
             btn_win.hide().removeAttr('id', 'click_me'); 
             $('#leftAside').removeClass('toggle_side');
      }
        $('#click_me').click(
          function  () {
            $('.toggle_side').addClass('hide_left');
            $(this).hide();
            $('#screen').show();
          });
        $('#screen').click(
          function  () {
            $(this).hide();
            $('.toggle_side').removeClass('hide_left');
            btn_win.show();
          })





}); 
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-48444080-1', 'eagle-essays.com');
  ga('send', 'pageview');
</script>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2TzXmB9ZRIjV7KryWVSKLzYNYybsFD54";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

<!-- Google Code for Purchases Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 974970579;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "RlpmCMmS3FYQ073z0AM";
var google_conversion_value = 1.00;
var google_conversion_currency = "GBP";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/974970579/?value=1.00&amp;currency_code=GBP&amp;label=RlpmCMmS3FYQ073z0AM&amp;guid=ON&amp;script=0
"/>
</div>
</noscript>
</body>
</html>