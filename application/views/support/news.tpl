<script type="text/javascript" src="/assets/editor/ckeditor.js"></script>
<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<div class="block_container">
	<div class="block_head">Our news</div>
	<div class="block_content">
		<div id="result" class="error"></div>
		<table width='100%'>
			<tr>
				<td width='60%'>
					<h3>Write some news</h3>
					<form id="news_form" action="/support/news/edit" method="post">
						<table>
							<tr>
								<td>
									<p><input type="text" name="title" value="" style="width:80%" size="250" /></p>
								</td>
							</tr>
							<tr>
								<td>
									<textarea name="text" class="text" id="text"></textarea>
								</td>
							</tr>
							<tr>
								<td>
									<div class="interface_buttons">
										<input class="btn_green" type="button" value="SEND" />
									</div>
								</td>
							</tr>
						</table>
					</form>
				</td>
				<td width='40%'>
					<table width='100%'>
						<?php if(isset($news) && $news): ?>
							
							<?php foreach ($news as $key => $value): ?>
								<tr width='100%'>
									<td>
									<ul>
									<li>
										<a href="/support/news/edit/<?php echo $value['news_id']; ?>">
											<?php echo $value['date']; ?> &nbsp;
											<?php echo $value['title']; ?>
										</a> &nbsp;
										<a onClick="delete_news(<?php echo $value['news_id']; ?>)">
											<img src="/img/delete.png" title="Delete" alt="Delete" />
										</a>
									</li>
									</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							
						<?php else: ?>
							<tr>
								<td>
									<p>Havent added news</p>
								</td>
							</tr>
						<?php endif; ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>
<script>
$().ready(function(){
	CKEDITOR.replace('text', {
		width: '600px'
	});
	$(".btn_green").click(function(){
		$("#news_form").submit();
	});
});
</script>