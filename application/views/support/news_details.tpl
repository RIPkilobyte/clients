<script type="text/javascript" src="/assets/editor/ckeditor.js"></script>
<div class="block_container">
	<?php if(isset($news_id) && $news_id): ?>
		<div class="block_head">Edit news #<?php echo $news_id; ?></div>
	<?php else: ?>
		<div class="block_head">Add news</div>
	<?php endif; ?>
	<div class="block_content">
		<table class="table_order_details">
			<tr>
				<td class="td_order_information">
					<div class="order_messages subblock">

						<?php if(isset($news_id) && $news_id): ?>

								<?php if(isset($errors) && $errors): ?>
									<h3>Please correct the errors form filling</h3>
									<div class="order_addon_pad">
										<div class="error"><?php echo $errors; ?></div>
										<form id="news_form" action="/support/news/edit/<?php echo $news_id; ?>" method="post">
										<input type="hidden" name="news_id" value="<?php echo $news_id; ?>" />
											<input type="text" style="width:100%" name="title" value="<?php echo $title; ?>" size="250"><br />
											<p><textarea name="text" id="text" class="text"><?php echo $text; ?></textarea><br /></p>
											<div class="interface_buttons">
												<input class="btn_green" type="button" value="SEND" />
											</div>
										</form>
										<script>
											$().ready(function(){
												CKEDITOR.replace('text', {
													width: '100%'
												});
											});
										</script>
									</div>
								<?php endif; ?>

								<?php if(isset($status) && $status == 'saved'): ?>
									<h3>News updated successfully</h3>
									<div class="order_addon_pad">
										<a href="/support/news/">Back to previous page</a>
									</div>
								<?php elseif(isset($status) && $status == 'show'): ?>
									<h3>Please correct the errors form filling</h3>
									<div class="order_addon_pad">
										<form id="news_form" action="/support/news/edit/<?php echo $news_id; ?>" method="post">
										<input type="hidden" name="news_id" value="<?php echo $news_id; ?>" />
											<input type="text" style="width:100%" name="title" value="<?php echo $title; ?>" size="250"><br />
											<p><textarea name="text" id="text" class="text"><?php echo $text; ?></textarea><br /></p>
											<div class="interface_buttons">
												<input class="btn_green" type="button" value="SEND" />
											</div>
										</form>
									</div>
									<script>
										$().ready(function(){
											CKEDITOR.replace('text', {
												width: '100%'
											});
										});
									</script>
								<?php else: ?>
								<?php endif; ?>
							</div>

						<?php else: ?>

							<?php if(isset($errors) && $errors): ?>
								<h3>Please correct the errors form filling</h3>
								<div class="order_addon_pad">
									<div class="error"><?php echo $errors; ?></div>
									<form id="news_form" action="/support/news/edit" method="post">
										<input type="text" style="width:100%" name="title" value="<?php echo $title; ?>" size="250"><br />
										<p><textarea name="text" id="text" class="text"><?php echo $text; ?></textarea><br /></p>
										<div class="interface_buttons">
											<input class="btn_green" type="button" value="SEND" />
										</div>
									</form>
								</div>
								<script>
									$().ready(function(){
										CKEDITOR.replace('text', {
											width: '100%'
										});
									});
								</script>
							<?php else: ?>
								<h3>News added successfully</h3>
								<div class="order_addon_pad">
									<a href="/support/news">Back to previous page</a>
								</div>
							<?php endif; ?>

						<?php endif; ?>

					</div>
				</td>
			</tr>
		</table>
	</div>
</div>
<script>
$().ready(function(){
	
	$(".btn_green").click(function(){
		$("#news_form").submit();
	});
});
</script>