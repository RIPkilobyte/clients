<script src="/js/jquery.form.min.js"></script>
<script src="/js/nicEdit.js" type="text/javascript"></script>
<div class="block_container">
	<?php if(isset($user) && count($user)): ?>
	<div class="block_head"><span>Writer #</span> <?php echo $user['user_id']; ?></div>
	<div class="block_content">
		<table class="table_order_details">
			<tr>
				<td class="td_order_information">
					<div class="order_information subblock">
						<h3>User information</h3>
						<table>
							<tr>
								<th>Status</th>
								<th>First name</th>
								<th>Last name</th>
								<th>Email</th>
								<th>Country</th>
								<th>City</th>
								<th>Phone</th>
								<th>Language</th>
							</tr>
							<tr>
								<td>
									<select id="writer_status" onchange="change_status('<?php echo $user['user_id']; ?>')">
										<option value="0" <?php echo ($user['status'] ? "":"SELECTED='SELECTED'"); ?>>Not active</option>
										<option value="1" <?php echo ($user['status'] ? "SELECTED='SELECTED'":""); ?>>Active</option>
									</select>
								</td>
								<td>
									<div id="user_first_name">
										<div id="cover_span_first_name">
											<span id="text_value_first_name"><?php echo $user['first_name']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'first_name')"><img src="/img/edit_icon.png" height="12px;" /></a>
										</div>
										<div id="cover_input_first_name" style="display:none;">
											<input type="text" size="10" id="input_first_name" value="<?php echo $user['first_name']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'first_name', '<?php echo $user['first_name']; ?>')"><img src="/img/chbx_check.png" /></a>
										</div>
										<div id="cover_loading_first_name" style="display:none;">
											<img src="/img/loading.gif" height="20px" />
										</div>
									</div>
								</td>
								<td>
									<div id="user_last_name">
										<div id="cover_span_last_name">
											<span id="text_value_last_name"><?php echo $user['last_name']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'last_name')"><img src="/img/edit_icon.png" height="12px;" /></a>
										</div>
										<div id="cover_input_last_name" style="display:none;">
											<input type="text" size="10" id="input_last_name" value="<?php echo $user['last_name']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'last_name', '<?php echo $user['last_name']; ?>')"><img src="/img/chbx_check.png" /></a>
										</div>
										<div id="cover_loading_last_name" style="display:none;">
											<img src="/img/loading.gif" height="20px" />
										</div>
									</div>								
								</td>
								<td>
									<?php if($user['verify_code'] != ''): ?>
										<div class="error">
											<div id="user_email">
												<div id="cover_span_email">
													<span id="text_value_email"><?php echo $user['email']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'email')"><img src="/img/edit_icon.png" height="12px;" /></a>
												</div>
												<div id="cover_input_email" style="display:none;">
													<input type="text" size="10" id="input_email" value="<?php echo $user['email']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'email', '<?php echo $user['email']; ?>')"><img src="/img/chbx_check.png" /></a>
												</div>
												<div id="cover_loading_email" style="display:none;">
													<img src="/img/loading.gif" height="20px" />
												</div>
											</div>
										</div>
									<?php else: ?>
										<div class="success">
											<div id="user_email">
												<div id="cover_span_email">
													<span id="text_value_email"><?php echo $user['email']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'email')"><img src="/img/edit_icon.png" height="12px;" /></a>
												</div>
												<div id="cover_input_email" style="display:none;">
													<input type="text" size="10" id="input_email" value="<?php echo $user['email']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'email', '<?php echo $user['email']; ?>')"><img src="/img/chbx_check.png" /></a>
												</div>
												<div id="cover_loading_email" style="display:none;">
													<img src="/img/loading.gif" height="20px" />
												</div>
											</div>
										</div>
									<?php endif; ?>
								</td>
								<td><?php echo $user['country_title']; ?></td>
								<td>
									<div id="user_city">
										<div id="cover_span_city">
											<span id="text_value_city"><?php echo $user['city']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'city')"><img src="/img/edit_icon.png" height="12px;" /></a>
										</div>
										<div id="cover_input_city" style="display:none;">
											<input type="text" size="10" id="input_city" value="<?php echo $user['city']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'city', '<?php echo $user['city']; ?>')"><img src="/img/chbx_check.png" /></a>
										</div>
										<div id="cover_loading_city" style="display:none;">
											<img src="/img/loading.gif" height="20px" />
										</div>
									</div>
								</td>
								<td>
								<span style="float:left;">+<?php echo $user['phone_prefix']; ?> &nbsp; </span>
									<div id="user_phone">
										<div id="cover_span_phone">
											<span id="text_value_phone"><?php echo $user['phone']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'phone')"><img src="/img/edit_icon.png" height="12px;" /></a>
										</div>
										<div id="cover_input_phone" style="display:none;">
											<input type="text" size="10" id="input_phone" value="<?php echo $user['phone']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'phone', '<?php echo $user['phone']; ?>')"><img src="/img/chbx_check.png" /></a>
										</div>
										<div id="cover_loading_phone" style="display:none;">
											<img src="/img/loading.gif" height="20px" />
										</div>
									</div>								
								</td>
								<td>
									<div id="user_language">
										<div id="cover_span_language">
											<span id="text_value_language"><?php echo $user['language']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'language')"><img src="/img/edit_icon.png" height="12px;" /></a>
										</div>
										<div id="cover_input_language" style="display:none;">
											<input type="text" size="10" id="input_language" value="<?php echo $user['language']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'language', '<?php echo $user['language']; ?>')"><img src="/img/chbx_check.png" /></a>
										</div>
										<div id="cover_loading_language" style="display:none;">
											<img src="/img/loading.gif" height="20px" />
										</div>
									</div>
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<th>Disciplines</th>
								<th>Paper formats</th>
								<th>About</th>
							</tr>
							<tr>
								<td><?php echo $user['disciplines']; ?></td>
								<td><?php echo $user['paper_formats']; ?></td>
								<td>
									<div id="user_about">
										<div id="cover_span_about">
											<span id="text_value_about"><?php echo $user['about']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $user['user_id']; ?>', 'about')"><img src="/img/edit_icon.png" height="12px;" /></a>
										</div>
										<div id="cover_input_about" style="display:none;">
											<input type="text" size="10" id="input_about" value="<?php echo $user['about']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info('<?php echo $user['user_id']; ?>', 'about', '<?php echo $user['about']; ?>')"><img src="/img/chbx_check.png" /></a>
										</div>
										<div id="cover_loading_about" style="display:none;">
											<img src="/img/loading.gif" height="20px" />
										</div>
									</div>								
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<th>No. of orders:</th>
								<th>Aver. Grade</th>
							</tr>
							<tr>
								<td><?php echo $user['orders']; ?></td>
								<td><?php echo $user['grades']; ?></td>
							</tr>
						</table>
						<table>
							<tr>
								<th>Comment</th>
							</tr>
							<tr>
								<td>
									<textarea id="user_comment" rows="5" cols="60"><?php echo $user['comment']; ?></textarea><br />
									<div class="interface_buttons" id="save_comment_result">
										<input class="btn_green" type="button" value="Save comment" onClick="nicEditors.findEditor('user_comment').saveContent();save_writer_comment(<?php echo $user['user_id']; ?>)" />
									</div>
								</td>
								<td id="reset_files_result">
									<input class="btn_green" type="button" value="Reset files" onClick="reset_files(<?php echo $user['user_id']; ?>)" />
								</td>
								<td id="reset_test_result">
									<input class="btn_green" type="button" value="Reset test" onClick="reset_test(<?php echo $user['user_id']; ?>)" />
								</td>
								<td id="reset_verification_result">
									<input class="btn_green" type="button" value="Reset verification" onClick="reset_verification(<?php echo $user['user_id']; ?>)" />
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
		<?php if($user['step'] == 'one' || $user['step'] == 'three' || $user['step'] == 'reject_files' || $user['step'] == 'reject_test'): ?>
		<table>
			<tr>
				<td class="td_order_information">
					<div class="order_information subblock">
						<h3>Writer actions</h3>
						<table>
							<tr>
								<td>
									<div class="interface_buttons" id="user_result">

										<?php if($user['step'] == 'one'): ?>
											<input class="btn_green" type="button" value="Accept files" onClick="accept_step1(<?php echo $user['user_id']; ?>)" />
											<input class="btn_gray" type="button" value="Reject files" onClick="reject_step1(<?php echo $user['user_id']; ?>)" />
											<!--<input class="btn_gray" type="button" value="Dismiss" onClick="reject_writer(<?php echo $user['user_id']; ?>)" />-->
										<?php elseif($user['step'] == 'three'): ?>
											<input class="btn_green" type="button" value="Accept test" onClick="accept_test(<?php echo $user_testing_results['users_testing_id']; ?>)" />
											<input class="btn_gray" type="button" value="Deciline test" onClick="reject_test(<?php echo $user_testing_results['users_testing_id']; ?>)" />
										<?php elseif($user['step'] == 'reject_files'): ?>
											<p>Writer has reset by files</p>
											<input class="btn_green" type="button" value="Reset files" onClick="reset_files(<?php echo $user['user_id']; ?>)" />
										<?php elseif($user['step'] == 'reject_test'): ?>
											<p>Writer has reset by test</p>
											<input class="btn_green" type="button" value="Reset test" onClick="reset_test(<?php echo $user['user_id']; ?>)" />
										<?php endif; ?>
										<!--<div id="user_result" class="error"></div>-->
									</div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
		<?php endif; ?>
		<?php if($user['step'] == 'one'): ?>
			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 1</h3>
							<?php if(isset($user_testing) && $user_testing):?>
								<table>
									<tr>
										<td>
											<h4>Diplom:</h4>
										</td>
										<td>
											<?php if($user_testing['diplom']): ?>
												<p><a href="/support/ajax/get_user_file/<?php echo $user_testing['diplom']['file_id']; ?>"><?php echo $user_testing['diplom']['client_name']; ?></a></p>
												<?php if($user_testing['diplom_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['diplom_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>
											<?php else: ?>
												<div id="submit_diplom_result" class="error">
												<p>User has not upload file</p>
												<div class="interface_buttons">
													<form id="diplom_form" method="post">
													<input type="file" name="support_diplom" />
													<input class="btn_green" type="button" value="Submit" onClick="submit_diplom(<?php echo $user['user_id']; ?>)" />
													</form>
												</div>
												</div>
												<div class="clear"></div>
											<?php endif; ?>
										</td>
										<td>

										</td>
									</tr>
									<tr>
										<td><h4>Passport:</h4></td>
										<td>
											<?php if($user_testing['passport']): ?>
												<p><a href="/support/ajax/get_user_file/<?php echo $user_testing['passport']['file_id']; ?>"><?php echo $user_testing['passport']['client_name']; ?></a></p>
												<?php if($user_testing['passport_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['passport_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>												
											<?php else: ?>
												<div id="submit_passport_result" class="error">
												<p>User has not upload file</p>
												<div  class="interface_buttons">
													<form id="passport_form" method="post">
													<input type="file" name="support_passport" />
													<input class="btn_green" type="button" value="Submit" onClick="submit_passport(<?php echo $user['user_id']; ?>)" />
													</form>
												</div>
												</div>
												<div class="clear"></div>
											<?php endif; ?>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td><h4>Essay:</h4></td>
										<td>
											<p><b>Theme:</b> <?php echo $essay['title']; ?></p>
											<?php if($user_testing['essay']): ?>
												<p><a href="/support/ajax/get_user_file/<?php echo $user_testing['essay']['file_id']; ?>"><?php echo $user_testing['essay']['client_name']; ?></a></p>
												<?php if($user_testing['essay_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['essay_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>												
											<?php else: ?>
												<div id="submit_essay_result" class="error">
												<p>User has not upload file</p>
												<div class="interface_buttons">
													<form id="essay_form" method="post">
													<input type="file" name="support_essay" />
													<input class="btn_green" type="button" value="Submit" onClick="submit_essay(<?php echo $user['user_id']; ?>)" />
													</form>
												</div>
												</div>
												<div class="clear"></div>
											<?php endif; ?>
										</td>
										<td>

										</td>
									</tr>
								</table>
							<?php else: ?>
								<table>
									<tr>
									<td>
										User has not yet chosen a theme essay
									</td>
									</tr>
								</table>
							<?php endif;?>
						</div>
					</td>
				</tr>
			</table>
		<?php elseif($user['step'] == 'two_old'):?>
			<link href="/css/kendo.common.min.css" rel="stylesheet" />
			<link href="/css/kendo.default.min.css" rel="stylesheet" />
			<script src="/js/kendo.web.min.js"></script>

			<form action="/support/ajax/timeintervals/<?php echo $user['user_id']; ?>" method="post" id="timeinterval_form">
			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 2</h3>
							<?php if($testing_dates): ?>
								<table>
									<tr>
										<td>
										<p>Dateintervals for user testing:</p>
										</td>
									</tr>
								</table>								
								<table class="paper_details">
									<?php 
										$selected = 0;
										foreach ($testing_dates as $date) {
											if($date['selected']) {
												$selected = 1;
												echo '<tr>';
												echo '<td>';
												echo '<h4>User choose interval:</h4>';
												echo '<b>'.$date['datetime'].'</b>';
												echo '</td>';
												echo '</tr>';

												echo '<tr>';
												echo '<td>';
												echo '<div class="interface_buttons">';
												echo '<input class="btn_green" type="button" value="Allow testing" onClick="allow_testing('.$user['user_id'].')" />';
												echo '</td>';
												echo '</tr>';
											}
											else {
												echo '<tr>';
												echo '<td>';
												echo 'Select date:';
												echo '</td>';
												echo '<td>';
												echo '<input type="text" name="timeinterval[]" class="datetimepicker" value="'.$date['datetime'].'" />';
												echo '</td>';
												echo '</tr>';
											}
										}
									?>
								</table>
								<table>
									<tr>
										<td>
											<div class="interface_buttons">
												<input class="btn_green" type="button" value="Add new interval" onClick="add_dateinterval()" />
												<input class="btn_green" type="button" value="Accept" onClick="accept_intervals()" />
											</div>
										</td>
									</tr>
								</table>								
							<?php else: ?>
								<table>
									<tr>
										<td>
										<p>To user not assigned testing date</p>
										<p>Please insert time intervals for user choose. Interval will be selected from the input you 30 minutes</p>
										</td>
									</tr>
								</table>
									
								<table class="paper_details">
									<tr>
										<td>
											Select date:
										</td>
										<td>
											<input type="text" class="datetimepicker" name="timeinterval[]" />
										</td>
									</tr>
									<?php 
									/*
										for($i=0; $i<10; $i++) {

											echo '<tr>';
											echo '<td>';
											echo 'Insert date eq. 2013-10-28 09:04:54';
											echo '</td>';
											echo '<td>';
											echo '<input type="text" class="datetimepicker" name="timeinterval[]" />';
											echo '</td>';
											echo '</tr>';
										}
										//*/
									?>
								</table>
								<table>
									<tr>
										<td>
											<div class="interface_buttons">
												<input class="btn_green" type="button" value="Add new interval" onClick="add_dateinterval()" />
												<input class="btn_green" type="button" value="Accept" onClick="accept_intervals()" />
											</div>
										</td>
									</tr>
								</table>
							<?php endif; ?>
						</div>
					</td>
				</tr>
			</table>
			</form>
			<script>
			$(document).ready(function () {
				$(".datetimepicker").each(function(){
					var value = convert_datetime($(this).val());
					$(this).kendoDateTimePicker({
						format: "yyyy-MM-dd hh:mm:ss",
						value: value
					});
				});
			});
			</script>			
		<?php elseif($user['step'] == 'two'):?>
			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 2</h3>
							<p>User passes testing</p>
						</div>
					</td>
				</tr>
			</table>
		<?php elseif($user['step'] == 'three'):?>
			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 1</h3>
							<?php if(isset($user_testing) && $user_testing):?>
								<table>
									<tr>
										<td>
											<h4>Diplom:</h4>
										</td>
										<td>
											<?php if($user_testing['diplom']): ?>
												<p><a href="/support/ajax/get_user_file/<?php echo $user_testing['diplom']['file_id']; ?>"><?php echo $user_testing['diplom']['client_name']; ?></a></p>
												<?php if($user_testing['diplom_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['diplom_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>
											<?php else: ?>
												<p>User has not upload file</p>
											<?php endif; ?>
											<div id="diplom_result" class="error"></div>
										</td>
										<td>
											<?php if(!isset($user_testing['diplom_id'])): ?>
												<div class="interface_buttons">
													<!--
													<input class="btn_gray" type="button" value="Reject" id="reject_diplom" onClick="reject_diplom(<?php echo $user['user_id']; ?>)" />
													<input class="btn_green" type="button" value="Accept" id="accept_diplom" onClick="accept_diplom(<?php echo $user['user_id']; ?>)" />
													-->
												</div>
											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td><h4>Passport:</h4></td>
										<td>
											<?php if($user_testing['passport']): ?>
												<p><a href="/support/ajax/get_user_file/<?php echo $user_testing['passport']['file_id']; ?>"><?php echo $user_testing['passport']['client_name']; ?></a></p>
												<?php if($user_testing['passport_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['passport_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>												
											<?php else: ?>
												<p>User has not upload file</p>
											<?php endif; ?>
											<div id="passport_result" class="error"></div>
										</td>
										<td>
											<?php if(!isset($user_testing['passport_id'])): ?>
												<div class="interface_buttons">
													<!--
													<input class="btn_gray" type="button" value="Reject" id="reject_passport" onClick="reject_passport(<?php echo $user['user_id']; ?>)" />
													<input class="btn_green" type="button" value="Accept" id="accept_passport" onClick="accept_passport(<?php echo $user['user_id']; ?>)" />
													-->
												</div>
											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td><h4>Essay:</h4></td>
										<td>
											<p><b>Theme:</b> <?php echo $essay['title']; ?></p>
											<?php if($user_testing['essay']): ?>
												<p><a href="/support/ajax/get_user_file/<?php echo $user_testing['essay']['file_id']; ?>"><?php echo $user_testing['essay']['client_name']; ?></a></p>
												<?php if($user_testing['essay_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['essay_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>												
											<?php else: ?>
												<p>User has not upload file</p>
											<?php endif; ?>
											<div id="essay_result" class="error"></div>
										</td>
										<td>
											<?php if(!isset($user_testing['essay_file'])): ?>
												<div class="interface_buttons">
													<!--
													<input class="btn_gray" type="button" value="Reject" id="reject_diplom" onClick="reject_essay(<?php echo $user['user_id']; ?>)" />
													<input class="btn_green" type="button" value="Accept" id="accept_diplom" onClick="accept_essay(<?php echo $user['user_id']; ?>)" />
													-->
												</div>
											<?php endif; ?>
										</td>
									</tr>
								</table>
							<?php else: ?>
								<table>
									<tr>
									<td>
										User has not yet chosen a theme essay
									</td>
									</tr>
								</table>
							<?php endif;?>
						</div>
					</td>
				</tr>
			</table>

			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 2</h3>
							<?php if(isset($user_testing_results) && $user_testing_results): ?>
							<table>
								<tr>
									<td>
										<p><b>Test on grammar and formatting text</b></p>
										<p>
										<?php
											if($user_testing_results['status'] == '1')
												echo '<div class="success">passed</div>';
											else
												echo '<div class="error">fail</div>';
										?>
										</p>
									</td>
									<td>
										<p>Questions: <b><?php echo $user_testing_results['questions']; ?></b></p>
										<p>Answers: <b><?php echo $user_testing_results['answers']; ?></b></p>
										<p>Points: <b><?php echo $user_testing_results['points']; ?></b></p>
									</td>
									<td>
										<div class="interface_buttons">
											<!--
											<input class="btn_green" type="button" value="Accept" onClick="accept_test(<?php echo $user_testing_results['users_testing_id']; ?>)" />
											<input class="btn_gray" type="button" value="Deciline" onClick="reject_test(<?php echo $user_testing_results['users_testing_id']; ?>)" />
											-->
										</div>
									</td>										
								</tr>
							</table>

							<?php if(isset($tests) && $tests): ?>
								<?php foreach ($tests as $test): ?>
									<h3><?php echo $test['title']; ?></h3>
									<?php $i = 1; ?>
									<?php foreach ($test['questions'] as $question): ?>
										<table>
											<tr>
												<td><?php echo $i; ?>. Question: <b><?php echo $question['question']; ?></b></td>
											</tr>
											<tr>
												<td>Answer: <b><?php echo $question['answer']; ?></b></td>
											</tr>
											<tr>
												<?php if($question['correct']): ?>
													<td><div>Status: &nbsp;</div><div class="success">success</div></td>
												<?php else: ?>
													<td><div>Status: &nbsp;</div><div class="error">error</div></td>
												<?php endif; ?>
											</tr>
										</table>
										<?php $i++; ?>
									<?php endforeach; ?>
								<?php endforeach; ?>
								<div class="interface_buttons">
									<!--<input class="btn_green" type="button" value="Approve" onClick="accept_writer(<?php echo $user['user_id']; ?>)" />
									<input class="btn_gray" type="button" value="Dismiss" onClick="reject_writer(<?php echo $user['user_id']; ?>)" />-->
								</div>								
							<?php endif; ?>

							<?php else: ?>
								<p>User not complete test</p>
							<?php endif;?>
						</div>
					</td>
				</tr>
			</table>
		<?php elseif($user['step'] == 'finished'):?>
			<div id="test_result" class="error"></div>
			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 1</h3>
							<?php if(isset($user_testing) && $user_testing):?>
								<table>
									<tr>
										<td>
											<h4>Diplom:</h4>
										</td>
										<td>
											<?php if($user_testing['diplom']): ?>
												<p><a href="/support/ajax/get_file/<?php echo $user_testing['diplom']['file_id']; ?>"><?php echo $user_testing['diplom']['client_name']; ?></a></p>
												<?php if($user_testing['diplom_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['diplom_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>
											<?php else: ?>
												<p>User has not upload file</p>
											<?php endif; ?>
											<div id="diplom_result" class="error"></div>
										</td>
										<td>
											<?php if(!isset($user_testing['diplom_id'])): ?>
												<div class="interface_buttons">
													<!--
													<input class="btn_gray" type="button" value="Reject" id="reject_diplom" onClick="reject_diplom(<?php echo $user['user_id']; ?>)" />
													<input class="btn_green" type="button" value="Accept" id="accept_diplom" onClick="accept_diplom(<?php echo $user['user_id']; ?>)" />
													-->
												</div>
											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td><h4>Passport:</h4></td>
										<td>
											<?php if($user_testing['passport']): ?>
												<p><a href="/support/ajax/get_file/<?php echo $user_testing['passport']['file_id']; ?>"><?php echo $user_testing['passport']['client_name']; ?></a></p>
												<?php if($user_testing['passport_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['passport_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>												
											<?php else: ?>
												<p>User has not upload file</p>
											<?php endif; ?>
											<div id="passport_result" class="error"></div>
										</td>
										<td>
											<?php if(!isset($user_testing['passport_id'])): ?>
												<div class="interface_buttons">
													<!--
													<input class="btn_gray" type="button" value="Reject" id="reject_passport" onClick="reject_passport(<?php echo $user['user_id']; ?>)" />
													<input class="btn_green" type="button" value="Accept" id="accept_passport" onClick="accept_passport(<?php echo $user['user_id']; ?>)" />
													-->
												</div>
											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td><h4>Essay:</h4></td>
										<td>
											<p><b>Theme:</b> <?php echo $essay['title']; ?></p>
											<?php if($user_testing['essay']): ?>
												<p><a href="/support/ajax/get_file/<?php echo $user_testing['essay']['file_id']; ?>"><?php echo $user_testing['essay']['client_name']; ?></a></p>
												<?php if($user_testing['essay_confirm'] == '1'): ?>
													<p class="success">File is accepted</p>
												<?php elseif($user_testing['essay_confirm'] == '2'): ?>
													<p class="error">File is not accepted</p>
												<?php endif; ?>												
											<?php else: ?>
												<p>User has not upload file</p>
											<?php endif; ?>
											<div id="essay_result" class="error"></div>
										</td>
										<td>
											<?php if(!isset($user_testing['essay_file'])): ?>
												<div class="interface_buttons">
													<!--
													<input class="btn_gray" type="button" value="Reject" id="reject_diplom" onClick="reject_essay(<?php echo $user['user_id']; ?>)" />
													<input class="btn_green" type="button" value="Accept" id="accept_diplom" onClick="accept_essay(<?php echo $user['user_id']; ?>)" />
													-->
												</div>
											<?php endif; ?>
										</td>
									</tr>
								</table>
							<?php else: ?>
								<table>
									<tr>
									<td>
										User has not yet chosen a theme essay
									</td>
									</tr>
								</table>
							<?php endif;?>
						</div>
					</td>
				</tr>
			</table>

			<table class="table_order_details">
				<tr>
					<td class="td_order_information">
						<div class="order_information subblock">
							<h3>Step 3</h3>
							<?php if(isset($user_testing_results) && $user_testing_results): ?>
							<table>
								<tr>
									<td>
										<p><b>Test on grammar and formatting text</b></p>
										<p>
										<?php
											if($user_testing_results['status'] == '1')
												echo '<div class="success">passed</div>';
											else
												echo '<div class="error">fail</div>';
										?>
										</p>
									</td>
									<td>
										<p>Questions: <b>15</b></p>
										<p>Answers: <b><?php echo $user_testing_results['answers']; ?></b></p>
										<p>Points: <b><?php echo $user_testing_results['points']; ?></b></p>
									</td>
									<td>
										<div class="interface_buttons">
											<!--
											<input class="btn_green" type="button" value="Accept" onClick="accept_test(<?php echo $user_testing_results['users_testing_id']; ?>)" />
											<input class="btn_gray" type="button" value="Deciline" onClick="reject_test(<?php echo $user_testing_results['users_testing_id']; ?>)" />
											-->
										</div>
									</td>										
								</tr>
							</table>

							<?php if(isset($questions) && $questions): ?>
								<?php foreach ($questions as $key => $value): ?>
									<table>
										<tr>
											<td>Question: <b><?php echo $value['question']; ?></b></td>
										</tr>
										<tr>
											<td>Answer: <b><?php echo $value['answer']; ?></b></td>
										</tr>
										<tr>
											<?php if($value['correct']): ?>
												<td><div>Status: &nbsp;</div><div class="success">success</div></td>
											<?php else: ?>
												<td><div>Status: &nbsp;</div><div class="error">error</div></td>
											<?php endif; ?>
										</tr>
									</table>
								<?php endforeach; ?>
							<?php endif; ?>

							<?php else: ?>
								<p>User not complete test</p>
							<?php endif;?>
						</div>
					</td>
				</tr>
			</table>
		<?php endif;?>
	</div>
	<?php else: ?>
		Blank page
	<?php endif; ?>
</div>
<script type="text/javascript">
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor();
    myNicEditor.addInstance('user_comment');    
});
</script>
