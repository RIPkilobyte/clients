<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<link rel="stylesheet" href="/css/ui.jqgrid.css" type="text/css" media="screen">

<div class="block_container">
  <div class="block_head">Payment statistics</div>
  <div class="block_content filter_grid">
  	<div>
  		<div style="cursor:pointer;" class="fr" onClick="export_excel()"><img src="/img/xls.png" height="48px" title="Export to Excel" /></div>
  		<div class="clear"></div>
  	</div>
<!--
    <div>Order ID:</div>
    <div>
      <input type="text" size="18" value="" id="search_id" /> &nbsp; 

      <div class="interface_buttons">
        <input class="btn_green" type="button" value="Search" onClick="doSearch()" />
        <input class="btn_gray" type="button" value="Reset" onClick="resetSearch()" />
      </div>
    </div>
  
-->
	</div>
	<div class="block_content">
		<div id="tabs">
<!--
		  <ul>
            <li><a id="orders_tab_all" href="#all">ALL</a></li>
	      </ul>
-->
	      <div id="all">
	        <div id="pager_orders_top_all"></div>
	        <table id="list_orders_all"></table>
	        <div id="pager_orders_all"></div>
	      </div>
		</div>
    <input type="hidden" id="defender" value="0" />
	</div>
</div>
<script type="text/javascript">
function export_excel()
{
  location.assign("http://"+window.location.hostname+"/support/ajax/payments_export_xls");
}
var per_page = 100;
var row_list = [100,200];
var sort_name = "date";
var sort_order = "desc";
function resetSearch()
{
  $("#search_id").val('');
  $("#orders_tab_all").click();
}
function doSearch()
{
  $("#orders_tab_all").click();
}
function build_grid(type)
{
  $("#search_id").val('');
  $("#list_orders_"+type).jqGrid({
    url:'/support/ajax/get_payments/'+type,
    datatype: "json",
    mtype: 'POST',
    colNames:['Site', 'Order date', 'Client name', 'Order ID', 'Topic', 'Amount', 'Receive date'],
    colModel:[
    	{name:'site_title', width:100, align:'center', sortable:false},
    	{name:'order_date_title', width:100, align:'center', sortable:false},
    	{name:'client_name', width:100, align:'center', sortable:false},
    	{name:'order_id', index:'order_id', width:100, align:'center'},
    	{name:'topic', width:100, align:'center', sortable:false},
    	{name:'amount_title', index:'amount', width:100, align:'center'},
    	{name:'date_title', index:'date', width:100, align:'center'},
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#pager_orders_"+type),
    sortname: sort_name,
    sortorder: sort_order,
    onSelectRow: function (row_id) {
    	return false;
      if($("#defender").val() == '0') {
        var rowData = jQuery(this).getRowData(row_id); 
        var id = rowData['payment_id'];
        document.location.href = '/support/payments/payment_details/'+id;
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,
  }).trigger("reloadGrid");
  $("#list_orders_"+type).jqGrid('navGrid','#pager_orders_top_'+type,{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false});
  $("#list_orders_"+type).jqGrid('navGrid','#pager_orders_'+type,{edit:false,add:false,del:false, search:false});   
}
$().ready(function(){
  $("#tabs").tabs();
	build_grid('all');
});	
</script>