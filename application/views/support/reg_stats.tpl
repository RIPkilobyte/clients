<div class="block_container">
	<div class="block_head">Registration statistics</div>
	<div class="block_content fl">
		<table class="reg_stats">
			<tr style="font-weight:bold">
				<td>From</td>
				<td>Count</td>
			</tr>
			<?php foreach ($learns as $key => $val): ?>
				<tr>
					<td><?php echo $key; ?></td>
					<td><?php echo $val; ?></td>
				</tr>
			<?php endforeach;?>
		</table>
	</div>
	
	<div class="block_content fl">
		<table class="reg_stats">
			<tr style="font-weight:bold">
				<td>Country</td>
				<td>Count</td>
			</tr>
			<?php foreach ($countries as $key => $val): ?>
				<?php if($val['count'] > '0'): ?>
					<tr>
						<td><?php echo $val['title']; ?></td>
						<td><?php echo $val['count']; ?></td>
					</tr>
				<?php endif;?>
			<?php endforeach;?>
		</table>
	</div>

	<div class="block_content fl">
		<table class="reg_stats">
			<tr style="font-weight:bold">
				<td>Discipline</td>
				<td>Count</td>
			</tr>
			<?php foreach ($disciplines as $key => $val): ?>
				<?php if($val['count'] > '0'): ?>
					<tr>
						<td><?php echo $val['title']; ?></td>
						<td><?php echo $val['count']; ?></td>
					</tr>
				<?php endif;?>
			<?php endforeach;?>
		</table>
	</div>

</div>
