<script type="text/javascript" src="/js/jquery-min-1.9.0.js"></script>
<script src="/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/js/jquery.form.min.js"></script>
<script src="/js/nicEdit.js" type="text/javascript"></script>
<link href="/css/kendo.common.min.css" rel="stylesheet" />
<link href="/css/kendo.default.min.css" rel="stylesheet" />
<script src="/js/kendo.web.min.js"></script>

<div class="block_container">
	<?php if(isset($order) && count($order)): ?>
	<div class="block_content">

		<table class="table_order_details" style="width:95%">
			<tr>
				<td colspan="3" class="td_order_information">
					<div class="order_messages subblock" style="width:100%">
						<table>
								<tr>
									<td><h3>Order #<?php echo $order['order_id']; ?></h3></td>

									<td><a href="/support/user/profile/<?php echo $order['customer_id']; ?>">Customer #<?php echo $order['customer_id']; ?></a></td>

									<?php if($order['writer_id'] > 0): ?>
										<td><a href="/support/writers/writer_details/<?php echo $order['writer_id']; ?>">Assigned writer #<?php echo $order['writer_id']; ?></a></td>
									<?php else: ?>
										<td>Writer not assigned</td>
									<?php endif; ?>

									<td>
										Full price: $<?php echo round($order['price']+$order['discount_save'], 2); ?>
									</td>
									<td>
										Discount: <?php echo $order['discount']; ?>%
									</td>
									<td>
										<div id="order_price">
											<div id="cover_span_price">
												<span id="text_value_price">Total price: $<?php echo $order['price']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $order['order_id']; ?>', 'price')"><img src="/img/edit_icon.png" height="12px;" /></a>
											</div>
											<div id="cover_input_price" style="display:none;">
												<input type="text" size="10" id="input_price" value="<?php echo $order['price']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_order_price('<?php echo $order['order_id']; ?>', '<?php echo $order['price']; ?>')"><img src="/img/chbx_check.png" /></a>
											</div>
											<div id="cover_loading_price" style="display:none;">
												<img src="/img/loading.gif" height="20px" />
											</div>
										</div>
									</td>
									<td>
										<div id="writer_price">
											<div id="cover_span_writer_price">
												<span id="text_value_writer_price">Writer price: $<?php echo $order['writer_price']; ?></span> &nbsp; <a href="javascript:void(0)" onclick="replace_to_input('<?php echo $order['order_id']; ?>', 'writer_price')"><img src="/img/edit_icon.png" height="12px;" /></a>
											</div>
											<div id="cover_input_writer_price" style="display:none;">
												<input type="text" size="10" id="input_writer_price" value="<?php echo $order['writer_price']; ?>" /> &nbsp; <a href="javascript:void(0)" onclick="save_order_writer_price('<?php echo $order['order_id']; ?>', '<?php echo $order['writer_price']; ?>')"><img src="/img/chbx_check.png" /></a>
											</div>
											<div id="cover_loading_writer_price" style="display:none;">
												<img src="/img/loading.gif" height="20px" />
											</div>
										</div>
									</td>
								</tr>
						</table>
						<table>
									<tr>
										<td style="padding:0px">
											<div class="order_addon_pad">
											Customer deadline: <input type="text" class="datetimepicker" id="ending_time_client" value="<?php echo $order['date_end']; ?>" />
											<input class="btn_green" type="button" value="OK" id="submit" onClick="change_deadline(<?php echo $order['order_id']; ?>, 'client')" />
											
											<span class="timer_client"></span>
											</div>
										</td>
										<td style="padding:0px">
											<div class="order_addon_pad">
											Writer deadline: <input type="text" class="datetimepicker" id="ending_time_writer" value="<?php echo $order['date_end_writer']; ?>" />
											<input class="btn_green" type="button" value="OK" id="submit_writer" onClick="change_deadline(<?php echo $order['order_id']; ?>, 'writer')" />
											
											<span class="timer_writer"></span>
											</div>
										</td>
									</tr>
									<tr>
										<td><div class="error" id="deadline_result"></div></td>
									</tr>
									<tr>
										<td style="padding:0px">
											<div class="order_addon_pad">
												<table>
													<tr>

													<td id="button_attention">
														<?php if($order['attention']): ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'attention', '0')" class="pointer" title="Remove attention from order"><img src="/img/icon/alert_red.png" height="31px" /></a>
														<?php else: ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'attention', '1')" class="pointer" title="Set attention to order"><img src="/img/icon/alert_gray.png" height="31px" /></a>
														<?php endif; ?>
													</td>

													<td id="button_message">
														<?php if($order['message']): ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'message', '0')" class="pointer" title="Remove message attention from order"><img src="/img/icon/message_red.png" height="31px" /></a>
														<?php else: ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'message', '1')" class="pointer" title="Set message attention to order"><img src="/img/icon/message_gray.png" height="31px" /></a>
														<?php endif; ?>
													</td>

													<td id="button_file">
														<?php if($order['file']): ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'file', '0')" class="pointer" title="Remove file attention from order"><img src="/img/icon/files_red.png" height="31px" /></a>
														<?php else: ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'file', '1')" class="pointer" title="Set file attention to order"><img src="/img/icon/files_gray.png" height="31px" /></a>
														<?php endif; ?>
													</td>
													
													<td id="report_button">
														<?php if($order['status'] == 'Completed' && $order['report_sent'] == '0'): ?>
															<a onclick="sent_report(<?php echo $order['order_id']; ?>)" class="pointer" title="Report was sent"><img src="/img/icon/report_red.png" height="31px" /></a>
														<?php elseif($order['status'] != 'Completed'): ?>
															<img src="/img/icon/report_gray.png" height="31px" />
														<?php else: ?>
															<img src="/img/icon/report_gray.png" class="pointer" title="Report has been sent" height="31px" />
														<?php endif; ?>
													</td>
													<td style="vertical-align:middle">
														<?php if($order['writer_id'] > 0): ?>
															<img src="/img/icon/user.png" title="Assigned Writer #<?php echo $order['writer_id']; ?>" height="21px" /> <?php echo $order['writer_id']; ?>
														<?php else: ?>
															<img src="/img/icon/user.png" title="Writer not assigned" height="21px" /> 0
														<?php endif; ?>
													</td>
													<td id="button_payment">
														<?php if($order['payment']): ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'payment', '0')" class="pointer" title="Remove complete payment"><img src="/img/icon/pay_blue.png" height="35px" /></a>
														<?php else: ?>
															<a onclick="reset_notify(<?php echo $order['order_id']; ?>, 'payment', '1')" class="pointer" title="Set complete payment"><img src="/img/icon/pay_white.png" height="35px" /></a>
														<?php endif; ?>
													</td>
													<td>
														Revisions:<br />
														<input type="text" id="revisions" size="3" value="<?php echo $order['revisions']; ?>" />
													</td>
												</tr>
												</table>
											</div>
										</td>
										<td style="padding:0px">
											<div class="order_addon_pad">
												STATUS:
												<select id="order_status" onchange="change_order_status(<?php echo $order['order_id']; ?>)">
													<?php foreach($order_statuses as $k=>$v): ?>
														<?php
															$selected = '';
															if($v['order_status_id'] == $order_current_status)
																$selected = 'selected="selected"';
														?>
														<?php if($v['order_status_id'] > 1): ?>
														<option <?php echo $selected; ?> value="<?php echo $v['order_status_id']; ?>"><?php echo $v['status']; ?> / <?php echo $v['status_client']; ?> / <?php echo $v['status_writer']; ?></option>
														<?php else: ?>
															<option <?php echo $selected; ?> value="<?php echo $v['order_status_id']; ?>"><?php echo $v['status']; ?> / <?php echo $v['status_client']; ?></option>
														<?php endif;?>
													<?php endforeach; ?>
												</select>												
											</div>
											<?php if($order['rating']): ?>
												<div class="order_addon_pad">
													FEEDBACK: 
													<?php
													switch ($order['rating_writer']) {
														case '-10':
															$img = 'angry.png';
														break;
														case '0':
															$img = 'neutral.png';
														break;
														case '10':
															$img = 'great.png';
														break;
														default:
														break;
													}
													?>
													Writer's work: <img width="20px" src="/img/icon/<?php echo $img; ?>" />
													<?php
													switch ($order['rating_support']) {
														case '-10':
															$img = 'angry.png';
														break;
														case '0':
															$img = 'neutral.png';
														break;
														case '10':
															$img = 'great.png';
														break;
														default:
														break;
													}
													?>
													Support work: <img width="20px" src="/img/icon/<?php echo $img; ?>" />
													<textarea cols="20" rows="5"><?php echo $order['rating_text']; ?></textarea>
												</div>
											<?php endif; ?>
										</td>
									</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>


		<table class="table_order_details" style="width:95%">
			<tr>
				<td colspan="3" class="td_order_information">
					<div class="order_messages subblock">
						<h3>CLIENT’S BRIEF</h3>
						<table>
							<tr>
								<th>Pages</th>
								<th>Slides</th>
								<th>Academic level</th>
								<th>Deadline</th>
								<th>Type of work</th>
								<th>Type of paper</th>
								<th>Subject</th>
								
							</tr>
							<tr>
								<td><input type="text" size="3" name="pages" id="pages" value="<?php echo $order['pages']; ?>" /></td>
								<td><input type="text" size="3" name="slides" id="slides" value="<?php echo $order['slides']; ?>" /></td>
								<td>
								<select name="academic_level" id="academic_level">
								<?php
								foreach ($academic_levels as $academic_level) {
									$selected = '';
									if($academic_level['academic_level_id'] == $order['academic_level'])
										$selected = ' SELECTED';
									echo '<option'.$selected.' value="'.$academic_level['academic_level_id'].'">'.$academic_level['title'].'</option>';
								}
								?>
								</select>
								</td>
								<td>
								<select name="deadline" id="deadline">
								<?php
								foreach ($deadlines as $deadline) {
									$selected = '';
									if($deadline['deadline_id'] == $order['deadline'])
										$selected = ' SELECTED';
									echo '<option'.$selected.' value="'.$deadline['deadline_id'].'">'.$deadline['title'].'</option>';
								}
								?>
								</select>								
								</td>
								<td>
								<select name="work_type" id="work_type">
									<option <?php if($order['work_type'] == 'new') echo 'SELECTED'; ?> value="new">Writing from scratch</option>
									<option <?php if($order['work_type'] == 'edit') echo 'SELECTED'; ?> value="edit">Editing/proofreading</option>
								</select>
								</td>
								<td>
								<select name="paper_type" id="paper_type">
								<?php
								foreach ($paper_types as $paper_type) {
									$selected = '';
									if($paper_type['paper_type_id'] == $order['paper_type'])
										$selected = ' SELECTED';
									echo '<option'.$selected.' value="'.$paper_type['paper_type_id'].'">'.$paper_type['title'].'</option>';
								}
								?>
								</select>								
								</td>
								<td>
								<select name="subject" id="subject">
								<?php
								foreach ($subjects as $subject) {
									$selected = '';
									if($subject['subject_id'] == $order['subject'])
										$selected = ' SELECTED';
									echo '<option'.$selected.' value="'.$subject['subject_id'].'">'.$subject['title'].'</option>';
								}
								?>
								</select>
								</td>								

							</tr>
							<tr>
								<th>Plagiarism report</th>
								<th>Abstract page</th>
								<th>Referencing style</th>
								<th>Sources</th>
								<th colspan="2">Topic</th>								
							</tr>
							<tr>
								<td>
								<?php
								if($order['plagiarism_report'])
									echo 'Yes';
								else
									echo 'No';
								?>
								</td>
								<td>
								<?php
								if($order['abstract_page'])
									echo 'Yes';
								else
									echo 'No';
								?>
								</td>
								<td>
								<select name="paper_format" id="paper_format">
								<?php
								foreach ($paper_formats as $paper_format) {
									$selected = '';
									if($paper_format['paper_format_id'] == $order['paper_format'])
										$selected = ' SELECTED';
									echo '<option'.$selected.' value="'.$paper_format['paper_format_id'].'">'.$paper_format['title'].'</option>';
								}
								?>
								</select>
								</td>
								<td><input type="text" size="3" name="sources" id="sources" value="<?php echo $order['sources']; ?>" /></td>
								<td colspan="3"><input style="width:80%" type="text" name="topic" id="topic" value="<?php echo $order['topic']; ?>" /></td>
							</tr>
						</table>
						<h3>Paper details</h3>
						<div class="" style="margin-left:15px;">
							<textarea style="width:95%;height:150px;" id="paper_details"><?php echo $order['paper_details']; ?></textarea>
						</div>
						<div class="paper_details">
							<input type="button" class="btn_green" value="Save changes" onclick="nicEditors.findEditor('paper_details').saveContent();save_order_details(<?php echo $order['order_id']; ?>)"/>
						</div>

						<h3>Note</h3>
						<div class="" style="margin-left:15px;">
							<textarea style="width:95%;height:150px;" id="note"><?php echo $order['note']; ?></textarea>
						</div>
						<div class="paper_details">
							<input type="button" class="btn_green" value="Save note" onclick="nicEditors.findEditor('note').saveContent();save_note(<?php echo $order['order_id']; ?>)"/><span id="note_result"></span>
						</div>
					</div>
				</td>
			</tr>
		</table>

		<table class="table_order_details" style="width:95%">
			<tr>
				<td colspan="3" class="td_order_information">
					<div class="order_messages subblock" style="width:100%">
						<h3>Payments</h3>
						<table>
							<tr style="vertical-align:middle">
								<?php if($payment): ?>
									<td>Registration: </td>
									<td><?php echo date("M j H:i", strtotime($order['date_add'])); ?></td>
									<td>Payment made: </td>
									<td><?php echo date("M j H:i", strtotime($payment['date'])); ?></td>
									<td>Payment type: </td>
									<td><?php echo $payment['system']; ?></td>
									<td>Amount received: </td>
									<td>$<?php echo $payment['amount']; ?></td>
									<td><input type="button" class="btn_green" value="Cancel this order" onclick="cancel_order(<?php echo $order['order_id']; ?>)"/></td>
								<?php else: ?>
									<td>User has not yet made the payment</td>
								<?php endif; ?>								
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
			<table class="table_order_details" style="width:95%">
				<tr>
					<td colspan="3" class="td_order_information">
						<div class="order_messages subblock" style="width:100%">
							<h3>Requests for this order</h3>
							<form id="request_form">
							<table>
								<?php if(isset($requests) && $requests): ?>
									<tr>
										<td width="5%"></td>
										<td>Writer</td>
										<td>Reason</td>
										<td>Price</td>
									</tr>
									
									<?php foreach ($requests as $request): ?>
										<tr>
										<?php if($request['approved']): ?>
											<td><input type="radio" name="user_request" checked="checked"value="<?php echo $request['user_id']; ?>" /></td>
										<?php else: ?>
											<td><input type="radio" name="user_request" value="<?php echo $request['user_id']; ?>" /></td>
										<?php endif; ?>
										<?php if($order['writer_id'] > 0 && $order['writer_id'] == $request['user_id']): ?>
											<td><a href="/support/writers/writer_details/<?php echo $request['user_id']; ?>">Writer #<?php echo $request['user_id']; ?></a> already assigned</td>
										<?php elseif($order['writer_id'] == 0 && $request['approved']): ?>
											<td><a href="/support/writers/writer_details/<?php echo $request['user_id']; ?>">Writer #<?php echo $request['user_id']; ?></a> already assigned</td>
										<?php else: ?>
											<td><a href="/support/writers/writer_details/<?php echo $request['user_id']; ?>">Writer #<?php echo $request['user_id']; ?></a></td>
										<?php endif; ?>
											<td><?php echo $request['text']; ?></td>
											<td>$<?php echo $request['price']; ?></td>
										</tr>
									<?php endforeach; ?>
									
								<?php else: ?>
									<tr>
										<td colspan="4">No requests for this order yet</td>
									</tr>
								<?php endif; ?>
							</table>
							<?php if(isset($requests) && $requests): ?>
								<div class="subblock_btn_pad" id="writer_assign_result">
									<input class="btn_green" type="button" value="Assign writer" id="submit_writer" onClick="assign_writer(<?php echo $order['order_id']; ?>)" />
									<?php if($order['type'] == 'inquiry'): ?>
										<input class="btn_green" type="button" value="Send an email" id="submit_writer" onClick="send_inquiry_email(<?php echo $order['order_id']; ?>)" />
									<?php endif; ?>
					<?php if($order['writer_id'] > 0): ?>
								<input type="button" class="btn_green" value="Dismiss the assigned writer from this order" onclick="dismiss_writer(<?php echo $order['order_id']; ?>)"/>
					<?php endif; ?>									
								</div>
							<?php endif; ?>
							</form>
						</div>
					</td>
				</tr>
			</table>
		<table class="table_order_details" style="width:95%">
			<tr>
				<td class="td_order_messages" style="width:33%">
					<div class="order_messages subblock" style="width:90%">
						<h3>Messages/files received from the customer:</h3>

						<div class="subblock_btn_pad">
							<input type="button" class="btn_green" value="Add new message to customer" onclick="add_message_customer()"/>
						</div>
						<div id="new_message_customer" class="order_interface hidden">
							<div class="interface_controls">
								<p><span class="">To: CUSTOMER #<?php echo $order['customer_id']; ?></span></p>
								<input type="hidden" id="recipient_customer" value="<?php echo $order['customer_id']; ?>" />
								<p><span class="">Message:</span></p>
								<textarea style="width:220px;height:100px;" id="message_customer_text"></textarea>
								
								<form method="POST" id="customerform" style="float:none;" enctype="multipart/form-data">
									<p style="margin:10px;"><input type="file" style="float:none;" name="file" id="customerfile" /></p>
								</form>

							</div>
							<div class="interface_buttons" id="message_customer_result">
								<input class="btn_gray" type="button" value="Cancel" onClick="cancel_message_customer()" />
								<input class="btn_green" type="button" value="Submit" onClick="nicEditors.findEditor('message_customer_text').saveContent();submit_message_customer(<?php echo $order['order_id']; ?>, <?php echo $_SESSION['user_id']; ?>)" />
							</div>
						</div>

						<?php foreach ($from_customer as $record): ?>
							<?php if(isset($record['message_id'])): ?>
								<?php
								echo '<div class="order_addon_pad message">';
									echo '<div class="message_title">';
										echo '<span class="message_sender">';
										echo $record['from'];
										echo '</span> : ';
										echo '<span class="message_receiver">';
										echo $record['to'];
										echo '</span><br />';
										echo '<span class="message_date">';
										echo $record['date'];
										echo '</span>';
										if(!$record['moderated']) {
											echo '<div class="subblock_btn_pad" id="buttons_'.$record['message_id'].'">';
											echo '<input type="button" class="btn_green" value="Approve" onclick="approve_message('.$record['message_id'].')" />';
											echo '</div>';
										}
									echo '</div>';
									echo '<div class="message_text" id="message_'.$record['message_id'].'">';
									if($record['moderated'])
										echo $record['text'];
									else {
										echo '<textarea style="width:100%" id="text_'.$record['message_id'].'">';
										echo $record['text'];
										echo '</textarea>';
									}
							if(isset($record['file']) && $record['file']) {
								echo '<br /><div class="file_link" onClick="get_file('.$record['file']['file_id'].')">';
								if(isset($record['file']['image']) && $record['file']['image'])
									echo '<img src="/img/icon/'.$record['file']['image'].'" height="30px" />';
								echo $record['file']['client_name'];
								echo '</div>';
							}									
									echo '</div>';
								echo '</div>';
								?>
							<?php elseif(isset($record['file_id'])): ?>
								<?php
								echo '<div class="order_addon_pad file">';
									echo '<div class="file_title">';
										echo '<span class="file_sender">';
										echo $record['from'];
										echo '</span> : ';
										echo '<span class="file_receiver">';
										echo $record['to'];
										echo '</span><br />';
										echo '<span class="file_date">';
										echo $record['date'];
										echo '</span>';
										if(!$record['moderated']) {
											echo '<div class="subblock_btn_pad" id="buttons_'.$record['file_id'].'">';
											echo '<input class="btn_green" type="button" value="Approve" onclick="approve_file('.$record['file_id'].')" />';
											echo '<input class="btn_gray" type="button" value="Reject" onclick="reject_file('.$record['file_id'].')" />';
											echo '</div>';
										}
									echo '</div>';
									echo '<div class="file_link" onClick="get_file('.$record['file_id'].')">';
									if(isset($record['image']) && $record['image'])
										echo '<img src="/img/icon/'.$record['image'].'" height="20px" />';									
									echo $record['client_name'];
									echo '</div>';
								echo '</div>';
								?>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</td>
				<td class="td_order_messages" style="width:33%;padding-left:10px!important;padding-right:10px!important;">
					<div class="order_messages subblock" style="width:97%">
						<h3>Messages/files received from the writer:</h3>
						<div class="subblock_btn_pad">
							<input type="button" class="btn_green" value="Add new message to writer" onclick="add_message_writer()"/>
						</div>
						<div id="new_message_writer" class="order_interface hidden">
							<div class="interface_controls">
								<p><span class="">To: WRITER #<?php echo $order['writer_id']; ?></span></p>
								<input type="hidden" id="recipient_writer" value="<?php echo $order['writer_id']; ?>" />
								<p><span class="">Message:</span></p>
								<textarea style="width:220px;height:100px;" id="message_writer_text"></textarea>

								<form method="POST" id="writerform" style="float:none;" enctype="multipart/form-data">
									<p style="margin:10px;"><input type="file" style="float:none;" name="file" id="writerfile" /></p>
								</form>

								<div  style="color:red;"></div>
							</div>
							<div class="interface_buttons" id="message_writer_result">
								<input class="btn_gray" type="button" value="Cancel" id="cancel" onClick="cancel_message_writer()" />
								<input class="btn_green" type="button" value="Submit" id="submit" onClick="nicEditors.findEditor('message_writer_text').saveContent();submit_message_writer(<?php echo $order['order_id']; ?>, <?php echo $_SESSION['user_id']; ?>)" />
							</div>
						</div>
						<?php foreach ($from_writer as $record): ?>
							<?php if(isset($record['message_id'])): ?>
								<?php
								echo '<div class="order_addon_pad message">';
									echo '<div class="message_title">';
										echo '<span class="message_sender">';
										echo $record['from'];
										echo '</span> : ';
										echo '<span class="message_receiver">';
										echo $record['to'];
										echo '</span><br />';
										echo '<span class="message_date">';
										echo $record['date'];
										echo '</span>';
										if(!$record['moderated']) {
											echo '<div class="subblock_btn_pad" id="buttons_'.$record['message_id'].'">';
											echo '<input type="button" class="btn_green" value="Approve" onclick="approve_message('.$record['message_id'].')" />';
											echo '</div>';
										}
									echo '</div>';
									echo '<div class="message_text" id="message_'.$record['message_id'].'">';
									if($record['moderated'])
										echo $record['text'];
									else {
										echo '<textarea style="width:100%" id="text_'.$record['message_id'].'">';
										echo $record['text'];
										echo '</textarea>';
									}
							if(isset($record['file']) && $record['file']) {
								echo '<br /><div class="file_link" onClick="get_file('.$record['file']['file_id'].')">';
								if(isset($record['file']['image']) && $record['file']['image'])
									echo '<img src="/img/icon/'.$record['file']['image'].'" height="30px" />';
								echo $record['file']['client_name'];
								echo '</div>';
							}									
									echo '</div>';
								echo '</div>';
								?>
							<?php elseif(isset($record['file_id'])): ?>
								<?php
								echo '<div class="order_addon_pad file" id="buttons_'.$record['file_id'].'">';
									echo '<div class="file_title">';
										echo '<span class="file_sender">';
										echo $record['from'];
										echo '</span> : ';
										echo '<span class="file_receiver">';
										echo $record['to'];
										echo '</span><br />';
										echo '<span class="file_date">';
										echo $record['date'];
										echo '</span>';
										if($record['type'] == '1'){
											echo '<br /><span class="file_date">';
											echo 'File is a draft';
											echo '</span>';
										}
										elseif($record['type'] == '2'){
											echo '<br /><span class="file_date">';
											echo 'File is final';
											echo '</span>';
										}	
										if(!$record['second'] || $record['moderated'] == '0')
											echo '<div class="subblock_btn_pad">';
										if(!$record['second']) {
											echo '<form method="POST" id="plagiasm_form'.$record['file_id'].'" enctype="multipart/form-data">';
											echo '<input type="file" name="plagiasm'.$record['file_id'].'" id="plagiasm'.$record['file_id'].'" /><br />';
											echo '<input type="button" class="btn_green" value="Upload" onclick="add_second_file('.$record['file_id'].')" /><br />';
											echo '</form>';
										}
										/*
										if($record['exist_preview'])
											echo '<input class="btn_green" type="button" value="Recreate preview" onclick="create_preview('.$record['file_id'].')" /><br />';
										else
											echo '<input class="btn_green" type="button" value="Create preview" onclick="create_preview('.$record['file_id'].')" /><br />';
										//*/
										if($record['moderated'] == '0') {
											echo '<input class="btn_green" type="button" value="Approve" onclick="approve_file('.$record['file_id'].')" />';
											echo '<input class="btn_gray" type="button" value="Reject" onclick="reject_file('.$record['file_id'].')" />';
										}
										if(!$record['second'] || $record['moderated'] == '0')
											echo '</div>';
									echo '</div>';
									echo '<div class="file_link" onClick="get_file('.$record['file_id'].')">';
									if(isset($record['image']) && $record['image'])
										echo '<img src="/img/icon/'.$record['image'].'" height="30px" />';									
									echo $record['client_name'];
									if($record['second'])
										echo ' (2 files)';
									
									if($record['moderated'] == '1') {
										echo ' <b>File approved</b>';
									}
									elseif($record['moderated'] == '2') {
										echo ' <b>File not accepted</b>';
									}
									echo '</div>';
									if($record['exist_preview']) {
										echo '<hr />';
										echo '<div class="file_link" onClick="show_preview('.$record['file_id'].')">';
										echo 'Show preview';
										echo '</div><br />';
										//echo '<input class="btn_green" type="button" value="Recreate preview" onclick="create_preview('.$record['file_id'].')" />';
									}

								echo '</div>';
								?>
							<?php endif; ?>
						<?php endforeach; ?>						
					</div>
				</td>
				<td class="td_order_messages" style="width:33%;padding-right:0px!important">
					<div class="order_messages subblock" style="width:90%;float: right;">
						<h3>Messages/files sent by support team:</h3>
						<?php foreach ($from_support as $record): ?>
							<?php if(isset($record['message_id'])): ?>
								<?php
								echo '<div class="order_addon_pad message">';
									echo '<div class="message_title">';
										echo '<span class="message_sender">';
										echo $record['from'];
										echo '</span> : ';
										echo '<span class="message_receiver">';
										echo $record['to'];
										echo '</span><br />';
										echo '<span class="message_date">';
										echo $record['date'];
										echo '</span>';
									echo '</div>';
									echo '<div class="message_text">';
									echo $record['text'];
									echo '</div>';
								echo '</div>';
								?>
							<?php elseif(isset($record['file_id'])): ?>
								<?php
								echo '<div class="order_addon_pad file">';
									echo '<div class="file_title">';
										echo '<span class="file_sender">';
										echo $record['from'];
										echo '</span> : ';
										echo '<span class="file_receiver">';
										echo $record['to'];
										echo '</span><br />';
										echo '<span class="file_date">';
										echo $record['date'];
										echo '</span>';
									echo '</div>';
									echo '<div class="file_link" onClick="get_file('.$record['file_id'].')">';
									if(isset($record['image']) && $record['image'])
										echo '<img src="/img/icon/'.$record['image'].'" height="20px" />';									
									echo $record['client_name'];
									echo '</div>';
								echo '</div>';
								?>
							<?php endif; ?>
						<?php endforeach; ?>

					</div>
				</td>
			</tr>	
		</table>
	</div>
	<?php else: ?>
		Blank page
	<?php endif; ?>
</div>

<script type="text/javascript">
function convert_datetime(string)
{
	if(string == '')
		var datetime = new Date();
	else if(typeof(string) != 'object') {
		var parts = string.split(' ');
		var date = parts[0].split('-');
		var time = parts[1].split(':');
		var datetime = new Date(date[0], parseInt(date[1])-1, date[2], time[0], time[1], time[2]);
	}
	else {
		var datetime = new Date();
	}
	return datetime;
}			
$().ready(function () {
	$(".datetimepicker").each(function(){
		var value = convert_datetime($(this).val());
		$(this).kendoDateTimePicker({
			format: "yyyy-MM-dd HH:mm:ss",
			timeFormat: "HH:mm",
			value: value
		});
	});
});
$('#plagiasm_form').ajaxForm(); 
$().ready(function(){
	<?php
		echo 'support_timer("client" ,'.$timer['y'].','.($timer['m']-1).','.$timer['d'].','.$timer['H'].','.$timer['i'].','.$timer['s'].');';
		echo 'support_timer("writer", '.$writer_timer['y'].','.($writer_timer['m']-1).','.$writer_timer['d'].','.$writer_timer['H'].','.$writer_timer['i'].','.$writer_timer['s'].');';
	?>	
});
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor();
    myNicEditor.addInstance('message_customer_text');
    myNicEditor.addInstance('message_writer_text');
    myNicEditor.addInstance('paper_details');
    myNicEditor.addInstance('note');
});
</script>
