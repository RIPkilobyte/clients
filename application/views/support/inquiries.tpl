<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<link rel="stylesheet" href="<?=$this -> config -> base_url(); ?>css/ui.jqgrid.css" type="text/css" media="screen">

<div class="block_container">
	<div class="block_head">Inquiries</div>
	<div class="block_content filter_grid">
		<div id="tabs">
			<ul style="display:none;">
				<li><a id="orders_tab_all" href="#all">All</a></li>
			</ul>
			<div id="all">
        <div id="pager_orders_top_all"></div>
				<table id="list_orders_all"></table>
				<div id="pager_orders_all"></div>
			</div>
      <input type="hidden" id="defender" value="0" />
		</div>
	</div>
</div>
<script type="text/javascript">
var per_page = 20;
var row_list = [20,50,100];
var sort_name = "date_add";
var sort_order = "desc";
function build_grid(type)
{
  $("#list_orders_"+type).jqGrid({
    url:'/support/ajax/get_inquiries/'+type,
    datatype: "json",
    mtype: 'POST',
    colNames:['Order id','Topic', 'Subject', 'Pages', 'Slides','Questions','Problems', 'Price', 'Deadline', ''],
    colModel:[
      {name:'order_id', width:80, align:'center'},
      {name:'topic', align:'center', width:350},
      {name:'subject_title', index:'subject', align:'center'},
      {name:'pages', align:'center', width:60},
      {name:'slides', align:'center', width:60},
      {name:'questions', align:'center', width:80},
      {name:'problems', align:'center', width:70},
      {name:'price_title', index:'price', width:70, align:'center'},
      {name:'deadline_title', index:'date_end', width:110, align:'center'},
      {name:'delete_order', align:'center', width:50, sortable:false}
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#pager_orders_"+type),
    sortname: sort_name,
    sortorder: sort_order,
    onSelectRow: function (row_id) {
      if($("#defender").val() == '0') {
        var rowData = jQuery(this).getRowData(row_id); 
        var id = rowData['order_id'];
        document.location.href = '/support/orders/order_details/'+id;
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,
  }).trigger("reloadGrid");
  $("#list_orders_"+type).jqGrid ('setLabel', 'pages', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid ('setLabel', 'slides', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid ('setLabel', 'questions', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid ('setLabel', 'problems', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid('navGrid','#pager_orders_top_'+type,{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false});
  $("#list_orders_"+type).jqGrid('navGrid','#pager_orders_'+type,{edit:false,add:false,del:false, search:false});   
}
$().ready(function(){
  $("#tabs").tabs();
  $("#orders_tab_all").click(function(){
    build_grid('all');
  });
	$("#orders_tab_all").click();
	$(".ui-jqgrid").width( $(".ui-jqgrid-bdiv").width() - 10); 
});	
</script>