<!DOCTYPE html>
<?php 

if (!isset($content))
	$content = '<h2>Пустая страница</h2>';
?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$title ?></title>

    <meta name="description" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_desc']; ?>">
    <meta name="keywords" content="<? if(isset($description)) echo $description; else echo $this->config->config['tp_meta_key']; ?>">
    <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/css/template.css" rel="stylesheet" media="all" />
    <link href="/css/support.css" rel="stylesheet" media="all" />

    <script src="/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript" ></script>
	
    <link href="/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/js/support.js"></script>
    <script type="text/javascript" src="/js/functions.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,700,600' rel='stylesheet' type='text/css'>
	<?php // Подключение скриптов из контроллера
	if(isset($scripts) && !empty($scripts))
		foreach ($scripts as $script)
			echo $script;
	?>
</head>
<body>
	<div id="nav">
		<div class="cont">
			<!--<a href="/"><div class="logo"></div></a>-->
        <div class="menu">
            <ul>
            	<li><a href="/"><span>Home</span></a></li>
				<li><a href="/user/logout"><span>Logout</span></a></li>
            </ul>
        </div>
        <div class="welcome_user">Welcome, <?php echo $_SESSION['userdata']['first_name']; ?></div>
        </div>
    </div>

<div id="wrapper-user">
	<table class="table_wrapper"><tr><td class="td_sidebar">
		<div id="sidebar">
			<div id="orders">
				<div class="writers_title <?php if(isset($active_item) && $active_item == 'orders') echo 'active'; ?>"><a href="/support/orders">Orders</a></div>
			</div>
			<!--
			<div id="requests">
				<div class="writers_title <?php if(isset($active_item) && $active_item == 'requests') echo 'active'; ?>"><a href="/support/requests">Orders requests</a></div>
			</div>
			-->
			<?php if($this->config->item('sitetype') == 'client'): ?>
				<div id="inquiries">
					<div class="writers_title <?php if(isset($active_item) && $active_item == 'inquiries') echo 'active'; ?>"><a href="/support/inquiries">Inquiries</a></div>
				</div>
			<?php endif; ?>
			<!--
			<div id="messages">
				<div class="writers_title <?php if(isset($active_item) && $active_item == 'messages') echo 'active'; ?>"><a href="/support/messages">Messages</a></div>
			</div>
			<div id="files">
				<div class="writers_title <?php if(isset($active_item) && $active_item == 'files') echo 'active'; ?>"><a href="/support/files">Files</a></div>
			</div>
			-->
			<?php if($this->config->item('sitetype') == 'writer'): ?>
				<div id="writers">
					<div class="writers_title <?php if(isset($active_item) && $active_item == 'writers') echo 'active'; ?>"><a href="/support/writers">Writers</a></div>
				</div>
				<div id="news">
					<div class="writers_title <?php if(isset($active_item) && $active_item == 'news') echo 'active'; ?>"><a href="/support/news">News</a></div>
				</div>
				<div id="reg_stats">
					<div class="writers_title <?php if(isset($active_item) && $active_item == 'reg_stats') echo 'active'; ?>"><a href="/support/reg_statistics">Reg. statistics</a></div>
				</div>
			<?php endif; ?>
			<div id="payments">
				<div class="writers_title <?php if(isset($active_item) && $active_item == 'payments') echo 'active'; ?>"><a href="/support/payments">Payment statistics</a></div>
			</div>
			<div id="mails">
				<div class="writers_title <?php if(isset($active_item) && $active_item == 'mails') echo 'active'; ?>"><a href="/support/mails">Mails</a></div>
			</div>
		</div>
	</td><td class="content_user">
		<div id="content-user">
			<?php echo $content; ?>
		</div>
	</td></tr></table>
</div>
</body>
</html>

