<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<link rel="stylesheet" href="/css/ui.jqgrid.css" type="text/css" media="screen">

<div class="block_container">
  <div class="block_head">Sended mails</div>
  <div class="block_content filter_grid">
    <p>
      Site:
      <select id="search_site">
        <option value="">All</option>
        <option value="client">Eagle-essays</option>
        <option value="professor">Professor-essays</option>
        <option value="writer">Writer-center</option>
      </select> &nbsp; 
      Email:
      <input type="text" size="18" value="" id="search_email" /> &nbsp; 
      Title:
      <input type="text" size="18" value="" id="search_title" /> &nbsp; 
      Date from:
      <input type="text" size="18" value="<?php echo date("Y-m-d H:i:s",mktime( 0, 0, 0, date("M")-1, date("d"), date("Y"))); ?>" id="search_date_from" /> &nbsp; 
      Date to:
      <input type="text" size="18" value="<?php echo date("Y-m-d H:i:s"); ?>" id="search_date_to" /> &nbsp; 
    </p>

      <div class="interface_buttons">
        <input class="btn_green" type="button" value="Search" onClick="doSearch()" />
        <input class="btn_gray" type="button" value="Reset" onClick="resetSearch()" />
      </div>
  	<div class="clear"></div>

	</div>
	<div class="block_content">
		<div id="tabs">
	      <div id="all">
	        <div id="pager_orders_top"></div>
	        <table id="list_orders"></table>
	        <div id="pager_orders"></div>
	      </div>
		</div>
    <input type="hidden" id="defender" value="0" />
	</div>
</div>
<script type="text/javascript">
var per_page = 100;
var row_list = [100,200];
var sort_name = "email_id";
var sort_order = "desc";
function resetSearch()
{
  $("#search_id").val('');
  $("#orders_tab_all").click();
}
function doSearch()
{
  build_grid();
}
function build_grid()
{
  $("#search_id").val('');
  $("#list_orders").jqGrid({
    url:'/support/ajax/get_mails/',
    datatype: "json",
    mtype: 'POST',
    colNames:['Site', 'User', 'Email', 'Title', 'Date'],
    colModel:[
    	{name:'site_title', width:100, align:'center', sortable:false},
    	{name:'user_title', index:'user_id', width:100, align:'center', sortable:false},
    	{name:'email_title', width:100, align:'center', sortable:false},
    	{name:'title', width:200, align:'center'},
    	{name:'date_title', index:'date', width:100, align:'center'},
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#pager_orders_top"),
    sortname: sort_name,
    sortorder: sort_order,
    postData: {
      filter: {
        search_site: function() { return $("#search_site").val(); },
        search_email: function() { return $("#search_email").val(); },
        search_title: function() { return $("#search_title").val(); },
        search_date_from: function() { return $("#search_date_from").val(); },
        search_date_to: function() { return $("#search_date_to").val(); }
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,
  }).trigger("reloadGrid");
  $("#list_orders").jqGrid('navGrid','#pager_orders_top',{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false});
  $("#list_orders").jqGrid('navGrid','#pager_orders',{edit:false,add:false,del:false, search:false});   
}
$().ready(function(){
	build_grid();
});	
</script>