<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<link rel="stylesheet" href="/css/ui.jqgrid.css" type="text/css" media="screen">

<div class="block_container">
	<div class="block_head">Orders</div>
  <div class="block_content filter_grid">
    <div>Order ID:</div>
    <div>
      <input type="text" size="18" value="" id="search_id" /> &nbsp; 

      <div class="interface_buttons">
        <input class="btn_green" type="button" value="Search" onClick="doSearch()" />
        <input class="btn_gray" type="button" value="Reset" onClick="resetSearch()" />
      </div>
    </div>
  </div>  
	<div class="block_content">
		<div id="tabs">
			<ul>
        <li><a id="orders_tab_new" href="#new">NEW</a></li>
        <li><a id="orders_tab_progress" href="#progress">IN PROGRESS</a></li>
        <li><a id="orders_tab_attention" href="#attention">ATTENTION</a></li>
        <li><a id="orders_tab_due" href="#due">DUE</a></li>
        <li><a id="orders_tab_completed" href="#completed">COMPLETED</a></li>
        <li><a id="orders_tab_cancelled" href="#cancelled">CANCELLED</a></li>
        <li><a id="orders_tab_all" href="#all">ALL</a></li>
        <!--
        <li><a id="orders_tab_pending" href="#pending">Pending</a></li>
        <li><a id="orders_tab_current" href="#current">Current</a></li>
        <li><a id="orders_tab_finished" href="#finished">Completed</a></li>
        <li><a id="orders_tab_canceled" href="#canceled">Canceled</a></li>
				<li><a id="orders_tab_all" href="#all">All</a></li>
        -->
			</ul>
      <div id="new">
        <div id="pager_orders_top_new"></div>
        <table id="list_orders_new"></table>
        <div id="pager_orders_new"></div>
      </div>
      <div id="progress">
        <div id="pager_orders_top_progress"></div>
        <table id="list_orders_progress"></table>
        <div id="pager_orders_progress"></div>
      </div>
      <div id="attention">
        <div id="pager_orders_top_attention"></div>
        <table id="list_orders_attention"></table>
        <div id="pager_orders_attention"></div>
      </div>
      <div id="due">
        <div id="pager_orders_top_due"></div>
        <table id="list_orders_due"></table>
        <div id="pager_orders_due"></div>
      </div>
      <div id="completed">
        <div id="pager_orders_top_completed"></div>
        <table id="list_orders_completed"></table>
        <div id="pager_orders_completed"></div>
      </div>
      <div id="cancelled">
        <div id="pager_orders_top_cancelled"></div>
        <table id="list_orders_cancelled"></table>
        <div id="pager_orders_cancelled"></div>
      </div>
      <div id="all">
        <div id="pager_orders_top_all"></div>
        <table id="list_orders_all"></table>
        <div id="pager_orders_all"></div>
      </div>
      <!--
      <div id="pending">
        <div id="pager_orders_top_pending"></div>
        <table id="list_orders_pending"></table>
        <div id="pager_orders_pending"></div>
      </div>
      <div id="current">
        <div id="pager_orders_top_current"></div>
        <table id="list_orders_current"></table>
        <div id="pager_orders_current"></div>
      </div>
      <div id="canceled">
        <div id="pager_orders_top_canceled"></div>
        <table id="list_orders_canceled"></table>
        <div id="pager_orders_canceled"></div>
      </div>
      <div id="finished">
        <div id="pager_orders_top_finished"></div>
        <table id="list_orders_finished"></table>
        <div id="pager_orders_finished"></div>
      </div>
			<div id="all">
        <div id="pager_orders_top_all"></div>
				<table id="list_orders_all"></table>
				<div id="pager_orders_all"></div>
			</div>
      -->
		</div>
    <input type="hidden" id="defender" value="0" />
	</div>
</div>
<script type="text/javascript">
var per_page = 100;
var row_list = [100,200];
var sort_name = "order_id";
var sort_order = "desc";
function resetSearch()
{
  $("#search_id").val('');
  $("#orders_tab_all").click();
}
function doSearch()
{
  $("#orders_tab_all").click();
}
$("#orders_tab_new").click(function(){
  build_grid('new');
});
$("#orders_tab_progress").click(function(){
  build_grid('progress');
});
$("#orders_tab_attention").click(function(){
  build_grid('attention');
});
$("#orders_tab_due").click(function(){
  build_grid('due');
});
$("#orders_tab_completed").click(function(){
  build_grid('completed');
});
$("#orders_tab_cancelled").click(function(){
  build_grid('cancelled');
});
$("#orders_tab_all").click(function(){
  build_grid('all');
});
/*
$("#orders_tab_pending").click(function(){
  build_grid('pending');
});
$("#orders_tab_current").click(function(){
  build_grid('current');
});
$("#orders_tab_canceled").click(function(){
  build_grid('canceled');
});
$("#orders_tab_finished").click(function(){
  build_grid('finished');
});
*/
function build_grid(type)
{
  var search_id = $("#search_id").val();
  $("#list_orders_"+type).jqGrid({
    url:'/support/ajax/get_orders/'+type,
    datatype: "json",
    mtype: 'POST',
    colNames:['ACTIONS', 'WRITERS' ,'ORDER ID', 'PAYMENT', 'TOPIC', 'CLIENT DEADLINE', 'TIME LEFT', 'WRITER DEADLINE', 'TIME LEFT', 'NOTE', ''],
    colModel:[
      {name:'last_action_value', index:'last_action', width:160, align:'center'},
      {name:'writer_id', width:80, align:'center'},
      {name:'order_id', width:80, align:'center'},
      {name:'price_title', index:'price', width:70, align:'center'},
      {name:'topic', align:'center', width:150},
      {name:'deadline_title', index:'date_end', width:90, align:'center'},
      {name:'deadline_timeleft', index:'date_end', width:90, align:'center'},
      {name:'writer_deadline_title', index:'date_end_writer', width:90, align:'center'},
      {name:'writer_deadline_timeleft', index:'date_end_writer', width:90, align:'center'},
      {name:'note', align:'center', width:60},
      {name:'delete_order', align:'center', width:20, sortable:false}
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#pager_orders_"+type),
    sortname: sort_name,
    sortorder: sort_order,
    postData: {
      filter: {
        order_id: function() { return $("#search_id").val(); }
      }
    },
    onSelectRow: function (row_id) {
      if($("#defender").val() == '0') {
        //var rowData = jQuery(this).getRowData(row_id); 
        //var id = rowData['order_id'];
        //document.location.href = '/support/orders/order_details/'+id;
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,
  }).trigger("reloadGrid");
  $("#list_orders_"+type).jqGrid ('setLabel', 'pages', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid ('setLabel', 'slides', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid ('setLabel', 'questions', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid ('setLabel', 'problems', '', {'text-align':'left'});
  $("#list_orders_"+type).jqGrid('navGrid','#pager_orders_top_'+type,{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false});
  $("#list_orders_"+type).jqGrid('navGrid','#pager_orders_'+type,{edit:false,add:false,del:false, search:false});   
}
/*
$("#orders_tab_all").click(function(){
  $("#list_orders_all").jqGrid({
    url:'/support/ajax/get_orders/all',
    datatype: "json",
    mtype: 'POST',
    colNames:['ACTIONS', 'WRITERS' ,'ORDER ID', 'PAYMENT', 'TOPIC', 'CLIENT DEADLINE', 'TIME LEFT', 'WRITER DEADLINE', 'TIME LEFT', 'NOTE', ''],
    colModel:[
      {name:'last_action_value', index:'last_action', width:180, align:'center'},
      {name:'writer_id', width:80, align:'center'},
      {name:'order_id', width:80, align:'center'},
      {name:'price_title', index:'price', width:70, align:'center'},
      {name:'topic', align:'center', width:150},
      {name:'deadline_title', index:'date_end', width:90, align:'center'},
      {name:'deadline_timeleft', index:'date_end', width:90, align:'center'},
      {name:'writer_deadline_title', index:'date_end_writer', width:90, align:'center'},
      {name:'writer_deadline_timeleft', index:'date_end_writer', width:90, align:'center'},
      {name:'note', align:'center', width:60, sortable:false},
      {name:'delete_order', align:'center', width:20, sortable:false}
    ],
    rowNum: per_page,
    search:true,
    postData: {
      filter: {
        order_id: function() { return $("#search_id").val(); }
      }
    },
    rowList: row_list,
    viewrecords: true,
    pager: $("#list_orders_all"),
    sortname: sort_name,
    sortorder: sort_order,
    onSelectRow: function (row_id) {
      if($("#defender").val() == '0') {
        var rowData = jQuery(this).getRowData(row_id); 
        var id = rowData['order_id'];
        document.location.href = '/support/orders/order_details/'+id;
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,
  }).trigger("reloadGrid");
  $("#list_orders_all").jqGrid ('setLabel', 'pages', '', {'text-align':'left'});
  $("#list_orders_all").jqGrid ('setLabel', 'slides', '', {'text-align':'left'});
  $("#list_orders_all").jqGrid ('setLabel', 'questions', '', {'text-align':'left'});
  $("#list_orders_all").jqGrid ('setLabel', 'problems', '', {'text-align':'left'});
  $("#list_orders_all").jqGrid('navGrid','#pager_orders_top_all',{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false});;
  $("#list_orders_all").jqGrid('navGrid','#pager_orders_all',{edit:false,add:false,del:false});    
});
*/
$().ready(function(){
  $("#tabs").tabs();
	//$("#orders_tab_all").click();
  $("#orders_tab_attention").click();
});	
</script>