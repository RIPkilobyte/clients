<script type="text/javascript" src="/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/js/grid.locale-en.js"></script>

<link rel="stylesheet" href="/css/ui.jqgrid.css" type="text/css" media="screen">

<div class="block_container">
	<div class="block_head">Writers</div>
  <div class="block_content filter_grid">
    <div>Writer ID:</div>
    <div>
      <input type="text" size="18" value="" id="search_id" /> &nbsp; 

      <div class="interface_buttons">
        <input class="btn_green" type="button" value="Search" onClick="doSearch()" />
        <input class="btn_gray" type="button" value="Reset" onClick="resetSearch()" /><br />
      </div>
      <div style="cursor:pointer;" class="fr" onClick="export_excel('writers', $('#active_tab').val())"><img src="/img/xls.png" height="48px" title="Export to Excel" /></div>
      <div class="clear"></div>
    </div>
  </div>
	<div class="block_content">
		<div id="tabs">
			<ul>
        <li><a id="writers_tab_new" href="#new">New</a></li>
        <li><a id="writers_tab_files" href="#files">Waiting for files</a></li>
        <li><a id="writers_tab_files_complete" href="#files_complete">File Uploaded</a></li>
        <li><a id="writers_tab_test" href="#test">Testing</a></li>
        <li><a id="writers_tab_test_complete" href="#test_complete">Test Completed</a></li>
        <li><a id="writers_tab_approved" href="#approved">Approved</a></li>
        <li><a id="writers_tab_rejected" href="#rejected">Rejected</a></li>
        <li><a id="writers_tab_all" href="#all">All</a></li>
			</ul>
      <div id="new">
        <div id="pager_writers_top_new"></div>
        <table id="list_writers_new"></table>
        <div id="pager_writers_new"></div>
      </div>
      <div id="files">
        <div id="pager_writers_top_files"></div>
        <table id="list_writers_files"></table>
        <div id="pager_writers_files"></div>
      </div>
      <div id="files_complete">
        <div id="pager_writers_top_files_complete"></div>
        <table id="list_writers_files_complete"></table>
        <div id="pager_writers_files_complete"></div>
      </div>
      <div id="test">
        <div id="pager_writers_top_test"></div>
        <table id="list_writers_test"></table>
        <div id="pager_writers_test"></div>
      </div>
      <div id="test_complete">
        <div id="pager_writers_top_test_complete"></div>
        <table id="list_writers_test_complete"></table>
        <div id="pager_writers_test_complete"></div>
      </div>
      <div id="approved">
        <div id="pager_writers_top_approved"></div>
        <table id="list_writers_approved"></table>
        <div id="pager_writers_approved"></div>
      </div>
      <div id="rejected">
        <div id="pager_writers_top_rejected"></div>
        <table id="list_writers_rejected"></table>
        <div id="pager_writers_rejected"></div>
      </div>      
			<div id="all">
        <div id="pager_writers_top_all"></div>
				<table id="list_writers_all"></table>
				<div id="pager_writers_all"></div>
			</div>
		</div>
    <input type="hidden" id="defender" value="0" />
    <input type="hidden" id="active_tab" value="all" />
	</div>
</div>
<script type="text/javascript">
var per_page = 20;
var row_list = [20,50,100];
var sort_name = "date_add";
var sort_order = "desc";
function export_excel(type, action)
{
  location.assign("http://"+window.location.hostname+"/support/ajax/export_xls/"+action);
}
function resetSearch()
{
  $("#search_id").val('');
  $("#writers_tab_all").click();
}
function doSearch()
{
  $("#writers_tab_all").click();
}
function build_grid(type)
{
  $("#search_id").val('');
  $("#active_tab").val(type);
  $("#list_writers_"+type).jqGrid({
    url:'/support/ajax/get_writers/'+type,
    datatype: "json",
    mtype: 'POST',
    colNames:['Date', 'Writer id', 'Status', 'First name', 'Last name', 'Email', 'Country', 'Disciplines', 'Comments', ''],
    colModel:[
      {name:'date_add', index:'date_add', width:120, align:'left'},
      {name:'user_id', index:'user_id', width:70, align:'center'},
      {name:'step', index:'step', align:'center', sortable:false},
      {name:'first_name', width:100, align:'center'},
      {name:'last_name', width:100, align:'center'},
      {name:'email', align:'center'},
      {name:'country_title', index:'country', width:80, align:'center'},
      {name:'disciplines', align:'center', width:200, sortable:false},
      {name:'comment', align:'center', width:60, sortable:false},
      {name:'delete_user', align:'center', width:50, sortable:false}
    ],
    rowNum: per_page,
    rowList: row_list,
    viewrecords: true,
    pager: $("#list_writers_"+type),
    sortname: sort_name,
    sortorder: sort_order,
    onSelectRow: function (row_id) {
      if($("#defender").val() == '0') {
        var rowData = jQuery(this).getRowData(row_id); 
        var id = rowData['user_id'];
        document.location.href = '/support/writers/writer_details/'+id;
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,
  }).trigger("reloadGrid").jqGrid('navGrid','#pager_writers_top_'+type,{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false});
  $("#list_writers_"+type).jqGrid('navGrid','#pager_writers_'+type,{edit:false,add:false,del:false, search:false});   
}
$("#writers_tab_new").click(function(){
  build_grid('new');
}); 
$("#writers_tab_files").click(function(){
  build_grid('files');
}); 
$("#writers_tab_files_complete").click(function(){
  build_grid('files_complete');
}); 
$("#writers_tab_test").click(function(){
  build_grid('test');
}); 
$("#writers_tab_test_complete").click(function(){
  build_grid('test_complete');
}); 
$("#writers_tab_approved").click(function(){
  build_grid('approved');
}); 
$("#writers_tab_rejected").click(function(){
  build_grid('rejected');
});
$("#writers_tab_all").click(function(){
  $("#active_tab").val('all');
  $("#list_writers_all").jqGrid({
    url:'/support/ajax/get_writers/all',
    datatype: "json",
    mtype: 'POST',
    colNames:['Date', 'Writer id', 'Status', 'First name', 'Last name', 'Email', 'Country', 'Disciplines', 'Comments', ''],
    colModel:[
      {name:'date_add', index:'date_add', width:120, align:'left'},
      {name:'user_id', index:'user_id', width:70, align:'center'},
      {name:'step', index:'step', align:'center', sortable:false},
      {name:'first_name', width:100, align:'center'},
      {name:'last_name', width:100, align:'center'},
      {name:'email', align:'center'},
      {name:'country_title', index:'country', width:80, align:'center'},
      {name:'disciplines', align:'center', width:200, sortable:false},
      {name:'comment', align:'center', width:60, sortable:false},
      {name:'delete_user', align:'center', width:50, sortable:false}
    ],
    rowNum: per_page,
    search:true,
    postData: {
      filter: {
        user_id: function() { return $("#search_id").val(); }
      }
    },
    rowList: row_list,
    viewrecords: true,
    pager: $("#list_writers_all"),
    sortname: sort_name,
    sortorder: sort_order,
    autowidth: true,
    onSelectRow: function (row_id) {
      if($("#defender").val() == '0') {
        var rowData = jQuery(this).getRowData(row_id); 
        var id = rowData['user_id'];
        document.location.href = '/support/writers/writer_details/'+id;
      }
    },
    autowidth: true,
    height: "auto",
    toppager: true,   
  }).trigger("reloadGrid");
  $("#list_writers_all").jqGrid('navGrid','#pager_writers_top_all',{cloneToTop:true,edit:false,add:false,del:false,search:false, refresh:false})
  $("#list_writers_all").jqGrid('navGrid','#pager_writers_all',{edit:false,add:false,del:false});
}); 
$().ready(function(){
  $("#tabs").tabs();
  $("#writers_tab_all").click();
  $("#list_writers_all").css('width', '1000');
}); 
</script>  