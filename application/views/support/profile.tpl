<div class="block_container">
	<?php if($user): ?>
		<div class="block_head"><span>Profile user #<?php echo $user['user_id']; ?></div>
		<div class="block_content">
			<table class="table_bordered">
				<tr>
					<td width="10%">User status</td>
					<td><?php echo $user['role']; ?></td>
				</tr>			
				<tr>
					<td>User ID</td>
					<td><?php echo $user['user_id']; ?></td>
				</tr>
				<tr>
					<td>First name</td>
					<td><?php echo $user['first_name']; ?></td>
				</tr>
				<tr>
					<td>Last name</td>
					<td><?php echo $user['last_name']; ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?php echo $user['email']; ?></td>
				</tr>
				<tr>
					<td>Country</td>
					<td><?php echo $user['country_title']; ?></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td>+<?php echo $user['phone_prefix']; ?> <?php echo $user['phone']; ?></td>
				</tr>
				<?php if(isset($user['meta']) && $user['meta']): ?>
					<tr>
						<td>City</td>
						<td><?php echo $user['meta']['city']; ?></td>
					</tr>
					<tr>
						<td>Gender</td>
						<td>
						<?php
							if($user['meta']['gender'] == '0')
								echo 'Female';
							elseif($user['meta']['gender'] == '1')
								echo 'Male';
							else
								echo '---';
						?>
						</td>
					</tr>
					<tr>
						<td>Avalible 24/7</td>
						<td>
						<?php
							if($user['meta']['available247'] > '0')
								echo 'Yes';
							else
								echo 'No';
						?>
						</td>
					</tr>
					<tr>
						<td>Academic level</td>
						<td><?php echo $user['meta']['academic_level_title']; ?></td>
					</tr>
					<tr>
						<td>Language</td>
						<td><?php echo $user['meta']['language']; ?></td>
					</tr>
					<tr>
						<td>Disciplines</td>
						<td><?php echo $user['meta']['disciplines_title']; ?></td>
					</tr>
					<tr>
						<td>Paper format</td>
						<td><?php echo $user['meta']['paper_format_title']; ?></td>
					</tr>

				<?php endif; ?>
					<tr>
						<td>Loyalty discount</td>
						<td><?php echo $user['user_id']; ?>01</td>
					</tr>
					<tr>
						<td>Referral code</td>
						<td><?php echo $user['user_id']; ?>02</td>
					</tr>
			</table>	
		</div>
		<?php if(isset($previous) && $previous): ?>
		<div class="block_content">
			<table class="table_bordered">
				<tr>
					<td><h2>History of orders</h2></td>
				</tr>
				<tr>
					<td><b>Order ID</b></td>
					<td><b>Writer ID</b></td>
					<td><b>Topic</b></td>
					<td><b>Price</b></td>
				</tr>			
			<?php foreach($previous as $order): ?>
				<tr>
					<td><a href="/support/writers/writer_details/<?php echo $order['order_id']; ?>">Order #<?php echo $order['order_id']; ?></a></td>
					<td><a href="/support/writers/writer_details/<?php echo $order['writer_id']; ?>">Writer #<?php echo $order['writer_id']; ?></a></td>
					<td><?php echo $order['topic']; ?></td>
					<td>$<?php echo $order['price']; ?></td>
				</tr>
			<?php endforeach; ?>
			</table>
		</div>
		<?php endif; ?>
	<?php endif; ?>
</div>