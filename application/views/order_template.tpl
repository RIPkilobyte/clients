<!doctype html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <title><?php echo $title; ?></title>
  <meta name="description" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_desc']; ?>">
  <meta name="keywords" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_key']; ?>">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/css/styles.css">
  <link rel="stylesheet" href="/css/order_styles.css">
  <link rel="stylesheet" href="/css/cusel.css" type="text/css" media="screen">
  <link rel="stylesheet" href="/css/jquery.selectbox.css" media="all" />
  <link rel="shortcut icon" href="/img/favicon.png">
  <link rel="icon" href="/img/favicon.png">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
  <script type="text/javascript" src="/js/order_scripts.js"></script>
  <script src="/js/nicEdit.js" type="text/javascript"></script>
  <script type="text/javascript" src="/js/user.js"></script>
  <script type="text/javascript" src="/js/jquery.numberMask.js"></script>
  <script type="text/javascript" src="/js/nicEdit.js"></script>
  <script type="text/javascript" src="/js/jquery.selectbox-0.2.js"></script>
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>

<script type="text/javascript">
var prices = {
<?php
foreach ($prices as $kt=>$type) {
    echo $kt.':{';
    foreach ($type as $kl => $level) {
        echo $kl.':{';
        foreach ($level as $kd => $dl)
            echo $kd.':'.$dl.',';
        echo '},';
    }
    echo '},';
}
?>
};
var phone_prefix = {};
<?php
    foreach ($countries as $key => $value) {
        echo 'phone_prefix['.$value['country_id'].'] = '.$value['phone_prefix'].';';
    }
?>
</script>
<div class="wrapper">
<div class="site-wrapper_order">
 <div class="header_block">
   <div class="logo_form float-left"><a href="/"></a></div>
   <ul class="nav_steps default_list">
     <li><a id="step_1_link" class="nav_steps_links pointer current" onclick="back_step_one()">1. Paper details</a></li>
     <li><a id="step_2_link" class="nav_steps_links pointer" onclick="go_step_two()">2. Price calculator</a></li>
     <li><a id="step_3_link" class="nav_steps_links pointer" onclick="go_step_three()">3. Contact information</a></li>
   </ul>
       <div class="float-left top_link top_link_h qqq www">
      <p class="pol">NO CHARGEBACK POLICY</p>
      <div class="pol_info">
        <p>As we use an Anti-Fraud Management system, we practice NO CHARGEBACK POLICY against payments that are claimed as “Unauthorized”.</p>
      </div>
    </div>
<div class="clear"></div>

   <div class="tabs_body">
<form method="post" id="order_form" action="/order" enctype="multipart/form-data" autocomplete="off">  
<input type="hidden" value="0" id="full_price" /> 
     <div class="step_1">
       <div class="row_wrap">
         <div class="cell float-left">
           <div class="title_side">Type of paper <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Please select the necessary type of paper</span>
             </span>
           </a></div>
            <select name="paper_type" id="paper_type" class="calc_selector selectbox">
                <option value="0">Choose Type Of Paper</option>
                <?php
                    foreach ($paper_types as $key => $value) {
                $selected = '';
                if(isset($paper_type) && $paper_type == $value['paper_type_id'])
                $selected = ' selected="selected"';
                $class = '';
                if(isset($value['class']) && $value['class'] == 'indent')
                $class = 'class="indent"';
                echo '<option '.$class.' value="'.$value['paper_type_id'].'"'.$selected.'>'.$value['title'].'</option>';
                }
                ?>
            </select>
            <div class="error hidden error_title" id="error_paper_type"></div>
         </div>
               <div class="cell float-left">
           <div class="title_side">Subject <a href="javascript:void(0);">
              <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Choose the subject field of your task</span>
             </span>
           </a></div>
            <select name="subject" id="subject" class="calc_selector selectbox">
                <option value="0">Choose Your Subject</option>
                <?php
                    foreach ($subjects as $key => $value) {
                $selected = '';
                if(isset($subject) && $subject == $value['subject_id'])
                $selected = ' selected="selected"';
                echo '<option value="'.$value['subject_id'].'"'.$selected.'>'.$value['title'].'</option>';
                }
                ?>
            </select>
            <div class="error hidden error_title" id="error_subject"></div>
         </div>

         <div class="cell float-left">
           <div class="title_side">Referencing Style <a href="javascript:void(0);">
              <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Please choose your referencing style that your paper has to be formatted into. If you do not know what style do you need – just choose “GENERAL”.</span>
             </span>
           </a></div>
            <select name="paper_format" id="paper_format" class="selectbox calc_selector">
              <option value="0">Choose Referencing Style</option>
              <?php
              foreach ($paper_formats as $key => $value) {
                $selected = '';
                if(isset($paper_format) && $paper_format == $value['paper_format_id'])
                  $selected = ' selected="selected"';
                echo '<option value="'.$value['paper_format_id'].'"'.$selected.'>'.$value['title'].'</option>';
              }
              ?>
            </select>
            <div class="error hidden error_title" id="error_paper_format"></div>
         </div>

    <div class="cell float-left">
           <div class="title_side">Sources needed <a href="javascript:void(0);" class="inf_popr">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Indicate a number of sources you want your writer to base your paper on. If you require specific sources, please indicate what kind of sources you need in the paper details box below. If you do not have any requirements on a number of sources, just leave this field blank. </span>
             </span>
           </a></div>
          <div class="plusminusblock">
          <div class="minus" id="source_minus">-</div>
          <div class="coint" id="sources_needed_div"><?php if(isset($sources_needed)) echo $sources_needed; else echo '0' ?></div>
          <input type="hidden" name="sources_needed" value="<?php if(isset($sources_needed)) echo $sources_needed; else echo '0' ?>" id="sources_needed" />
          <div class="plus" id="source_plus">+</div>
          </div>
         </div>
       </div>
       <div class="row_wrap row_wrap_last_input">
       <div class="cell_topic">
 <div class="title_side">Topic <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Indicate the topic of your paper</span>
             </span>
           </a></div>
           <input type="text" name="topic" placeholder="Your text..." id="topic" value="<?php if(isset($topic)) echo $topic; ?>" />
           <div class="error hidden error_title" id="error_topic"></div>
           </div> 
         <div class="cell float-left">
           <div class="title_side">Upload a file (max 5MB)<a class="pop_up_top" href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Here  you can upload a document that might be helpful for your writer to use in his/her work. Please attach your document if it is necessary. We accept: Word, PDF, Powerpoint, JPG, or Excel. At this stage you can only upload one document, but you can upload as many documents as you want  when you are already registered. </span>
             </span>
           </a></div>
           <input type="file" name="file" id="file" />
            <div class="note_block">
              You can upload only one document on this stage
            </div>
            <?php
              if(isset($file_error) && $file_error) {
                echo '<div class="error error_title error_title_files">There was a mistake, however all your information is saved but you have to upload your file again and then you can complete the order procedure.</div><br />';
              }
            ?>
         </div>
       </div>
    	<div class="row_wrap row_wrap_last_area">
           <div class="title_side title_side_paper">Paper Details <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">To ensure that the final paper meets your requirements, please give us as detailed instructions as possible</span>
             </span>
           </a></div>
           <div class="text_area_bl">
           <textarea name="paper_details" placeholder="" id="paper_details"><?php if(isset($paper_details)) echo $paper_details; ?></textarea>
           <!--<textarea name="" id="" cols="30" rows="10"></textarea>-->
       </div>
       <div class="clear"></div>
       <div class="error hidden error_title" id="error_paper_details"></div>
</div>
     <div class="bottom_panel">
     <a href="/" class="pointer btn_blue_fr float-left"><span class="back">Cancel</span></a>
     <a onclick="go_step_two()" class="pointer btn_red_fr btn_red_fr_step2 float-right"><span class="next">Go to step II</span></a>
      <?php if(isset($_SESSION['user_id']) && isset($payment_orders) && $payment_orders): ?>

           <div class="discount_bottom">
             <div class="block_text">
               Loyalty discount code: 
               <a href="javascript:void(0);" class="inf_popr">
             <span class="popin_block popin_block_1">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a>
             </div>
             <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
           </div>

           <div class="discount_bottom wi">
             <div class="block_text">
               Referral code: <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>02</span>
               <a href="#">
             <span class="pop_up">
             <span class="bg_popup"></span>
                 <span class="text">
                  We value our customers, and we want them to stay with us for a long time. We also want our customers to introduce us to their friends. So, we give back as much as it is practically possible using our discount & reward mechanism. Currently, there are two types of discounts available:</span>
              </span>
             </a>
             </div>
           </div>

        <?php endif; ?>
       <div class="prie_bottom">

         <div class="block_text hi">
           Total price USD <span class='order_form_price hidden total_price calc_result'>0,00</span>
           <a href="#">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">The payment can be done in any currency as it will be automatically converted into USD.</span>
             </span>
           </a>
         </div>
       </div>
     </div>

     </div><!-- .step_1 -->

     <div class="step_2 hidden">
       <div class="row_wrap">
         
         <div class="cell float-left">
           <div class="title_side">Type of service <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">
               <span class="weight_sent">Writing from scratch  </span> implies that writer will create a plagiarism free original paper for you.<br/> 
                <span class="weight_sent">Editing/Proofreading </span> implies that we look over an existing paper and suggest ways to improve it.</span>
             </span>
           </a></div>
            <select name="work_type" id="work_type" class="input_hints selectbox calc_selector">
                <?php 
                    $checked1 = '';
                    $checked2 = '';
                    if(isset($work_type) && $work_type == 'new')
                        $checked1 = 'selected="selected"';
                    if(isset($work_type) && $work_type == 'edit')
                        $checked2 = 'selected="selected"';
                ?>
                <option value="new" <?php echo $checked1; ?>>Writing from scratch</option>
                <option value="edit" <?php echo $checked2; ?>>Editing/proofreading</option>
            </select>
         </div>


         <div class="cell float-left">
           <div class="title_side">Academic Level <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Select your academic level
             </span>
           </a></div>
                <select name="academic_level" id="academic_level" class="calc_selector selectbox">
                    <option value="0">Choose Academic Level</option>
                    <?php
                        foreach ($academic_levels as $key => $value) {
                            $selected = '';
                            if(isset($academic_level) && $academic_level == $value['academic_level_id'])
                                $selected = ' selected="selected"';
                            echo '<option value="'.$value['academic_level_id'].'"'.$selected.'>'.$value['title'].'</option>';
                        }
                    ?>
                </select>
                <div class="error hidden error_title" id="error_academic_level"></div>
         </div>

		 <div class="cell float-left">
           <div class="title_side">Number of pages <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Please enter a desired number of pages. Should be a full number. Please note that a standard page is 275 words.
               </span>
             </span>
           </a></div>
          <div class="plusminusblock">
          <div class="minus" id="number_of_pages_minus">-</div>
          <div class="coint" id="number_of_pages_div"><?php if(isset($number_of_pages)) echo $number_of_pages; else echo '1' ?></div>
          <input type="hidden" name="number_of_pages" value="<?php if(isset($number_of_pages)) echo $number_of_pages; else echo '1' ?>" id="number_of_pages" />
          <div class="plus" id="number_of_pages_plus">+</div>
          </div>
         </div>


         

         <div class="cell float-left">
           <div class="title_side">Number of slides <a href="javascript:void(0);">
             <span class="pop_up pop_up_tt">
               <span class="bg_popup"></span>
               <span class="text">If you need Power Point Presentation, specify the number of slides you need. 1 slide=50% of the cost per page.
               </span>
             </span>
           </a></div>
          <div class="plusminusblock">
            <div class="minus" id="slides_minus">-</div>
            <div class="coint" id="slides_div"><?php if(isset($slides)) echo $slides; else echo '0' ?></div>
            <input type="hidden" name="slides" value="<?php if(isset($slides)) echo $slides; else echo '0' ?>" id="number_of_slides" />
            <div class="plus" id="slides_plus">+</div>
          </div>
         </div>

       </div>
       <div class="row_wrap">
     <div class="cell float-left">
           <div class="title_side">Deadline <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Please choose your deadline date to receive the assignment. Please make note to give yourself enough time to review the work before handing it in
             </span>
           </a></div>
          <select name="deadline" id="deadline" class="selectbox calc_selector">
          <option value="0">Choose Deadline</option>
          <?php
          foreach ($deadlines as $key => $value) {
          $selected = '';
          if(isset($deadline) && $deadline == $value['deadline_id'])
          $selected = ' selected="selected"';
          echo '<option value="'.$value['deadline_id'].'"'.$selected.'>'.$value['title'].'</option>';
          }
          ?>          
          </select>
          <div class="error hidden error_title" id="error_deadline"></div>
         </div>
<!--
		 <div class="cell float-left">
           <div class="title_side">Deadline2 <a href="#">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Please choose your deadline date to receive the assignment. Please make note to give yourself enough time to review the work before handing it in
             </span>
           </a></div>
          <select name="deadline" id="deadline" class="selectbox" style="width:220px;">
          <option value="0">Choose Deadline</option>
          <?php
          foreach ($deadlines as $key => $value) {
          $selected = '';
          if(isset($deadline) && $deadline == $value['deadline_id'])
          $selected = ' selected="selected"';
          echo '<option value="'.$value['deadline_id'].'"'.$selected.'>'.$value['title'].'</option>';
          }
          ?>          
          </select>
          <div class="error hidden error_title" id="error_deadline"></div>
         </div>
         -->
          <div class="cell float-left">
           <div class="title_side title_side_discont">Discount code <a href="javascript:void(0);">
             <span class="pop_up">
               <span class="bg_popup"></span>
               <span class="text">Please type in your discount code into this box if you have one. If you don’t have one then please leave it blank
             </span>
           </a></div>
           <input type="text" id="discount_code" class="discount_code_inn" name="discount_code" value="<?php if(isset($discount_code)) echo $discount_code; ?>" />
           <input type="hidden" value="0" id="applied_discount" name="applied_discount" />
          <div class="error hidden error_title" id="error_discount_code"><?php echo form_error('discount_code'); ?></div>
         </div>  

         <div class="cell float-left">
           <div class="title_side title_input title_input1">
            <?php
              $checked = '';
              if(isset($abstract_page) && $abstract_page == '1')
                $checked = 'checked="checked"';
            ?>
           <label><input type="checkbox" name="abstract_page" onChange="order_calc_selector()" id="abstract_page" value="1"<?php echo $checked; ?> /> &nbsp; Add an Abstract page to my paper $ 14.99</label> <a href="#">
             <span class="pop_up pop_up_tt">
               <span class="bg_popup"></span>
               <span class="text">Often academic papers must contain an abstract, particularly if the writing consists of more than two pages and APA formatting. Tick this option if you want your writing to include an abstract page.
               </span>
             </span>
           </a></div>
               <div class="title_side title_input ">
            <?php
              $checked = '';
              if(isset($plagiarism_report) && $plagiarism_report == '1')
                $checked = 'checked="checked"';
            ?>
           <label><input type="checkbox" name="plagiarism_report" id="plagiarism_report" value="1"<?php echo $checked; ?> /> &nbsp; I want to receive official Plagiarism report $ 0</label> <a href="#">
             <span class="pop_up pop_up_tt">
               <span class="bg_popup"></span>
               <span class="text">We guarantee that our papers are plagiarism free. 
If you tick this option we will provide you an official third party detailed plagiarism report that will be attached to the final version of your writing. Please note that Proof Reading has no plagiarism report due to the nature of this service.
               </span>
             </span>
           </a></div>
         </div>
       </div>
         <div class="bottom_panel">
         <a onclick="back_step_one()" class="pointer btn_blue_fr btn_blue_fr_step_2 float-left"><span class="back">Back to step I</span></a>
           <a onclick="go_step_three()" class="pointer btn_red_fr btn_red_fr_step2 float-right"><span class="next">Go to step III</span></a>
      <?php if(isset($_SESSION['user_id']) && isset($payment_orders) && $payment_orders): ?>
           <div class="discount_bottom">
             <div class="block_text">
               Loyalty discount code: <a href="javascript:void(0);" class="inf_popr">
             <span class="popin_block popin_block_1">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a>

             </div>
             <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
           </div>
           <div class="discount_bottom">
             <div class="block_text">
               Referral code: <a href="javascript:void(0);" class="inf_popr">
             <span class="popin_block popin_block_1">
             <span class="bg_popbl"></span>
                 <span>
                  We value our customers, and we want them to stay with us for a long time. We also want our customers to introduce us to their friends. So, we give back as much as it is practically possible using our discount & reward mechanism. Currently, there are two types of discounts available:
                 </span>
                 </span>
             </a>

             </div>
             <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>02</span>
           </div>
        <?php endif; ?>
           <div class="prie_bottom">
             <div class="block_text">
               Total price USD <span class='order_form_price hidden total_price calc_result'>0,00</span>
               <a href="#">
                 <span class="pop_up">
                   <span class="bg_popup"></span>
                   <span class="text">The payment can be done in any currency as it will be automatically converted into USD.</span>
                 </span>
               </a>
             </div>
           </div>
         </div>
       </div><!-- .step_2 -->

     <div class="step_3 hidden">
       
        <div class="center_blocks" id="order_list_center">
          <div class="cell_block">
            <div class="services_line">
             <span class="note_block">Type of service:</span>
             <span id="preview_service_type">Writing from scratch</span>
            </div>
            <div class="services_line">
            <span class="note_block">Academic level:</span>
            <span id="preview_academic_level">Academic level</span>
            </div>
            <div class="services_line">
            <span class="note_block">Type of paper:</span>
            <span id="preview_paper_type">Type of paper</span>
            </div>
            <div class="services_line">
            <span class="note_block">Subject:</span>
            <span id="preview_subject">Subject</span>
            </div>
            <div class="services_line">
            <span class="note_block">No. of pages:</span>
            <span id="preview_pages">0</span>
            </div>
            <div class="services_line">
            <span class="note_block">No. of slides:</span>
            <span id="preview_slides">0</span>
            </div>
            <div class="services_line">
            <span class="note_block">Referencing:</span>
            <span id="preview_referencing">Referencing</span>
            </div>
          </div>
          <div class="cell_block">
             <div class="services_line">
             <span class="note_block">No. of sources: </span>
             <span id="preview_sources">0</span>
            </div>
            <div class="services_line">
            <span class="note_block">Abstract page:</span>
            <span id="preview_abstract">No</span>
            </div>
            <div class="services_line">
            <span class="note_block">Plagiarism rep.:</span>
            <span id="preview_plagiarism">No</span>
            </div>
            <div class="services_line">
            <span class="note_block">Paper details:</span>
            <span id="preview_details">Paper details</span>
            </div>
            <div class="services_line">
            <span class="note_block note_attachment">Attachments:</span>
            <span id="preview_attachment">No</span>
            </div>
            <div class="services_line">
            <span class="note_block">Price:</span> <span id="final_price">USD 0</span>
            </div>
            <div class="services_line">
            <span class="note_block">Discount:</span>
            <span id="final_discount">0 % (USD 0)</span>
            </div>
            <div class="services_line">
            <span class="note_block">Discounted price:</span>
            <span id="discounted_price"><span id="discount_price">USD 0</span></span>
            </div>
          </div>
          <div class="services_line">
            <span class="note_block note_block_bottom">Topic:</span>
            <span id="preview_topic">Topic</span>
          </div>
          <div class="services_line">
            <span class="note_block note_block_bottom">Paper details:</span>
            <span id="preview_full_paper_details">Paper details</span>
          </div>
        </div> <!-- .center_blocks -->
        <div class="float-right right_side_step3 right_side ">
        <div class="time_right_wrap_bb">
          <div class="blocks_title">
            Time left:
            <div class="time_left_block error_title">
              <div> 
              <!--время-->
              

              </div>
            </div>
          </div>
          <div class="time_left_bb">
            <div class="balls_row" id="time_left_balls">
              <div class="ball">1</div>
              <div class="ball">17</div>
              <div class="ball">45</div>
              </div>
              <div class="balls_row">
              <div class="ball_tit">Day</div>
              <div class="ball_tit">Hour</div>
              <div class="ball_tit">Min</div>
              </div>
          </div>
        </div>
            <div class="current_bb right_blocks">
                        <div class="blocks_title">
            CURRENT STATUS:
          </div>
           <div class="note_block">
Waiting for payment 
             <a href="" class="inf_popr">
              <span class="pop_up">
              <span class="bg_popup"></span>
                 <span class="text">
                   This is a status of your order at this moment 
                 </span>
               </span>
             </a>
           </div>
            </div>
            <div class="ecpected_time_bb right_blocks">
                        <div class="blocks_title">
            EXPECTED:
          </div>
           <div class="note_block">
           <span id="blue_t_date">Dec 19 08:28 AM</span>
             <a href="" class="inf_popr">
              <span class="pop_up">
              <span class="bg_popup"></span>
                 <span class="text">
                   This is an expected time to deliver a first draft. If the work was under revision, this time indicates when your revised work will be submitted. Please note that substantial revisions might require more time and might affect the expected delivery time. 
                 </span>
               </span>
             </a>
           </div>
            </div>
        </div>
         <!--afasfsafasfasfsafasfsafsaf-->
<div class="clear"></div>
<?php if(!isset($_SESSION['user_id'])): ?>
        <?php 
            $checked1 = '';
            $checked2 = '';
            if(isset($user) && $user == '1')
                $checked1 = 'checked="checked"';
            if(isset($user) && $user == '2')
                $checked2 = 'checked="checked"';
            else
                $checked2 = 'checked="checked"';
        ?>
        <div class="inf_bottom">
<ul class="order-tabs error_title">
    <input type="hidden" name="user" id="user_choise" value="<?php if($checked1) echo '1'; elseif($checked2) echo '2'; ?>" />
    <li class="title_user_select pointer<?php if($checked2) echo ' active';?>" id="title_login"><a onclick="change_user(2)">Sign in</a></li>
    <li class="title_user_select pointer<?php if($checked1) echo ' active';?>" id="title_reg"><a onclick="change_user(1)">Register</a></li>
    <span id="discount_fail"></span>
    <div class="error_title">
              <?php
              if(isset($errors['login_error']))
              echo $errors['login_error'];
              ?>
    </div>
</ul>
        </div>
        
        <fieldset class="hidden" id="client_login">
          <div class="row_wrap">
           <div class="cell float-left">
             <div class="title_side">Email <a href="javascript:void(0);">
               <span class="pop_up">
                 <span class="bg_popup"></span>
                 <span class="text">Please input the email of your existing account
               </span>
             </a></div>
             <input type="text" class="error_bb_input <?php if(!isset($email_login) || form_error('email_login')) echo 'error_bb'; ?>" name="email_login" id="email_login" value="<?php if(isset($email_login)) echo $email_login; ?>" /><br />
            <div class="error_title">
              <?php echo form_error('email_login'); ?>

            </div>
           </div>
         <div class="cell float-left">
             <div class="title_side">Password <a href="#">
               <span class="pop_up">
                 <span class="bg_popup"></span>
                 <span class="text">Please input the password to your existing account
               </span>
             </a></div>
             <input type="password" class="error_bb_input <?php if(!isset($password_login) || form_error('password_login')) echo 'error_bb'; ?>" name="password_login" id="password_login" value="<?php if(isset($password_login)) echo $password_login; ?>" /><br />
            <div class="error_title">
              <?php echo form_error('password_login'); ?>
            </div>
           </div>
           <div class="cell float-left  capcha_wrap">
             <div class="title_side capcha">Please enter Captcha</div>
              <?php
              if(isset($captcha))
                echo $captcha['image'];
              ?>
              <input class="error_bb_input error_bb capcha_input_m" name="captcha[]" type="text" />
             <div class="error_title"><?php echo form_error('captcha'); ?></div>
           </div>           
          </div>

          <div class="row_wrap">
  

         <div class="cell checkbox_agree">
           <div class="error_title">
            <label><input class="error_bb_chbx <?php if(!isset($agree) || form_error('agree')) echo 'error_bb'; ?>" type="checkbox" name="agree" value="1" />&nbsp; I agree with:</label> <a href='/money-back' target='_blank'>Money Back Guarantee</a>, <a href='/privacy-policy' target='_blank'>Privacy Policy</a>,  <a href='/terms-of-use' target='_blank'>Terms of Use</a>.
           </div>
            <div class="error_title">
              <?php echo form_error('agree'); ?>
            </div>
         </div>           
          </div>
        </fieldset>
        <fieldset class="hidden" id="client_create">

          <div class="row_wrap">
            <div class="cell float-left">
              <div class="title_side">Email <a href="javascript:void(0);">
                <span class="pop_up">
                  <span class="bg_popup"></span>
                  <span class="text">Please make sure you have entered the correct email address to receive notifications of order completion status.
                  </span>
              </a></div>
              <input type="text" class="error_bb_input <?php if(!isset($email_reg) || form_error('email_reg')) echo 'error_bb'; ?>" id="email_reg" name="email_reg" value="<?php if(isset($email_reg)) echo $email_reg; ?>" />
              <div class="error_title"><?php echo form_error('email_reg'); ?></div>
            </div>
            <div class="cell float-right">
              <div class="title_side">Password <a href="javascript:void(0);">
                <span class="pop_up">
                  <span class="bg_popup"></span>
                  <span class="text">Your password must contain at least 6 symbols (digits and letters only).
                  </span>
              </a></div>
              <input class="error_bb_input <?php if(!isset($password_reg) || form_error('password_reg')) echo 'error_bb'; ?>" type="password" id="password_reg" name="password_reg" value="<?php if(isset($password_reg)) echo $password_reg; ?>" />
              <div class="error_title"><?php echo form_error('password_reg'); ?></div>
            </div>
          </div>

          <div class="row_wrap">
            <div class="cell float-left">
              <div class="title_side">Confirm email</div>
              <input type="text" class="error_bb_input <?php if(!isset($confirm_email) || form_error('confirm_email')) echo 'error_bb'; ?>" name="confirm_email" value="<?php if(isset($confirm_email)) echo $confirm_email; ?>" />
              <div class="error_title"><?php echo form_error('confirm_email'); ?></div>
            </div>
            <div class="cell float-right">
              <div class="title_side">Confirm password</div>
              <input type="password" class="error_bb_input <?php if(!isset($confirm_password) || form_error('confirm_password')) echo 'error_bb'; ?>" name="confirm_password" value="<?php if(isset($confirm_password)) echo $confirm_password; ?>" />
              <div class="error_title"><?php echo form_error('confirm_password'); ?></div>
            </div>
          </div>

          <div class="row_wrap">
            <div class="cell float-left">
              <div class="title_side">First Name</div>
              <input type="text" class="error_bb_input <?php if(!isset($first_name) || form_error('first_name')) echo 'error_bb'; ?>" name="first_name" maxlength="15" value="<?php if(isset($first_name)) echo $first_name; ?>" />
              <div class="error_title"><?php echo form_error('first_name'); ?></div>
            </div>
            <div class="cell float-right">
              <div class="title_side">Last name</div>
              <input type="text" class="error_bb_input <?php if(!isset($last_name) || form_error('last_name')) echo 'error_bb'; ?>" name="last_name" maxlength="15" value="<?php if(isset($last_name)) echo $last_name; ?>" />
              <div class="error_title"><?php echo form_error('last_name'); ?></div>
            </div>
          </div>

          <div class="row_wrap">
            <div class="cell float-left">
              <div class="title_side">Choose country</div>
                <select name="country" id="country" onChange="change_country()">
                  <option value="">Choose country</option>
                  <?php
                    foreach ($countries as $key => $value) {
                      $selected = '';
                      if(isset($country) && $country == $value['country_id'])
                        $selected = ' selected="selected"';
                      echo '<option value="'.$value['country_id'].'"'.$selected.'>'.$value['title'].'</option>';
                    }
                  ?>
                </select>
              <div class="error_title"><?php echo form_error('country'); ?></div>
            </div>
            <div class="cell float-right">
              <div class="title_side title_side_tell">Phone <a href="javascript:void(0);">
                <span class="pop_up">
                  <span class="bg_popup"></span>
                  <span class="text">Please make sure you have entered the correct phone number, so that we could contact you to check on the details of your order and to verify your payment.
                  </span>
              </a></div>
              <input disabled="disabled" size="2" id="phone_prefix" class="phone_prefix input-mini" type="text" value="" style="width:44px !important;height:20px;vertical-align: top;" />
              <input size="16" class="error_bb_input <?php if(!isset($phone_number) || form_error('phone_number')) echo 'error_bb'; ?>" maxlength="10" id="phone_number" name="phone_number" type="text" value="<?php if(isset($phone_number)) echo $phone_number; ?>" />
              <div class="error_title"><?php echo form_error('phone_number'); ?></div>
            </div>
          </div>

          <div class="row_wrap">
            <div class="cell float-left capchawrap">
              <div class="title_side">Please enter Captcha</div>
              <div class="capca_img">
              <?php
              if(isset($captcha))
                echo $captcha['image'];
              ?>
              </div>
              <input class="error_bb_input error_bb capca_btn" name="captcha[]" type="text" />
              <div class="error_title" style="display:block;"><?php echo form_error('captcha'); ?></div>
            </div>
            <div class="cell float-right">
              <div class="error_title">
                <label><input class="error_bb_chbx <?php if(!isset($agree) || form_error('agree')) echo 'error_bb'; ?>" type="checkbox" name="agree" value="1" />&nbsp; I agree with:</label> <a href='/money-back' target='_blank'>Money Back Guarantee</a>, <a href='/privacy-policy' target='_blank'>Privacy Policy</a>,  <a href='/terms-of-use' target='_blank'>Terms of Use</a>.
              </div>
              <div class="error_title"><?php echo form_error('agree'); ?></div>
            </div>
          </div>

        </fieldset>
<?php else: ?>
<div class="row_wrap">
  <span class="error_title" id="discount_fail"></span>
</div>
<?php endif;?>

         <div class="bottom_panel">
         <a onclick="back_step_two()" class="pointer btn_blue_fr btn_blue_fr_step_2 float-left"><span class="back">Back to step II</span></a>
         <span id="proceed_btn"><a onclick="nicEditors.findEditor('paper_details').saveContent();fsubmitter_form()" class="pointer proceed btn_red_fr float-right ">Proceed</a></span>
         <a onclick="apply_discount()" class="pointer btn_red_fr float-right apply_discount float-right">Apply discount</a>

           <input type="submit" id="submitter_form" style="display:none;" />
      <?php if(isset($_SESSION['user_id']) && isset($payment_orders) && $payment_orders): ?>
           <div class="discount_bottom">
             <div class="block_text">
               Loyalty discount code: <a href="javascript:void(0);" class="inf_popr">
             <span class="popin_block popin_block_1">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a>

             </div>
             <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
           </div>
           <div class="discount_bottom">
             <div class="block_text">
               Referral code: <a href="javascript:void(0);" class="inf_popr">
             <span class="popin_block popin_block_1">
             <span class="bg_popbl"></span>
                 <span>
                  We value our customers, and we want them to stay with us for a long time. We also want our customers to introduce us to their friends. So, we give back as much as it is practically possible using our discount & reward mechanism. Currently, there are two types of discounts available:
                 </span>
                 </span>
             </a>

             </div>
             <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>02</span>
           </div>
        <?php endif; ?>
           <div class="prie_bottom">
             <div class="block_text">
               Total price USD <span class='order_form_price hidden total_price calc_result'>0,00</span>
               <a href="#">
                 <span class="pop_up pop_up_1">
                   <span class="bg_popup"></span>
                   <span class="text">The payment can be done in any currency as it will be automatically converted into USD.</span>
                 </span>
               </a>
             </div>
           </div>
         </div>

       </div><!-- .step_3 -->   
<input type="hidden" name="submit" value="1" />
</form>       
   </div><!-- .tabs_body -->

 </div>
</div> <!-- .site-wrapper -->
</div>
<style type="text/css">
.nicEdit-main[placeholder]:empty:before {
    /*content: attr(placeholder);*/
    color: #9aa1b6; 
}
.nicEdit-main[placeholder]:empty:focus:before {
    content: "";
}
</style>
 <script type="text/javascript">
$().ready(function(){
    //$("*").animate({ scrollTop: $("#order_form").offset().top - 130}, { duration: 'fast', easing: 'swing'});
    change_country();
    <?php if(isset($user) && $user == '1'): ?>
        change_user(1);
    <?php elseif(isset($user) && $user == '2'): ?>
        change_user(2);
    <?php else: ?>
        change_user(2);
    <?php endif; ?>
    $(".selectbox").selectbox({});
});
bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor({maxHeight : 150});
    myNicEditor.addInstance('paper_details');
$(".nicEdit-main").focusout(function(){
        var element = $(this);        
        if (!element.text().replace(" ", "").length) {
            element.empty();
        }
});
$(".nicEdit-main").focusin(function(){
        var element = $(this);        
        if (!element.text().replace(" ", "").length) {
            element.empty();
        }
});
    <?php
    if(!isset($file_error) || !$file_error)
        if(isset($errors) && $errors)
            echo 'after_errors();';
    ?>    
});
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2TzXmB9ZRIjV7KryWVSKLzYNYybsFD54";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>