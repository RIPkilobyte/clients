<!DOCTYPE html>
<?php 

if (!isset($content))
	$content = '<h2>Empty page</h2>';
?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$title ?></title>
    <meta name="description" content="<?=$this->config->config['tp_meta_desc'] ?>">
    <meta name="keywords" content="<?=$this->config->config['tp_meta_key'] ?>">

    <script src="/js/jquery-1.8.3.js" type="text/javascript"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script>

    <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/css/template.css" rel="stylesheet" media="all" />
    <link href="/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,700,600' rel='stylesheet' type='text/css'>
	
    <script type="text/javascript" src="/js/cusel.js"></script>
    <script type="text/javascript" src="/js/cusel_set.js"></script>
    <script type="text/javascript" src="/js/jScrollPane.js"></script>
    <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
 	<link rel="stylesheet" href="/css/cusel.css" type="text/css" media="screen">
 	<script src="/js/functions.js"></script>
 	
	<?php 
	if(!empty($scripts))
		foreach ($scripts as $script)
			echo $script;
	?>
</head>
<body>

    <div id="nav">
        <div class="cont">
            <a href="/"><div class="logo"></div></a>
            <div class="menu">
                
                <div class="toggle">
                    <ul>
                       <!-- <li><a href="/"><span>Home</span></a></li>-->
                        <li class="l1">
                            <a href="/services"><span>Services</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul>
                                    <li><a href="/how-it-works"><span>How it works</span></a></li>
                                    <li><a href="/faq-top-essay"><span>FAQ</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="l2">
                            <a href="/prices"><span>Prices</span></a>

                        </li>
                        <li class="l3">
                            <a href="/about-writing-services"><span>About us</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul>
                                    <li><a href="/contact-us"><span>Contact us</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <!--
                        <li>
                            <a href="/company" onclick="return false;"><span>Company</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul>
                                </ul>
                            </div>
                        </li>
                        -->
                        <li><a href="/testimonials"><span>Testimonials</span></a></li>
                        <li class="l4">
                            <a href="/free-essays"><span>Free Essays</span></a>
                            <div class="visible">
                                <img src="../img/ygol.png" />
                                <ul >
                                    <li><a href="/free-essay-tips"><span>Essay writing tips</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <!--<li><a href="/blog"><span>Blog</span></a></li>-->
                        <?php if(isset($_SESSION['user_id']) && $_SESSION['userdata']['role'] == 'client'): ?>
                            <li><a href="/user"><span><?php echo $_SESSION['userdata']['first_name']; ?></span></a></li>
                        <?php elseif(isset($_SESSION['user_id']) && $_SESSION['userdata']['role'] == 'support'):?>
                            <li><a href="/support"><span>Support</span></a></li>
                        <?php else: ?>
                            <li><a href="/user/login"><span>Sign in</span></a></li>
                        <?php endif;?>                
                    </ul>
                
                <!--
                <div class="chat"><a href="#">Chat now</a>
                </div>
                -->
                </div>

                <div class="buttons">

                    <div class="social"><span class="show-menu" href="#"><!-- --></span></div>
                    <div class="social"><a href="https://www.facebook.com/theeagleessays?ref=hl"><img src="/img/facebook45.png" alt="Eagle-essays.com  Buy  Essay"/></a></div>
                        
  

                    <div class="tell">
                        <a href="tel:+442032868140">
                            <img src="/img/phone45.png" title="+442032868140" alt="+442032868140" style="margin-top: 2px;"/>
                        </a>
                    </div>
                    <div class="skype">
                        <a href="skype:writemypapers?call">
                            <img src="/img/skype45.png" style="margin-top: 2px;" alt="Eagle-essays.com Writing essay"/>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

<div id="wrapper">
	<div id="content" style="margin:0 auto;width: 90%">
		<?php echo $content; ?>
	</div>
</div>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2TzXmB9ZRIjV7KryWVSKLzYNYybsFD54';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
<!-- Google Code for Purchases Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 974970579;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "RlpmCMmS3FYQ073z0AM";
var google_conversion_value = 1.00;
var google_conversion_currency = "GBP";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/974970579/?value=1.00&amp;currency_code=GBP&amp;label=RlpmCMmS3FYQ073z0AM&amp;guid=ON&amp;script=0
"/>
</div>
</noscript>
</body>
</html>

