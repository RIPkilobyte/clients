<script type="text/javascript" src="<?=$this -> config -> base_url(); ?>js/jquery.numberMask.js"></script>
<h3><?=$title?></h3>
<div class="body_content">
	<table>
		<tr>
			<td>
<div id="mycontact" style="position:relative;">
    <div class="form-wrap">
    <form id="mycontact_form" method="POST">
        <table>
            <tr>
                <td>
                	Name <span class="error">*</span><br />
                	<div id="name_error" class="hidden error">Field Name is required</div>
                	<input type="text" id="name" name="name" required="required" />
                </td>
            </tr>

            <tr>
                <td>
                	E-mail <span class="error">*</span><br />
                	<div id="email_error" class="hidden error">Field E-mail is required</div>
                	<input type="email" id="email" name="email" required="required" />
                </td>
            </tr>
            <tr>
                <td>
                	ID number (for registered writers)<br />
                	<input type="text" id="id_number" name="id_number" required="required" />
                </td>
            </tr>      
            <tr>
                <td>
                	Subject <span class="error">*</span><br />
                	<div id="subject_error" class="hidden error">Field Subject is required</div>
                	<select name="subject" id="subject" required="required">
                		<option value="0">Please select</option>
                		<option value="1">Application/Registration Procedure</option>
                		<option value="2">Suggestion/Complaint</option>
                		<option value="3">Technical problem</option>
                		<option value="4">Question regarding payment</option>
                		<option value="5">Issues with a case</option>
                	</select>
                </td>
            </tr>
            <tr id="case_block" class="hidden">
                <td>
                	Case number <span class="error">*</span><br />
                	<div id="case_number_error" class="hidden error">Field Case number is required</div>
                	<input type="text" id="case_number" name="case_number" required="required" />
                </td>
            </tr>            
            <tr>
                <td>
                	Message <span class="error">*</span><br />
                	<div id="message_error" class="hidden error">Field Message is required</div>
                	<textarea name="msg" id="message" required="required"></textarea>
                </td>
            </tr>
            <tr>
            	<td>
            		Captcha  <span class="error">*</span>(please enter the characters you see in this picture)<br />
            		<div id="captcha_error" class="hidden error">Field Captcha is required</div>
            		<input type="text" id="captcha" name="captcha" required="required" /><br />
            		<?php echo $captcha['image']; ?>
            	</td>
            </tr>
            <tr>
                <td>
                	<input type="hidden" value="0" id="error">
                	<input type="button" name="send_data" class="send_data" value="SEND" />
                </td>
            </tr>
        </table>
    </form>
    <img src="../img/home.jpg" style="position: absolute;top: 0px;width: 262px;right: -367px;">
    </div>
    <div class="msg-wrap" style="display:none; padding:10px;">
        <span id="result">
            Your letter is sent and will be read soon
        </span>
    </div>
</div>
    		</td>
    		<td>
			    <?php
			    if(isset($right_block))
			    	echo $right_block;
			    ?>
    		</td>
    	</tr>
    </table>
</div>

<script>
    $(document).ready(function(){
    	$("#id_number").numberMask({beforePoint:10});
    	$("#case_number").numberMask({beforePoint:10});
    	$("#subject").change(function(){
    		var subject = $("#subject").val();
    		if(subject == '5')
    			$("#case_block").removeClass('hidden');
    		else
    			$("#case_block").addClass('hidden');
    	});
        $('.send_data').click(function(){
        	$("#captcha_error").html('Field Captcha is required');
        	$("#name_error").addClass('hidden');
        	$("#email_error").addClass('hidden');
        	$("#subject_error").addClass('hidden');
        	$("#case_number_error").addClass('hidden');
        	$("#message_error").addClass('hidden');
        	$("#captcha_error").addClass('hidden');
        	$("#error").val('0');
        	var name = $("#name").val();
        	var email = $("#email").val();
        	var user_id  = $("#id_number").val();
        	var subject = $("#subject").val();
        	var case_number = $("#case_number").val();
        	var message = $("#message").val();
        	var captcha = $("#captcha").val();
        	if(name == '') {
        		$("#name_error").removeClass('hidden');
        		$("#error").val('1');
        	}
        	if(email == '') {
        		$("#email_error").removeClass('hidden');
        		$("#error").val('1');
        	}
        	if(subject == '0') {
        		$("#subject_error").removeClass('hidden');
        		$("#error").val('1');
        	}
        	if(case_number == '' && subject == '5') {
        		$("#case_number_error").removeClass('hidden');
        		$("#error").val('1');
        	}
        	if(message == '') {
        		$("#message_error").removeClass('hidden');
        		$("#error").val('1');
        	}
        	if(captcha == '') {
        		$("#captcha_error").removeClass('hidden');
        		$("#error").val('1');
        	}
        	var error = $("#error").val();
        	if(error == '0')
        	$.post('/ajax/mycontactsend', {name:name, email:email, user_id:user_id, subject:subject, case_number:case_number, message:message, captcha:captcha}, function(data){
        		if(data.status == 'ok') {
        			$('.form-wrap').hide();
        			$('.msg-wrap').show();
        		}
        		else if(data.status == 'captcha_error') {
        			$("#captcha_error").html('Please enter correct symbols Captcha');
        			$("#captcha_error").removeClass('hidden');
        		}
        		else {
        			$("#result").html('An error occurred. Please try again later');
        			$('.form-wrap').hide();
        			$('.msg-wrap').show();
        		}
        	}, 'json');
        });
    });
</script>