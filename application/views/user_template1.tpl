<!doctype html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <title><?php echo $title; ?></title>
  <meta name="description" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_desc']; ?>">
  <meta name="keywords" content="<?php if(isset($description)) echo $description; else echo $this->config->config['tp_meta_key']; ?>">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
  <link rel="stylesheet" href="/css/styles.css">
  <link rel="shortcut icon" href="/img/favicon.ico">
  <link rel="icon" href="/img/favicon.ico">
  <!--<link rel="stylesheet" href="/css/jquery-ui-1.10.3.custom.min.css" />-->

  <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
  <script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
  <script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="/js/functions.js"></script>
  <script type="text/javascript" src="/js/user.js"></script>
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<div class="site-wrapper">
    <header role="banner" class="header">
      <div class="header_in">
        <div class="site-logo float-left">
          <a href="/">
            <img src="/img/user_logo.png" alt=""/>
          </a>
        </div>
        <div class="n_order float-left">
          <a href="/order">New Order</a>
        </div>
        <div class="logout float-left"><a href="/user/logout">LOgout</a></div>
      </div>
      <div class="user_welc float-right">
        Welcome, <span><?php echo $_SESSION['userdata']['first_name']; ?></span> !
      </div>
    </header><!-- .header-->
    <div class="content_wrap">
        <aside class="site-content-left" role="complementary">
        <div class="left_side">
          <div class="left_in">
         
            
            <?php if(isset($sidebar_orders) && $sidebar_orders): ?>
              <?php foreach($sidebar_orders as $orders): ?>
                <div class="ord_list">
                  <?php
                    /*
                    $class = '';
                    if(isset($orders['unread_messages']) && $orders['unread_messages'])
                      $class = 'message_envelope';
                    if($orders['status_client'] == 'Delivered' || $orders['status_client'] == 'Approved')
                      if(isset($orders['file_id']) && $orders['file_id'])
                        $class = 'file_envelope';
                    //*/
                  ?>
                  <?php if(isset($order) && count($order) && $order['order_id'] == $orders['order_id']): ?>
                    <div class="ord_list_in current_order btn_left_active">
                  <?php else: ?>
                    <div class="ord_list_in">
                  <?php endif; ?>
                    <a href="/user/order_details/<?php echo $orders['order_id']; ?>">Order #<?php echo $orders['order_id']; ?></a>
                  <?php 
                    /*
                    $status = '';
                    if($orders['payment_id']) {
                        switch ($orders['status_client']) {
                            case 'Writer’s Review':
                                $status = 'Work in progress';
                                break;
                            case 'In progress':
                                $status = 'Work in progress';
                                break;
                            case 'Delivered':
                                $status = 'Draft submitted';
                                break;
                            case 'Revision in progress':
                                $status = 'Revision';
                                break;
                            case 'Canceled':
                                $status = 'Canceled';
                                break;
                            case 'Approved':
                                $status = 'Completed';
                                break;
                            default:
                                $status = '';
                                break;
                        }
                    }
                    else {
                        if($orders['status_client'] == 'Canceled')
                            $status = 'Canceled';
                        else
                            $status = 'Waiting for payment';
                    }
                    //*/
                  ?>
                  <span class="active">
                  
                  <?php if(isset($orders['unread_messages']) && $orders['unread_messages']): ?><!-- надо подсветить картинку-->
                    <a id="left_blink1_<?php echo $orders['order_id']; ?>" href="" class="icons_p  blinker" title="You have a <?php echo $orders['unread_messages']; ?> new messages"></a>
                  <?php else: ?><!-- картинка не подсвечивается-->
                    <a id="left_blink1_<?php echo $orders['order_id']; ?>" href="" class="icons_p" title="You have no new messages"></a>
                  <?php endif; ?>

                  
                  <?php if($orders['status'] == 'File delivered' && isset($orders['file_id']) && $orders['file_id']): ?><!-- надо подсветить картинку-->
                    <a id="left_blink2_<?php echo $orders['order_id']; ?>" href="" class="icons_p icons_l  blinker" title="You have a file"></a>
                  <?php else: ?><!-- картинка не подсвечивается-->
                    <a id="left_blink2_<?php echo $orders['order_id']; ?>" href="" class="icons_p icons_l" title="You have no file"></a>
                  <?php endif; ?>

                  <?php if(!$orders['payment_id']): ?><!-- надо подсветить картинку-->
                    <a id="left_blink3_<?php echo $orders['order_id']; ?>" href="" class="icons_p icons_m  blinker" title="Waiting for a payment"></a>
                  <?php else: ?><!-- картинка не подсвечивается-->
                    <a id="left_blink3_<?php echo $orders['order_id']; ?>" href="" class="icons_p icons_m" title="Payment has been already made"></a>
                  <?php endif; ?>

                  </span>
                  </div>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

          </div>
          <div class="prof_block">
            <?php
              $active = '';
              if($_SERVER['REQUEST_URI'] == '/user')
                $active = 'current_order btn_left_active';
            ?>
             <a href="/user" class="<?php echo $active; ?>">Orders</a>
            <?php
              $active = '';
              if($_SERVER['REQUEST_URI'] == '/user/profile')
                $active = 'current_order btn_left_active';
            ?>
              <a href="/user/profile" class="<?php echo $active; ?>">Profile</a>
            <?php
              $active = '';
              if($_SERVER['REQUEST_URI'] == '/discounts')
                $active = 'current_order btn_left_active';
            ?>
             <a href="/discounts" class="<?php echo $active; ?>">Your discounts</a>

          </div>
        </div>
      </aside><!-- .site-content-left -->
<?php 
  if(isset($content) && $content)
    echo $content;
  else
    echo '<div class="site-content-middle" role="main"><div class="site-content-middle-in"></div></div>';
?>

  </div><!-- .content_wrap -->
</div> <!-- .site-wrapper -->
<footer role="contentinfo" class="footer">
  <div class="footer_in">
  </div>
</footer><!-- .footer -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-48444080-1', 'eagle-essays.com');
  ga('send', 'pageview');
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2TzXmB9ZRIjV7KryWVSKLzYNYybsFD54';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
<!-- Google Code for Purchases Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 974970579;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "RlpmCMmS3FYQ073z0AM";
var google_conversion_value = 1.00;
var google_conversion_currency = "GBP";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/974970579/?value=1.00&amp;currency_code=GBP&amp;label=RlpmCMmS3FYQ073z0AM&amp;guid=ON&amp;script=0
"/>
</div>
</noscript>
<script type="text/javascript">
var timeOuts = new Array();
function blink(id)
{
  if(!$('#'+id).is(":hover")) {
  $('#'+id).fadeOut('slow',function() {
    $('#'+id).fadeIn('slow',function() {
      clearTimeout(timeOuts[id]);
      timeOuts[id] = setTimeout('blink("'+id+'")', 700);
    });
  });
  }
}
$().ready(function(){
  $(".blinker").each(function(){
    blink(""+$(this).attr('id')+"");
  });
  
  $(".blinker").hover(
    function() {
      if($(this).hasClass("icons_red1")) {
        $(this).removeClass('icons_red1');
        $(this).addClass('icons_blue1');
      }
      else if($(this).hasClass("icons_red2")) {
        $(this).removeClass('icons_red2');
        $(this).addClass('icons_blue2');
      }
      else if($(this).hasClass("icons_red3")) {
        $(this).removeClass('icons_red3');
        $(this).addClass('icons_blue3');
      }
      
      var id = $(this).attr('id');
      clearTimeout(timeOuts[id]);
      $('#'+id).fadeIn('slow',function() {
        clearTimeout(timeOuts[id]);
      });
    },
    function() {
      if($(this).hasClass("icons_blue1")) {
        $(this).addClass('icons_red1');
        $(this).removeClass('icons_blue1');
      }
      else if($(this).hasClass("icons_blue2")) {
        $(this).addClass('icons_red2');
        $(this).removeClass('icons_blue2');
      }
      else if($(this).hasClass("icons_blue3")) {
        $(this).addClass('icons_red3');
        $(this).removeClass('icons_blue3');
      }      
      var id = $(this).attr('id');
      timeOuts[id] = setTimeout('blink("'+id+'")', 700);
    }
  );
}); 
</script>
</body>
</html>