    <div class="right_block">
      <div class="right_side">

        <div class="contacts">
          <div class="blocks_title">
            CONTACT US:
          </div>
          <div class="line_step">
            <div class="note_block">Email:</div>
            info@eagle-essays.com
          </div>
          <div class="line_step line_step1">
            <div class="note_block">Skype:</div>
              writemypapers
          </div>
          <div class="line_step line_step2">
            <div class="note_block">Phone:</div>
            +442032868140
          </div>
        </div>
        <div class="contacts contacts_1">
            <div class="note_block title_dis title_dis11">Loyalty discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                  We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
            <div class="note_block title_dis">Referral discount code: <a href="" class="inf_popr ">
             <span class="popin_block">
             <span class="bg_popbl"></span>
                 <span>
                 Help your friend get 10% discount by giving them your introduction code, all they need to do is enter the code in the discount box whenever they place their order, not only that, you will also earn 10% reward credit every time they use the introductory code , it is a Win-Win scenario! The credit that you will earn can be used to pay for your next order. The more friend you refer the more credit you can get.
                 </span>
                 </span>
             </a></div>
            <span class="weight_sp1 weight_sp111">  <?php echo $_SESSION['user_id']; ?>02</span>   

          </div>
      </div>
    </div><!-- .right_block -->



       <div class="block_center">
     <div class="block_center_in">
      <div class="order_top">
        Checkout Order # <span><?php echo $order_id; ?></span>  
      </div>
 
     </div>
   </div><!-- .block_center -->


    <div class="block_center">
      <div class="block_center_in">
         <div class="center_blocks" id="payment_blok">
          <div class="blocks_title">Payment</div>

          <?php
          $paypal = 1;
          $loyality = 1;
          if($_SESSION['userdata']['amount'] > $order['price']) {
            $paypal = 0;
          }
          elseif($_SESSION['userdata']['amount'] == $order['price']) {
            $paypal = 0;
          }
          else {
            $loyality = 0;
          }
          ?>
<label id="paypal_ch" class="<?php if(!$paypal) echo 'cursor_text'; ?> <?php if($paypal) echo 'pay_btn_active'; ?> pay_btn"><input checked="checked" <?php if(!$paypal) echo 'disabled="disabled"'; ?> onchange="change_type()" type="radio" name="pay_type" id="pay_type_paypal" value="paypal" /> Paypal</label>
<label id="card_dch" class="<?php if(!$paypal) echo 'cursor_text'; ?> pay_btn "><input onchange="change_type()" type="radio" <?php if(!$paypal) echo 'disabled="disabled"'; ?> name="pay_type" id="pay_type_card" value="card"  /> Credit/Debit Card</label>

          <?php if($loyality): ?>
            <label id="loyality_ch" class="pay_btn pay_btn_active"><input onchange="change_type()" type="radio" name="pay_type" id="pay_type_loyality" value="loyality" checked="checked"/>Introduction credit</label>
          <?php endif; ?>

          <div id="payment_wrap"  style="display:none;">
 
<iframe name="pp_iframe" width="570px" height="340px" scrolling="no"></iframe>
<form style="display:none" target="pp_iframe" name="form_iframe" method="post"
      action="https://securepayments.sandbox.paypal.com/cgi-bin/acquiringweb">
<!--action="https://securepayments.paypal.com/acquiringweb?cmd=_hosted-payment"> live endpoint-->
<input type="hidden" name="cmd" value="_hosted-payment" />
   <input type="hidden" name="template" value="templateD" />  <!--templateD for iFrame | mobile-iframe for mobile iFrame | templateA for redirected layout-->
    <input type="hidden" name="business" value="3W7K3RXKZX3YW" />
    <input type name="subtotal" value="60.00">
    <input type="hidden" name="currency_code" value="USD" />
    <input type="hidden" name="billing_first_name" value="Slav" />
    <input type="hidden" name="billing_last_name" value="Marin" />
    <input type="hidden" name="billing_address1" value="35 Princess Park Manor" />
    <input type="hidden" name="billing_address2" value="Royal Drive" />
    <input type="hidden" name="billing_city" value="London" />
    <input type="hidden" name="billing_state" value="New Jersey London" />
    <input type="hidden" name="billing_zip" value="N11 3FL" />
    <input type="hidden" name="billing_country" value="GB" />
    <input type="hidden" name="first_name" value="<?php echo $_SESSION['userdata']['first_name']; ?>" />
    <input type="hidden" name="last_name" value="<?php echo $_SESSION['userdata']['last_name']; ?>" />
    <input type="hidden" name="address1" value="" />
    <input type="hidden" name="address2" value="" />
    <input type="hidden" name="city" value="" />
    <input type="hidden" name="state" value="" />
    <input type="hidden" name="zip" value="" />
    <input type="hidden" name="showShippingAddress" value="true"/>
    <input type="hidden" name="country" value="<?php if($user_country) echo $user_country['shor_code']; ?>" />
    <input type="hidden" name="notify_url" value="https://www.paypal.com/IPN/">
    <input type="hidden" name="return" value="https://eagle-essays.com/cart/receipt_card">
 
</form>
<script type="text/javascript">
    document.form_iframe.submit();
</script>

          <div id="submit_btn" class="pay_now_wrap">
            <a onclick="pay()" id="btn_pay_blink" class="pointer blinker order_now_btn">Pay Now</a>
          </div>          

          </div>

        </div><!-- .center_blocks -->  

        <div class="center_blocks" id="order_list_center">
          <div class="cell_block">
            <div class="services_line">
             <span class="note_block">Type of service:</span>  
              <?php if($order['work_type'] == 'new'): ?>
                Writing from scratch
              <?php else: ?>
                Editing/Proofreading
              <?php endif; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Academic level:</span> <?php echo $order['academic_level']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Type of paper:</span> <?php echo $order['paper_type']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Subject:</span> <?php echo $order['subject']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">No. of pages:</span>  <?php echo $order['pages']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">No. of slides:</span> <?php echo $order['slides']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Referencing:</span> <?php echo $order['paper_format']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Topic:</span> <?php echo substr($order['topic'], 0, 400); ?>
            </div>
            <div class="services_line">
            <span class="note_block">Paper details:</span> <?php echo $order['paper_details']; ?>
            </div>
          </div>
          <div class="cell_block">
             <div class="services_line">
             <span class="note_block">No. of sources: </span> <?php echo $order['sources']; ?>
            </div>
            <div class="services_line">
            <span class="note_block">Abstract page:</span> 
        <?php if($order['abstract_page']): ?>
          Yes
        <?php else: ?>
          No
        <?php endif; ?>            
            </div>
            <div class="services_line">
            <span class="note_block">Plagiarism rep.:</span>
        <?php if($order['plagiarism_report']): ?>
          Yes
        <?php else: ?>
          No
        <?php endif; ?>            
            </div>
            <div class="services_line">
            <span class="note_block">Paper details:</span> Submitted
            </div>
            <div class="services_line">
            <span class="note_block">Attachments:</span>
        <?php if($order['file']): ?>
          <?php
          switch ($order['file']['file_ext']) {
            case '.doc':
            case '.docx':
              $image = 'doc.png';
            break;
            case '.pdf':
              $image = 'pdf.png';
            break;
            case '.ppt':
            case '.pptx':
            case '.pps':
              $image = 'ppt.png';
            break;
            case '.xls':
            case '.xlss':
              $image = 'xls.png';
            break;
            case '.jpg':
            case '.jpeg':
            case '.bmp':
            case '.png':
              $image = 'jpg.png';
            break;
            default:
              $image = 'doc.png';
            break;
          }
          ?>
          <img src="/img/icon/<?php echo $image; ?>" width="15px"/> <span class="attach_name"><?php echo substr($order['file']['client_name'], 0, 20); ?></span>
        <?php else: ?>
          No
        <?php endif; ?>            
            </div>
            <div class="services_line">
            <span class="note_block">Full price:</span> USD <?php echo number_format($order['price']+$order['discount_save'], 2, '.', ''); ?>
            </div>
            <div class="services_line">
            <span class="note_block">Discounted price:</span> <?php echo $order['discount']; ?>% (USD <?php echo $order['price']; ?>)
            </div>
            <div class="services_line">
            <span class="note_block">Introduction credit:</span> USD <?php echo $_SESSION['userdata']['amount']; ?>
            </div>
<?php if(!$order['payment_id']): ?>
            <div class="services_line">
            <span class="note_block">Total price:</span> USD 
            <?php 
              if($_SESSION['userdata']['amount'] > $order['price'])
                echo '0.00';
              elseif($_SESSION['userdata']['amount'] == $order['price'])
                echo '0.00';
              else
                echo number_format($order['price']-$_SESSION['userdata']['amount'], 2, '.', ''); 
            ?>
            </div>
<?php else: ?>
            <div class="services_line">
            <span class="note_block">Total price:</span> USD <?php echo $payment['amount']; ?>
            </div>
<?php endif; ?>
          </div>
        </div><!-- .center_blocks -->
      </div>
    </div>
<script type="text/javascript">
function change_type()
{
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'card') {
    $("#submit_btn").addClass('hidden');
    $("#payment_wrap").slideDown();
    $("#card_dch").addClass("pay_btn_active");
    $("#paypal_ch").removeClass('pay_btn_active')
  }
  if(pay_type == 'paypal') {
    $("#submit_btn").removeClass('hidden');
    $("#payment_wrap").slideUp();
    $("#paypal_ch").addClass("pay_btn_active");
    $("#card_dch").removeClass("pay_btn_active");
  }
}
function pay()
{
  $("#submit_btn").html('<img src="/img/loading.gif" height="40px" />');
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'card') {
    $.post('/cart/pay_card', {
      order_id: <?php echo $order['order_id']; ?>,
      card_type: $("#card_type").val(),
      card_number: $("#card_number").val(),
      card_exp_month: $("#card_exp_month").val(),
      card_exp_year: $("#card_exp_year").val(),
      card_cvv2: $("#card_cvv2").val(),
      country: $("#country").val(),
      state: $("#state").val(),
      city: $("#city").val(),
      street: $("#street").val(),
      zip: $("#zip").val(),
    }, function(data){
      if(data.status == 'ok') {
        $("#submit_btn").html('Success!');
        location.assign('http://'+window.location.hostname+'/user/order_details/<?php echo $order['order_id']; ?>');
      }
      else if(data.status == 'failure') {
        var text_html = '';
        
        text_html += 'An error was occured. Please check card details<br />';
        text_html += '<a onclick="pay()" class="order_now_btn">Pay Now</a>';
        $("#submit_btn").html(text_html);
      }
      else {
        var text_html = '';
        text_html += 'An error was occured. Please try again later<br />';
        text_html += '<a onclick="pay()" class="order_now_btn">Pay Now</a>';
        $("#submit_btn").html(text_html);
      }
    }, 'json');
  }
  else {
    location.assign('http://'+window.location.hostname+'/cart/pay/<?php echo $order['order_id']; ?>');
  }
}
$().ready(function(){
});
</script>