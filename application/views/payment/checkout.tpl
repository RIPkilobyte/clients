<div class="right_block">
  <div class="right_side">
    <div class="contacts">
      <div class="blocks_title">
        CONTACT US:
      </div>
      <div class="line_step">
        <div class="note_block">Email:</div>
        info@eagle-essays.com
      </div>
      <div class="line_step line_step1">
        <div class="note_block">Skype:</div>
        writemypapers
      </div>
      <div class="line_step line_step2">
        <div class="note_block">Phone:</div>
          +442032868140
        </div>
    </div>
    <div class="contacts contacts_1">
      <div class="note_block title_dis title_dis11">Loyalty discount code: 
        <a href="" class="inf_popr ">
          <span class="popin_block">
            <span class="bg_popbl"></span>
            <span>We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.</span>
          </span>
        </a>
      </div>
      <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
      <div class="note_block title_dis">Referral discount code: 
        <a href="" class="inf_popr ">
          <span class="popin_block">
            <span class="bg_popbl"></span>
            <span>Help your friend get 10% discount by giving them your introduction code, all they need to do is enter the code in the discount box whenever they place their order, not only that, you will also earn 10% reward credit every time they use the introductory code , it is a Win-Win scenario! The credit that you will earn can be used to pay for your next order. The more friend you refer the more credit you can get.</span>
          </span>
        </a>
      </div>
      <span class="weight_sp1 weight_sp111"><?php echo $_SESSION['user_id']; ?>02</span>   
    </div>
  </div>
</div><!-- .right_block -->

<div class="block_center">
  <div class="block_center_in">
    <div class="order_top">
      Checkout Order # <span><?php echo $order_id; ?></span>  
    </div>
  </div>
</div><!-- .block_center -->

<div class="block_center">
  <div class="block_center_in">

    <div class="center_blocks" id="payment_blok">
      <div class="blocks_title">Payment</div>

      <label id="paypal_ch" class="pay_btn "><input onchange="change_type()" checked="checked" type="radio" name="pay_type" id="pay_type_paypal" value="paypal"  /> Paypal</label>
      <!--<label id="stripe_ch" class="pay_btn "><input onchange="change_type()" type="radio" name="pay_type" id="pay_type_stripe" value="stripe"  /> Bank Card</label>-->
      <?php if($order_id == '1353' && isset($response)): ?>
      <label id="wepay_ch" class="pay_btn "><input onchange="change_type()" type="radio" name="pay_type" id="pay_type_wepay" value="wepay"  /> Wepay</label>
      <?php endif; ?>
        
      <div id="paypal_wrap" style="display:none;">
          <div class="cell_block cell_block_half">
            <iframe name="pp_iframe" width="570px" height="540px" scrolling="no"></iframe>
            <form style="display:none" target="pp_iframe" name="form_iframe" method="post" action="https://securepayments.paypal.com/acquiringweb">
              <!--action="https://securepayments.sandbox.paypal.com/cgi-bin/acquiringweb" sandbox -->
              <!--action="https://securepayments.paypal.com/acquiringweb?cmd=_hosted-payment"> live endpoint-->
              <input type="hidden" name="cmd" value="_hosted-payment" />
              <input type="hidden" name="template" value="templateD" />  <!--templateD for iFrame | mobile-iframe for mobile iFrame | templateA for redirected layout-->
              <input type="hidden" name="business" value="BR6WE2E2U5VZU" />
              <input type="hidden" name="custom" value="<?php echo $order['order_id']; ?>" />
              <input type="hidden" name="invoice" value="eagle_invoiced" />
              <input type="hidden" name="charset" value="utf-8" />
              <input type="hidden" name="subtotal" value="<?php echo $order['price']; ?>">
              <input type="hidden" name="paymentaction" value="sale" />
              <input type="hidden" name="currency_code" value="USD" />
              <input type="hidden" name="billing_first_name" value="<?php echo $_SESSION['userdata']['first_name']; ?>" />
              <input type="hidden" name="billing_last_name" value="<?php echo $_SESSION['userdata']['last_name']; ?>" />
              <input type="hidden" name="billing_address1" value="" />
              <input type="hidden" name="billing_address2" value="" />
              <input type="hidden" name="billing_city" value="" />
              <input type="hidden" name="billing_state" value="" />
              <input type="hidden" name="billing_zip" value="" />
              <input type="hidden" name="billing_country" value="<?php if(isset($user_country) && $user_country) echo $user_country['short_code']; ?>" />
              <input type="hidden" name="item_number1" value="<?php echo $order['order_id']; ?>" />
              <input type="hidden" name="showShippingAddress" value="true"/>
              <!--
              <input type="hidden" name="first_name" value="" />
              <input type="hidden" name="last_name" value="" />
              <input type="hidden" name="address1" value="" />
              <input type="hidden" name="address2" value="" />
              <input type="hidden" name="city" value="" />
              <input type="hidden" name="state" value="" />
              <input type="hidden" name="zip" value="" />
              <input type="hidden" name="country" value="" />
              -->
              <input type="hidden" name="notify_url" value="http://eagle-essays.com/cart/ipn" />
              <input type="hidden" name="return" value="http://eagle-essays.com/user/order_details/<?php echo $order['order_id']; ?>" />
              <input type="hidden" name="logoImage" value="http://eagle-essays.com/img/email/Logo.png">
            </form>
          </div>
      </div>

      <div id="stripe_wrap" style="display:none;">
        <div class="cell_block cell_block_half">
          <form action="" method="POST">
            <script
              src="https://checkout.stripe.com/checkout.js" class="stripe-button"
              data-key="pk_live_CgLneazufDbSUgWvzhQsKJEl"
              data-amount="<?php echo $order['price']*100; ?>"
              data-name="Order #<?php echo $order['order_id']; ?>"
              data-description=""
              data-image="/img/email/Logo.png"
              data-locale="auto">
            </script>
          </form>
        </div>
      </div>
      <?php if($order_id == '1353' && isset($response)): ?>
      <div id="wepay_wrap" style="display:none;">
        <div class="cell_block cell_block_half">
          <div id="wepay_checkout"></div>

        </div>
      </div>
      <?php endif; ?>
    </div><!-- .center_blocks -->  

    <div class="center_blocks" id="order_list_center">
      <div class="cell_block">
        <div class="services_line">
          <span class="note_block">Type of service:</span>  
          <?php if($order['work_type'] == 'new'): ?>
            Writing from scratch
          <?php else: ?>
            Editing/Proofreading
          <?php endif; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Academic level:</span> <?php echo $order['academic_level']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Type of paper:</span> <?php echo $order['paper_type']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Subject:</span> <?php echo $order['subject']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">No. of pages:</span>  <?php echo $order['pages']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">No. of slides:</span> <?php echo $order['slides']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Referencing:</span> <?php echo $order['paper_format']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Topic:</span> <?php echo substr($order['topic'], 0, 400); ?>
        </div>
        <div class="services_line">
          <span class="note_block">Paper details:</span> <?php echo $order['paper_details']; ?>
        </div>
      </div>
      <div class="cell_block">
        <div class="services_line">
          <span class="note_block">No. of sources: </span> <?php echo $order['sources']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Abstract page:</span> 
          <?php if($order['abstract_page']): ?>
            Yes
          <?php else: ?>
            No
          <?php endif; ?>            
        </div>
        <div class="services_line">
          <span class="note_block">Plagiarism rep.:</span>
          <?php if($order['plagiarism_report']): ?>
            Yes
          <?php else: ?>
            No
          <?php endif; ?>            
        </div>
        <div class="services_line">
          <span class="note_block">Paper details:</span> Submitted
        </div>
        <div class="services_line">
          <span class="note_block">Attachments:</span>
          <?php if($order['file']): ?>
            <?php
              switch ($order['file']['file_ext']) {
                case '.doc':
                case '.docx':
                  $image = 'doc.png';
                break;
                case '.pdf':
                  $image = 'pdf.png';
                break;
                case '.ppt':
                case '.pptx':
                case '.pps':
                  $image = 'ppt.png';
                break;
                case '.xls':
                case '.xlss':
                  $image = 'xls.png';
                break;
                case '.jpg':
                case '.jpeg':
                case '.bmp':
                case '.png':
                  $image = 'jpg.png';
                break;
                default:
                  $image = 'doc.png';
                break;
              }
            ?>
            <img src="/img/icon/<?php echo $image; ?>" width="15px"/> <span class="attach_name"><?php echo substr($order['file']['client_name'], 0, 20); ?></span>
          <?php else: ?>
            No
          <?php endif; ?>            
        </div>
        <div class="services_line">
          <span class="note_block">Full price:</span> USD <?php echo number_format($order['price']+$order['discount_save'], 2, '.', ''); ?>
        </div>
        <div class="services_line">
          <span class="note_block">Discounted price:</span> <?php echo $order['discount']; ?>% (USD <?php echo $order['price']; ?>)
        </div>
        <div class="services_line">
          <span class="note_block">Introduction credit:</span> USD <?php echo $_SESSION['userdata']['amount']; ?>
        </div>
        <?php if(!$order['payment_id']): ?>
          <div class="services_line">
            <span class="note_block">Total price:</span> 
            USD 
            <?php 
              if($_SESSION['userdata']['amount'] > $order['price'])
                echo '0.00';
              elseif($_SESSION['userdata']['amount'] == $order['price'])
                echo '0.00';
              else
                echo number_format($order['price']-$_SESSION['userdata']['amount'], 2, '.', ''); 
            ?>
          </div>
        <?php else: ?>
          <div class="services_line">
            <span class="note_block">Total price:</span> USD <?php echo $payment['amount']; ?>
          </div>
        <?php endif; ?>
      </div>
    </div><!-- .center_blocks -->
    
  </div>
</div>

<script type="text/javascript" src="https://www.wepay.com/min/js/iframe.wepay.js"></script>
<script type="text/javascript">
function change_type()
{
  $("#paypal_wrap").slideDown();
  $("#paypal_ch").addClass("pay_btn_active");
  
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'paypal') {
    $("#stripe_wrap").slideUp();
    $("#stripe_ch").removeClass("pay_btn_active");
    $("#wepay_wrap").slideUp();
    $("#wepay_ch").removeClass("pay_btn_active");

    $("#paypal_wrap").slideDown();
    $("#paypal_ch").addClass("pay_btn_active");
  }
  if(pay_type == 'stripe') {
    $("#paypal_wrap").slideUp();
    $("#paypal_ch").removeClass("pay_btn_active");
    $("#wepay_wrap").slideUp();
    $("#wepay_ch").removeClass("pay_btn_active");

    $("#stripe_wrap").slideDown();
    $("#stripe_ch").addClass("pay_btn_active");
  }
  if(pay_type == 'wepay') {
    $("#paypal_wrap").slideUp();
    $("#paypal_ch").removeClass("pay_btn_active");
    $("#stripe_wrap").slideUp();
    $("#stripe_ch").removeClass("pay_btn_active");

    $("#wepay_wrap").slideDown();
    $("#wepay_ch").addClass("pay_btn_active");
  }
}

$().ready(function(){
  change_type();
  document.form_iframe.submit();
  <?php if($order_id == '1353' && isset($response)): ?>
  WePay.iframe_checkout("wepay_checkout", "<?php echo $response->hosted_checkout->checkout_uri; ?>");
  <?php endif; ?>
});
</script>