<div class="right_block">
  <div class="right_side">
    <div class="contacts">
      <div class="blocks_title">
        CONTACT US:
      </div>
      <div class="line_step">
        <div class="note_block">Email:</div>
        info@eagle-essays.com
      </div>
      <div class="line_step line_step1">
        <div class="note_block">Skype:</div>
        writemypapers
      </div>
      <div class="line_step line_step2">
        <div class="note_block">Phone:</div>
          +442032868140
        </div>
    </div>
    <div class="contacts contacts_1">
      <div class="note_block title_dis title_dis11">Loyalty discount code: 
        <a href="" class="inf_popr ">
          <span class="popin_block">
            <span class="bg_popbl"></span>
            <span>We give you 20% off from all new orders that you place with us. A Loyalty discount code is provided in the box below. You just insert this code when it is requested as you place your following orders with us. Please note that this is your personal discount code that will only work when it matches with your registered email address.</span>
          </span>
        </a>
      </div>
      <span class="weight_sp1"><?php echo $_SESSION['user_id']; ?>01</span>
      <div class="note_block title_dis">Referral discount code: 
        <a href="" class="inf_popr ">
          <span class="popin_block">
            <span class="bg_popbl"></span>
            <span>Help your friend get 10% discount by giving them your introduction code, all they need to do is enter the code in the discount box whenever they place their order, not only that, you will also earn 10% reward credit every time they use the introductory code , it is a Win-Win scenario! The credit that you will earn can be used to pay for your next order. The more friend you refer the more credit you can get.</span>
          </span>
        </a>
      </div>
      <span class="weight_sp1 weight_sp111"><?php echo $_SESSION['user_id']; ?>02</span>   
    </div>
  </div>
</div><!-- .right_block -->

<div class="block_center">
  <div class="block_center_in">
    <div class="order_top">
      Checkout Order # <span><?php echo $order_id; ?></span>  
    </div>
  </div>
</div><!-- .block_center -->

<div class="block_center">
  <div class="block_center_in">

    <div class="center_blocks" id="payment_blok">
      <div class="blocks_title">Payment</div>

      <div id="paypal_wrap" style="display:block;">
          <div class="cell_block cell_block_half">
            <iframe width="570px" height="540px" scrolling="no" frameborder="0" src="http://eagleediting.co/iframe/load/<?php echo $order['order_id']; ?>"></iframe>
          </div>
      </div>

    </div><!-- .center_blocks -->  

    <div class="center_blocks" id="order_list_center">
      <div class="cell_block">
        <div class="services_line">
          <span class="note_block">Type of service:</span>  
          <?php if($order['work_type'] == 'new'): ?>
            Writing from scratch
          <?php else: ?>
            Editing/Proofreading
          <?php endif; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Academic level:</span> <?php echo $order['academic_level']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Type of paper:</span> <?php echo $order['paper_type']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Subject:</span> <?php echo $order['subject']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">No. of pages:</span>  <?php echo $order['pages']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">No. of slides:</span> <?php echo $order['slides']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Referencing:</span> <?php echo $order['paper_format']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Topic:</span> <?php echo substr($order['topic'], 0, 400); ?>
        </div>
        <div class="services_line">
          <span class="note_block">Paper details:</span> <?php echo $order['paper_details']; ?>
        </div>
      </div>
      <div class="cell_block">
        <div class="services_line">
          <span class="note_block">No. of sources: </span> <?php echo $order['sources']; ?>
        </div>
        <div class="services_line">
          <span class="note_block">Abstract page:</span> 
          <?php if($order['abstract_page']): ?>
            Yes
          <?php else: ?>
            No
          <?php endif; ?>            
        </div>
        <div class="services_line">
          <span class="note_block">Plagiarism rep.:</span>
          <?php if($order['plagiarism_report']): ?>
            Yes
          <?php else: ?>
            No
          <?php endif; ?>            
        </div>
        <div class="services_line">
          <span class="note_block">Paper details:</span> Submitted
        </div>
        <div class="services_line">
          <span class="note_block">Attachments:</span>
          <?php if($order['file']): ?>
            <?php
              switch ($order['file']['file_ext']) {
                case '.doc':
                case '.docx':
                  $image = 'doc.png';
                break;
                case '.pdf':
                  $image = 'pdf.png';
                break;
                case '.ppt':
                case '.pptx':
                case '.pps':
                  $image = 'ppt.png';
                break;
                case '.xls':
                case '.xlss':
                  $image = 'xls.png';
                break;
                case '.jpg':
                case '.jpeg':
                case '.bmp':
                case '.png':
                  $image = 'jpg.png';
                break;
                default:
                  $image = 'doc.png';
                break;
              }
            ?>
            <img src="/img/icon/<?php echo $image; ?>" width="15px"/> <span class="attach_name"><?php echo substr($order['file']['client_name'], 0, 20); ?></span>
          <?php else: ?>
            No
          <?php endif; ?>            
        </div>
        <div class="services_line">
          <span class="note_block">Full price:</span> USD <?php echo number_format($order['price']+$order['discount_save'], 2, '.', ''); ?>
        </div>
        <div class="services_line">
          <span class="note_block">Discounted price:</span> <?php echo $order['discount']; ?>% (USD <?php echo $order['price']; ?>)
        </div>
        <div class="services_line">
          <span class="note_block">Introduction credit:</span> USD <?php echo $_SESSION['userdata']['amount']; ?>
        </div>
        <?php if(!$order['payment_id']): ?>
          <div class="services_line">
            <span class="note_block">Total price:</span> 
            USD 
            <?php 
              if($_SESSION['userdata']['amount'] > $order['price'])
                echo '0.00';
              elseif($_SESSION['userdata']['amount'] == $order['price'])
                echo '0.00';
              else
                echo number_format($order['price']-$_SESSION['userdata']['amount'], 2, '.', ''); 
            ?>
          </div>
        <?php else: ?>
          <div class="services_line">
            <span class="note_block">Total price:</span> USD <?php echo $payment['amount']; ?>
          </div>
        <?php endif; ?>
      </div>
    </div><!-- .center_blocks -->
    
  </div>
</div>

<script type="text/javascript">
$().ready(function(){
});
</script>