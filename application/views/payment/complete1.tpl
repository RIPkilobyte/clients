<div class="site-content-middle" role="main">
  <div class="site-content-middle-in">
        
    <div class="main_blocks main_blocks_order">
    	Payment for Order #<?php echo $order['order_id']; ?> successful
    </div>
    <div class="main_blocks main_blocks_payments">
    	Thank you for payment. <a href="/user/order_details/<?php echo $order['order_id']; ?>">Return to order</a>
    </div>
    
  </div><!-- .site-content-middle-in -->
</div><!-- .site-content-middle -->