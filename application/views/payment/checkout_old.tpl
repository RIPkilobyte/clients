<div class="block_container">
  <div class="block_head">Checkout Order#<?php echo $order_id; ?></div>
  <div class="block_content">
  <table>
  	<tr>
  		<td class="td_order_information">
			<div class="order_information"><h3>Order information</h3>
				<table>
					<tr>
						<th>Number of pages</th>
						<th>Number of slides</th>
						<th>Academic level</th>
						<th>Deadline</th>
						<th>Type of work</th>
						<th>Type of paper</th>
					</tr>
					<tr>
						<td><?php echo $order['pages']; ?></td>
						<td><?php echo $order['slides']; ?></td>
						<td><?php echo $order['academic_level']; ?></td>
						<td><?php echo $order['deadline']; ?></td>
						<td>					<?php
					if($order['work_type'] == 'new')
						echo 'Writing from scratch';
					elseif($order['work_type'] == 'edit')
						echo 'Editing/proofreading';
					?></td>
						<td><?php echo $order['paper_type']; ?></td>
					</tr>
				</table>
				<table  class="table_subject_topic">
					<tr>
						<th>Subject</th>
						<th>Topic</th>
						<th>Plagiarism report</th>
						<th>Abstract page</th>
					</tr>
					<tr>
						<td><?php echo $order['subject']; ?></td>
						<td><?php echo $order['topic']; ?></td>

								<td>
								<?php
								if($order['plagiarism_report'])
									echo 'Yes';
								else
									echo 'No';
								?>
								</td>
								<td>
								<?php
								if($order['abstract_page'])
									echo 'Yes';
								else
									echo 'No';
								?>
								</td>						
					</tr>
				</table>
				<div class="paper_details">
					<h4>Paper details</h4>
					<p><?php echo $order['paper_details']; ?></p>
				</div>
				
			</div>  
  		</td>
  	</tr>
  	<tr>
  		<td>
  		<div class="paper_details second" style="margin:0px;">
			<h4>Please select your prefered payment method to pay the amount of USD <span id="order_amount"><?php echo $order['price']; ?></span></h4>
		</div>

		</div>

  			<div class="">
        	<table>
        		<tr>
        			<td colspan="2">
        				<h4>Please select your prefered payment method to pay the amount of USD <span id="order_amount"><?php echo $order['price']; ?></span></h4>
        			</td>
        		</tr>
        		<tr>
        			<td>
        				<label><input onchange="change_type()" type="radio" name="pay_type" id="pay_type_card" value="card" checked="checked" /> Credit/Debit Card</label>
        			</td>
        			<td>
        				<label><input onchange="change_type()" type="radio" name="pay_type" id="pay_type_paypal" value="paypal" /> Paypal</label>
        			</td>
        		</tr>
        		<tr>
        			<td colspan="2">
				      <table>
				        <tr>
				          <td>Credit Card Type</td>
				          <td>
				            <select name="card_type" id="card_type">
				              <option value="0">--Please Select--</option>
				              <option value="American Express">American Express</option>
				              <option value="Discover">Discover</option>
				              <option value="MasterCard">MasterCard</option>
				              <option value="Visa">Visa</option>
				            </select>
				          </td>
				        </tr>
				        <tr>
				          <td>Credit Card Number</td>
				          <td><input type="text" name="card_number" id="card_number" size="25" /></td>
				        </tr>
				        <tr>
				          <td>Expiration Date</td>
				          <td>
				            <select name="card_exp_month" id="card_exp_month">
				              <option value="0">Month</option>
				              <option value="01">01</option>
				              <option value="02">02</option>
				              <option value="03">03</option>
				              <option value="04">04</option>
				              <option value="05">05</option>
				              <option value="06">06</option>
				              <option value="07">07</option>
				              <option value="08">08</option>
				              <option value="09">09</option>
				              <option value="10">10</option>
				              <option value="11">11</option>
				              <option value="12">12</option>
				            </select>
				            <select name="card_exp_year" id="card_exp_year">
				              <option value="0">Year</option>
				              <?php $year = date('Y'); ?>
				              <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
				              <option value="<?php echo $year+1; ?>"><?php echo $year+1; ?></option>
				              <option value="<?php echo $year+2; ?>"><?php echo $year+2; ?></option>
				              <option value="<?php echo $year+3; ?>"><?php echo $year+3; ?></option>
				              <option value="<?php echo $year+4; ?>"><?php echo $year+4; ?></option>
				              <option value="<?php echo $year+5; ?>"><?php echo $year+5; ?></option>
				              <option value="<?php echo $year+6; ?>"><?php echo $year+6; ?></option>
				            </select>
				          </td>
				        </tr>
				        <tr>
				          <td>Card Verification Number</td>
				          <td><input type="text" name="card_cvv2" id="card_cvv2" size="3"></td>
				        </tr>
				        <tr>
				          <td>Country</td>
				          <td>
				            <select id="country" name="country">
				              <option value="0">Choose country</option>
				              <?php
				                foreach ($countries as $key => $value) {
				                  echo '<option value="'.$value['country_id'].'">'.$value['title'].'</option>';
				                }
				              ?>
				            </select>
				          </td>
				        </tr>
				        <tr>
				          <td>State / County / Province</td>
				          <td>
				            <input type="text" name="state" id="state" size="25" />
				          </td>
				        </tr>
				        <tr>
				          <td>City</td>
				          <td>
				            <input type="text" name="city" id="city" size="25" />
				          </td>
				        </tr>
				        <tr>
				          <td>Street</td>
				          <td>
				            <input type="text" name="street" id="street" size="25" />
				          </td>
				        </tr>
				        <tr>
				          <td>Zip postal code</td>
				          <td>
				            <input type="text" name="zip" id="zip" size="25" />
				          </td>
				        </tr>
				      </table>

        			</td>
        		</tr>
        		<tr>
        			<td colspan="2" id="submit_btn">
        				<input class="blue_btn" type="button" onclick="pay()" value="Pay" />
        			</td>
        		</tr>
        	</table>
        	

  			</div>
  		</td>
  	</tr>
  </table>
  </div>
</div>

<script type="text/javascript">
function change_type()
{
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'card') {
    $("#card_type").removeAttr('disabled');
    $("#card_number").removeAttr('disabled');
    $("#card_exp_month").removeAttr('disabled');
    $("#card_exp_year").removeAttr('disabled');
    $("#card_cvv2").removeAttr('disabled');
    $("#country").removeAttr('disabled');
    $("#state").removeAttr('disabled');
    $("#city").removeAttr('disabled');
    $("#street").removeAttr('disabled');
    $("#zip").removeAttr('disabled');
  }
  if(pay_type == 'paypal') {
    $("#card_type").attr('disabled', 'disabled');
    $("#card_number").attr('disabled', 'disabled');
    $("#card_exp_month").attr('disabled', 'disabled');
    $("#card_exp_year").attr('disabled', 'disabled');
    $("#card_cvv2").attr('disabled', 'disabled');
    $("#country").attr('disabled', 'disabled');
    $("#state").attr('disabled', 'disabled');
    $("#city").attr('disabled', 'disabled');
    $("#street").attr('disabled', 'disabled');
    $("#zip").attr('disabled', 'disabled');
  }
}
function pay()
{
  $("#submit_btn").html('<img src="/img/loading.gif" height="40px" />');
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'card') {
    $.post('/cart/pay_card', {
      order_id: <?php echo $order['order_id']; ?>,
      card_type: $("#card_type").val(),
      card_number: $("#card_number").val(),
      card_exp_month: $("#card_exp_month").val(),
      card_exp_year: $("#card_exp_year").val(),
      card_cvv2: $("#card_cvv2").val(),
      country: $("#country").val(),
      state: $("#state").val(),
      city: $("#city").val(),
      street: $("#street").val(),
      zip: $("#zip").val(),
    }, function(data){
      if(data.status == 'ok') {
        $("#submit_btn").html('Success!');
        location.replace('http://'+window.location.hostname+'/user/order_details/<?php echo $order['order_id']; ?>');
      }
      else if(data.status == 'failure') {
        var text_html = '';
        text_html += 'An error was occured. Please check card details<br />';
        text_html += '<input class="btn_green" type="button" onclick="pay()" value="Pay" />';
        $("#submit_btn").html(text_html);
      }
      else {
        $("#submit_btn").html('An error was occured. Please try again later');
      }
    }, 'json');
  }
  else {
    location.replace('http://'+window.location.hostname+'/cart/pay/<?php echo $order['order_id']; ?>');
  }
}
$().ready(function(){
});
</script>