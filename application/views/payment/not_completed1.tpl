<div class="site-content-middle" role="main">
  <div class="site-content-middle-in">
        
    <div class="main_blocks main_blocks_order">
    	Payment for Order not completed
    </div>
    <div class="main_blocks main_blocks_payments">
    	An error was occured with payment. Please check details on your payment system and contact to support. <a href="/user">Return to orders</a>
    </div>

  </div><!-- .site-content-middle-in -->
</div><!-- .site-content-middle -->