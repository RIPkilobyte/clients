<div class="site-content-middle" role="main">
	<div class="site-content-middle-in">
        
		<div class="main_blocks main_blocks_order">
			Checkout Order # <span><?php echo $order_id; ?></span>  
        </div>
        <div class="con_right_side float-left">
          <div class="con_right_side_in">

            <div class="main_blocks main_blocks_profile main_blocks_inform">
              <div class="title_i cont_titles">Checkout</div>
              <label id="paypal_ch" class="pay_btn_active pay_btn"><input checked="checked" onchange="change_type()" type="radio" name="pay_type" id="pay_type_paypal" value="paypal" /> Paypal</label>
<label id="card_dch" class=" pay_btn "><input onchange="change_type()" type="radio" name="pay_type" id="pay_type_card" value="card"  /> Credit/Debit Card</label>

          <div id="payment_wrap"  style="display:none;">
          <div class="cell_block cell_block_half">
              <div class="pay_title">Credit Card Type</div>
              <div class="select_wrap">
                  <select name="card_type" id="card_type">
                  <option value="0">--Please Select--</option>
                  <option value="American Express">American Express</option>
                  <option value="Discover">Discover</option>
                  <option value="MasterCard">MasterCard</option>
                  <option value="Visa">Visa</option>
                  </select>
              </div>
              <div class="pay_title">Credit Card Number</div>
                <input type="text" placeholder="Credit Number" name="card_number" id="card_number" size="25" />
              <div class="pay_title">Expiration Date</div>
              <div class="select_wrap" id="date_year">
                <select name="card_exp_month" id="card_exp_month">
                      <option value="0">Month</option>
                      <option value="01">01</option>
                      <option value="02">02</option>
                      <option value="03">03</option>
                      <option value="04">04</option>
                      <option value="05">05</option>
                      <option value="06">06</option>
                      <option value="07">07</option>
                      <option value="08">08</option>
                      <option value="09">09</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
              </div>
              <div class="select_wrap" id="date_year">
                 <select name="card_exp_year" id="card_exp_year">
                      <option value="0">Year</option>
                      <?php $year = date('Y'); ?>
                      <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                      <option value="<?php echo $year+1; ?>"><?php echo $year+1; ?></option>
                      <option value="<?php echo $year+2; ?>"><?php echo $year+2; ?></option>
                      <option value="<?php echo $year+3; ?>"><?php echo $year+3; ?></option>
                      <option value="<?php echo $year+4; ?>"><?php echo $year+4; ?></option>
                      <option value="<?php echo $year+5; ?>"><?php echo $year+5; ?></option>
                      <option value="<?php echo $year+6; ?>"><?php echo $year+6; ?></option>
                    </select>
              </div>
              <div class="pay_title">Card Verification Number</div>
              <input type="text"placeholder="Card Number" name="card_cvv2" id="card_cvv2" size="3">
            </div>
            <div class="cell_block cell_block_half">
              <div class="pay_title">Country</div>
              <div class="select_wrap">
                <select id="country" name="country">
                      <option value="0">Choose country</option>
                      <?php
                        foreach ($countries as $key => $value) {
                          echo '<option value="'.$value['country_id'].'">'.$value['title'].'</option>';
                        }
                      ?>
                    </select>
              </div>
              <div class="pay_title">State / County / Province</div>
              <input type="text" placeholder="Write your State, County, Province" name="state" id="state" size="25" />
              <div class="pay_title">City</div>
              <input type="text" placeholder="Write your City" name="city" id="city" size="25" />
              <div class="pay_title">Street</div>
              <input type="text" placeholder="Write your Street" name="street" id="street" size="25" />
              <div class="pay_title">Zip postal code</div>
              <input type="text" placeholder="Write your Zip postal code" name="zip" id="zip" size="25" />
            </div>
          </div>
          <div id="submit_btn" class="pay_now_wrap">
            <a onclick="pay()" class="order_now_btn">Pay Now</a>
          </div>          
            </div>
          
            <div class="main_blocks main_blocks_profile main_blocks_inform">
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Number of pages:
                </div>
                <div class="cells_bl"><?php echo $order['pages']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Number of slides: 
                </div>
                <div class="cells_bl"><?php echo $order['slides']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Academic level: 
                </div>
                <div class="cells_bl"><?php echo $order['academic_level']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Deadline:
                </div>
                <div class="cells_bl"><?php echo $order['deadline']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Type of work:  
                </div>
                <div class="cells_bl">
                  <?php
                    if($order['work_type'] == 'new')
                      echo 'Writing from scratch';
                    elseif($order['work_type'] == 'edit')
                      echo 'Editing/proofreading';
                  ?>
                </div>
                <div class="cells_bl cells_bl_title">
                  Type of paper: 
                </div>
                <div class="cells_bl"><?php echo $order['paper_type']; ?></div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Subject: 
                </div>
                <div class="cells_bl"><?php echo $order['subject']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Plagiarism report: 
                </div>
                <div class="cells_bl">
                <?php
                  if($order['plagiarism_report'])
                    echo 'Yes';
                  else
                    echo 'No';
                ?>
                </div>
              </div>
              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Abstract page:
                </div>
                <div class="cells_bl">
                <?php
                  if($order['abstract_page'])
                    echo 'Yes';
                  else
                    echo 'No';
                ?>
                </div>
                <div class="cells_bl cells_bl_title">
                  Referencing style:
                </div>
                <div class="cells_bl"><?php echo $order['paper_format']; ?></div>
              </div>

              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Number of Sources:
                </div>
                <div class="cells_bl"><?php echo $order['sources']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Topic:   
                </div>
                <div class="cells_bl"><?php echo $order['topic']; ?></div>
              </div>

              <div class="row_i">
                <div class="cells_bl cells_bl_title">
                  Price:
                </div>
                <div class="cells_bl"><?php echo $order['price']; ?></div>
                <div class="cells_bl cells_bl_title">
                  Discount:   
                </div>
                <div class="cells_bl"><?php echo $order['discount']; ?>% (USD <?php echo $order['discount_save']; ?>)</div>
              </div>

              <div class="last_row row_i">
                <div class="cells_bl cells_bl_title">
                  Paper details: 
                </div>
                <div class="cells_bl"><?php echo $order['paper_details']; ?></div>
              </div>
            </div>




          </div>
        </div>

	</div><!-- .site-content-middle-in -->
</div><!-- .site-content-middle -->
<script type="text/javascript">
function change_type()
{
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'card') {
    $("#card_type").removeAttr('disabled');
    $("#card_number").removeAttr('disabled');
    $("#card_exp_month").removeAttr('disabled');
    $("#card_exp_year").removeAttr('disabled');
    $("#card_cvv2").removeAttr('disabled');
    $("#country").removeAttr('disabled');
    $("#state").removeAttr('disabled');
    $("#city").removeAttr('disabled');
    $("#street").removeAttr('disabled');
    $("#zip").removeAttr('disabled');
    $("#payment_wrap").slideDown();
    $("#card_dch").addClass("pay_btn_active");
    $("#paypal_ch").removeClass('pay_btn_active')
  }
  if(pay_type == 'paypal') {
    $("#card_type").attr('disabled', 'disabled');
    $("#card_number").attr('disabled', 'disabled');
    $("#card_exp_month").attr('disabled', 'disabled');
    $("#card_exp_year").attr('disabled', 'disabled');
    $("#card_cvv2").attr('disabled', 'disabled');
    $("#country").attr('disabled', 'disabled');
    $("#state").attr('disabled', 'disabled');
    $("#city").attr('disabled', 'disabled');
    $("#street").attr('disabled', 'disabled');
    $("#zip").attr('disabled', 'disabled');
    $("#payment_wrap").slideUp();
    $("#paypal_ch").addClass("pay_btn_active");
     $("#card_dch").removeClass("pay_btn_active");

  }
}
function pay()
{
  $("#submit_btn").html('<img src="/img/loading.gif" height="40px" />');
  var pay_type = $("input[name=pay_type]:checked").val();
  if(pay_type == 'card') {
    $.post('/cart/pay_card', {
      order_id: <?php echo $order['order_id']; ?>,
      card_type: $("#card_type").val(),
      card_number: $("#card_number").val(),
      card_exp_month: $("#card_exp_month").val(),
      card_exp_year: $("#card_exp_year").val(),
      card_cvv2: $("#card_cvv2").val(),
      country: $("#country").val(),
      state: $("#state").val(),
      city: $("#city").val(),
      street: $("#street").val(),
      zip: $("#zip").val(),
    }, function(data){
      if(data.status == 'ok') {
        $("#submit_btn").html('Success!');
        location.replace('http://'+window.location.hostname+'/user/order_details/<?php echo $order['order_id']; ?>');
      }
      else if(data.status == 'failure') {
        var text_html = '';
        
        text_html += 'An error was occured. Please check card details<br />';
        text_html += '<a onclick="pay()" class="order_now_btn">Pay Now</a>';
        $("#submit_btn").html(text_html);
      }
      else {
        var text_html = '';
        text_html += 'An error was occured. Please try again later<br />';
        text_html += '<a onclick="pay()" class="order_now_btn">Pay Now</a>';
        $("#submit_btn").html(text_html);
      }
    }, 'json');
  }
  else {
    location.replace('http://'+window.location.hostname+'/cart/pay/<?php echo $order['order_id']; ?>');
  }
}
$().ready(function(){
});
</script>