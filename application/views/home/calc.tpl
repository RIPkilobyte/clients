<script type="text/javascript" src="/js/jquery.numberMask.js"></script>
<div class="calc"><div class="title">Click submit to get a free instant quote</div>
    <form action="/order" method="post" id="calc_form">
    <div class="select_1">
    	<select id="academic_level" class='calc_selector' name="academic_level" style="width:178px;">
            <option value="0">Choose Academic Level</option>
            <?php
            foreach ($academic_levels as $academic_level) {
                echo '<option value="'.$academic_level['academic_level_id'].'">'.$academic_level['title'].'</option>';
            }
            ?>
        </select>
    </div>
    <div class="select_2">
    	<select id="paper_type" class='calc_selector' name="paper_type" style="width:168px">
            <option value="0">Choose Type of Paper</option>
            <?php
            foreach ($paper_types as $paper_type) {
                $class = '';
                if(isset($paper_type['class']) && $paper_type['class'] == 'indent')
                    $class = 'class="indent"';
                echo '<option '.$class.' value="'.$paper_type['paper_type_id'].'">'.$paper_type['title'].'</option>';
            }
            ?>
        </select>
    </div>
    <div class="select_3">
        <select id="deadline" class='calc_selector' name="deadline" style="width:138px;">
            <option value="0">Choose Deadline</option>
            <?php
            foreach ($deadlines as $deadline) {
                echo '<option value="'.$deadline['deadline_id'].'">'.$deadline['title'].'</option>';
            }
            ?>
        </select>
    </div>
    <div class="select_6" style="width:140px;padding-top: 17px;">
        <div class="button" onclick="page_minus()"><img src="/img/minus.png" /></div>
        <input type="text" id="number_of_pages" name="pages" size="4" placeholder="Pages" />
        <div class="button" onclick="page_plus()"><img src="/img/plus.png" /></div>
    </div>
    <div class="select_4" style="width:99px;">
    	<div class="ue" id="calc_result">0</div>
    </div>
    <div class="select_5">
        <div class="submit"><span class="submit_btn_text">Submit</span></div>
    </div>
    <div class="clear"></div>
    <div class="select_work" >
        <label><input type="radio" class="calc_selector work_type" name="work_type[]" value="new" checked="checked" /><span class="icon"></span> Writing from scratch</label>
        <label><input type="radio" class="calc_selector work_type" name="work_type[]" value="edit" /><span class="icon"></span>Editing/proofreading</label>
    </div>        
    </form>
    <script type="text/javascript">
        function page_minus()
        {
            var number_of_pages = $("#number_of_pages").val();
            number_of_pages = parseInt(number_of_pages);
            if(number_of_pages > 1)
                $("#number_of_pages").val(parseInt(number_of_pages-1));
            else
                $("#number_of_pages").val(1);
            calc_selector();
        }
        function page_plus()
        {
            var number_of_pages = $("#number_of_pages").val();
            if(number_of_pages == '')
                number_of_pages = 0
            else
                number_of_pages = parseInt(number_of_pages);
            if(number_of_pages == 9999)
                $("#number_of_pages").val(9999);
            else
                $("#number_of_pages").val(parseInt(number_of_pages+1));
            calc_selector();
        }        
        function calc_selector()
        {
            var work_type = $(".work_type:checked").val();
            var academic_level = $("#academic_level").val();
            var deadline = $("#deadline").val();
            var number_of_pages = $("#number_of_pages").val();

            number_of_pages = parseInt(number_of_pages);
            if(isNaN(number_of_pages))
                number_of_pages = 1;

            if(typeof(academic_level) != 'undefined' && typeof(deadline) != 'undefined' && typeof(number_of_pages) != 'undefined')
            if(academic_level != '0' && deadline != '0') {
                var price = prices[work_type][academic_level][deadline];
                price = parseFloat(price * number_of_pages).toFixed(2);
            }
            else
                var price = 0.00;

            $("#calc_result").html(price);
        }    
        $(".submit").click(function(){
            $("#calc_form").submit();
        });
        $().ready(function(){
            $("#number_of_pages").numberMask({beforePoint:4});
            calc_selector();
            $(".calc_selector").change(function(){
                calc_selector();
            });
            $('#number_of_pages').mouseout(function(){
                calc_selector();
            }); 
            $('#number_of_pages').change(function(){
                calc_selector();
            });
        });        
    </script>
   
</div>