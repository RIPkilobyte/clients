<div class="block1">
	<div class="bl_1">
		<img src="../img/ic_1.png" style="float:left; margin-right:15px;">
		<div class="title">
			Quick
			<br>
			delivery
		</div>
		<div class="text">
			Our advisors and specialists of writing will provide an instant help for you and your case
		</div>
	</div>
	<div class="bl_1">
		<img src="../img/ic_2.png" style="float:left; margin-right:15px;">
		<div class="title">
			Track your
			<br>
			order
		</div>
		<div class="text">
			Our user-friendly system will allow you to communicate with advisors & take charge of the process
		</div>
	</div>
	<div class="bl_1">
		<img src="../img/ic_3.png" style="float:left; margin-right:15px;">
		<div class="title">
			Instant
			<br>
			quote
		</div>
		<div class="text">
			Get a free instant quote in order to find the most suitable specialist to help you with your case
		</div>
	</div>
	<div class="bl_2">
		<img src="../img/ic_4.png" style="float:left; margin-right:15px;">
		<div class="title">
			Affordable
			<br>
			prices
		</div>
		<div class="text">
			Our prices start from as little as $9.99 per page for professional advisory help
		</div>
	</div>
</div>
<div class="mega_line"></div>
<div class="block2">
	<div class="our">
		Our
		<br>
		services
	</div>
	<?php /*<table class="ff_table">
		<tr class="ff">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>

		</tr>
		<tr>
			<td><a href="/admission-help">Admission Help</a></td>
			<td><a href="/case-study">Case Study</a></td>
			<td><a href="/dissertation">Dissertation</a></td>
			<td><a href="/lab-report">Lab Report</a></td>
			<td><a href="/research-papers">Research Papers</a></td>

		</tr>
		<tr>
			<td><a href="/assignment">Assignment</a></td>
			<td><a href="/college-papers">College Papers</a></td>
			<td><a href="/editing">Editing</a></td>
			<td><a href="/movie-review">Movie Review</a></td>
			<td><a href="/school-papers">School Papers</a></td>

		</tr>
		<tr>
			<td><a href="/book-report">Book Report</a></td>
			<td>Course Work</td>
			<td><a href="/ghostwriting">Ghostwriting</a></td>
			<td><a href="/personal-statement">Personal Statement</a></td>
			<td><a href="/speech">Speech</a></td>

		</tr>
		<tr>
			<td><a href="/book-review">Book Review</a></td>
			<td><a href="/custom-essays">Custom Essays</a></td>
			<td><a href="/homework">Homework</a></td>
			<td><a href="/proofreading">Proofreading</a></td>
			<td><a href="/term-papers">Term Papers</a></td>

		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><a href="/thesis">Thesis</a></td>

		</tr>
	</table> */ ?>
	
	
	
	<table class="ff_table">
		<tr class="ff">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>

		</tr>
		<tr>
			<td><a href="/admission-help-cover-letter">Admission Help</a></td>
			<td><a href="/case-study">Case Study</a></td>
			<td><a href="/dissertation">Dissertation</a></td>
			<td><a href="/lab-report">Lab Report</a></td>
			<td><a href="/research-paper-essay-writing">Research Papers</a></td>

		</tr>
		<tr>
			<td><a href="/assignment-help">Assignment</a></td>
			<td><a href="/college-papers-essay">College Papers</a></td>
			<td><a href="/essay-proofreading-services">Editing</a></td>
			<td><a href="/movie-review">Movie Review</a></td>
			<td><a href="/school-paper-writing-services">School Papers</a></td>

		</tr>
		<tr>
			<td><a href="/book-report">Book Report</a></td>
			<td><a href="#">Course Work</a></td>
			<td><a href="/ghostwriting">Ghostwriting</a></td>
			<td><a href="/personal-statement">Personal Statement</a></td>
			<td><a href="/speech">Speech</a></td>

		</tr>
		<tr>
			<td><a href="/book-review">Book Review</a></td>
			<td><a href="/writing-thesis">Thesis</a></td>
			<td><a href="/homework">Homework</a></td>
			<td><a href="/proofreading_services">Proofreading</a></td>
			<td><a href="/writing-term-paper">Term Papers</a></td>

		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>

		</tr>
	</table>
</div>
<div class="mega_line"></div>
<div class="block1_1">
	<div class="bl_1">
		<img src="../img/ic_5.png" style="float:left; margin-right:15px;">
		<div class="title">
			Plagiarism
			<br>
			free
		</div>
		<div class="text">
			We guarantee original & unique content on all of the services we provide.
		</div>
	</div>
	<div class="bl_1">
		<img src="../img/ic_6.png" style="float:left; margin-right:15px;">
		<div class="title">
			Fully<br>anonymous
		</div>
		<div class="text">
			We follow very strict security measures to keep you and your material completely anonymous. 
		</div>
	</div>
	<div class="bl_1">
		<img src="../img/ic_7.png" style="float:left; margin-right:15px;">
		<div class="title">
			Best
			<br>
			professionals
		</div>
		<div class="text">
			The best professionals will help you to solve any difficulties with your writing cases.
		</div>
	</div>
	<div class="bl_2">
<a href="/benefits">More Benefits</a>	
	</div>
</div>
<div class="mega_line"></div>
<div class="block_3">
	<div class="how">
		how it works
	</div>
	<div class="how_img"></div>
</div>
<div class="mega_line"></div>
<div class="block_4">
	<div class="bl_1">
		<div class="title">
			We provide the best custom writing services for clients!
		</div>
		<div class="text">
			Our online writing service is the best option for you to get top quality, plagiarism free papers.<br>
We specialize in papers on a broad variety of subjects and from different fields, including custom essays, custom papers, coursework, and so on. <br>
Our writers are well respected in the industry, having been providing custom writing services for years. <br>
We, as a company, have gained great experience professional essay writing service, we’ve built a strong team of top quality writers, and this along with our 27/7 customers support system guaranties that our customers only receive the best. 
		</div>
	</div>

	<div class="bl_1">
		<div class="title">
			Academic writing is the core of our business. 
		</div>
		<div class="text">
			We are always eager to provide you with a superb essay writing service. Our essay writing help is quick, fairly priced, and delivered to the highest industry standards. 
<br>Our customers are guaranteed to get a full refund in the case that they are not happy with the quality of our work. However dissatisfaction cases are extremely rare due to our Revisions Policy. The Policy is that you get three Free Revisions of your paper. Just tell the writer what you don’t like, and it’s fixed!
<br>Being an Academic Writing Service, we take plagiarism very seriously. We use the best Plagiarism Detection Software available, and so can guarantee that all our work is completely original and Plagiarism Free.
		</div>
	</div>

	<div class="bl_2">
		<div class="title">
			Order from us to get reliable, punctual, top quality work.
		</div>
		<div class="text">
			A little help with an essay may sometimes be all that you need. Even if you can write the on your own, you may run into unexpected problems that will take time and focus away from your work. In such cases we can take on the completion of your essay at any stage. 
<br>Our service and Customer Support team is available 24/7, all year. 
<br>We like to reward our customers’ loyalty at every opportunity, so should you return to us after your first order, we will be happy to offer you significant discounts on your writing work. The more you order from us, the better the discounts that you get from us! 
		</div>
	</div>
</div>

