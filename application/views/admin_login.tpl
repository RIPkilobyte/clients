<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $this->lang->line('Authorization'); ?></title>
    <link href="<?=$this->config->base_url();?>img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="<?=$this->config->base_url();?>css/login.css" rel="stylesheet" media="all" />
</head>

<body>

<div id="content">
<H2 class="msg"><?php echo @$msg; ?></H2>
<section class="login">
    <h1><?php echo $this->lang->line('Authorization'); ?></h1>
    <form method="post">
        <p>
            <input type="text" name="login" value="" placeholder="<?php echo $this->lang->line('Login'); ?>">
        </p>
        <p>
            <input type="password" name="password" value="" placeholder="<?php echo $this->lang->line('Password'); ?>">
        </p>
        <p class="submit">
            <input type="submit" value="<?php echo $this->lang->line('Enter'); ?>">
        </p>
    </form>
</section>
</div>

</body>
</html>