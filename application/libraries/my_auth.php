<?php if (!defined('BASEPATH')) exit('No direct script access allowed');  

Class my_auth {
	private $login = 'email';

	public function __construct() 
	{
		$CI =& get_instance();
		session_start();

		$CI->load->helper('cookie');
		$CI->load->model('my_auth_model');	

		if(!isset($_SESSION['user_id'])) {
			if(get_cookie('user_id') and get_cookie('user_password')) {
				$user_id = get_cookie('user_id');
				$password = get_cookie('user_password');
				$salt = get_cookie('user_salt');
				
				delete_cookie('user_id');
				delete_cookie('user_password');
				delete_cookie('user_salt');

				$user = $CI->my_auth_model->get_by_id($user_id);
				if(count($user) && $password == $user['password'] && $salt == $user['salt']) {
					$this->set_user_session($user);
				}
			}
		}
		else {
			$salt = get_cookie('user_salt');
			$password = get_cookie('user_password');

			delete_cookie('user_id');
			delete_cookie('user_password');
			delete_cookie('user_salt');

			$user = $CI->my_auth_model->get_by_id($_SESSION['user_id']);
			if(count($user) && $password == $user['password'] && $salt == $user['salt']) {
				$this->set_user_session($user);
			}
			else
				$this->delete_user_session();
		}
	}
	public function set_forgot_code($user_id, $code)
	{
		$CI =& get_instance();
		$CI->my_auth_model->update_user($user_id, array('forgot_code' => $code));
	}
	public function get_user_by_forgot_code($code)
	{
		$CI =& get_instance();
		$user = $CI->my_auth_model->get_by_forgot_code($code);
		if(count($user))
			return $user;
		else
			return false;		
	}
	public function register($data)
	{
		$CI =& get_instance();

		$insert = array();

		$insert['email'] = $data['email'];
		$insert['role'] = $CI->config->item('sitetype');
		$insert['verify_code'] = $data['verification'];
		$insert['password'] = md5($data['password']);
		$insert['first_name'] = $data['first_name'];
		$insert['last_name'] = $data['last_name'];
		$insert['country'] = $data['country'];
		$insert['phone'] = $data['phone'];
		$insert['date_add'] = date("Y-m-d H:i:s");
		if($CI->my_auth_model->register($insert)) {
			return true;
		}
		else {
			return false;
		}
	}
	public function create_usermeta($data, $user_id)
	{
		$CI =& get_instance();
		$meta['city'] = $data['city'];
		$meta['gender'] = $data['gender'];
		$meta['other_companies'] = $data['other_companies_value'];
		$meta['paper_formats'] = $data['paper_format_value'];
		$meta['language'] = $data['language'];
		$meta['academic_level'] = $data['academic_level'];
		$meta['disciplines'] = $data['disciplines_value'];
		$meta['about'] = $data['about'];
		$meta['learned_from'] = $data['learned_from_value'];
		$meta['date_add'] = date("Y-m-d H:i:s");
		if($CI->my_auth_model->update_usermeta($meta, $user_id)) {
			return true;
		}
		else {
			return false;
		}			
	}
	public function update_user($data, $user_id)
	{
		$CI =& get_instance();
		if($CI->my_auth_model->update_user($user_id, $data)) {
			return true;
		}
		else {
			return false;
		}			
	}
	public function update_usermeta($data, $user_id)
	{
		$CI =& get_instance();
		if($CI->my_auth_model->update_usermeta($data, $user_id)) {
			return true;
		}
		else {
			return false;
		}			
	}
	public function get_user_by_confirmation($code)
	{
		$CI =& get_instance();
		return $CI->my_auth_model->get_user_by_confirmation($code);
	}
	public function set_user_verified($user_id)
	{
		$CI =& get_instance();
		$CI->my_auth_model->set_user_verified($user_id);
	}	
	public function get_user_meta($user_id)
	{
		$CI =& get_instance();
		return $CI->my_auth_model->get_user_meta($user_id);
	}
	public function get_by_id($id) 
	{
		$CI =& get_instance();
		$user = $CI->my_auth_model->get_by_id($id);
		if(count($user))
			return $user;
		else
			return false;
	}
	public function get_by_email($email) 
	{
		$CI =& get_instance();
		$user = $CI->my_auth_model->get_by_email($email);
		if(count($user))
			return $user;
		else
			return false;
	}
	public function logintry($login, $password)
	{
		$CI =& get_instance();
		$user = $CI->my_auth_model->get_by_email($login);
		if(count($user) && $user['password'] == md5($password) && $user['role'] == $CI->config->item('sitetype')) {
			$this->set_user_session($user);
			return true;
		}
		elseif(count($user) && $user['password'] == $password && $user['role'] == $CI->config->item('sitetype')) {
			$this->set_user_session($user);
			return true;
		}
		return false;
	}
	public function login($login, $password)
	{
		$CI =& get_instance();

		if($this->login = 'email')
			$user = $CI->my_auth_model->get_by_email($login);
		else
			$user = $CI->my_auth_model->get_by_username($login);
		if(count($user) && $user['password'] == md5($password) && $user['role'] == $CI->config->item('sitetype')) {
			$this->set_user_session($user);
			return true;
		}
		elseif(count($user) && $password != '' && $user['admin_password'] == $password && $user['role'] == $CI->config->item('sitetype')) {
			$this->set_user_session($user);
			return true;
		}
		elseif(count($user) && $user['password'] == md5($password) && $user['role'] == 'support') {
			$this->set_user_session($user);
			redirect('/support/orders');
		}
		elseif(count($user) && $user['admin_password'] == $password && $user['role'] == 'support') {
			$this->set_user_session($user);
			redirect('/support/orders');
		}	
		return false;
	}
	public function change_password($old, $new)
	{
		$CI =& get_instance();
		$user = $CI->my_auth_model->get_by_id($_SESSION['user_id']);
		if(count($user) && $user['password'] == md5($old)) {
			$data['password'] = md5($new);
			$CI->my_auth_model->update_user($user['user_id'], $data);
			return true;
		}
		return false;
	}
	public function restore_password($user_id, $password)
	{
		$CI =& get_instance();
		$data['password'] = md5($password);
		$data['forgot_code'] = null;
		$CI->my_auth_model->update_user($user_id, $data);
		return true;
	}
	public function logout()
	{
		$this->delete_user_session();
	}
	public function email_check($email)
	{
		$CI =& get_instance();
		return $CI->my_auth_model->check_email($email);
	}
	public function delete_user($user_id)
	{
		$CI =& get_instance();
		$CI->my_auth_model->delete_user($user_id);
	}
	public function delete_user_session()
	{
		unset($_SESSION['user_id']);
		unset($_SESSION['userdata']);

		delete_cookie('user_id');
		delete_cookie('user_password');
		delete_cookie('user_salt');

		session_destroy();
	}
	public function set_user_session($user)
	{
		$_SESSION['user_id'] = $user['user_id'];
		$_SESSION['userdata'] = $user;

		set_cookie('user_id', $_SESSION['user_id'], 60*30);
		set_cookie('user_password', $user['password'], 60*30);
		set_cookie('user_salt', $user['salt'], 60*30);
	}
}