<?php
$lang['Main_site_settings'] = 'Основные настройки сайта';
$lang['Settings_meta'] = 'Настройки метаданных';
$lang['sitename'] = 'название сайта';
$lang['meta_keywords'] = 'ключевые слова';
$lang['meta_description'] = 'мета-описние';
$lang['Save'] = 'Сохранить';
$lang['change_admin_password'] = 'Смена админ пароля';
$lang['Old_password'] = 'Старый пароль';
$lang['New_password'] = 'Новый пароль';
$lang['Retype_password'] = 'Повтор пароля';
$lang['Change'] = 'Изменить';

$lang['admin'] = 'админка';
$lang['Main'] = 'Главная';
$lang['Articles'] = 'Статьи';
$lang['Categories'] = 'Категории';
$lang['Gallery'] = 'Галлерея';
$lang['Create_admin_seo'] = 'Создание, администрирование, продвижение сайтов';
$lang['Web_studio'] = 'Веб-студия';
$lang['Frontend'] = 'Фронтенд';
$lang['Exit'] = 'Выход';

$lang['Are_you_sure_delete_record_with_files'] = 'Вы действительно хотите удалить эту запись вместе с файлами';
$lang['Record_successful'] = 'Запись успешно удалена';
$lang['Error_with_deleting'] = 'Произошла ошибка при удалении';
$lang['Add_image'] = 'Добавить изображение';
$lang['Title'] = 'Название';
$lang['File'] = 'Файл';
$lang['Date_add'] = 'Дата добавления';
$lang['Delete'] = 'Удалить';
$lang['No_added_records'] = 'Нет добавленных избражений';

$lang['Change_file'] = 'Изменение файла';
$lang['Add_new_file'] = 'Добавить новый файл';
$lang['Return'] = 'Вернуться';
$lang['Description'] = 'Описание';
$lang['File_located_in'] = 'Файл находится в';
$lang['If_you_replace_file_select_new'] = 'Если вы хотите заменить файл, выберите новый (старый будет удален)';

$lang['Materials'] = 'Материалы';
$lang['Create_material'] = 'Создать материал';
$lang['category_filter'] = 'фильтр категории';
$lang['Elect'] = 'Избранные';
$lang['Topic'] = 'Заголовок';
$lang['State'] = 'Состояние';
$lang['Category'] = 'Категория';
$lang['Date'] = 'Дата';

$lang['Edit_material'] = 'Редактирование материала';
$lang['New_material'] = 'Новый материал';
$lang['Save_and_close'] = 'Сохранить и закрыть';
$lang['Save_copy'] = 'Сохранить копию';
$lang['Close'] = 'Закрыть';
$lang['Saved'] = 'Сохранено';
$lang['Display_options'] = 'Настройки отображения';
$lang['Alias'] = 'Алиас';
$lang['select_category'] = 'выберите категорию';
$lang['Options'] = 'Опции';
$lang['Included'] = 'Включен';
$lang['Independent'] = 'Самостоятельный';
$lang['Foreword'] = 'Предисловие';
$lang['Image_download'] = 'Загрузка изображения';
$lang['For_replace_image_select_new_old_deleted'] = 'Для замены изображения необходимо загрузить новое. Старое будет удалено.';
$lang['Error_filesize'] = 'Ошибка! Размер файла не корректый';
$lang['bytes'] = 'байт';
$lang['An_error_occurred_when_saving'] = 'Произошла ошибка при сохранении';

$lang['Edit_category'] = 'Редактирование категории';
$lang['New_category'] = 'Новая категория';
$lang['Create_category'] = 'Создать категорию';
$lang['Status'] = 'Статус';
$lang['Position'] = 'Позиция';

$lang['no_category'] = 'нет категории';
$lang['You_have_not_entered_the_username_and_password'] = 'Вы не ввели логин или пароль';
$lang['Not_true_login_or_password'] = 'Не верно введен логин или пароль';

$lang['Password_has_changed'] = 'Пароль изменен!';
$lang['Passwords_do_not_match'] = 'Введенные пароли не совпадают';
$lang['Password_is_not_correct'] = 'Пароль введен не верно';
$lang['Authorization'] = 'Авторизация';
$lang['Login'] = 'Логин';
$lang['Password'] = 'Пароль';
$lang['Enter'] = 'Вход';
$lang['For_correct_work_install_browser'] = 'Для коректной работы сайта установите браузер';