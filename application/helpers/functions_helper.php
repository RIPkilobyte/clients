<?php

Class Check extends CI_Controller {

    public static function adminlogin() {
        $CI = & get_instance();
        if (@$CI->session->userdata('role') == 'superuser') {
            $CI->load->model("adminusers_model");
            $user = $CI->adminusers_model->get($CI->session->userdata('login'));
            
            if (($user != FALSE) && ($CI->session->userdata('password') == $user['password'])) 
            {       
                return NULL;
            }
        }
    
        redirect($CI->config->base_url() . 'admin/login', 'refresh');
        exit();
    }
    public static function login()
    {
        if(isset($_SESSION['user_id']))
            return true;
        return false;
    }
    public static function admin()
    {
        if(isset($_SESSION['user_id'])) {
            if($_SESSION['userdata']['role'] == 'admin')
                return true;
            return false;
        }
        return false;
    }
    public static function support()
    {
        if(isset($_SESSION['user_id'])) {
            if($_SESSION['userdata']['role'] == 'support')
                return true;
            return false;
        }
        return false;
    }   
    public static function check_unread_news()
    {
        $CI = & get_instance();
        $CI->load->model("user_model");
        return $CI->user_model->check_unread_news($_SESSION['user_id']);
    }
    public static function check_user_status()
    {
        if(isset($_SESSION['user_id'])) {
            return $_SESSION['userdata']['status'];
        }
        return false;
    }    
}

Class Mail {

    function Send($from, $to, $theme, $body) {
        
        $this->email->from($from, 'Support Writingfreelance');
        $this->email->to($to);
        $this->email->cc('');
        $this->email->bcc('');

        $this->email->subject($theme);
        $this->email->message($body);

        $this->email->send();

        return $this->email->print_debugger();
    }
    function text_title($type, $data = array())
    {
        $CI = & get_instance();
        switch ($type) {
            case 'professor_register':
                $text = 'Thank you for registering!';
            break;
            case 'professor_receive_payment':
                $text = 'We have received your payment!';
            break;
            case 'professor_writer_assign':
                $text = 'We have found a writer for your order!';
            break;
            case 'professor_writer_finished':
                $text = 'The writer has finished his work!';
            break;
            case 'professor_request_revision':
                $text = 'You have requested a revision for your paper!';
            break;
            case 'professor_cancel_order':
                $text = 'You have successfully cancelled your order!';
            break;
            case 'professor_approve_order':
                $text = 'You can download the final version of your order!';
            break;
            case 'professor_writer_commented':
                $text = 'The writer has left some comments regarding to your order!';
            break;
            case 'professor_support_commented':
                $text = 'The support team has left some comments regarding to your order!';
            break;
            case 'professor_change_password':
                $text = 'You have changed your password!';
            break;
            case 'professor_forgot_password':
                $text = 'Forgot password';
            break; 
            case 'professor_place_order':
                $text = 'Thank you for placing an order!';
            break;
            case 'professor_found_writer':
                $text = 'We found a writer!';
            break;

            case 'client_register'://w
                $text = 'Thank you for registering!';
            break;
            case 'client_place_order'://w
                $text = 'Thank you for placing an order!';
            break;
            case 'client_receive_payment'://w
                $text = 'We have received your payment!';
            break;
            case 'client_writer_assign'://w
                $text = 'We have found a writer for your order!';
            break;
            case 'client_writer_finished'://w
                $text = 'The writer has finished his work!';
            break;
            case 'client_request_revision'://w
                $text = 'You have requested a revision for your paper!';
            break;
            case 'client_cancel_order'://w
                $text = 'You have successfully cancelled your order!';
            break;
            case 'client_approve_order'://w
                $text = 'You can download the final version of your order!';
            break;
            case 'client_found_writer'://w
                $text = 'We found a writer!';
            break;
            case 'client_writer_commented'://w
                $text = 'The writer has left some comments regarding to your order!';
            break;
            case 'client_support_commented'://w
                $text = 'The support team has left some comments regarding to your order!';
            break;
            case 'client_change_password'://w
                $text = 'You have changed your password!';
            break;
            case 'client_forgot_password':
                $text = 'Forgot password';
            break; 
            case 'writer_register'://w
                $text = 'Thank you for registering!';
            break;
            case 'writer_all_files_uploaded'://w
                $text = 'Thank you for uploading files!';
            break;
            case 'writer_files_confirm_ok'://w
                $text = 'Your files have been confirmed!';
            break;
            case 'writer_files_confirm_fail'://w
                $text = 'Your files haven&rsquo;t been accepted...';
            break;
            case 'writer_test_complete'://w
                $text = 'You have completed the test!';
            break;
            case 'writer_test_complete_ok'://w
                $text = 'You have passed the test!';
            break;
            case 'writer_test_complete_fail'://w
                $text = 'You have failed the test...';
            break;
            case 'writer_email_verification_code'://w
                $text = 'Email verification code';
            break;
            case 'writer_email_verification_ok'://w
                $text = 'Email address has been verified!';
            break;
            case 'writer_apply_case'://w
                $text = 'You have applied for a case #'.$data['order_id'];
            break;
            case 'writer_assigned_to_case'://w
                $text = 'You have been assigned to the case #'.$data['order_id'].'!';
            break;
            case 'writer_support_reject_file'://w
                $text = 'You have has been rejected #'.$data['order_id'].'!';
            break;            
            case 'writer_customer_request_revision'://w
                $text = 'The customer requested a revision';
            break;
            case 'writer_half_time_deadline':
                $text = 'Half of the deadline left for the order #'.$data['order_id'];
            break;
            case 'writer_two_hours_deadline':
                $text = 'Only two hours left till the deadline of the order #'.$data['order_id'].'!';
            break;
            case 'writer_order_cancelled'://w
                $text = 'The order #'.$data['order_id'].' has been canceled!';
            break;
            case 'writer_customer_approved'://w
                $text = 'The customer has approved the order #'.$data['order_id'].'!';
            break;
            case 'writer_choose_paypal':
                $text = 'You have chosen the payment method!';
            break;
            case 'writer_choose_payoner':
                $text = 'You have chosen the payment method!';
            break;
            case 'writer_choose_payment_method':
                $text = 'Don&rsquo;t forget to choose the payment method!';
            break;
            case 'writer_change_password'://w
                $text = 'You have changed your password!';
            break;
            case 'writer_support_commented':
                $text = 'The support team has left some comments regarding to the order!';
            break;
            case 'writer_client_commented'://w
                $text = 'The customer has left some comments regarding to the order!';
            break; 
            case 'writer_forgot_password':
                $text = 'Forgot password';
            break; 
            case 'writer_dismiss_order':
                $text = 'Dismiss from assigned order';
            break;
            case 'support_client_place_order'://w
                $text = 'You have an order!';
            break;
            default:
                $text = '';
            break;
        }
        return $text;
    }
    function text_body($type, $data)
    {
        $CI = & get_instance();
        switch ($type) {
            case 'professor_register'://e
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Thank you for registering with Professor Essay! We hope you would enjoy our online academic services.<br><br>';
                $text .= 'The information that you provided during the registration:<br>';
                $text .= 'User&rsquo;s email: <b><i>'.$data['email'].'</b></i><br>';
                $text .= 'Password: <b><i>'.$data['password'].'</b></i><br>';
                $text .= 'Please use this link to login - <a href="'.$CI->config->item('professor_base_url').'user/login">'.$CI->config->item('professor_base_url').'user/login</a><br><br>';
                $text .= 'By signing into your account at Professor-Essays.com, you will be able to track your order&rsquo;s status and communicate directly with the writer and our Support team. We will upload the final draft of your order onto your account, which would be available for you to download.<br><br>';
                $text .= 'You must approve the writers&rsquo; work before you can download the final draft of your order. Please make sure that email and the contact number you provided are valid.<br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page (Please make the word Page a hyperlink to FAQ page <a href="'.$CI->config->item('professor_base_url').'faq">'.$CI->config->item('professor_base_url').'faq</a>) or contact our support team via email – <a href="mailto:'.$CI->config->item('professor_support_email').'">'.$CI->config->item('professor_support_email').'</a>';
            break;
            case 'professor_receive_payment':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We have successfully received your payment! Thank you for purchasing your paper at Professor Essay!<br><br>';
                $text .= 'Your order&rsquo;s number is <b>'.$data['order_id'].'</b>. We will notify you via email when an appropriate writer is assigned to your paper. Now, you can log into your personal user&rsquo;s panel where you will be able to track your order&rsquo;s status and communicate directly with the writer and our Support team. Please use this link to login – <a href="'.$CI->config->item('professor_base_url').'user/login">'.$CI->config->item('professor_base_url').'user/login</a>.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'professor_writer_assign':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Just to let you know that we are working on your order #<b>'.$data['order_id'].'</b>!<br><br>';
                $text .= 'Please use this link to monitor the progress of your order, and to communicate with your writer directly:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'You will be notified via email if the writer has some questions related to this order; and also when he finalizes the work.<br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;
            case 'professor_writer_finished':
                $text = 'Hello '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your order #<b>'.$data['order_id'].'</b> is ready for a preview! Now you can view the prepared paper in a preview mode on our website.<br><br>';
                $text .= 'Please use this link to see the prepared paper in a preview mode:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'In order for you to be able to download the final document you must approve it. However, you can also ask for revisions if you are not satisfied with the paper that the writer has prepared by clicking on the "Revision" button in the order&rsquo;s menu. Further instructions will be provided.<br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;
            case 'professor_request_revision':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have successfully requested a revision stating the following reason "<i>'.$data['reason'].'</i>". <br><br>You added '.$data['hours'].' hours to your deadline. <br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break; 
            case 'professor_cancel_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have successfully cancelled your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Thank you for ordering from us, we look forward to your next order.<br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;
            case 'professor_approve_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your have approved the order #<b>'.$data['order_id'].'</b>! Now you can download the final editable document from the order&rsquo;s panel. Thank You for choosing Professor-Essay and we hope to see you again!<br><br>';
                $text .= 'Please use this link to download the final document:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'ajax/get_order_file/'.$data['order_id'].'">Download</a><br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;
            case 'professor_writer_commented':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to notify you that the writer has made some comments regarding to your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Please use this link to see the comments or files uploaded:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'">order details</a><br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;       
            case 'professor_support_commented':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to inform you that the Support Team has left some comments regarding your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Please use this link to see the comments or files uploaded:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'">order details</a><br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;
            case 'professor_change_password':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have just changed you password at Professor-Essays.com.<br><br>';
                $text .= 'These are the details of your account:<br>';
                $text .= 'Email: '.$data['email'].'<br>';
                $text .= 'Password: '.$data['password'].'<br>';
                $text .= 'Please use this link to login – <a href="'.$CI->config->item('professor_base_url').'user/login/">Login</a><br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;
            case 'professor_forgot_password':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'To restore your password, please follow this <a href="'.$data['link'].'">link</a> or copy this link '.$data['link'].' to the adress bar';
            break;
            case 'professor_place_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Thank you for placing an order on Professor-Essays.com! We hope you would enjoy our online services.<br><br>';
                $text .= 'The details that you provided during placing the Order #<b>'.$data['order_id'].'</b>:<br>';

                $text .= 'Subject: <i>'.$data['subject_title'].'</i><br>';
                $text .= 'Topic: <i>'.$data['topic'].'</i><br>';
                $text .= 'Type of paper: <i>'.$data['paper_type_title'].'</i><br>';
                $text .= 'Academic level: <i>'.$data['academic_level_title'].'</i><br>';
                $text .= 'Number of pages: <i>'.$data['pages'].'</i><br>';
                if(isset($data['slides']) && $data['slides'])
                    $text .= 'Number of slides: <i>'.$data['slides'].'</i><br>';
                if(isset($data['questions']) && $data['questions'])
                    $text .= 'Number of questions: <i>'.$data['questions'].'</i><br>';
                if(isset($data['problems']) && $data['problems'])
                    $text .= 'Number of problems: <i>'.$data['problems'].'</i><br>';
                $text .= 'Deadline: '.$data['deadline_title'].'<br><br>';

                $text .= 'Please use this link to monitor the progress of your order:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('professor_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('professor_base_url').'faq">'.$CI->config->item('professor_base_url').'faq</a> or contact our support team via email <a href="mailto:'.$CI->config->item('professor_support_email').'">'.$CI->config->item('professor_support_email').'</a>.';
            break;
            case 'professor_found_writer':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We have found a suitable writer, who would be able to complete your order #<b>'.$data['order_id'].'</b>! As soon as you make a payment, the writer will start working on your order.<br><br>';
                $text .= 'Please use this link to make the payment:<br>';
                $text .= '<a href="'.$CI->config->item('professor_base_url').'cart/checkout/'.$data['order_id'].'">payment</a><br><br>';
                $text .= 'If you have some questions, please contact our Support Team.';
            break;

            case 'client_register'://e
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Thank you for registering with Eagle-Essays.com! We hope you would enjoy our online services.<br><br>';
                $text .= 'The information that you provided during the registration:<br><br>';
                $text .= 'User&rsquo;s email: <b><i>'.$data['email'].'</b></i><br>';
                $text .= 'Password: <b><i>'.$data['password'].'</b></i><br>';
                $text .= 'Please use this link to login - <a href="'.$CI->config->item('client_base_url').'user/login">'.$CI->config->item('client_base_url').'user/login</a><br><br>';
                $text .= 'By signing into your account at Eagle-Essays.com, you will be able to track your order&rsquo;s status and communicate directly with the writer and our Support team. We will upload the final draft of your order onto your account, which would be available for you to download.<br><br>';
                $text .= 'You must approve the writer&rsquo;s work before you can download the final draft of your order. Please make sure that email and the contact number you provided are correct.<br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_place_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Thank you for placing an order on Eagle-Essays.com! We hope you would enjoy our online writing services.<br><br>';
                $text .= 'These are the details that you provided upon placing the Order #<b>'.$data['order_id'].'</b>:<br>';

                $text .= 'Subject: <i>'.$data['subject_title'].'</i><br>';
                $text .= 'Topic: <i>'.$data['topic'].'</i><br>';
                $text .= 'Type of paper: <i>'.$data['paper_type_title'].'</i><br>';
                $text .= 'Academic level: <i>'.$data['academic_level_title'].'</i><br>';
                $text .= 'Number of pages: <i>'.$data['pages'].'</i><br>';
                if(isset($data['slides']) && $data['slides'])
                    $text .= 'Number of slides: <i>'.$data['slides'].'</i><br>';
                if(isset($data['questions']) && $data['questions'])
                    $text .= 'Number of questions: <i>'.$data['questions'].'</i><br>';
                if(isset($data['problems']) && $data['problems'])
                    $text .= 'Number of problems: <i>'.$data['problems'].'</i><br>';
                $text .= 'Deadline: '.$data['deadline_title'].'<br><br>';

                $text .= 'Please use this link to monitor the progress of your order:<br>';
                $text .= '<a href="'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_receive_payment':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We have successfully received your payment! Thank you for purchasing your paper at Eagle-Essays.com.<br><br>';
                $text .= 'Your order&rsquo;s number is <b>'.$data['order_id'].'</b>. We will notify you via email when an appropriate writer is assigned to your case. Now, you can log into your personal user&rsquo;s panel where you will be able to track your order&rsquo;s status and communicate directly with the writer and our Support team. Please use this link to login - <a href="'.$CI->config->item('client_base_url').'user/login">'.$CI->config->item('client_base_url').'user/login</a>.<br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_writer_assign':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We have found a writer, who can complete your order #<b>'.$data['order_id'].'</b>!<br><br>';
                $text .= 'Please use this link to monitor the progress of your order:<br>';
                $text .= '<a href="'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'You will be notified via email if the writer has some questions related to this order; and also when he finalizes the work.<br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_writer_finished':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your order #<b>'.$data['order_id'].'</b> is ready for a preview! Now you can view the submitted paper in a preview mode at your account.<br><br>';
                $text .= 'Please, follow this link to see the paper in a preview mode: ';
                $text .= '<a href="'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'">Order details</a><br><br>';
                $text .= 'In order to be able to download the final version you must approve it. However, you can also request up to 3 revisions if you are not satisfied with the paper that the writer has prepared by clicking the “Revision” button at your account. Further instructions will be provided.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team';
            break;
            case 'client_request_revision':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have successfully requested a revision stating the following reason "<i>'.$data['reason'].'</i>". <br><br>You added '.$data['hours'].' hours to your deadline. <br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break; 
            case 'client_cancel_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have successfully cancelled your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Thank you for ordering from us, we look forward to your next order.<br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_approve_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your have approved the order #<b>'.$data['order_id'].'</b>! Now you can download the final editable document from the order&rsquo;s panel.<br><br>';
                $text .= 'Please use this link to download the final document:<br>';
                $text .= '<a href="'.$CI->config->item('client_base_url').'ajax/get_order_file/'.$data['order_id'].'">Download</a><br><br>';
                $text .= 'If you have any questions, please contact our Support Team';
            break;
            case 'client_found_writer':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We have found a suitable writer, who would be able to complete your order #<b>'.$data['order_id'].'</b>! As soon as you make a payment, the writer will start working on your order.<br><br>';
                $text .= 'Please use this link to make the payment:<br>';
                $text .= '<a href="'.$CI->config->item('client_base_url').'cart/checkout/'.$data['order_id'].'">payment</a><br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_writer_commented':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to notify you that the writer has made some comments regarding to your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Please use this link to see the comments:<br>';
                $text .= '<a href="'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'">order details</a><br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;       
            case 'client_support_commented':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to inform you that the Support Team has left some comments regarding your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Please use this link to see the comments:<br>';
                $text .= '<a href="'.$CI->config->item('client_base_url').'user/order_details/'.$data['order_id'].'">order details</a><br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_change_password':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have just changed you password at Eagle-Essays.com.<br><br>';
                $text .= 'These are the details of your account:<br>';
                $text .= 'Email: '.$data['email'].'<br>';
                $text .= 'Password: '.$data['password'].'<br>';
                $text .= 'Please use this link to login – <a href="'.$CI->config->item('client_base_url').'user/login/">Login</a><br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('client_base_url').'faq-top-essays">'.$CI->config->item('client_base_url').'faq-top-essays</a> or contact our Support Team';
            break;
            case 'client_forgot_password'://e
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'To restore your password, please follow this <a href="'.$data['link'].'">link</a> or copy this link '.$data['link'].' to the adress bar';
            break;
            case 'writer_register':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Congratulations! You have completed the first stage of your registration at Writer-Center.com. Nonetheless, to evaluate your writing skills and English language proficiency you will have to prepare a sample essay. In order to confirm your identity, you will have to upload a photocopy of your identity document alongside with a photocopy or E-version of your qualification certificate. Please, upload these documents as soon as possible to accelerate your approval process.<br><br>';
                $text .= 'If you have any questions or concerns, please visit our FAQ page - <a href="'.$CI->config->item('writer_base_url').'faq-writing-jobs">'.$CI->config->item('writer_base_url').'faq-writing-jobs</a> or contact our support team via email – <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.<br><br>';
                $text .= 'The information that you provided during the registration:<br><br>';
                $text .= 'User&rsquo;s email: <b><i>'.$data['email'].'</b></i><br>';
                $text .= 'Password: <b><i>'.$data['password'].'</b></i><br><br>';
                $text .= 'Please use this link to login - <a href="'.$CI->config->item('writer_base_url').'user/login">'.$CI->config->item('writer_base_url').'user/login</a>';
            break;
            case 'writer_all_files_uploaded':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Thank you for uploading the required documents! We promise that we will check them within 10 days. You will be notified via email about your further progression.<br><br>';
                $text .= 'As the next step of becoming a freelance writer at Writer-Center is passing a grammar and formatting styles test, we strongly advise you to visit our <a href="'.$CI->config->item('writer_base_url').'helpful-links">Useful Links</a> page. There you will be able to enrich your writing skills and learn about main requirements of different citation styles.<br><br>';
                $text .= 'If you have any questions, please do not hesitate and contact our support team via email <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.';
            break; 
            case 'writer_files_confirm_ok'://e
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Congratulations! All your uploaded documents met our criteria! Now you will have to complete an online grammar and formatting styles test which will assess your versatile knowledge of English language. Bear in mind, that you will have a 1-hour and only one chance to complete the test.<br><br>';
                $text .= 'To begin the test, please login to your personal user`s panel <a href="'.$CI->config->item('writer_base_url').'user/login">'.$CI->config->item('writer_base_url').'user/login</a> and press the <b>"Begin testing"</b> button.<br><br>';
                $text .= 'If you encounter any technical difficulties, please contact our support team.';
            break;
            case 'writer_files_confirm_fail':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Thank you for taking the time to upload all necessary files. Unfortunately, the documents you provided have not been accepted. As a result, we will not be able to process your application further. However, you may apply again after 12 months.<br><br>';
                $text .= 'If you have any concerns or queries, please do not hesitate to email our support team on <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.';
            break;
            case 'writer_test_complete':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to congratulate you in completing our test! We promise that we will check all the answers that you provided within 10 days from this moment. You will be notified via email about further progression of your application.<br><br>';
                $text .= 'If you require any support, you can email us on <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.<br><br>';
                $text .= 'We look forward to working with you.';
            break;
            case 'writer_test_complete_ok':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to congratulate you in successfully passing our test! Now, you are officially qualified as a freelance writer at Writer-Center.com.<br><br>';
                $text .= '<b>What next...</b><br><br>';
                $text .= 'Firstly, you will have to verify your email address. In order to do that, you will have to login into your personal user&rsquo;s panel <a href="'.$CI->config->item('writer_base_url').'user/login">'.$CI->config->item('writer_base_url').'user/login</a> and click on the "Verify email address" button. Then, all the necessary instructions will be provided.<br><br>';
                $text .= 'Afterwards, you will be able to access the "Available Orders" page.  On this page, you will find a list of detailed cases. Select the case that suits you and apply by writing "why you should be chosen". Once your application for the case has been selected you can start writing. Finally, you will have to submit the case within the given time and receive your payment. Do not worry, we will notify you about the current statuses of the orders via email.<br><br>';
                $text .= 'If you require any support, you can email us on <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.<br><br>';
                $text .= 'We look forward to working with you.';
            break;
            case 'writer_test_complete_fail':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Unfortunately, you have failed to pass our assessment process, and therefore, we will not be proceeding with your application at this point.<br><br>';
                $text .= 'Please do not be discouraged as you can reapply in 12 months. In order to increase your chances of passing the test, we suggest you to read our <a href="'.$CI->config->item('writer_base_url').'helpful-links">Useful Links</a> page, which could help develop your writing skills.<br><br>';
                $text .= 'If you have any concerns or queries, please do not hesitate to email our support team on <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.<br><br>';
                $text .= 'We look forward to working with you in the future.';
            break;    
            case 'writer_email_verification_code':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your email verification code is: <br><br><b><i>'.$data['verify_code'].'</b></i><br><br>';
                $text .= 'Please enter this code into the box in your personal user`s panel <a href="'.$CI->config->item('writer_base_url').'verification.html">'.$CI->config->item('writer_base_url').'verification.html</a>.<br><br>';
                $text .= 'If you face any problems or difficulties, please contact our Support Team via email <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.<br><br>';
                $text .= 'We look forward to working with you.';
            break;
            case 'writer_email_verification_ok':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your email address has been verified! Now you are fully registered.<br><br>';
                $text .= 'If you require any support, you can email us at <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>.<br><br>';
                $text .= 'We look forward to working with you.';
            break;
            case 'writer_apply_case':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have applied for the case #<b>'.$data['order_id'].'</b> stating the reason why you should be chosen – <i>'.$data['reason'].'</i>. We will notify you via email if you are assigned to complete this paper.<br><br>';
                $text .= 'If you have any questions, please contact our support team.';
            break;
            case 'writer_assigned_to_case':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have been assigned to the case #<b>'.$data['order_id'].'</b>. You have <b>'.$data['timeleft'].'</b> hours left to complete this case (from the me this message was sent). You will receive $<b>'.$data['amount'].'</b> if you successfully finish this paper on time without any major mistakes.<br><br>';
                $text .= 'Please, follow this link and confirm that you have started working on this case:<a href="'.$CI->config->item('writer_base_url').'order/order_details/'.$data['order_id'].'">'.$CI->config->item('writer_base_url').'order/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= '<b>Please make sure that you’ve pressed the CONFIRM BUTTON within 30 minutes from the time this message was sent.</b><br><br>';
                $text .= 'Please note that we can re-allocate the order shall you fail to confirm it within 30 minutes.<br><br>';
                $text .= 'If you have any questions, please contact our support team.';
                $text .= 'Kind regards,<br>';
                $text .= 'Support Team';
            break;
            case 'writer_support_reject_file'://w
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'The final paper you uploaded for the order #<b>'.$data['order_id'].'</b> has been rejected by our support team. Please revise the report that the support team has uploaded for you in your account, correct all the mistakes and upload you corrected file ASAP.<br>';
                $text .= 'Please, follow this link to view the report: <a href="'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'">Order details</a><br><br>';
                $text .= 'If you have any questions, please contact our support team.<br><br>';
                $text .= 'Kind regards,<br>';
                $text .= 'Customer Support<br>';
                $text .= 'Writer-Center.com';
            break;            
            case 'writer_customer_request_revision':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'The customer has requested a revision for the paper that you prepared. These are the details that the customer has provided: <i>'.$data['reason'].'</i>.<br>';
                $text .= 'You have '.$data['hours'].' extra hours to revise the work.<br><br>';
                $text .= 'Please, follow this link and confirm that you are able to revise your work: <a href="'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'If you have any questions, please contact our support team.';
            break;
            case 'writer_half_time_deadline':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to notify you that half of the deadline for the order #<b>'.$data['order_id'].'</b> has passed. Now you have '.$data['timeleft'].' left to complete this case. Please make sure that you will finish it before the time runs out.<br><br>';
                $text .= 'If you are facing some problems or just have some questions, please contact our Support Team.';
            break;
            case 'writer_two_hours_deadline':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to inform, that you have only two hours to complete order #<b>'.$data['order_id'].'</b>. Please make sure that you will manage to upload it onto the order&rsquo;s personal panel before the time runs out. Otherwise, please contact our Support Team for further instructions.<br><br>';
                $text .= 'If you are facing some problems or just have some questions, please contact our Support Team.';
            break;
            case 'writer_order_cancelled':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to inform you that the customer has canceled the order #<b>'.$data['order_id'].'</b>, please stop working on this order.<br><br>';
                $text .= 'You will be paid according to our <a href="'.$CI->config->item('writer_base_url').'materials">Terms of Use</a> regulations.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_customer_approved':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to inform you that the customer has approved the paper that you have prepared for order #<b>'.$data['order_id'].'</b>.<br><br>';
                //$text .= 'You will shortly be paid <b>$'.$data['amount'].'</b> into your account. Note, that this amount could be smaller if you made some minor or major mistakes during the order. More information about the fines could be found at our <a href="'.$CI->config->item('writer_base_url').'materials">Terms of Use</a> page.<br><br>';
                $text .= 'Please take note of this order and have it included in your Payment Statistics that you would need to accomplish and submit by the end of the month so as to receive your payment by the 1st or 2nd day of the following month., Note, that this amount could be smaller if you made some minor or major mistakes during the order. More information about the fines could be found at our Terms of Use page.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_choose_paypal':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to inform you that you have chosen the PayPal payment method to receive the funds from Writer-Center. You have provided this email: '.$data['paypal_email'].'.<br><br>';
                $text .= 'Note, that the payments are processed and transferred to the freelance writers between the 25th and 30th of each month.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_choose_payoner':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to inform you that you have chosen the Payoneer payment method to receive the funds from Writer-Center.<br><br>';
                $text .= 'Note, that the payments are processed and transferred to the freelance writers between the 25th and 30th of each month.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_choose_payment_method':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We want to inform you that you have to choose a payment method to receive the funds from Writer-Center. At this point we offer two different options – PayPal and Payoneer. Please visit our <a href="'.$CI->config->item('writer_base_url').'payments.html">Payments</a> page to find out more about these payment options.<br><br>';
                $text .= 'Note, that the payments are processed and transferred to the freelance writers between the 25th and 30th of each month.<br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_change_password':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'You have just changed you password at Writer-Center.com.<br><br>';
                $text .= 'These are the details of your account:<br><br>';
                $text .= 'Email: <b><i>'.$data['email'].'</b></i><br>';
                $text .= 'Password: <b><i>'.$data['password'].'</b></i><br><br>';
                $text .= 'Please use this link to login - <a href="'.$CI->config->item('writer_base_url').'user/login">'.$CI->config->item('writer_base_url').'user/login</a><br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_support_commented':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to inform you that the Support Team has left some comments regarding your order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Please use this link to see the comments:<br>';
                $text .= '<a href="'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_client_commented':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'We would like to notify you that the customer has made some comments regarding to the order #<b>'.$data['order_id'].'</b>.<br><br>';
                $text .= 'Please use this link to see the comments:<br>';
                $text .= '<a href="'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'">'.$CI->config->item('writer_base_url').'user/order_details/'.$data['order_id'].'</a><br><br>';
                $text .= 'If you have any questions, please contact our Support Team.';
            break;
            case 'writer_forgot_password'://e
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'To restore your password, please follow this <a href="'.$data['link'].'">link</a> or copy this link '.$data['link'].' to the adress bar';
            break;
            case 'writer_dismiss_order':
                $text = 'Dear '.$data['first_name'].' '.$data['last_name'].',<br><br>';
                $text .= 'Your assignment to the order #'.$data['order_id'].' was cancelled. The reason for the cancellaon will be explained to you shortly in a separate lerer.<br><br>';
                $text .= 'Please do not proceed working on this order as you will be not paid for that work.<br><br>';
                $text .= 'If you have any questions, please contact our support team directly on <a href="mailto:'.$CI->config->item('writer_support_email').'">'.$CI->config->item('writer_support_email').'</a>';
                $text .= 'Kind regards,<br>';
                $text .= 'Customer Support<br>';                
            break;            
            case 'support_client_place_order':
                $text = 'Please review order details:<br>';
                $text .= 'Subject: <i>'.$data['subject_title'].'</i><br>';
                $text .= 'Topic: <i>'.$data['topic'].'</i><br>';
                $text .= 'Type of paper: <i>'.$data['paper_type_title'].'</i><br>';
                $text .= 'Academic level: <i>'.$data['academic_level_title'].'</i><br>';
                $text .= 'Number of pages: <i>'.$data['pages'].'</i><br>';
                if(isset($data['slides']) && $data['slides'])
                    $text .= 'Number of slides: <i>'.$data['slides'].'</i><br>';
                if(isset($data['questions']) && $data['questions'])
                    $text .= 'Number of questions: <i>'.$data['questions'].'</i><br>';
                if(isset($data['problems']) && $data['problems'])
                    $text .= 'Number of problems: <i>'.$data['problems'].'</i><br>';
                $text .= 'Deadline: <i>'.$data['deadline_title'].'</i><br><br>';                
            break; 
            default:
                $text = '';
            break;
        }
        return $text;
    }
    function prepare($type, $data)
    {
        $CI = & get_instance();
        $to = $data['email'];
        
        $data['hash_email'] = md5($data['email'].time());
        $CI->load->model('user_model');
        $status = false;
        $client = false;
        $writer = false;
        $professor = false;
        switch ($type) {
            case 'client_register'://e
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_register', $data),
                    'text' => Mail::text_body('client_register', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_register', $data);
                $data['text'] = Mail::text_body('client_register', $data);
            break;
            case 'client_place_order'://e
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_place_order', $data),
                    'text' => Mail::text_body('client_place_order', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_place_order', $data);
                $data['text'] = Mail::text_body('client_place_order', $data);
            break;
            case 'client_receive_payment':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_receive_payment', $data),
                    'text' => Mail::text_body('client_receive_payment', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_receive_payment', $data);
                $data['text'] = Mail::text_body('client_receive_payment', $data);
            break;
            case 'client_writer_assign':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_writer_assign', $data),
                    'text' => Mail::text_body('client_writer_assign', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_writer_assign', $data);
                $data['text'] = Mail::text_body('client_writer_assign', $data);
            break;
            case 'client_writer_finished':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_writer_finished', $data),
                    'text' => Mail::text_body('client_writer_finished', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_writer_finished', $data);
                $data['text'] = Mail::text_body('client_writer_finished', $data);
            break;
            case 'client_request_revision':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_request_revision', $data),
                    'text' => Mail::text_body('client_request_revision', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_request_revision', $data);
                $data['text'] = Mail::text_body('client_request_revision', $data);
            break;
            case 'client_cancel_order':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_cancel_order', $data),
                    'text' => Mail::text_body('client_cancel_order', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_cancel_order', $data);
                $data['text'] = Mail::text_body('client_cancel_order', $data);
            break;
            case 'client_approve_order':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_approve_order', $data),
                    'text' => Mail::text_body('client_approve_order', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_approve_order', $data);
                $data['text'] = Mail::text_body('client_approve_order', $data);
            break;
            case 'client_found_writer':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_found_writer', $data),
                    'text' => Mail::text_body('client_found_writer', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_found_writer', $data);
                $data['text'] = Mail::text_body('client_found_writer', $data);
            break;
            case 'client_writer_commented':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_writer_commented', $data),
                    'text' => Mail::text_body('client_writer_commented', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_writer_commented', $data);
                $data['text'] = Mail::text_body('client_writer_commented', $data);
            break;
            case 'client_support_commented':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_support_commented', $data),
                    'text' => Mail::text_body('client_support_commented', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_support_commented', $data);
                $data['text'] = Mail::text_body('client_support_commented', $data);
            break;
            case 'client_change_password':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_change_password', $data),
                    'text' => Mail::text_body('client_change_password', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_change_password', $data);
                $data['text'] = Mail::text_body('client_change_password', $data);
            break;
            case 'client_forgot_password':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('client_forgot_password', $data),
                    'text' => Mail::text_body('client_forgot_password', $data),
                ));
                $status = true;
                $client = true;
                $data['title'] = Mail::text_title('client_forgot_password', $data);
                $data['text'] = Mail::text_body('client_forgot_password', $data);
            break;
            case 'writer_register':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_register', $data),
                    'text' => Mail::text_body('writer_register', $data),
                ));
                $data['title'] = Mail::text_title('writer_register', $data);
                $data['text'] = Mail::text_body('writer_register', $data);
            break;
            case 'writer_all_files_uploaded':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_all_files_uploaded', $data),
                    'text' => Mail::text_body('writer_all_files_uploaded', $data),
                ));
                $data['title'] = Mail::text_title('writer_all_files_uploaded', $data);
                $data['text'] = Mail::text_body('writer_all_files_uploaded', $data);
            break;
            case 'writer_files_confirm_ok'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_files_confirm_ok', $data),
                    'text' => Mail::text_body('writer_files_confirm_ok', $data),
                ));
                $data['title'] = Mail::text_title('writer_files_confirm_ok', $data);
                $data['text'] = Mail::text_body('writer_files_confirm_ok', $data);
            break;
            case 'writer_files_confirm_fail'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_files_confirm_fail', $data),
                    'text' => Mail::text_body('writer_files_confirm_fail', $data),
                ));
                $data['title'] = Mail::text_title('writer_files_confirm_fail', $data);
                $data['text'] = Mail::text_body('writer_files_confirm_fail', $data);
            break;
            case 'writer_test_complete':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_test_complete', $data),
                    'text' => Mail::text_body('writer_test_complete', $data),
                ));
                $data['title'] = Mail::text_title('writer_test_complete', $data);
                $data['text'] = Mail::text_body('writer_test_complete', $data);
            break;
            case 'writer_test_complete_ok'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_test_complete_ok', $data),
                    'text' => Mail::text_body('writer_test_complete_ok', $data),
                ));
                $data['title'] = Mail::text_title('writer_test_complete_ok', $data);
                $data['text'] = Mail::text_body('writer_test_complete_ok', $data);
            break;
            case 'writer_test_complete_fail'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_test_complete_fail', $data),
                    'text' => Mail::text_body('writer_test_complete_fail', $data),
                ));
                $data['title'] = Mail::text_title('writer_test_complete_fail', $data);
                $data['text'] = Mail::text_body('writer_test_complete_fail', $data);
            break;
            case 'writer_email_verification_code'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_email_verification_code', $data),
                    'text' => Mail::text_body('writer_email_verification_code', $data),
                ));
                $data['title'] = Mail::text_title('writer_email_verification_code', $data);
                $data['text'] = Mail::text_body('writer_email_verification_code', $data);
            break;
            case 'writer_email_verification_ok'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_email_verification_ok', $data),
                    'text' => Mail::text_body('writer_email_verification_ok', $data),
                ));
                $data['title'] = Mail::text_title('writer_email_verification_ok', $data);
                $data['text'] = Mail::text_body('writer_email_verification_ok', $data);
            break;
            case 'writer_apply_case':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_apply_case', $data),
                    'text' => Mail::text_body('writer_apply_case', $data),
                ));
                $data['title'] = Mail::text_title('writer_apply_case', $data);
                $data['text'] = Mail::text_body('writer_apply_case', $data);
            break;
            case 'writer_assigned_to_case':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_assigned_to_case', $data),
                    'text' => Mail::text_body('writer_assigned_to_case', $data),
                ));
                $data['title'] = Mail::text_title('writer_assigned_to_case', $data);
                $data['text'] = Mail::text_body('writer_assigned_to_case', $data);
            break;
            case 'writer_support_reject_file':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_support_reject_file', $data),
                    'text' => Mail::text_body('writer_support_reject_file', $data),
                ));
                $data['title'] = Mail::text_title('writer_support_reject_file', $data);
                $data['text'] = Mail::text_body('writer_support_reject_file', $data);
            break;
            case 'writer_customer_request_revision':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_customer_request_revision', $data),
                    'text' => Mail::text_body('writer_customer_request_revision', $data),
                ));
                $data['title'] = Mail::text_title('writer_customer_request_revision', $data);
                $data['text'] = Mail::text_body('writer_customer_request_revision', $data);
            break;
            case 'writer_half_time_deadline':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_half_time_deadline', $data),
                    'text' => Mail::text_body('writer_half_time_deadline', $data),
                ));
                $data['title'] = Mail::text_title('writer_half_time_deadline', $data);
                $data['text'] = Mail::text_body('writer_half_time_deadline', $data);
            break;
            case 'writer_two_hours_deadline':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_two_hours_deadline', $data),
                    'text' => Mail::text_body('writer_two_hours_deadline', $data),
                ));
                $data['title'] = Mail::text_title('writer_two_hours_deadline', $data);
                $data['text'] = Mail::text_body('writer_two_hours_deadline', $data);
            break;
            case 'writer_order_cancelled':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_order_cancelled', $data),
                    'text' => Mail::text_body('writer_order_cancelled', $data),
                ));
                $data['title'] = Mail::text_title('writer_order_cancelled', $data);
                $data['text'] = Mail::text_body('writer_order_cancelled', $data);
            break;
            case 'writer_customer_approved':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_customer_approved', $data),
                    'text' => Mail::text_body('writer_customer_approved', $data),
                ));
                $data['title'] = Mail::text_title('writer_customer_approved', $data);
                $data['text'] = Mail::text_body('writer_customer_approved', $data);
            break;
            case 'writer_choose_paypal':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_choose_paypal', $data),
                    'text' => Mail::text_body('writer_choose_paypal', $data),
                ));
                $data['title'] = Mail::text_title('writer_choose_paypal', $data);
                $data['text'] = Mail::text_body('writer_choose_paypal', $data);
            break;
            case 'writer_choose_payoner':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_choose_payoner', $data),
                    'text' => Mail::text_body('writer_choose_payoner', $data),
                ));
                $data['title'] = Mail::text_title('writer_choose_payoner', $data);
                $data['text'] = Mail::text_body('writer_choose_payoner', $data);
            break;
            case 'writer_choose_payment_method':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_choose_payment_method', $data),
                    'text' => Mail::text_body('writer_choose_payment_method', $data),
                ));
                $data['title'] = Mail::text_title('writer_choose_payment_method', $data);
                $data['text'] = Mail::text_body('writer_choose_payment_method', $data);
            break;
            case 'writer_change_password':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_change_password', $data),
                    'text' => Mail::text_body('writer_change_password', $data),
                ));
                $data['title'] = Mail::text_title('writer_change_password');
                $data['text'] = Mail::text_body('writer_change_password', $data);
            break;  
            case 'writer_support_commented':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_support_commented', $data),
                    'text' => Mail::text_body('writer_support_commented', $data),
                ));
                $data['title'] = Mail::text_title('writer_support_commented');
                $data['text'] = Mail::text_body('writer_support_commented', $data);
            break;  
            case 'writer_client_commented':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_client_commented', $data),
                    'text' => Mail::text_body('writer_client_commented', $data),
                ));
                $data['title'] = Mail::text_title('writer_client_commented');
                $data['text'] = Mail::text_body('writer_client_commented', $data);
            break;  
            case 'writer_forgot_password'://e
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_forgot_password', $data),
                    'text' => Mail::text_body('writer_forgot_password', $data),
                ));
                $data['title'] = Mail::text_title('writer_forgot_password');
                $data['text'] = Mail::text_body('writer_forgot_password', $data);
            break;
            case 'writer_dismiss_order':
                $status = true;
                $writer = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('writer_dismiss_order', $data),
                    'text' => Mail::text_body('writer_dismiss_order', $data),
                ));
                $data['title'] = Mail::text_title('writer_dismiss_order');
                $data['text'] = Mail::text_body('writer_dismiss_order', $data);
            break;
            case 'support_client_place_order'://e
                $status = true;
                $client = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('support_client_place_order', $data),
                    'text' => Mail::text_body('support_client_place_order', $data),
                ));
                $data['title'] = Mail::text_title('support_client_place_order');
                $data['text'] = Mail::text_body('support_client_place_order', $data);
            break;

            case 'professor_register':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_register', $data),
                    'text' => Mail::text_body('professor_register', $data),
                ));
                $data['title'] = Mail::text_title('professor_register');
                $data['text'] = Mail::text_body('professor_register', $data);
            break;
            case 'professor_receive_payment':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_receive_payment', $data),
                    'text' => Mail::text_body('professor_receive_payment', $data),
                ));
                $data['title'] = Mail::text_title('professor_receive_payment');
                $data['text'] = Mail::text_body('professor_receive_payment', $data);
            break;
            case 'professor_writer_assign':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_writer_assign', $data),
                    'text' => Mail::text_body('professor_writer_assign', $data),
                ));
                $data['title'] = Mail::text_title('professor_writer_assign');
                $data['text'] = Mail::text_body('professor_writer_assign', $data);
            break;
            case 'professor_writer_finished':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_writer_finished', $data),
                    'text' => Mail::text_body('professor_writer_finished', $data),
                ));
                $data['title'] = Mail::text_title('professor_writer_finished');
                $data['text'] = Mail::text_body('professor_writer_finished', $data);
            break;
            case 'professor_request_revision':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_request_revision', $data),
                    'text' => Mail::text_body('professor_request_revision', $data),
                ));
                $data['title'] = Mail::text_title('professor_request_revision');
                $data['text'] = Mail::text_body('professor_request_revision', $data);
            break;
            case 'professor_cancel_order':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_cancel_order', $data),
                    'text' => Mail::text_body('professor_cancel_order', $data),
                ));
                $data['title'] = Mail::text_title('professor_cancel_order');
                $data['text'] = Mail::text_body('professor_cancel_order', $data);
            break;
            case 'professor_approve_order':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_approve_order', $data),
                    'text' => Mail::text_body('professor_approve_order', $data),
                ));
                $data['title'] = Mail::text_title('professor_approve_order');
                $data['text'] = Mail::text_body('professor_approve_order', $data);
            break;
            case 'professor_writer_commented':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_writer_commented', $data),
                    'text' => Mail::text_body('professor_writer_commented', $data),
                ));
                $data['title'] = Mail::text_title('professor_writer_commented');
                $data['text'] = Mail::text_body('professor_writer_commented', $data);
            break;
            case 'professor_support_commented':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_support_commented', $data),
                    'text' => Mail::text_body('professor_support_commented', $data),
                ));
                $data['title'] = Mail::text_title('professor_support_commented');
                $data['text'] = Mail::text_body('professor_support_commented', $data);
            break;
            case 'professor_change_password':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_change_password', $data),
                    'text' => Mail::text_body('professor_change_password', $data),
                ));
                $data['title'] = Mail::text_title('professor_change_password');
                $data['text'] = Mail::text_body('professor_change_password', $data);
            break;
            case 'professor_forgot_password':
                $status = true;
                $professor = true;
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_forgot_password', $data),
                    'text' => Mail::text_body('professor_forgot_password', $data),
                ));
                $data['title'] = Mail::text_title('professor_forgot_password');
                $data['text'] = Mail::text_body('professor_forgot_password', $data);
            break;
            case 'professor_place_order':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_place_order', $data),
                    'text' => Mail::text_body('professor_place_order', $data),
                ));
                $status = true;
                $professor = true;
                $data['title'] = Mail::text_title('professor_place_order', $data);
                $data['text'] = Mail::text_body('professor_place_order', $data);
            break;
            case 'professor_found_writer':
                $CI->user_model->save_email_hash(array(
                    'user_id' => $data['user_id'],
                    'hash' => $data['hash_email'],
                    'title' => Mail::text_title('professor_found_writer', $data),
                    'text' => Mail::text_body('professor_found_writer', $data),
                ));
                $status = true;
                $professor = true;
                $data['title'] = Mail::text_title('professor_found_writer', $data);
                $data['text'] = Mail::text_body('professor_found_writer', $data);
            break;
            default:
                $data['title'] = '';
                $data['text'] = '';
            break;
        }
        if($status && $writer) {
            $prefix = '';
            if($CI->config->item('sitetype') != 'writer')
                $prefix = '../../../writer-center.com/application/views/';
            $body = $CI->load->view($prefix.'user/email.tpl', $data, TRUE);
            require_once 'assets/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug = 0;

            $mail->Host = 'smtp.zoho.com';
            $mail->Port = 465;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth = true;
            $mail->Username = $CI->config->item('writer_email_from');
            $mail->Password = $CI->config->item('writer_email_password');

            $mail->From = $CI->config->item('writer_email_from');
            $mail->FromName = $CI->config->item('writer_email_from_name');
            $mail->addAddress($to);

            $mail->WordWrap = 50;
            $mail->isHTML(true);

            $mail->Subject = $data['title'];
            $mail->Body = $body;
            $mail->MsgHTML($body);

            $mail->send();  
            $mail = null;          
        }
        elseif($status && $client) {
            $prefix = '';
            if($CI->config->item('sitetype') != 'client')
                $prefix = '../../../eagle-essays.com/application/views/';
            $body = $CI->load->view($prefix.'user/email.tpl', $data, TRUE);
            require_once 'assets/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug = 0;

            $mail->Host = 'smtp.zoho.com';
            $mail->Port = 465;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth = true;
            $mail->Username = $CI->config->item('client_email_from');
            $mail->Password = $CI->config->item('client_email_password');

            $mail->From = $CI->config->item('client_email_from');
            $mail->FromName = $CI->config->item('client_email_from_name');
            $mail->addAddress($to);

            $mail->WordWrap = 50;
            $mail->isHTML(true);

            $mail->Subject = $data['title'];
            $mail->Body = $body;
            $mail->MsgHTML($body);

            $mail->send();  
            $mail = null;          
        }
        elseif($status && $professor) {
            $prefix = '';
            if($CI->config->item('sitetype') != 'professor')
                $prefix = '../../../professor-essays.com/application/views/';
            $body = $CI->load->view($prefix.'user/email.tpl', $data, TRUE);
            require_once 'assets/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug = 0;

            $mail->Host = 'smtp.zoho.com';
            $mail->Port = 465;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth = true;
            $mail->Username = $CI->config->item('professor_support_email');
            $mail->Password = $CI->config->item('professor_email_password');

            $mail->From = $CI->config->item('professor_support_email');
            $mail->FromName = $CI->config->item('professor_email_from_name');
            $mail->addAddress($to);

            $mail->WordWrap = 50;
            $mail->isHTML(true);

            $mail->Subject = $data['title'];
            $mail->Body = $body;
            $mail->MsgHTML($body);

            $mail->send();  
            $mail = null;          
        }
    }
}
class Image {
    
    function deleteImage($fileName, $path = "/upload/images/")
    {
        $file = $_SERVER['DOCUMENT_ROOT'].$path.$fileName;
        if (is_file($file))
        {
            return unlink($file);   
        }
        return FALSE;
        
    }
    
    function upload($tmp, $destination, $thumb = FALSE) 
    {
        $extensions = array('png', 'jpg', 'jpeg', 'gif');
        $types = array(1, 2, 3);    //1 = GIF, 2 = JPG, 3 = PNG
        if (is_file($tmp["tmp_name"]))
        {
            
            $ext = substr(strrchr($tmp["name"], '.'), 1);
            if (in_array($ext, $extensions))
            {
                $PFileInf = getimagesize ($tmp["tmp_name"]);

                if(in_array($PFileInf[2], $types))
                {   
                    $res_filename = time();
                    $res_filename .= ".".$ext;
                    if (move_uploaded_file($tmp["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $destination.$res_filename))
                    {   
                        if ($thumb != FALSE)
                        {
                            Image::thumbCreate($_SERVER['DOCUMENT_ROOT'] . $destination.$res_filename, $_SERVER['DOCUMENT_ROOT'] . $destination.$res_filename, $thumb);
                        }
                        return $res_filename;
                    }
                }
            }   
            
        }   
        return FALSE;
    }
    
    function thumbCreate($file, $destination, $size = array('width' => 1024, 'height' => 768)) {
        
        $file_type = getimagesize($file);
        $w = (float)$size['width']/$file_type[0];
        $h = (float)$size['height']/$file_type[1];
        $type = $file_type[2];
        if ($w <= $h)
        {
            $size['height'] = ((int)($file_type[1]*$w));
        }
        else
        {
            $size['width'] = ((int)($file_type[0]*$h));
        }
        
        if ($file_type[2] == 1)
        {
            $image=imagecreatefromgif($file);
        }
        elseif ($file_type[2] == 2)
        {
            $image=imagecreatefromjpeg($file);
        }
        elseif ($file_type[2] == 3)
        {
            $image=imagecreatefrompng($file);
        }
        else {
            $image=imagecreatefromjpg($file);
        }
        $image2=@imagecreatetruecolor($size['width'], $size['height']);

        imagecopyresized($image2, $image, 0, 0, 0, 0, $size['width'], $size['height'], $file_type[0], $file_type[1]);

        if ($file_type[2] == 1)
        {
            if (imagegif($image2, $destination)) return TRUE;
        }
        elseif ($file_type[2] == 2)
        {
            if (imagejpeg($image2, $destination)) return TRUE;
        }
        elseif ($file_type[2] == 3)
        {
            if (imagepng($image2, $destination)) return TRUE;
        }
        else {
            if (imagejpeg($image2, $destination)) return TRUE;
        }
        return FALSE;
        
    }
}
if (! function_exists('ceil_float'))
{
    function ceil_float($money)
    {
        $ret = round($money, 2);
        while($ret < $money)
        {
            $ret = $ret + 0.01;
        }        
        return $ret;
    }
}

