<?php


// Вытаскиваем настройки из базы данных
$CI = & get_instance();
$CI->load->database();
$CI -> load -> model('Other_model');
$setbds = $CI -> Other_model -> get_settings();
foreach($setbds as $setbd) {
    $settings[$setbd['name']] = $setbd['value'];
}
$CI->config->set_item('settings', $settings);



Class Menu extends CI_Controller {

    // Активная кнопка меню
    function check_active($string) {
        $CI = & get_instance();
        if(substr_count($_SERVER['REQUEST_URI'], $string)) {
            if($string == '/' || $string == '/admin') {
                if($_SERVER['REQUEST_URI'] == $string) {
                    return ' class="active" ';
                } else {
                    return NULL;
                }
            }
            return ' class="active" ';
        } else {
            return NULL;
        }

    }

}

Class Content extends CI_Controller {
    
    // Проверка на ИЕ
    function check_ie() {
        if(stristr($_SERVER['HTTP_USER_AGENT'],'MSIE')) {
            echo "
                <h1 style='background:#ccc; border:1px solid red; padding:10px;'>".$this->lang->line('For_correct_work_install_browser')."
                <a href='http://www.opera.com/ru/'>Opera</a>, <a href='http://www.mozilla.org/ru/firefox/fx/'>Mozilla</a>,
                <a href='http://www.google.com/intl/ru/chrome/browser/'>Google Chrome</a></h1>
            ";
        }
    }
    
}

// Настройки сайта
/*
Class Settings extends CI_Controller {
    
    // Достаем настройки сайта из БД
    
    function get() {
        
    }
    function get() {
        
    }
    function get_meta_keywords() {
        
    }
    
}*/

?>