<?php
	// Will need to set these variables to valid a MerchantID and Password
	// These were obtained during sign up
	$config['merchant_id'] = "merchant6083009";
	$config['password'] = "Papers123$";
	
	// Will need to put a valid path here for where the payment pages reside 
	// e.g. https://www.yoursitename.com/Pages/ 
	// NOTE: This path MUST include the trailing "/" 
	$config['site_url'] = "http://write-my-papers.co.uk/payment";

	// This is the domain (minus any host header or port number for your payment processor
	// e.g. for "https://gwX.paymentprocessor.net:4430/", this should just be "paymentprocessor.net"
	$config['domain'] = "paymentprocessor.net";
	// This is the port that the gateway communicates on
	$config['port'] = 4430;

	if ($config['port'] == 443)
	{
		$config['full_domain'] = $config['domain']."/";
	}
	else
	{
		$config['full_domain'] = $config['domain'].":".$config['port']."/";
	}
?>