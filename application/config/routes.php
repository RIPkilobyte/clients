<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['contact'] = "contact";
$route['order'] = "order";
$route['admin'] = "admin/main";
$route['support'] = "support/main";
$route['gallery'] = "gallery";



$route['(^order.*$)'] = "$1";
$route['(^user.*$)'] = "$1";
$route['(^admin.*$)'] = "$1";
$route['(^support.*$)'] = "$1";
$route['(^ajax.*$)'] = "$1";
$route['(^inquiry.*$)'] = "$1";
$route['(^payment.*$)'] = "$1";
$route['(^paypal.*$)'] = "$1";
$route['(^cart.*$)'] = "$1";
$route['(^search.*$)'] = "$1";
$route['(^discounts.*$)'] = "$1";
$route['blog'] = "blog";
$route['blog/page'] = "blog/page/0";
$route['blog/add_comment'] = "blog/add_comment";
$route['blog/page/(:any)'] = "blog/page/$1";
$route['blog/(:any)'] = "blog/view/$1";

$route['(^show_email.*$)'] = "show_email/index/$1";
//$route['(^contact.*$)'] = "$1";

// Если из вышеперечисленного ничего не нашлось - ищем в базе данных через контроллер router
$route['(^.+$)'] = "router";
$route['default_controller'] = "home";

$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */