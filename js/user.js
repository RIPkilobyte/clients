function submit_inquiry(order_id)
{
  $.post('/ajax/submit_inquiry', {order_id: order_id}, function(data){
    if(data.status == 'ok') {
      alert("Order has been sent successfully");
      location.reload(false);
    }
    else
      alert("An error was occured. Please try again later");
  }, 'json');
}
function get_file(file_id)
{
	location.assign("http://"+window.location.hostname+"/ajax/get_file/"+file_id);
}
function download_file(file_id, status)
{
  location.assign("http://"+window.location.hostname+"/ajax/get_order_file/"+file_id);
  if(status == '0')
    $("#rating_dialog").dialog("open");
}
function preview_file(file_id)
{
	location.assign("http://"+window.location.hostname+"/ajax/get_preview/"+file_id);
}
function pay_order(order_id, token)
{
	location.assign('http://'+window.location.hostname+'/payment/pay_order/'+order_id+'/'+token);
}
function order_checkout(order_id)
{
	location.assign('http://'+window.location.hostname+'/cart/checkout/'+order_id);
}
function add_message()
{
	$("#message_result").val('');
	//$("#message_text").val('');
  var recipient = $("#recipient").val();
  if($("#writer_id").val() == '0') {
  if(recipient == 0){
    $(".message_about_support").addClass('hidden');
    $(".message_about_general").removeClass('hidden');
  }
  else if(recipient == 1){
    $(".message_about_general").addClass('hidden');
    $(".message_about_support").removeClass('hidden');
  }
  else {
    $(".message_about_general").addClass('hidden');
    $(".message_about_support").addClass('hidden');
  }
  }
	$("#new_message").removeClass('hidden');
}
function cancel_message()
{
	$("#message_result").val('');
	$("#message_text").val('');
	$("#new_message").addClass('hidden');
}
function submit_file()
{
  if($("#file").val() != '') {
    $("#fileform").ajaxSubmit({
        dataType:  'json',
        url: "http://"+window.location.hostname+"/ajax/add_file/",
        data: ({
          order : $("#order_id").val(),
          text : $("#message_text").val(),
          sender : $("#sender_id").val()
        }),
        beforeSubmit: function(){
          $("#submit_result").css('text-align', 'center');
          $("#submit_result").html('<img src="/img/loading.gif" height="40px" />');
        },
        success: function(data) {
          $("#submit_result").addClass('error');
          if(data.status == 'ok') {
            $("#submit_result").html('File sent successfully!');
            setTimeout('location.reload(false);', 2000);
          }
          else {
            $("#submit_result").html('An error occurred. Please try later');
            setTimeout('location.reload(false);', 2000);
          }
        }
    });
  }
  else
    submit_message();
}
function submit_message()
{
  var text = $("#message_text").val();
  var sender_id = $("#sender").val();
  var order_id = $("#order_id").val();
  if(text != '') {
    $("#new_message").html('<img src="/img/loading.gif" height="40px" />');
    $.post('/ajax/send_message',{order: order_id, text: text, sender: sender_id},function(data){
      $("#new_message").css('text-align', 'center');
      $("#new_message").addClass('error');
      if(data.status == 'ok') {
        $("#new_message").html('Message send successfully!');
        setTimeout('location.reload(false);', 2000);
      }
      else if(data.status == 'writer_fail') {
        $("#new_message").html('For your order WRITER is not picked up. Please try later');
        setTimeout('location.reload(false);', 2000);
      }
      else {
        $("#new_message").html('An error occurred. Please try later');
        setTimeout('location.reload(false);', 2000);
      }
    }, 'json');
  }
}
function set_read(id, order_id)
{
  $("#read"+id).html('<img src="/img/loading.gif" height="40px" />');
  $.post('/ajax/set_read', {id:id}, function(data){
    if(data.status == 'ok') {
      $("#read"+id).html('<div class="mess_readed">Read</div>');
      if(data.result) {
        $("#message_customer").addClass('attention');
        $("#message_customer_text").html('You have a '+data.result+' new messages');
        $("#left_message_customer"+order_id).addClass('attention');
        $("#left_message_customer_text"+order_id).html('You have a '+data.result+' new messages');
      }
      else {
        $("#message_customer").removeClass('attention');
        $("#message_customer_text").html('You have no new messages');
        $("#left_message_customer"+order_id).removeClass('attention');
        $("#left_message_customer_text"+order_id).html('You have no new messages');

      }
    }
    else {
      $("#read"+id).html('An error was occurred. Please try again later');
      setTimeout('location.reload(false);', 2000);
    }
  }, 'json');
}