function support_timer(field, y, m, d, H, i, s){       
    var finishday = new Date(y, m, d, H, i, s);            
    var endTS = finishday.getTime();
        
    var intervalid = setInterval(function(){        
        var now = new Date();
        var totalRemains = (endTS - now.getTime());
        if (totalRemains>1) {
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60)));            
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60;
            if (RemainsFullHours<10) {
                RemainsFullHours="0"+RemainsFullHours
            };
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            //if(!$(".timer_"+field).hasClass('success'))
                //$(".timer_"+field).addClass('success');
            $(".timer_"+field).html("Left: "+(RemainsFullDays ? RemainsFullDays+" D" :"")+" "+RemainsFullHours+":"+RemainsMinutes+" H");
        }
        else { 
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60))); 
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60; 

            RemainsFullDays = Math.abs(RemainsFullDays);
            RemainsFullHours = Math.abs(RemainsFullHours);
            if (RemainsFullHours<10) {
                RemainsFullHours="0"+RemainsFullHours
            };
            RemainsMinutes = Math.abs(RemainsMinutes);
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };                        
            lastSec = Math.abs(lastSec);
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            if(!$(".timer_"+field).hasClass('error'))
                $(".timer_"+field).addClass('error');
            $(".timer_"+field).html("Left: "+"-"+(RemainsFullDays ? RemainsFullDays+" D" :"")+" "+RemainsFullHours+":"+RemainsMinutes+" H");
        }
    }, 1000);
}
function timer(y, m, d, H, i, s){       
    var finishday = new Date(y, m, d, H, i, s);            
    endTS = finishday.getTime();
        
    var intervalid = setInterval(function(){        
        var now = new Date();
        var totalRemains = (endTS - now.getTime());
        if (totalRemains>1) {
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60)));            
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60;
            if (RemainsFullHours<10) {
                RemainsFullHours="0"+RemainsFullHours
            };
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            if(!$(".timer").hasClass('success'))
                $(".timer").addClass('success');
            $(".timer").html((RemainsFullDays ? RemainsFullDays+" d." :"")+" "+RemainsFullHours+":"+RemainsMinutes);
        }
        else { 
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60))); 
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60; 

            RemainsFullDays = Math.abs(RemainsFullDays);
            RemainsFullHours = Math.abs(RemainsFullHours);
            if (RemainsFullHours<10) {
                RemainsFullHours="0"+RemainsFullHours
            };
            RemainsMinutes = Math.abs(RemainsMinutes);
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };                        
            lastSec = Math.abs(lastSec);
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            if(!$(".timer").hasClass('error'))
                $(".timer").addClass('error');
            $(".timer").html("-"+(RemainsFullDays ? RemainsFullDays+"d." :"")+" "+RemainsFullHours+":"+RemainsMinutes);
        }
    }, 1000);
}
function order_timer(y, m, d, H, i, s){       
    var finishday = new Date(y, m, d, H, i, s);            
    endTS = finishday.getTime();
        
    var intervalid = setInterval(function(){        
        var now = new Date();
        var totalRemains = (endTS - now.getTime());
        if (totalRemains>1) {
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60)));            
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60;
            //if (RemainsFullHours<10) {
                //RemainsFullHours="0"+RemainsFullHours
            //};
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            //if(!$("#span_timer").hasClass('success'))
                //$("#span_timer").addClass('success');
            $("#timer_days").html((RemainsFullDays ? RemainsFullDays :"0"));
            $("#timer_hours").html((RemainsFullHours ? RemainsFullHours :"0"));
            $("#timer_minutes").html(RemainsMinutes);
            //$('.timer').html((RemainsFullDays ? "<span>"+RemainsFullDays+"day</span> " :"")+RemainsFullHours+":"+RemainsMinutes);
        }
        else { 
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60))); 
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60; 

            RemainsFullDays = Math.abs(RemainsFullDays);
            RemainsFullHours = Math.abs(RemainsFullHours);
            //if (RemainsFullHours<10) {
                //RemainsFullHours="0"+RemainsFullHours
            //};
            RemainsMinutes = Math.abs(RemainsMinutes);
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };                        
            lastSec = Math.abs(lastSec);
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            //if(!$("#span_timer").hasClass('error'))
                //$("#span_timer").addClass('error');
            $("#timer_days").html((RemainsFullDays ? RemainsFullDays :"0"));
            $("#timer_hours").html((RemainsFullHours ? RemainsFullHours :"0"));
            $("#timer_minutes").html(RemainsMinutes);         
            //$('.timer').html("<span style='color:red'>"+(RemainsFullDays ? "<span>"+RemainsFullDays+"day</span> " :"")+RemainsFullHours+":"+RemainsMinutes+"</span>");
        }
    }, 1000);
}
function order_timer_nopay(y, m, d, H, i, s){       
    var finishday = new Date(y, m, d, H, i, s);            
    endTS = finishday.getTime();
        
        var now = new Date();
        var totalRemains = (endTS - now.getTime());
        if (totalRemains>1) {
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60)));            
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60;
            //if (RemainsFullHours<10) {
                //RemainsFullHours="0"+RemainsFullHours
            //};
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            //if(!$("#span_timer").hasClass('success'))
                //$("#span_timer").addClass('success');
            $("#timer_days").html((RemainsFullDays ? RemainsFullDays :"0"));
            $("#timer_hours").html((RemainsFullHours ? RemainsFullHours :"0"));
            $("#timer_minutes").html(RemainsMinutes);
            //$('.timer').html((RemainsFullDays ? "<span>"+RemainsFullDays+"day</span> " :"")+RemainsFullHours+":"+RemainsMinutes);
        }
        else { 
            var RemainsSec=(parseInt(totalRemains/1000));
            var RemainsFullDays=(parseInt(RemainsSec/(24*60*60))); 
            var secInLastDay=RemainsSec-RemainsFullDays*24*3600;
            var RemainsFullHours=(parseInt(secInLastDay/3600));
            var secInLastHour=secInLastDay-RemainsFullHours*3600;
            var RemainsMinutes=(parseInt(secInLastHour/60));
            var lastSec=secInLastHour-RemainsMinutes*60; 

            RemainsFullDays = Math.abs(RemainsFullDays);
            RemainsFullHours = Math.abs(RemainsFullHours);
            //if (RemainsFullHours<10) {
                //RemainsFullHours="0"+RemainsFullHours
            //};
            RemainsMinutes = Math.abs(RemainsMinutes);
            if (RemainsMinutes<10) {
                RemainsMinutes="0"+RemainsMinutes
            };                        
            lastSec = Math.abs(lastSec);
            if (lastSec<10) {
                lastSec="0"+lastSec
            };
            //if(!$("#span_timer").hasClass('error'))
                //$("#span_timer").addClass('error');
            $("#timer_days").html((RemainsFullDays ? RemainsFullDays :"0"));
            $("#timer_hours").html((RemainsFullHours ? RemainsFullHours :"0"));
            $("#timer_minutes").html(RemainsMinutes);         
            //$('.timer').html("<span style='color:red'>"+(RemainsFullDays ? "<span>"+RemainsFullDays+"day</span> " :"")+RemainsFullHours+":"+RemainsMinutes+"</span>");
        }
}
$(document).ready(function(){
	
	// Спойлер
	$(".spoiler .sphead").click(function(){
		if ($(this).next().css("display") == "none") {
			$(this).next().slideDown(200);
		}else{
			$(this).next().slideUp(100);
		}
	});
    
});