function after_errors()
{
    if(check_step1()) {
        $(".nav_steps_links").removeClass('current');
        $("#step_2_link").addClass('current');
        $(".step_1").css('display', 'none');
        $(".step_2").css('display', 'block');
        $(".step_3").css('display', 'none');
        if(check_step2()) {
            $(".nav_steps_links").removeClass('current');
            $("#step_3_link").addClass('current');
            $(".step_1").css('display', 'none');
            $(".step_2").css('display', 'none');
            $(".step_3").css('display', 'block');
            fill_result();
        }
    }
}
function go_step_two()
{
    if(check_step1()) {
        $(".nav_steps_links").removeClass('current');
        $("#step_2_link").addClass('current');
        $(".step_1").css('display', 'none');
        $(".step_2").css('display', 'block');
        $(".step_3").css('display', 'none');
    }
}
function back_step_one()
{
    fill_result();
    $(".nav_steps_links").removeClass('current');
    $("#step_1_link").addClass('current');
    $(".step_1").css('display', 'block');
    $(".step_2").css('display', 'none');
    $(".step_3").css('display', 'none');
}
function go_step_three()
{
    go_step_two();
    if(check_step2()) {
        $(".nav_steps_links").removeClass('current');
        $("#step_3_link").addClass('current');
        $(".step_1").css('display', 'none');
        $(".step_2").css('display', 'none');
        $(".step_3").css('display', 'block');
        fill_result();
    }
}
function back_step_two()
{
    $(".nav_steps_links").removeClass('current');
    $("#step_2_link").addClass('current');
    $(".step_1").css('display', 'none');
    $(".step_2").css('display', 'block');
    $(".step_3").css('display', 'none');
    fill_result();
}
function check_step1()
{
    fill_result();
    nicEditors.findEditor('paper_details').saveContent()
    var error = 0;
    var paper_type = $("#paper_type").val();
    if(paper_type == '0') {
        $("#error_paper_type").html('The Type Of Paper field is required.');
        $("#error_paper_type").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_paper_type").addClass('hidden');

    var subject = $("#subject").val();
    if(subject == '0') {
        $("#error_subject").html('The Your Subject field is required.');
        $("#error_subject").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_subject").addClass('hidden');

    var paper_format = $("#paper_format").val();
    if(paper_format == '0') {
        $("#error_paper_format").html('The Referencing Style field is required.');
        $("#error_paper_format").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_paper_format").addClass('hidden');

    var topic = $("#topic").val();
    if(!topic) {
        $("#error_topic").html('The Topic field is required.');
        $("#error_topic").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_topic").addClass('hidden');

    var paper_details = $("#paper_details").val();
    if(!paper_details || paper_details.length === 0 || paper_details == '<br>') {
        $("#error_paper_details").html('The Paper details field is required.');
        $("#error_paper_details").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_paper_details").addClass('hidden');

    if(error)
        return false;
    else
        return true;
}
function check_step2()
{
    fill_result();
    var error = 0;
    var academic_level = $("#academic_level").val();
    if(academic_level == '0') {
        $("#error_academic_level").html('The Academic Level field is required.');
        $("#error_academic_level").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_academic_level").addClass('hidden');

    var number_of_pages = parseInt($("#number_of_pages").val());
    if(number_of_pages === 0) {
        $("#error_number_of_pages").html('The Number Of Pages must be greater than zero.');
        $("#error_number_of_pages").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_number_of_pages").addClass('hidden');

    var deadline = $("#deadline").val();
    if(deadline == '0') {
        $("#error_deadline").html('The Deadline field is required.');
        $("#error_deadline").removeClass('hidden');
        error = 1;
    }
    else
        $("#error_deadline").addClass('hidden');

    if(error)
        return false;
    else {
        return true;
    }
}
function check_step3()
{
    var error = 0;
    if(error)
        return false;
    else
        return true;
}
function fsubmitter_form()
{
    $("#submitter_form").click();
}
function change_deadline()
{
    var deadlines = {
        0: 0,
        1: 28800000,
        2: 86400000,
        3: 172800000,
        4: 259200000,
        5: 518400000,
        6: 864000000,
        7: 1209600000
    };
    var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");
    var deadline = $("#deadline").val();
    var now = new Date().getTime();
    var end = new Date(now+deadlines[deadline]);

    var hours = end.getHours()+1;
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    var day = end.getDate();
    var month = end.getMonth();
    var year = end.getFullYear();
    var date = day+" "+m_names[month]+" "+hours+" "+ampm;

    $("#deadline_calc").html('The deadline for the first draft is <b>'+date+'</b>.');
    var php_date1 = end.getFullYear()+'-'+(end.getMonth()+1)+'-'+end.getDate();
    var php_date2 = end.getHours()+':'+end.getMinutes()+':'+end.getSeconds();
    var php_date = php_date1+' '+php_date2;
   

    var timeleft = deadlines[deadline];
    var d = Math.floor(timeleft/86400000);
    var h = (timeleft - Math.floor(timeleft/86400000)*86400000)/(60*60*1000);
    $("#time_left_balls").html('<div class="ball">'+d+'</div><div class="ball">'+h+'</div><div class="ball">00</div>');

    h = '0'+h+':00';
    $(".day_left").html(d);
    $(".day_left_last").html(h);

    $("#blue_t_date").html(date);
}
function change_country()
{
    var country = $("#country").val();
    if(typeof(phone_prefix[country]) != 'undefined')
        $("#phone_prefix").val('+'+phone_prefix[country]);
    else
        $("#phone_prefix").val('');
}
function fill_result()
{
    change_deadline();
    if($("#three_step").css('display') == 'block')
        $(".order-price").addClass('step_3_price');
    else
        $(".order-price").removeClass('step_3_price');
    $("#discount_fail").html('');
    $("#preview_service_type").html($("#work_type option:selected").html());
    $("#preview_academic_level").html($("#academic_level option:selected").html());
    $("#preview_paper_type").html($("#paper_type option:selected").html());
    $("#preview_subject").html($("#subject option:selected").html());
    $("#preview_pages").html($("#number_of_pages").val());
    $("#preview_slides").html($("#number_of_slides").val());
    $("#preview_referencing").html($("#paper_format option:selected").html());
    $("#preview_topic").html($("#topic").val());
    $("#preview_sources").html($("#sources_needed").val());
    if($("#abstract_page").is(':checked'))
        $("#preview_abstract").html('Yes');
    if($("#plagiarism_report").is(':checked'))
        $("#preview_plagiarism").html('Yes');
    nicEditors.findEditor('paper_details').saveContent();
    if($("#paper_details").val() != '') {
        $("#preview_details").html('Submitted');
        $("#preview_full_paper_details").html($("#paper_details").val().substr(0,200));
    }

    if($("#file").val() != '') {
        var file = $("#file").val();
        var filename = file.split('.');
        var filepath = file.replace(/^.*[\\\/]/, '')
        var ext = filename[filename.length-1];
        var image = 'doc.png';
        switch(ext) {
            case 'doc':
            case 'docx':
                image = 'doc.png';
            break
            case 'pdf':
                image = 'pdf.png';
            break
            case 'ppt':
            case 'pptx':
            case 'pps':
                image = 'ppt.png';
            break
            case 'xls':
            case 'xlss':
                image = 'xls.png';
            break
            case 'jpg':
            case 'jpeg':
            case 'bmp':
            case 'png':
                image = 'jpg.png';
            break
            default:
                image = 'doc.png';
        }
        $("#preview_attachment").html('Yes <img src="/img/icon/'+image+'" width="15px"/> <span class="attach_name">'+filepath.substr(0,20)+'</span>');
    }
}
function order_calc_selector()
{
    var slides = $("#number_of_slides").val();
    var academic_level = $("#academic_level").val();
    var paper_type = $("#paper_type").val();
    var deadline = $("#deadline").val();
    var work_type = $("#work_type").val();
    var number_of_pages = $("#number_of_pages").val();
    number_of_pages = parseInt(number_of_pages);
    if(isNaN(number_of_pages))
        number_of_pages = 1;
    
    var preferred_writer = $("#preferred_writer").val();

    $("#preview_pages").html($("#number_of_pages").val());

    var price = 0.00;
    if(typeof(academic_level) != 'undefined' && typeof(paper_type) != 'undefined' && typeof(deadline) != 'undefined' && typeof(number_of_pages) != 'undefined')
        if(academic_level != '0' && deadline != '0' && academic_level != '' && deadline != '') {
            price_page = prices[work_type][academic_level][deadline];
            var price_slides = 0;
            var price_pages = 0;

            if(slides != '0') {
                price_slides = price_page * (slides / 2 );
                price_slides = parseFloat(price_slides).toFixed(2);
            }

            price_pages = price_page * number_of_pages;
            
            price = parseFloat(price_pages) + parseFloat(price_slides);

            price = parseFloat(price);
            if($("#abstract_page").is(':checked'))
                price = price + 14.99;
            price = parseFloat(price).toFixed(2);
        }
    $("#final_price").html('USD '+price);
    $(".calc_result").html(price);
    $("#full_price").val(price);
}
function check_paper_words()
{
    var text = $("#paper_details").val();
    var q = text.split(" ");
    var words = q.length;
    if(words < '3')
        $("#paper_details_error").css('display', 'block');
    else
        $("#paper_details_error").css('display', 'none');
}

$().ready(function(){
    $("#paper_details").keyup(function(){
        check_paper_words();
    });
    $("#number_of_pages").numberMask({beforePoint:4});
    $("#number_of_slides").numberMask({beforePoint:4});
    $("#sources_needed").numberMask({beforePoint:4});
    $("#previous_writer").numberMask({beforePoint:11});
    
    order_calc_selector();
    change_deadline();
    $("#deadline").change(function(){
        change_deadline();
    });
    $(".user_select").click(function(){
        change_user();
    });
    $(".calc_selector").change(function(){
        order_calc_selector();
    });
    $('#number_of_pages').mouseout(function(){
        order_calc_selector();
    }); 
    $('#number_of_pages').change(function(){
        order_calc_selector();
    });    
    $('#number_of_slides').mouseout(function(){
        order_calc_selector();
    }); 
    $('#number_of_slides').change(function(){
        order_calc_selector();
    });
    $("#source_plus").click(function(){
        var val = parseInt($("#sources_needed_div").html());
        if(!val)
            val = 0
        val = val+1;
        
        $("#sources_needed_div").html(val);
        $("#sources_needed").val(val);
        order_calc_selector();
    });
    $("#source_minus").click(function(){
        var val = parseInt($("#sources_needed_div").html());
        if(!val)
            val = 1
        val = val-1;
        
        $("#sources_needed_div").html(val);
        $("#sources_needed").val(val);
        order_calc_selector();
    });

    $("#number_of_pages_plus").click(function(){
        var val = parseInt($("#number_of_pages_div").html());
        if(!val)
            val = 0
        val = val+1;
        $("#number_of_pages_div").html(val);
        $("#number_of_pages").val(val);
        order_calc_selector();
    });
    $("#number_of_pages_minus").click(function(){
        var val = parseInt($("#number_of_pages_div").html());
        if(!val)
            val = 1
        val = val-1;
        $("#number_of_pages_div").html(val);
        $("#number_of_pages").val(val);
        order_calc_selector();
    });

    $("#slides_plus").click(function(){
        var val = parseInt($("#slides_div").html());
        if(!val)
            val = 0
        val = val+1;
        
        $("#slides_div").html(val);
        $("#number_of_slides").val(val);
        order_calc_selector();
    });
    $("#slides_minus").click(function(){
        var val = parseInt($("#slides_div").html());
        if(!val)
            val = 1
        val = val-1;
        
        $("#slides_div").html(val);
        $("#number_of_slides").val(val);
        order_calc_selector();
    });
});