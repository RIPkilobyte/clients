//-------------ORDER---------------------
function approve_order(order_id)
{
	if(confirm("Are you sure to approve?")) {
		$.post('/support/ajax/moderate_order', {order_id: order_id, action: 'approve'}, function(data){
			if(data.status == 'ok') {
				alert('Success');
				location.reload(false);
			}
			else {
				alert('An error was occured. Try again later');
			}
		}, 'json');
	}
}
function reject_order(order_id)
{
	if(confirm("Are you sure to reject?")) {
		$.post('/support/ajax/moderate_order', {order_id: order_id, action: 'reject'}, function(data){
			if(data.status == 'ok') {
				alert('Success');
				location.reload(false);
			}
			else {
				alert('An error was occured. Try again later');
			}
		}, 'json');
	}
}
function save_order_changes(order_id)
{
	if(confirm("Are you sure to save this changes?")) {
		//var rating_points = $("#rating_points").val();
		//var plagiarism_checks = $("#plagiarism_checks").val();
		var revisions = $("#revisions").val();
		$.post('/support/ajax/save_order_changes', {order: order_id, revisions: revisions}, function(data){
			if(data.status == 'ok') {
				alert('Order changes saved successful');
				location.reload(false);
			}
			else
				alert('An error was occured. Please try again later');
		}, 'json');
	}
}
function save_order_details(order_id)
{
	if(confirm("Are you sure to save this changes?")) {
		$.post('/support/ajax/save_order_details', {
			order: order_id, 
			pages: $("#pages").val(), 
			slides: $("#slides").val(), 
			questions: $("#questions").val(), 
			problems: $("#problems").val(), 
			academic_level: $("#academic_level").val(), 
			deadline: $("#deadline").val(), 
			work_type: $("#work_type").val(), 
			paper_type: $("#paper_type").val(), 
			subject: $("#subject").val(), 
			paper_format: $("#paper_format").val(), 
			sources: $("#sources").val(), 
			topic: $("#topic").val(),
			paper_details: $("#paper_details").val(),
			revisions: $("#revisions").val(),
		}, function(data){
			if(data.status == 'ok') {
				alert('Order changes saved successful');
				location.reload(false);
			}
			else
				alert('An error was occured. Please try again later');
		}, 'json');
	}
}
function change_order_status(order_id)
{
	if(confirm("Are you sure to save this changes?")) {
		var order_status = $("#order_status").val();
		$.post('/support/ajax/change_order_status', {order: order_id, order_status: order_status}, function(data){
			if(data.status == 'ok') {
				alert('Changing status successful');
				location.reload(false);
			}
			else
				alert('An error was occured. Please try again later');
		}, 'json');
	}
}
function assign_writer(order_id)
{
	var user = $("input[name=user_request]:checked").val();
	if(confirm("Are you sure to assign Writer #"+user+" for this order?")) {
		$.post('/support/ajax/assign_writer', {order_id:order_id, user_id:user}, function(data){
			if(data.status == 'ok') {
				alert("Success!");
				location.reload(false);
			}
			else {
				alert('An error occurred. Please try again later');
			}
		}, 'json');
	}
}
function dismiss_writer(order_id)
{
	if(confirm("Are you sure to dismis this writer?")) {
		$.post('/support/ajax/dismiss_writer', {order: order_id}, function(data){
			if(data.status == 'ok') {
				alert('Writer dismissed successful');
				location.reload(false);
			}
			else
				alert('An error was occured. Please try again later');
		}, 'json');
	}
}
function mark_finished(order_id)
{
	if(confirm("Are you sure to mark this order finished?")) {
		$.post('/support/ajax/mark_finished', {order: order_id}, function(data){
			if(data.status == 'ok') {
				alert('Order finished successful');
				location.reload(false);
			}
			else
				alert('An error was occured. Please try again later');
		}, 'json');
	}
}
function cancel_order(order_id)
{
	if(confirm("Are you sure to cancel this order?")) {
		$.post('/support/ajax/cancel_order', {order: order_id}, function(data){
			if(data.status == 'ok') {
				alert('Order cancelled successful');
				location.reload(false);
			}
			else
				alert('An error was occured. Please try again later');
		}, 'json');
	}
}
function delete_order(order_id)
{
	$("#defender").val(1);
	if(confirm("Are you sure to delete this order?")) {
		$.post('/support/ajax/delete_order', {order_id: order_id}, function(data){
			$("#defender").val(0);
			if(data.status == 'ok') {
				alert('Success!');
				location.reload(false);
			}
			else {
				alert('An error was occurred. Please try again later');
			}
		}, 'json');
	}
	else
		setTimeout(function(){$("#defender").val(0);}, 500);
}
function send_inquiry_email(order_id)
{
	$("#writer_assign_result").html('<img src="/img/loading.gif" height="40px" />');
	$.post('/support/ajax/send_inquiry_email',{order_id:order_id}, function(data){
		if(data.status == 'ok') {
			$("#writer_assign_result").html('Success!');
			setTimeout('location.reload(false)', 2000);
		}
		else {
			$("#writer_assign_result").html('An error was occured. Please try again later');
			setTimeout('location.reload(false)', 2000);
		}
	}, 'json');
}
function reset_attention(order_id, value)
{
	$("#attention_button").html('<img src="/img/loading.gif" height="30px" />');
	$.post('/support/ajax/reset_attention',{order_id:order_id, value:value}, function(data){
		if(data.status == 'ok') {
			$("#attention_button").html('Success!');
			setTimeout('location.reload(false)', 2000);
		}
		else {
			$("#attention_button").html('An error was occured. Please try again later');
			setTimeout('location.reload(false)', 2000);
		}
	}, 'json');
}
function reset_notify(order_id, type, value)
{
	if(confirm("Are you sure to complete this changes?")) {
	$("#button_"+type).html('<img src="/img/loading.gif" height="30px" />');
	$.post('/support/ajax/reset_notify',{order_id:order_id, type:type, value:value}, function(data){
		if(data.status == 'ok') {
			$("#button_"+type).html('Success!');
			setTimeout('location.reload(false)', 2000);
		}
		else {
			$("#button_"+type).html('An error was occured. Please try again later');
			setTimeout('location.reload(false)', 2000);
		}
	}, 'json');
	}
}
function sent_report(order_id)
{
	$("#report_button").html('<img src="/img/loading.gif" height="30px" />');
	$.post('/support/ajax/sent_report',{order_id:order_id}, function(data){
		if(data.status == 'ok') {
			$("#report_button").html('Success!');
			setTimeout('location.reload(false)', 2000);
		}
		else {
			$("#report_button").html('An error was occured. Please try again later');
			setTimeout('location.reload(false)', 2000);
		}
	}, 'json');
}
//-------------WRITER--------------------
function submit_diplom(user_id)
{
	$("#diplom_form").ajaxSubmit({
        dataType:  'json',
        url: "http://"+window.location.hostname+"/support/ajax/send_diplom/"+user_id,
        beforeSubmit: function(){
        	$("#submit_diplom_result").html('<img src="/img/loading.gif" height="40px" />');
        },
        success: function(data) {
        	if(data.status == 'ok') {
        		$("#submit_diplom_result").html('Success!');
        		setTimeout('location.reload(false)', 2000);
        	}
        	else {
        		$("#submit_diplom_result").html('An error occurred. Please try again later');
        	}
        }
	});	
}
function submit_passport(user_id)
{
	$("#passport_form").ajaxSubmit({
        dataType:  'json',
        url: "http://"+window.location.hostname+"/support/ajax/send_passport/"+user_id,
        beforeSubmit: function(){
        	$("#submit_passport_result").html('<img src="/img/loading.gif" height="40px" />');
        },
        success: function(data) {
        	if(data.status == 'ok') {
        		$("#submit_passport_result").html('Success!');
        		setTimeout('location.reload(false)', 2000);
        	}
        	else {
        		$("#submit_passport_result").html('An error occurred. Please try again later');
        	}
        }
	});	
}
function submit_essay(user_id)
{
	$("#essay_form").ajaxSubmit({
        dataType:  'json',
        url: "http://"+window.location.hostname+"/support/ajax/send_essay/"+user_id,
        beforeSubmit: function(){
        	$("#submit_essay_result").html('<img src="/img/loading.gif" height="40px" />');
        },
        success: function(data) {
        	if(data.status == 'ok') {
        		$("#submit_essay_result").html('Success!');
        		setTimeout('location.reload(false)', 2000);
        	}
        	else {
        		$("#submit_essay_result").html('An error occurred. Please try again later');
        	}
        }
	});	
}
/*
function accept_diplom(user_id)
{
	if(confirm("Are you sure to accept diplom?")) {
		$.post('/support/ajax/step1/', {type: 'diplom', action:'accept', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('diplom_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('diplom_result');
			else
				write_html_result('diplom_result', 'fail');
		}, 'json');
	}
}
function reject_diplom(user_id)
{
	if(confirm("Are you sure to reject diplom?")) {
		$.post('/support/ajax/step1/', {type: 'diplom', action:'reject', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('diplom_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('diplom_result');			
			else
				write_html_result('diplom_result', 'fail');
		}, 'json');
	}
}
function accept_passport(user_id)
{
	if(confirm("Are you sure to accept passport?")) {
		$.post('/support/ajax/step1/', {type: 'passport', action:'accept', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('passport_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('passport_result');			
			else
				write_html_result('passport_result', 'fail');
		}, 'json');
	}
}
function reject_passport(user_id)
{
	if(confirm("Are you sure to reject passport?")) {
		$.post('/support/ajax/step1/', {type: 'passport', action:'reject', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('passport_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('passport_result');			
			else
				write_html_result('passport_result', 'fail');
		}, 'json');
	}
}
function accept_essay(user_id)
{
	if(confirm("Are you sure to accept essay?")) {
		$.post('/support/ajax/step1/', {type: 'essay', action:'accept', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('essay_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('essay_result');			
			else
				write_html_result('essay_result', 'fail');
		}, 'json');
	}
}
function reject_essay(user_id)
{
	if(confirm("Are you sure to reject essay?")) {
		$.post('/support/ajax/step1/', {type: 'essay', action:'reject', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('essay_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('essay_result');			
			else
				write_html_result('essay_result', 'fail');
		}, 'json');
	}
}//*/
function accept_step1(user_id)
{
	if(confirm("Are you sure to accept all files?")) {
		$("#user_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/step1/', {type: 'all', action:'accept', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');			
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}
}
function reject_step1(user_id)
{
	if(confirm("Are you sure to reject all files?")) {
		$("#user_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/step1/', {type: 'all', action:'reject', user: user_id}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');			
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}	
}
/*
function allow_testing(user_id)
{
	if(confirm("Are you sure to allow testing this user?")) {
		$.post('/support/ajax/step2', {user: user_id, action: 'allow'}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');			
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}
}//*/
function accept_test(id)
{
	if(confirm("Are you sure to accept this test?")) {
		$("#user_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/step3/', {id: id, action:'accept'}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');			
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}	
}
function reject_test(id)
{
	if(confirm("Are you sure to reject this test?")) {
		$("#user_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/step3/', {id: id, action:'reject'}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');			
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}	
}
function delete_user(user_id)
{
	$("#defender").val(1);
	if(confirm("Are you sure to delete this user?")) {
		$.post('/support/ajax/delete_user', {user_id: user_id}, function(data){
			$("#defender").val(0);
			if(data.status == 'ok') {
				alert('Success!');
				location.reload(false);
			}
			else {
				alert('An error was occurred. Please try again later');
			}
		}, 'json');
	}
	else
		setTimeout(function(){$("#defender").val(0);}, 500);
}
function save_writer_comment(user_id)
{
	$("#save_comment_result").html('<img src="/img/loading.gif" height="40px" />');
	$.post('/support/ajax/save_writer_comment', {user:user_id, comment:$("#user_comment").val()}, function(data) {
		if(data.status == 'ok') {
			alert('Success!');
			location.reload(false);
		}
		else
			$("#save_comment_result").html('<span style="color:red">An error was occurred. Please try again later</span>');
	}, 'json');
}
function accept_writer(user_id)
{
	if(confirm("Are you sure to accept this writer?")) {
		$.post('/support/ajax/writer_actions/', {id: user_id, action:'accept'}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');			
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}
}
function reject_writer(user_id)
{
	if(confirm("Are you sure to reject this writer?")) {
		$.post('/support/ajax/writer_actions/', {id: user_id, action:'reject'}, function(data){
			if(data.status == 'ok')
				write_html_result('user_result', 'ok');
			else if(data.status == 'reload')
				write_html_and_reload('user_result');
			else
				write_html_result('user_result', 'fail');
		}, 'json');
	}	
}
function reset_files(user_id)
{
	if(confirm("Are you sure to reset ?")) {
		$("#reset_files_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/reset_writer', {user:user_id, type:'files'}, function(data) {
			if(data.status == 'ok') {
				alert('Success!');
				location.reload(false);
			}
			else
				$("#save_comment_result").html('<span style="color:red">An error was occurred. Please try again later</span>');
		}, 'json');
	}
}
function reset_test(user_id)
{
	if(confirm("Are you sure to reset ?")) {
		$("#reset_test_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/reset_writer', {user:user_id, type:'test'}, function(data) {
			if(data.status == 'ok') {
				alert('Success!');
				location.reload(false);
			}
			else
				$("#save_comment_result").html('<span style="color:red">An error was occurred. Please try again later</span>');
		}, 'json');
	}
}
function reset_verification(user_id)
{
	if(confirm("Are you sure to reset ?")) {
		$("#reset_verification_result").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/reset_writer', {user:user_id, type:'verification'}, function(data) {
			if(data.status == 'ok') {
				alert('Success!');
				location.reload(false);
			}
			else
				$("#save_comment_result").html('<span style="color:red">An error was occurred. Please try again later</span>');
		}, 'json');
	}
}
function replace_to_input(user_id, field)
{
	$("#cover_span_"+field).css('display', 'none');
	$("#cover_input_"+field).css('display', 'block');
}
function save_user_info(user_id, field, old_value)
{
	$("#cover_input_"+field).css('display', 'none');
	$("#cover_loading_"+field).css('display', 'block');

	var new_value = $("#input_"+field).val();
	$.post('/support/ajax/save_user_info', {user_id:user_id, field:field, new_value:new_value}, function(data){
		if(data.status == 'ok') {
			$("#text_value_"+field).html(new_value);
			$("#cover_input_"+field).html('<input type="text" size="10" id="input_'+field+'" value="'+new_value+'" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info(\''+user_id+'\', \''+field+'\', \''+new_value+'\')"><img src="/img/chbx_check.png" /></a>');
			$("#cover_span_"+field).css('display', 'block');
			$("#cover_loading_"+field).css('display', 'none');
		}
		else {
			$("#text_value_"+field).css('color', 'red');
			if(data.status == 'exist') {
				$("#text_value_"+field).html(old_value+' <img src="/img/error_icon.jpg" title="This value already exist in database" height="12px;" />');
			}
			else {
				$("#text_value_"+field).html(old_value+' <img src="/img/error_icon.jpg" title="An error was occured. Please try again later." height="12px;" />');
			}
			$("#cover_input_"+field).html('<input type="text" size="10" id="input_'+field+'" value="'+old_value+'" /> &nbsp; <a href="javascript:void(0)" onclick="save_user_info(\''+user_id+'\', \''+field+'\', \''+old_value+'\')"><img src="/img/chbx_check.png" /></a>');
			$("#cover_span_"+field).css('display', 'block');
			$("#cover_loading_"+field).css('display', 'none');
		}
		setTimeout('location.reload(false)', 2000);
	}, 'json');
}
function change_status(user_id)
{
	var new_value = $("#writer_status").val();
	$.post('/support/ajax/save_user_info', {user_id:user_id, field:'status', new_value:new_value}, function(data){
		var text = 'An error was occured. Please try again later';
		if(data.status == 'ok') {
			text = 'Success!';
		}
		$("#writer_status").after(text);
		setTimeout('location.reload(false)', 2000);
	}, 'json');
}
function save_order_price(order_id, old_value)
{
	$("#cover_input_price").css('display', 'none');
	$("#cover_loading_price").css('display', 'block');
	var value = $("#input_price").val();
	$.post('/support/ajax/save_order_price',{order_id:order_id,value:value},function(data){
		if(data.status == 'ok') {
			$("#text_value_price").html('Price: $'+value);
			$("#cover_input_price").html('<input type="text" size="10" id="input_price" value="'+value+'" /> &nbsp; <a href="javascript:void(0)" onclick="save_order_price(\''+order_id+'\', \''+value+'\')"><img src="/img/chbx_check.png" /></a>');
			$("#cover_loading_price").css('display', 'none');
			$("#cover_span_price").css('display', 'block');
		}
		else {
			$("#text_value_price").html('Price: $'+old_value);
			$("#text_value_price").css('color', 'red');
			$("#cover_loading_price").css('display', 'none');
			$("#cover_input_price").html('<input type="text" size="10" id="input_price" value="'+old_value+'" /> &nbsp; <a href="javascript:void(0)" onclick="save_order_price(\''+order_id+'\', \''+old_value+'\')"><img src="/img/chbx_check.png" /></a>');
			$("#cover_span_price").css('display', 'block');
		}
	},'json');	
}
function save_order_writer_price(order_id, old_value)
{
	$("#cover_input_writer_price").css('display', 'none');
	$("#cover_loading_writer_price").css('display', 'block');
	var value = $("#input_writer_price").val();
	$.post('/support/ajax/save_order_writer_price',{order_id:order_id,value:value},function(data){
		if(data.status == 'ok') {
			$("#text_value_writer_price").html('Writer price: $'+value);
			$("#cover_input_writer_price").html('<input type="text" size="10" id="input_writer_price" value="'+value+'" /> &nbsp; <a href="javascript:void(0)" onclick="save_order_writer_price(\''+order_id+'\', \''+value+'\')"><img src="/img/chbx_check.png" /></a>');
			$("#cover_loading_writer_price").css('display', 'none');
			$("#cover_span_writer_price").css('display', 'block');
		}
		else {
			$("#text_value_writer_price").html('Writer price: $'+old_value);
			$("#text_value_writer_price").css('color', 'red');
			$("#cover_loading_writer_price").css('display', 'none');
			$("#cover_input_writer_price").html('<input type="text" size="10" id="input_writer_price" value="'+old_value+'" /> &nbsp; <a href="javascript:void(0)" onclick="save_order_writer_price(\''+order_id+'\', \''+old_value+'\')"><img src="/img/chbx_check.png" /></a>');
			$("#cover_span_writer_price").css('display', 'block');
		}
	},'json');	
}
function change_deadline(order_id, type)
{
	var value = $("#ending_time_"+type).val();
	if(confirm("Are you sure to set this deadline ?")) {
		$.post('/support/ajax/change_order_deadline',{order_id:order_id,value:value, type:type},function(data){
			if(data.status == 'ok') {
				$("#deadline_result").html('Success!');
				setTimeout('location.reload(false)', 2000);
			}
			else {
				$("#deadline_result").html('An error was occured. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
		},'json');
	}
}
function save_note(order_id)
{
	var value = $("#note").val();
	$.post('/support/ajax/save_note',{order_id:order_id,note:value},function(data){
		if(data.status == 'ok') {
			$("#note_result").html('Success!');
			setTimeout('location.reload(false)', 2000);
		}
		else {
			$("#note_result").html('An error was occured. Please try again later');
			setTimeout('location.reload(false)', 2000);
		}
	},'json');
}
/*
function accept_intervals()
{
	$("#timeinterval_form").submit();
}
function getRandomInt()
{
	return Math.floor(Math.random() *  10000) + 1;
}
function add_dateinterval()
{
	var rand = getRandomInt();
	var text = '<tr><td>Select date:</td><td><input type="text" class="datetimepicker" id="timeinterval_'+rand+'" name="timeinterval[]" /></td></tr>';

	$(".paper_details").append(text);

	$("#timeinterval_"+rand).kendoDateTimePicker({
		value: new Date(),
		format: "yyyy-MM-dd hh:mm:ss"
	});	
}
function convert_datetime(string)
{
	if(string == '')
		var datetime = new Date();
	else if(typeof(string) != 'object') {
		var parts = string.split(' ');
		var date = parts[0].split('-');
		var time = parts[1].split(':');
		var datetime = new Date(date[0], parseInt(date[1])-1, date[2], time[0], time[1], time[2]);
	}
	else {
		var datetime = new Date();
	}
	return datetime;
}//*/
//-------------FILES---------------------
function create_preview(file_id)
{
	//if(confirm("Create preview from this file?")) {
		$("#buttons_"+file_id).html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/create_preview',{file_id:file_id},function(data){
			if(data.status == 'ok') {
				$("#buttons_"+file_id).html('Success!');
				setTimeout('location.reload(false)', 2000);
			}
			else{
				$("#buttons_"+file_id).html('An error was occured. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}

		}, 'json');
	//}
}
function add_second_file(file_id)
{
	//$("#buttons_"+file_id).html('<img src="/img/loading.gif" height="40px" />');
	if($("#plagiasm"+file_id).val() != "") {
		$("#plagiasm_form"+file_id).ajaxSubmit({
			url: '/support/ajax/add_second_file',
			type: 'post',
			dataType: 'json',
			data: ({
				plagiasm: $("#plagiasm"+file_id).val(),
				file_id: file_id
			}),
        	beforeSubmit: function(){
        		$("#buttons_"+file_id).html('<img src="/img/loading.gif" height="40px" />');
        	},			
			success: function(data) {
				if(data.status == 'ok') {
					//create_preview(file_id);
					$("#buttons_"+file_id).html('Success!');
					setTimeout('location.reload(false)', 2000);
				}
				else {
					$("#buttons_"+file_id).html('An error was occured. Please try again later');
					setTimeout('location.reload(false)', 2000);
				} 
			}
		});
	}	
}
function approve_file(file_id)
{
	//if(confirm("Are you sure to approve?")) {
		$("#buttons_"+file_id).html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/moderate_file', {file_id: file_id, action: 'approve'}, function(data){
			if(data.status == 'ok') {
				$("#buttons_"+file_id).html('Success!');
				setTimeout('location.reload(false)', 2000);
			}
			else {
				$("#buttons_"+file_id).html('An error was occured. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
		}, 'json');
	//}
}
function reject_file(file_id)
{
	//if(confirm("Are you sure to reject?")) {
		$("#buttons_"+file_id).html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/moderate_file', {file_id: file_id, action: 'reject'}, function(data){
			if(data.status == 'ok') {
				$("#buttons_"+file_id).html('Success!');
				setTimeout('location.reload(false)', 2000);
			}
			else {
				$("#buttons_"+file_id).html('An error was occured. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
		}, 'json');
	//}
}
function add_file()
{
	$("#file_result").html('');
	$("#file").val('');
	$("#new_file").removeClass('hidden');
}
function cancel_file()
{
	$("#file_result").html('');
	$("#file").val('');
	$("#new_file").addClass('hidden');
}
function submit_file()
{
	$("#fileform").ajaxSubmit({
        dataType:  'json',
        success: function(data) {
        	if(data.status == 'ok') {
        		$("#file_result").html('Success!');
        		setTimeout('cancel_file()', 2000);
        	}
        	else {
        		$("#file_result").html('An error occurred. Please try again later');
        		setTimeout('cancel_file()', 2000);
        	}
        }
	});
}

//-------------MESSAGES------------------
function approve_message(message_id)
{
	if(confirm("Are you sure to approve?")) {
		$.post('/support/ajax/moderate_message', {message: message_id, text:$("#text_"+message_id).val(), action: 'approve'}, function(data){
			if(data.status == 'ok') {
				alert('Success');
				$("#message_"+message_id).html($("#text_"+message_id).val());
				$("#buttons_"+message_id).remove();
			}
			else {
				alert('An error was occured. Try again later');
			}
		}, 'json');
	}
}
function reject_message(message_id)
{
	if(confirm("Are you sure to reject?")) {
		$.post('/support/ajax/moderate_message', {message: message_id, text:$("#text_"+message_id).val(), action: 'reject'}, function(data){
			if(data.status == 'ok') {
				alert('Success');
				$("#message_"+message_id).html($("#text_"+message_id).val());
				$("#buttons_"+message_id).remove();
			}
			else {
				alert('An error was occured. Try again later');
			}
		}, 'json');
	}
}
function add_message()
{
	$("#message_result").html('');
	$("#message_text").val('');
	$("#new_message").removeClass('hidden');
}
function add_message_customer()
{
	//$("#message_customer_result").html('');
	//$("#message_customer_text").val('');
	$("#new_message_customer").removeClass('hidden');
}
function add_message_writer()
{
	//$("#message_writer_result").html('');
	$("#message_writer_text").val('');
	$("#new_message_writer").removeClass('hidden');
}
function cancel_message()
{
	$("#message_result").html('');
	$("#message_text").val('');
	$("#new_message").addClass('hidden');
}
function cancel_message_customer()
{
	//$("#message_customer_result").html('');
	//$("#message_customer_text").val('');
	$("#new_message_customer").addClass('hidden');
}
function cancel_message_writer()
{
	//$("#message_writer_result").html('');
	$("#message_writer_text").val('');
	$("#new_message_writer").addClass('hidden');
}
function submit_message(order_id, sender)
{
	var recipient = $("#recipient").val();
	var text = $("#message_text").val();
	if(text != '') {
		$.post('/support/ajax/send_message',{order: order_id, recipient: recipient, text: text, sender: sender},function(data){
			if(data.status == 'ok') {
				$("#message_result").html('Success!');
				setTimeout('cancel_message()', 2000);
			}
			else if(data.status == 'writer_fail') {
				$("#message_result").html('For your order WRITER is not picked up. Please try again later');
				setTimeout('cancel_message()', 2000);
			}
			else {
				$("#message_result").html('An error occurred. Please try again later');
				setTimeout('cancel_message()', 2000);
			}
		}, 'json');
	}
}
function submit_message_customer(order_id, sender)
{
	var recipient = $("#recipient_customer").val();
	var text = $("#message_customer_text").val();
	if($("#customerfile").val() != '') {
		$("#new_message_customer").addClass('error');
	    $("#customerform").ajaxSubmit({
	        dataType:  'json',
	        url: "http://"+window.location.hostname+"/support/ajax/submit_file/",
	        data: ({
	          order : order_id,
	          recipient : recipient,
	          text : text,
	          sender : sender
	        }),
	        beforeSubmit: function(){
	          $("#new_message_customer").css('text-align', 'center');
	          $("#new_message_customer").html('<img src="/img/loading.gif" height="40px" />');
	        },
	        success: function(data) {
	          if(data.status == 'ok') {
	            $("#new_message_customer").html('File sent successfully!');
	            setTimeout('location.reload(false);', 2000);
	          }
	          else {
	            $("#new_message_customer").html('An error occurred. Please try later');
	            setTimeout('location.reload(false);', 2000);
	          }
	        }
	    });
	}
	else if(text != '') {
		$("#new_message_customer").addClass('error');
		$("#new_message_customer").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/send_message',{order: order_id, recipient: recipient, text: text, sender: sender},function(data){
			if(data.status == 'ok') {
				$("#new_message_customer").html('Success!');
				setTimeout('location.reload(false)', 2000);
			}
			else if(data.status == 'writer_fail') {
				$("#new_message_customer").html('For your order WRITER is not picked up. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
			else {
				$("#new_message_customer").html('An error occurred. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
		}, 'json');
	}
}
function submit_message_writer(order_id, sender)
{
	var recipient = $("#recipient_writer").val();
	var text = $("#message_writer_text").val();
	if($("#writerfile").val() != '') {
		$("#new_message_writer").addClass('error');
	    $("#writerform").ajaxSubmit({
	        dataType:  'json',
	        url: "http://"+window.location.hostname+"/support/ajax/submit_file/",
	        data: ({
	          order : order_id,
	          recipient : recipient,
	          text : text,
	          sender : sender
	        }),
	        beforeSubmit: function(){
	          $("#new_message_writer").css('text-align', 'center');
	          $("#new_message_writer").html('<img src="/img/loading.gif" height="40px" />');
	        },
	        success: function(data) {
	          if(data.status == 'ok') {
	            $("#new_message_writer").html('File sent successfully!');
	            setTimeout('location.reload(false);', 2000);
	          }
	          else {
	            $("#new_message_writer").html('An error occurred. Please try later');
	            setTimeout('location.reload(false);', 2000);
	          }
	        }
	    });
	}
	else if(text != '') {
		$("#new_message_writer").addClass('error');
		$("#new_message_writer").html('<img src="/img/loading.gif" height="40px" />');
		$.post('/support/ajax/send_message',{order: order_id, recipient: recipient, text: text, sender: sender},function(data){
			if(data.status == 'ok') {
				$("#new_message_writer").html('Success!');
				setTimeout('location.reload(false)', 2000);
			}
			else if(data.status == 'writer_fail') {
				$("#new_message_writer").html('For your order WRITER is not picked up. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
			else {
				$("#new_message_writer").html('An error occurred. Please try again later');
				setTimeout('location.reload(false)', 2000);
			}
		}, 'json');
	}
}
//-------------NEWS----------------------
function delete_news(news_id)
{
	if(confirm("Are you sure to delete this news?")) {
		$.post('/support/ajax/delete_news', {news_id:news_id}, function(data){
			if(data.status == 'ok')
				write_html_and_reload('result');
			else
				write_html_result('result', 'fail');
		}, 'json');
	}
}
//-------------OTHER---------------------
function write_html_result(target, status)
{
	if(status == 'ok')
		$("#"+target).html('Success!');
	else
		$("#"+target).html('An error was occured. Please try again later');
	setTimeout("remove_html_result('"+target+"')", 2000);
}
function write_html_and_reload(target)
{
	$("#"+target).html('Success!');
	setTimeout('location.reload(false)', 2000)
}
function remove_html_result(target)
{
	$("#"+target).html('');
}
function soundClick() 
{
  var audio = new Audio();
  audio.src = '/upload/notify.ogg';
  audio.autoplay = true;
}
function check_updates()
{
	$.post('/support/ajax/check_updates',{},function(data){
		if(data.status == 'ok') {
			if(data.updates)
				soundClick();
		}
	}, 'json');
	setTimeout('check_updates()', 60000);
}
function get_file(file_id)
{
	location.assign("http://"+window.location.hostname+"/support/ajax/get_file/"+file_id);
}
function show_preview(file_id)
{
	location.assign("http://"+window.location.hostname+"/support/ajax/get_preview/"+file_id);
}
$().ready(function(){
	//setTimeout('check_updates()', 10000);
});