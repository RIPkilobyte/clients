function go_step_two()
{
    var step = validate_step1();
    if(step) {
        $("#first_step").css('display', 'none');
        $("#two_step").css('display', 'block');
    }
}
function back_step_one()
{
    $("#first_step").css('display', 'block');
    $("#two_step").css('display', 'none');
}
function go_step_three()
{
    var step = validate_step2();
    if(step) {
        $("#two_step").css('display', 'none');
        $("#three_step").css('display', 'block');
        $("#div_t").addClass('left_space');
    }
}
function back_step_two()
{
    $("#two_step").css('display', 'block');
    $("#three_step").css('display', 'none');
    $("#div_t").removeClass('left_space');
}
function user_checkout()
{
    var step = validate_step2();
    
    if($("input[name=agree]:checked").val()) {
        if(step) {
            nicEditors.findEditor('paper_details').saveContent();
            $("#submit_form").click();
        }
        else
            return false;
    }
    else {
        $("#agree_error").addClass('error');
        $("#agree_error").html('The Agree with Money Back Guarantee, Privacy Policy, Terms of Use field is required.');
    }
}
function validate_step1()
{
    nicEditors.findEditor('paper_details').saveContent();
    var errors = 0;
    if($("#paper_type").val())
        $("#paper_type_error").html('');
    else {
        errors = 1;
        $("#paper_type_error").addClass('error');
        $("#paper_type_error").html('The Type of paper field is required.');
    }
    if($("#subject").val())
        $("#subject_error").html('');
    else {
        errors = 1;
        $("#subject_error").addClass('error');
        $("#subject_error").html('The Subject field is required.');
    }
    if($("#paper_format").val())
        $("#paper_format_error").html('');
    else {
        errors = 1;
        $("#paper_format_error").addClass('error');
        $("#paper_format_error").html('The Paper format field is required.');
    }
    if($("#topic").val())
        $("#topic_error").html('');
    else {
        errors = 1;
        $("#topic_error").addClass('error');
        $("#topic_error").html('The Topic field is required.');
    }
    if(errors) {
        return false;
    }
    else {
        return true;
    }
}
function validate_step2()
{
    var errors = 0;
    if($("#academic_level").val() !=0)
        $("#academic_level_error").html('');
    else {
        errors = 1;
        $("#academic_level_error").addClass('error');
        $("#academic_level_error").html('The Academic level field is required.');
    }
    if($("#deadline").val())
        $("#deadline_error").html('');
    else {
        errors = 1;
        $("#deadline_error").addClass('error');
        $("#deadline_error").html('The Deadline field is required.');
    }
    if(errors)
        return false;
    else
        return true;
}
function validate_step3()
{
    nicEditors.findEditor('paper_details').saveContent();
    var errors = 0;
    if($("#captcha").val())
        $("#captcha_error").html('');
    else {
        errors = 1;
        $("#captcha_error").addClass('error');
        $("#captcha_error").html('The Captcha field is required.');
    }
    if($("input[name=agree]:checked").val()) {
        $("#agree_error").html('');
    }
    else {
        errors = 1;
        $("#agree_error").addClass('error');
        $("#agree_error").html('The Agree with Money Back Guarantee, Privacy Policy, Terms of Use field is required.');
    }
    if(errors)
        return false;
    else {
        $("#checkout_btn").html('<img src="/img/loading.gif" height="40px" />')
        $(".order-price").html('Your order in progress. It will take few seconds.');
        $("#order_form .order-pricee").attr('css', 'font-size:12px');        
        $("#submit_form").click();
    }
}
function change_work_type()
{
    var work_type = $("#work_type").val();
    if(work_type == 'edit') {
        $("#work_type_hint_new").css('display', 'none');
        $("#work_type_hint_edit").css('display', 'block');
    }
    else {
        $("#work_type_hint_edit").css('display', 'none');
        $("#work_type_hint_new").css('display', 'block');
    }
}
function change_country()
{
    var country = $("#country").val();
    if(typeof(phone_prefix[country]) != 'undefined')
        $("#phone_prefix").val(phone_prefix[country]);
    else
        $("#phone_prefix").val('');
}
function change_paper_types()
{
    var paper_type = $("#paper_type").val();
    if(paper_type == '25') {
        $("#institution_type_block").addClass('hidden');
        $("#institution_name_block").addClass('hidden');
        $("#job_title_or_industry_block").addClass('hidden');
        $("#reasons_for_applying_block").addClass('hidden');
        $("#general_format_block").addClass('hidden');
        $("#paper_format_block").addClass('hidden');
        
        $("#academic_level_block").removeClass('hidden');
        $("#subject_block").removeClass('hidden');
        $("#hint_for_mult_questions").removeClass('hidden');
    }
    else if(paper_type == '28') {
        $("#hint_for_mult_questions").addClass('hidden');
        $("#subject_block").addClass('hidden');
        $("#institution_type_block").addClass('hidden');
        $("#institution_name_block").addClass('hidden');
        $("#job_title_or_industry_block").addClass('hidden');
        $("#reasons_for_applying_block").addClass('hidden');
        $("#general_format_block").addClass('hidden');

        $("#paper_format_block").removeClass('hidden');
        $("#academic_level_block").removeClass('hidden');
    }
    else if(paper_type == '1' || paper_type == '26') {
        //$("#academic_level_block").addClass('hidden');
        $("#hint_for_mult_questions").addClass('hidden');
        $("#job_title_or_industry_block").addClass('hidden');
        $("#reasons_for_applying_block").addClass('hidden');
        $("#general_format_block").addClass('hidden');

        $("#paper_format_block").removeClass('hidden');
        $("#subject_block").removeClass('hidden');
        $("#institution_type_block").removeClass('hidden');
        $("#institution_name_block").removeClass('hidden');
        
    }
    else if(paper_type == '3' || paper_type == '11') {
        //$("#academic_level_block").addClass('hidden');
        $("#hint_for_mult_questions").addClass('hidden');
        $("#general_format_block").addClass('hidden');

        $("#paper_format_block").removeClass('hidden');
        $("#subject_block").removeClass('hidden');
        $("#job_title_or_industry_block").removeClass('hidden');
        $("#reasons_for_applying_block").removeClass('hidden');
    }    
    else {
        $("#hint_for_mult_questions").addClass('hidden');
        $("#institution_type_block").addClass('hidden');
        $("#institution_name_block").addClass('hidden');
        $("#job_title_or_industry_block").addClass('hidden');
        $("#reasons_for_applying_block").addClass('hidden');

        $("#paper_format_block").removeClass('hidden');
        $("#academic_level_block").removeClass('hidden');
        $("#subject_block").removeClass('hidden');
        $("#general_format_block").removeClass('hidden');
    }
}
function calc_selector()
{
    var academic_level = $("#academic_level").val();
    var deadline = $("#deadline").val();
    var work_type = $("#work_type").val();
    var number_of_pages = $("#number_of_pages").val();
    number_of_pages = parseInt(number_of_pages);
    if(isNaN(number_of_pages))
        number_of_pages = 1;
    
    var price = 0;
    if(typeof(academic_level) != 'undefined' && typeof(deadline) != 'undefined' && typeof(number_of_pages) != 'undefined')
        if(academic_level != '0' && deadline != '0' && academic_level != '' && deadline != '') {
            price = prices[work_type][academic_level][deadline];
            price = parseFloat(price * number_of_pages).toFixed(2);
        }

    $("#calc_result").html(price);    
}
function change_additional_materials()
{
    var additional_materials = $("#additional_materials").val();
    if(additional_materials == 'later')
        $("#additional_materials_hint_for_2").removeClass('hidden');
    else
        $("#additional_materials_hint_for_2").addClass('hidden');
}
function change_preferred_writer()
{
    var preferred_writer = $("#preferred_writer").val();
    if(preferred_writer == '3')
        $("#previous_writer_block").removeClass('hidden');
    else
        $("#previous_writer_block").addClass('hidden');    
}
function change_user()
{
    var user_choise = $("input[name='user']:checked").val();
    if(user_choise == '2') {
        $("#user_reg").attr('checked', '');
        $("#user_login").attr('checked', 'checked');
        $("#client_create").addClass('hidden');
        $("#client_login").removeClass('hidden');
    }
    else {
        $("#user_reg").attr('checked', 'checked');
        $("#user_login").attr('checked', '');
        $("#client_login").addClass('hidden');
        $("#client_create").removeClass('hidden');
    }
}
function change_spacing()
{
    var spacing = $("input[name=spacing]:checked").val();
    if(spacing == 'Single') {
        $("#spacing_hint_2").css('display', 'none');
        $("#spacing_hint_1").css('display', 'block');
    }
    else {
        $("#spacing_hint_1").css('display', 'none');
        $("#spacing_hint_2").css('display', 'block');
    }
}
function check_paper_words()
{
    var text = $("#paper_details").val();
    var q = text.split(" ");
    var words = q.length;
    if(words < '3')
        $("#paper_details_error").css('display', 'block');
    else
        $("#paper_details_error").css('display', 'none');
}
$().ready(function(){
    $("#paper_details").keyup(function(){
        check_paper_words();
    });
    $("#number_of_pages").numberMask({beforePoint:4});
    $("#number_of_slides").numberMask({beforePoint:4});
    $("#sources_needed").numberMask({beforePoint:3});
    $("#previous_writer").numberMask({beforePoint:11});
    
    change_spacing();
    
    change_paper_types();
    calc_selector();
    change_additional_materials();
    change_preferred_writer();
    change_user();

    $(".user_select").click(function(){
        change_user();
    });
    $(".calc_selector").change(function(){
        calc_selector();
    });
    $('#number_of_pages').mouseout(function(){
        calc_selector();
    }); 
    $('#number_of_pages').change(function(){
        calc_selector();
    });    
    $('#number_of_slides').mouseout(function(){
        calc_selector();
    }); 
    $('#number_of_slides').change(function(){
        calc_selector();
    });

    $("#cuselFrame-work_type").hover(
        function(){
            if($("#work_type").val() == 'edit')
                $("#work_type_hint_edit").css('display', 'block');
            else
                $("#work_type_hint_new").css('display', 'block'); 
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-paper_type").hover(
        function(){
            $("#paper_type_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#paper_type").change(function(){
        if($("#paper_type").val() == '25')
            $("#hint_for_mult_questions").css('display', 'block');
        else
            $("#hint_for_mult_questions").css('display', 'none');
    });
    
    $("#cuselFrame-paper_type").hover(
        function(){
            $("#paper_type_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-academic_level").hover(
        function(){
            $("#academic_level_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-subject").hover(
        function(){
            $("#subject_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-institution").hover(
        function(){
            $("#institution_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-institution_type").hover(
        function(){
            $("#institution_type_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-job_title_or_industry").hover(
        function(){
            $("#job_title_or_industry_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-reasons_for_applying").hover(
        function(){
            $("#reasons_for_applying_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-paper_format").hover(
        function(){
            $("#paper_format_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#topic").hover(
        function(){
            $("#topic_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#paper_details").hover(
        function(){
            $("#paper_details_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );   
    $("#sources_needed").hover(
        function(){
            $("#sources_needed_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#number_of_pages").hover(
        function(){
            $("#number_of_pages_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#number_of_slides").hover(
        function(){
            $("#number_of_slides_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-deadline").hover(
        function(){
            $("#deadline_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-additional_materials").hover(
        function(){
            $("#additional_materials_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#plagiarism_report_input_hints").hover(
        function(){
            $("#plagiarism_report_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
            $(".label_hints").css('display', 'none');
        }
    );
    $("#abstract_input_hints").hover(
        function(){
            $("#abstract_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
            $(".label_hints").css('display', 'none');
        }
    );
    $("#top_priority_input_hints").hover(
        function(){
            $("#top_priority_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
            $(".label_hints").css('display', 'none');
        }
    );
    $("#discount_code").hover(
        function(){
            $("#discount_code_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-preferred_writer").hover(
        function(){
            $("#preferred_writer_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#previous_writer").hover(
        function(){
            $("#previous_writer_hint_2").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#email_reg").hover(
        function(){
            $("#email_reg_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#password_reg").hover(
        function(){
            $("#password_reg_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#phone_number").hover(
        function(){
            $("#phone_number_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#email_login").hover(
        function(){
            $("#email_login_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#password_login").hover(
        function(){
            $("#password_login_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );
    $("#cuselFrame-institution_type").hover(
        function(){
            $("#institution_type_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );    
    $("#institution").hover(
        function(){
            $("#institution_hint").css('display', 'block');
        },
        function() {
            $(".order_hints").css('display', 'none');
        }
    );    
});