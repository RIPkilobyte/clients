var prices = {
        multiple_choice_questions : {
            d11_14 : {
                high_school : 1,
                college : 1.4,
                bachelor : 1.6,
                masters : 2.4,
                phd : 2.7
            },
            d7_10 : {
                high_school : 1.4,
                college : 1.6,
                bachelor : 1.8,
                masters : 2.6,
                phd : 3
            },
            d4_6 : {
                high_school : 1.6,
                college : 1.8,
                bachelor : 2,
                masters : 2.8,
                phd : 3.4
            },
            d3 : {
                high_school : 1.8,
                college : 2,
                bachelor : 2.3,
                masters : 3.1,
                phd : 3.6
            },
            d2 : {
                high_school : 2,
                college : 2.3,
                bachelor : 2.6,
                masters : 3.4,
                phd : 4.4
            },
            h24 : {
                high_school : 2.3,
                college : 2.6,
                bachelor : 3.1,
                masters : 3.8,
                phd : 4.8
            },
            h8 : {
                high_school : 2.6,
                college : 3.1,
                bachelor : 3.7,
                masters : 4.6,
                phd : 8.5
            }
        },
        problem_solving : {
            d11_14 : {
                high_school : 14.99,
                college : 19.99,
                bachelor : 24.99,
                masters : 34.99,
                phd : 39.99
            },
            d7_10 : {
                high_school : 19.99,
                college : 23.99,
                bachelor : 26.99,
                masters : 39.99,
                phd : 43.99
            },
            d4_6 : {
                high_school : 23.99,
                college : 26.99,
                bachelor : 30.99,
                masters : 41.99,
                phd : 50.99
            },
            d3 : {
                high_school : 26.99,
                college : 30.99,
                bachelor : 33.99,
                masters : 45.99,
                phd : 53.99
            },
            d2 : {
                high_school : 30.99,
                college : 33.99,
                bachelor : 39.99,
                masters : 50.99,
                phd : 65.99
            },
            h24 : {
                high_school : 33.99,
                college : 39.99,
                bachelor : 45.99,
                masters : 56.99,
                phd : 70.99
            },
            h8 : {
                high_school : 39.99,
                college : 45.99,
                bachelor : 55.99,
                masters : 68.99,
                phd : 127.99
            }
        },
        admission_essay : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }   
        },
        annotated_bibliography : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }   
        },
        application_letter : {
            d11_14 : {
                high_school : 40.99,
                college : 40.99,
                bachelor : 40.99,
                masters : 40.99,
                phd : 40.99
            },
            d7_10 : {
                high_school : 44.99,
                college : 44.99,
                bachelor : 44.99,
                masters : 44.99,
                phd : 44.99
            },
            d4_6 : {
                high_school : 50.99,
                college : 50.99,
                bachelor : 50.99,
                masters : 50.99,
                phd : 50.99
            },
            d3 : {
                high_school : 53.99,
                college : 53.99,
                bachelor : 53.99,
                masters : 53.99,
                phd : 53.99
            },
            d2 : {
                high_school : 65.99,
                college : 65.99,
                bachelor : 65.99,
                masters : 65.99,
                phd : 65.99
            },
            h24 : {
                high_school : 70.99,
                college : 70.99,
                bachelor : 70.99,
                masters : 70.99,
                phd : 70.99
            },
            h8 : {
                high_school : 126.99,
                college : 126.99,
                bachelor : 126.99,
                masters : 126.99,
                phd : 126.99
            }   
        },
        argumentative_essay : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }   
        },
        article : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }   
        },
        article_review : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        biography : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }       
        },
        business_plan : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        case_study : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        course_work : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        cover_letter : {
            d11_14 : {
                high_school : 40.99,
                college : 40.99,
                bachelor : 40.99,
                masters : 40.99,
                phd : 40.99
            },
            d7_10 : {
                high_school : 44.99,
                college : 44.99,
                bachelor : 44.99,
                masters : 44.99,
                phd : 44.99
            },
            d4_6 : {
                high_school : 50.99,
                college : 50.99,
                bachelor : 50.99,
                masters : 50.99,
                phd : 50.99
            },
            d3 : {
                high_school : 53.99,
                college : 53.99,
                bachelor : 53.99,
                masters : 53.99,
                phd : 53.99
            },
            d2 : {
                high_school : 65.99,
                college : 65.99,
                bachelor : 65.99,
                masters : 65.99,
                phd : 65.99
            },
            h24 : {
                high_school : 70.99,
                college : 70.99,
                bachelor : 70.99,
                masters : 70.99,
                phd : 70.99
            },
            h8 : {
                high_school : 126.99,
                college : 126.99,
                bachelor : 126.99,
                masters : 126.99,
                phd : 126.99
            }           
        },
        creative_writing : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        critical_thinking : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_abstract : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_chapter : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_conclusion : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_introduction : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_methodology : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_proposal : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        dissertation_result : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        essay : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        literature_review : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        movie_review : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        personal_statement : {
            d11_14 : {
                high_school : 40.99,
                college : 40.99,
                bachelor : 40.99,
                masters : 40.99,
                phd : 40.99
            },
            d7_10 : {
                high_school : 44.99,
                college : 44.99,
                bachelor : 44.99,
                masters : 44.99,
                phd : 44.99
            },
            d4_6 : {
                high_school : 50.99,
                college : 50.99,
                bachelor : 50.99,
                masters : 50.99,
                phd : 50.99
            },
            d3 : {
                high_school : 53.99,
                college : 53.99,
                bachelor : 53.99,
                masters : 53.99,
                phd : 53.99
            },
            d2 : {
                high_school : 65.99,
                college : 65.99,
                bachelor : 65.99,
                masters : 65.99,
                phd : 65.99
            },
            h24 : {
                high_school : 70.99,
                college : 70.99,
                bachelor : 70.99,
                masters : 70.99,
                phd : 70.99
            },
            h8 : {
                high_school : 126.99,
                college : 126.99,
                bachelor : 126.99,
                masters : 126.99,
                phd : 126.99
            }           
        },
        presentation : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        report : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        research_paper : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        research_proposal : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        term_paper : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        thesis : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        thesis_proposal : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        thesis_statement : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        },
        other : {
            d11_14 : {
                high_school : 9.99,
                college : 13.99,
                bachelor : 15.99,
                masters : 23.99,
                phd : 26.99
            },
            d7_10 : {
                high_school : 13.99,
                college : 15.99,
                bachelor : 17.99,
                masters : 25.99,
                phd : 29.99
            },
            d4_6 : {
                high_school : 15.99,
                college : 17.99,
                bachelor : 19.99,
                masters : 27.99,
                phd : 33.99
            },
            d3 : {
                high_school : 17.99,
                college : 19.99,
                bachelor : 22.99,
                masters : 30.99,
                phd : 35.99
            },
            d2 : {
                high_school : 19.99,
                college : 22.99,
                bachelor : 25.99,
                masters : 33.99,
                phd : 43.99
            },
            h24 : {
                high_school : 22.99,
                college : 25.99,
                bachelor : 30.99,
                masters : 37.99,
                phd : 47.99
            },
            h8 : {
                high_school : 25.99,
                college : 30.99,
                bachelor : 36.99,
                masters : 45.99,
                phd : 84.99
            }           
        }
};
var paper_type_comparison = {
    1: "admission_essay",
    2: "annotated_bibliography",
    3: "application_letter",
    4: "argumentative_essay",
    5: "article",
    6: "article_review",
    7: "biography",
    8: "business_plan",
    9: "case_study",
    10: "course_work",
    11: "cover_letter",
    12: "creative_writing",
    13: "critical_thinking",
    14: "dissertation",
    15: "dissertation_abstract",
    16: "dissertation_chapter",
    17: "dissertation_conclusion",
    18: "dissertation_introduction",
    19: "dissertation_methodology",
    20: "dissertation_proposal",
    21: "dissertation_result",
    22: "essay",
    23: "literature_review",
    24: "movie_review",
    25: "multiple_choice_questions",
    26: "personal_statement",
    27: "presentation",
    28: "problem_solving",
    29: "report",
    30: "research_paper",
    31: "research_proposal",
    32: "term_paper",
    33: "thesis",
    34: "thesis_proposal",
    35: "thesis_statement",
    36: "other"
};
var deadline_comparison = {
    1: "h8",
    2: "h24",
    3: "d2",
    4: "d3",
    5: "d4_6",
    6: "d7_10",
    7: "d11_14"
};
var academic_level_comparison = {
    1: "high_school",
    2: "college",
    3: "bachelor",
    4: "masters",
    5: "phd"
};
var paper_format_comparison = {
    1: "MLA",
    2: "APA",
    3: "Chicago/Turabian",
    4: "Harvard",
    5: "Other"
};