<?php
//CardSave Hosted Payment Page Solution - Post Redirect
//Copyright (C) 2010 Modacs Limited trading as CardSave.
//Support: support@cardsave.net

//Created:		13/04/2010
//Created By:	David McCann, CardSave Online
//Modified:		31/01/2012
//Modified By: 	Alistair Richardson, CardSave Online
//Version:		3.0
    
//Terms of Use:
//This file and its enclosed scripts may be modified without limitation for the benefit of CardSave customers, CardSave Approved Partners and CardSave Approved Developers only.
//This file and its enclosed scripts must not be modified in any way to allow it to work with any other gateway/payment system other than that which is provided by CardSave.

//Disclaimer: This code is provided on an "as is" basis. It is the responsibility of the merchant/merchants web developer to test its implementation and function.

//-------------------------------------------------------------------------------------------------------------------------

//Include the config.php file to pull in Pre-Shared Key, Gateway ID, Password etc.
include("config.php");

//magic quotes fix
if (get_magic_quotes_gpc()) {
	$_POST = array_map('stripslashes', $_POST);
}

//Generate Hashstring - use combination of post variables and variables from config.php
$HashString="PreSharedKey=" . $PreSharedKey;
$HashString=$HashString . '&MerchantID=' . $_POST["MerchantID"];
$HashString=$HashString . '&Password=' . $Password;
$HashString=$HashString . '&StatusCode=' . $_POST["StatusCode"];
$HashString=$HashString . '&Message=' . $_POST["Message"];
$HashString=$HashString . '&PreviousStatusCode=' . $_POST["PreviousStatusCode"];
$HashString=$HashString . '&PreviousMessage=' . $_POST["PreviousMessage"];
$HashString=$HashString . '&CrossReference=' . $_POST["CrossReference"];
$HashString=$HashString . '&AddressNumericCheckResult=' . $_POST["AddressNumericCheckResult"];
$HashString=$HashString . '&PostCodeCheckResult=' . $_POST["PostCodeCheckResult"];
$HashString=$HashString . '&CV2CheckResult=' . $_POST["CV2CheckResult"];
$HashString=$HashString . '&ThreeDSecureAuthenticationCheckResult=' . $_POST["ThreeDSecureAuthenticationCheckResult"];
$HashString=$HashString . '&CardType=' . $_POST["CardType"];
$HashString=$HashString . '&CardClass=' . $_POST["CardClass"];
$HashString=$HashString . '&CardIssuer=' . $_POST["CardIssuer"];
$HashString=$HashString . '&CardIssuerCountryCode=' . $_POST["CardIssuerCountryCode"];
$HashString=$HashString . '&Amount=' . $_POST["Amount"];
$HashString=$HashString . '&CurrencyCode=' . $_POST["CurrencyCode"];
$HashString=$HashString . '&OrderID=' . $_POST["OrderID"];
$HashString=$HashString . '&TransactionType=' . $_POST["TransactionType"];
$HashString=$HashString . '&TransactionDateTime=' . $_POST["TransactionDateTime"];
$HashString=$HashString . '&OrderDescription=' . $_POST["OrderDescription"];
$HashString=$HashString . '&CustomerName=' . $_POST["CustomerName"];
$HashString=$HashString . '&Address1=' . $_POST["Address1"];
$HashString=$HashString . '&Address2=' . $_POST["Address2"];
$HashString=$HashString . '&Address3=' . $_POST["Address3"];
$HashString=$HashString . '&Address4=' . $_POST["Address4"];
$HashString=$HashString . '&City=' . $_POST["City"];
$HashString=$HashString . '&State=' . $_POST["State"];
$HashString=$HashString . '&PostCode=' . $_POST["PostCode"];
$HashString=$HashString . '&CountryCode=' . $_POST["CountryCode"];
$HashString=$HashString . '&EmailAddress=' . $_POST["EmailAddress"];
$HashString=$HashString . '&PhoneNumber=' . $_POST["PhoneNumber"];

//Encode HashDigest using SHA1 encryption (and create HashDigest for later use) - This is used as a checksum to ensure that the form post from the gateway back to this page hasn't been tampered with.
$HashDigest = sha1($HashString);  
 
//Function to compare Hash returned from the gateway and that generated in the script above.
function checkhash($HashDigest) {
   $ReturnedHash = $_POST["HashDigest"];
   if ($HashDigest == $ReturnedHash) { 
       echo "PASSED"; 
   } else { 
       echo "FAILED"; 
   } 
}

//Simple HTML page to display the variables returned from the gateway + the result of the hash checks
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>PHP Post Redirect Callback</title>
</head>
<body>
<?php var_dump($_POST); ?>
	<p>
		RETURNEDHASH - <?php echo $_POST["HashDigest"] ?> <br /> <br />
		HASHDIGEST - <?php echo $HashDigest; ?> <br /><br />
		HASHCHECK - <?php checkhash($HashDigest); ?> <br /><br />
		
		StatusCode - <?php echo $_POST["StatusCode"]; ?> <br />
		Response Message - <?php echo $_POST["Message"]; ?> <br />
		Previous Status Code - <?php echo $_POST["PreviousStatusCode"]; ?> <br />
		Previous Message - <?php echo $_POST["PreviousMessage"]; ?> <br />        
		CrossReference - <?php echo $_POST["CrossReference"]; ?> <br /><br />
		
		AddressNumericCheckResult - <?php echo $_POST["AddressNumericCheckResult"]; ?> <br />
		PostCodeCheckResult - <?php echo $_POST["PostCodeCheckResult"]; ?> <br />
		CV2CheckResult - <?php echo $_POST["CV2CheckResult"]; ?> <br />
		ThreeDSecureAuthenticationCheckResult - <?php echo $_POST["ThreeDSecureAuthenticationCheckResult"]; ?> <br />
		CardType - <?php echo $_POST["CardType"]; ?> <br />
		CardClass - <?php echo $_POST["CardClass"]; ?> <br />
		CardIssuer - <?php echo $_POST["CardIssuer"]; ?> <br />
		CardIssuerCountryCode - <?php echo $_POST["CardIssuerCountryCode"]; ?> <br /><br />		
		
		MerchantID - <?php echo $_POST["MerchantID"]; ?> <br />
		Amount - <?php echo $_POST["Amount"]; ?> <br />                                                                        
		CurrencyCode - <?php echo $_POST["CurrencyCode"]; ?> <br />
		OrderID - <?php echo $_POST["OrderID"]; ?> <br />
		TransactionType - <?php echo $_POST["TransactionType"]; ?> <br />
		TransactionDateTime - <?php echo $_POST["TransactionDateTime"]; ?> <br />
		CallbackURL - <?php echo $_POST["CallbackURL"]; ?> <br />
		OrderDescription - <?php echo $_POST["OrderDescription"]; ?> <br />
		CustomerName - <?php echo $_POST["CustomerName"]; ?> <br />
		Address1 - <?php echo $_POST["Address1"]; ?> <br />
		Address2 - <?php echo $_POST["Address2"]; ?> <br />
		Address3 - <?php echo $_POST["Address3"]; ?> <br />
		Address4 - <?php echo $_POST["Address4"]; ?> <br />
		City - <?php echo $_POST["City"]; ?> <br /> 
		State - <?php echo $_POST["State"]; ?> <br />
		PostCode - <?php echo $_POST["PostCode"]; ?> <br /> 
		CountryCode - <?php echo $_POST["CountryCode"]; ?> <br /><br />
		
		EmailAddress - <?php echo $_POST["EmailAddress"]; ?> <br />
		PhoneNumber - <?php echo $_POST["PhoneNumber"]; ?> <br />
	</p>
</body>
</html>
