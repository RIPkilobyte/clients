<?php
//CardSave Hosted Payment Page Solution - Post Redirect
//Copyright (C) 2010 Modacs Limited trading as CardSave.
//Support: support@cardsave.net

//Created:		13/04/2010
//Created By:	David McCann, CardSave Online
//Modified:		31/01/2012
//Modified By: 	Alistair Richardson, CardSave Online
//Version:		3.0
    
//Terms of Use:
//This file and its enclosed scripts may be modified without limitation for the benefit of CardSave customers, CardSave Approved Partners and CardSave Approved Developers only.
//This file and its enclosed scripts must not be modified in any way to allow it to work with any other gateway/payment system other than that which is provided by CardSave.

//Disclaimer: This code is provided on an "as is" basis. It is the responsibility of the merchant/merchants web developer to test its implementation and function.

//-------------------------------------------------------------------------------------------------------------------------

//Include the config.php file to pull in Pre-Shared Key, Gateway ID, Password etc.
include("config.php");

//Function to remove invalid characters / characters which will cause the gateway to error.
function stripGWInvalidChars($strToCheck) {
	$toReplace = array("#","\\",">","<", "\"", "[", "]");
	$cleanString = str_replace($toReplace, "", $strToCheck);
	return $cleanString;
}

//Strip Invalid characters on the following fields for use in HashString and Form Post
$CustomerName = stripGWInvalidChars($_POST["CustomerName"]);
$Address1 =  stripGWInvalidChars($_POST["Address1"]);
$Address2 =  stripGWInvalidChars($_POST["Address2"]);
$Address3 =  stripGWInvalidChars($_POST["Address3"]);
$Address4 =  stripGWInvalidChars($_POST["Address4"]);
$City =  stripGWInvalidChars($_POST["City"]);
$State =  stripGWInvalidChars($_POST["State"]);
$PostCode =  stripGWInvalidChars($_POST["PostCode"]);
$EmailAddress =  stripGWInvalidChars($_POST["EmailAddress"]);
$PhoneNumber =  stripGWInvalidChars($_POST["PhoneNumber"]);

//Generate Hashstring - use combination of post variables and variables stripped of invalid characters
$HashString="PreSharedKey=" . $PreSharedKey;
$HashString=$HashString . '&MerchantID=' . $_POST["MerchantID"];
$HashString=$HashString . '&Password=' . $Password;
$HashString=$HashString . '&Amount=' . $_POST["Amount"];
$HashString=$HashString . '&CurrencyCode=' . $_POST["CurrencyCode"];
$HashString=$HashString . '&EchoAVSCheckResult=' . $_POST["EchoAVSCheckResult"];
$HashString=$HashString . '&EchoCV2CheckResult=' . $_POST["EchoCV2CheckResult"];
$HashString=$HashString . '&EchoThreeDSecureAuthenticationCheckResult=' . $_POST["EchoThreeDSecureAuthenticationCheckResult"];
$HashString=$HashString . '&EchoCardType=' . $_POST["EchoCardType"];
$HashString=$HashString . '&OrderID=' . $_POST["OrderID"];
$HashString=$HashString . '&TransactionType=' . $_POST["TransactionType"];
$HashString=$HashString . '&TransactionDateTime=' . $_POST["TransactionDateTime"];
$HashString=$HashString . '&CallbackURL=' . $_POST["CallbackURL"];
$HashString=$HashString . '&OrderDescription=' . $_POST["OrderDescription"];
$HashString=$HashString . '&CustomerName=' . $CustomerName;
$HashString=$HashString . '&Address1=' . $Address1;
$HashString=$HashString . '&Address2=' . $Address2;
$HashString=$HashString . '&Address3=' . $Address3;
$HashString=$HashString . '&Address4=' . $Address4;
$HashString=$HashString . '&City=' . $City;
$HashString=$HashString . '&State=' . $State;
$HashString=$HashString . '&PostCode=' . $PostCode;
$HashString=$HashString . '&CountryCode=' . $_POST["CountryCode"];
$HashString=$HashString . '&EmailAddress=' . $EmailAddress;
$HashString=$HashString . '&PhoneNumber=' . $PhoneNumber;
$HashString=$HashString . '&EmailAddressEditable=' . $_POST["EmailAddressEditable"];
$HashString=$HashString . '&PhoneNumberEditable=' . $_POST["PhoneNumberEditable"];
$HashString=$HashString . "&CV2Mandatory=" . $_POST["CV2Mandatory"];
$HashString=$HashString . "&Address1Mandatory=" . $_POST["Address1Mandatory"];
$HashString=$HashString . "&CityMandatory=" . $_POST["CityMandatory"];
$HashString=$HashString . "&PostCodeMandatory=" . $_POST["PostCodeMandatory"];
$HashString=$HashString . "&StateMandatory=" . $_POST["StateMandatory"];
$HashString=$HashString . "&CountryMandatory=" . $_POST["CountryMandatory"];
$HashString=$HashString . "&ResultDeliveryMethod=" . 'POST';
$HashString=$HashString . "&ServerResultURL=" . '';
$HashString=$HashString . "&PaymentFormDisplaysResult=" . 'false';

//Encode HashDigest using SHA1 encryption (and create HashDigest for later use) - This is used as a checksum by the gateway to ensure form post hasn't been tampered with.
$HashDigest = sha1($HashString); 

//Simple HTML page to display the variables created by the script above and create the HTML form to post to the hosted payment page.
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>PHP Post Redirect Process</title>
</head>
<body>
	<p>
		HashDigest - <?php echo $HashDigest; ?><br />
		MerchantID - <?php echo $_POST["MerchantID"]; ?><br />
		HashString - <?php echo $HashString; ?><br />
	</p>

	<form name="contactForm" id="contactForm" method="post" action="https://mms.cardsaveonlinepayments.com/Pages/PublicPages/PaymentForm.aspx" target="_self">
		<input type="hidden" name="HashDigest" value="<?php echo $HashDigest; ?>" />
		<input type="hidden" name="MerchantID" value="<?php echo $_POST["MerchantID"]; ?>" />
		<input type="hidden" name="Amount" value="<?php echo $_POST["Amount"]; ?>" />                                       
		<input type="hidden" name="CurrencyCode" value="<?php echo $_POST["CurrencyCode"]; ?>" />
		<input type="hidden" name="EchoAVSCheckResult" value="<?php echo $_POST["EchoAVSCheckResult"]; ?>" />
		<input type="hidden" name="EchoCV2CheckResult" value="<?php echo $_POST["EchoCV2CheckResult"]; ?>" />
		<input type="hidden" name="EchoThreeDSecureAuthenticationCheckResult" value="<?php echo $_POST["EchoThreeDSecureAuthenticationCheckResult"]; ?>" />
		<input type="hidden" name="EchoCardType" value="<?php echo $_POST["EchoCardType"]; ?>" />
		<input type="hidden" name="OrderID" value="<?php echo $_POST["OrderID"]; ?>" />
		<input type="hidden" name="TransactionType" value="<?php echo $_POST["TransactionType"]; ?>" />
		<input type="hidden" name="TransactionDateTime" value="<?php echo $_POST["TransactionDateTime"]; ?>" />
		<input type="hidden" name="CallbackURL" value="<?php echo $_POST["CallbackURL"]; ?>" />
		<input type="hidden" name="OrderDescription" value="<?php echo $_POST["OrderDescription"]; ?>" />
		<input type="hidden" name="CustomerName" value="<?php echo $CustomerName; ?>" />
		<input type="hidden" name="Address1" value="<?php echo $Address1; ?>" />
		<input type="hidden" name="Address2" value="<?php echo $Address2; ?>" />
		<input type="hidden" name="Address3" value="<?php echo $Address3; ?>" />
		<input type="hidden" name="Address4" value="<?php echo $Address4; ?>" />
		<input type="hidden" name="City" value="<?php echo $City; ?>" /> 
		<input type="hidden" name="State" value="<?php echo $State; ?>" />
		<input type="hidden" name="PostCode" value="<?php echo $PostCode; ?>" />
		<input type="hidden" name="CountryCode" value="<?php echo $_POST["CountryCode"]; ?>" />
		<input type="hidden" name="EmailAddress" value="<?php echo $EmailAddress; ?>" />
		<input type="hidden" name="PhoneNumber" value="<?php echo $PhoneNumber; ?>" />
		<input type="hidden" name="EmailAddressEditable" value="<?php echo $_POST["EmailAddressEditable"]; ?>" />
		<input type="hidden" name="PhoneNumberEditable" value="<?php echo $_POST["PhoneNumberEditable"]; ?>" />
		<input type="hidden" name="CV2Mandatory" value="<?php echo $_POST["CV2Mandatory"]; ?>" />
		<input type="hidden" name="Address1Mandatory" value="<?php echo $_POST["Address1Mandatory"]; ?>" />
		<input type="hidden" name="CityMandatory" value="<?php echo $_POST["CityMandatory"]; ?>" />
		<input type="hidden" name="PostCodeMandatory" value="<?php echo $_POST["PostCodeMandatory"]; ?>" />
		<input type="hidden" name="StateMandatory" value="<?php echo $_POST["StateMandatory"]; ?>" />
		<input type="hidden" name="CountryMandatory" value="<?php echo $_POST["CountryMandatory"]; ?>" />
		<input type="hidden" name="ResultDeliveryMethod" value="POST" />
		<input type="hidden" name="ServerResultURL" value="" />
		<input type="hidden" name="PaymentFormDisplaysResult" value="false" />
		<input type="hidden" name="ThreeDSecureCompatMode" value="false" />
		<br /><input type="submit" value="TEST NOW" />
	</form>
</body>
</html>