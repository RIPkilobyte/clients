CardSave Hosted Payment Page Solution - Post Redirect
Copyright (C) 2012 Modacs Limited trading as CardSave.
Support: support@cardsave.net

Created:	13/04/2010
Created By:	David McCann, CardSave Online
Modified:	01/02/2012
Modified By: 	Alistair Richardson, CardSave Online
Version:	3.0
    
Terms of Use:
This file and its enclosed scripts may be modified without limitation for the benefit of CardSave customers, CardSave Approved Partners and CardSave Approved Developers only.
This file and its enclosed scripts must not be modified in any way to allow it to work with any other gateway/payment system other than that which is provided by CardSave.

Disclaimer: This code is provided on an "as is" basis. It is the responsibility of the merchant/merchants web developer to test its implementation and function.

-------------------------------------------------------------------------------------------------------------------------

NOTE: This system uses a POST redirect. Some browsers will display a notification when returning from the secure gateway pages and the non-secure callback page on your website.
If the user does not click to continue when shown this message, the callback will not complete and your website will not be notified of the transaction result.
To avoid this, we recommend installing a valid SSL certificate on the website and ensuring the callback page is in a secure part of the site, using the https protocol.

1) Please ask your merchant for your his TEST Gateway Account and Merchant Management System login's.

If you do not have these details or you do not have a merchant yet then you can register a test account here...

https://mms.cardsaveonlinepayments.com/Pages/PublicPages/RegisterMerchant.aspx 

You will receive a verification email which you must respond to complete the registration process. You will then receive two separate emails with login details to the back office management system and payment gateway. When you login to the back office system, Merchant Management System for the first time, (you will be asked to change your password on first time entry). 


2) You will need the TEST Gateway email merchantID and password to plug into your code. Please make a note. 


3) You will also need the pre-shared key. Login to the Merchant Management System and select merchant information from the left menu. At the bottom of that page you will see the security information and pre-shared key.


4) Copy the code into your web space and make the following changes...


In Config.php enter your website address, Pre-Shared Key, MerchantID and Password.


The Test Pages are now good to go!
Just browse to the PaymentFormHosted.php page in your browser and test.

You can download some test cards to try here, they have varying responses...
https://mms.cardsaveonlinepayments.com/SiteFiles/VirtualFiles/TEST_CARD_DETAILS/TestCardDetails.zip

NOTE: You need to use the card billing address as specified in the PDF file alongside each card - If you use the incorrect billing address, as default, the payment will fail (AVS/Postcode Check will fail).