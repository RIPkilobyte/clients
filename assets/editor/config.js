/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'en';
	// config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = '/assets/editor/kc/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '/assets/editor/kc/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '/assets/editor/kc/browse.php?type=flash';
    config.filebrowserUploadUrl = '/assets/editor/kc/upload.php?type=files';
    config.filebrowserImageUploadUrl = '/assets/editor/kc/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '/assets/editor/kc/upload.php?type=flash';
	config.width = 950;
	config.height = 300;
};

